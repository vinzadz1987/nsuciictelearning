<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Online_quiz_examinees extends CI_Controller {

var $data = array();
    
    public function __construct()
    {
        parent::__construct();		
        checklogin();
        $this->data = array(
            'page_title' =>  strtoupper(''),
        );
    }
    
	public function index()
	{
		$this->benchmark->mark('code_start'); // For benchmarking
		$quiz_id = $this->input->get('quiz');
		$this->data['title'] = $this->main_model->get_table('toq_name','tbl_online_quiz','WHERE toq_id = "'.$quiz_id.'"','single');
		$this->data['quiz_id'] = $quiz_id;
        $this->data['content'] = $this->load->view('settings/online_quiz_examinees_view', $this->data ,TRUE);				
        $this->load->view('template', $this->data);
		$this->benchmark->mark('code_end'); // End Benchmarking     
	}

}
