<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class JqGrid_settings_ctrl extends CI_Controller {
	
	public function __construct()
        {
		parent::__construct();
		$this->load->model('settings/jqGrid_settings_model','jsm');
	}
        
        

	public function load_data(){
		$module = $this->input->post('module'); // module use for case
		$module_data = $this->input->post('module_data'); // data of each module
		$page = $this->input->post('page'); // get the requested page
		$loadonce = $this->input->post('loadonce'); // get loadonce
		$limit = $this->input->post('rows'); // get how many rows we want to have into the grid
		$sidx = $this->input->post('sidx'); // get index row - i.e. user click to sort
		$sord = $this->input->post('sord'); // get the direction
		$sortname = $this->input->post('sortname'); // get the direction
		$fields = $this->input->post('fields'); // index of fields
		$start = $limit*$page - $limit; 
	    $start = ($start<0)?0:$start; 
		$filters = $this->input->post('filters'); // get filters

		$where = get_filter($filters); // call from function helper
		$query = $this->jsm->getAllData($start,NULL,$sidx,$sord,$where,$module,$module_data);
		    $count = count($query); 
		    if( $count > 0 ) 
		        $total_pages = ceil($count/$limit);
		    else
		    	$total_pages = 0;
		    if ($page > $total_pages) 
		        $page=$total_pages;
		if( $loadonce != 'true' ){
			$query = $this->jsm->getAllData($start,$limit,$sidx,$sord,$where,$module,$module_data);
			
		}
		$responce = new stdClass();
		$responce->page = $page;
		$responce->total = $total_pages;
		$responce->records = $count;
	    $i=0; 
	    $flag = true;
		foreach($query as $row){
			$cell = array();
			switch($module){
				case 'online_quiz_questions':
					foreach ( $fields as $val ) {
						switch($val){
							case 'toqq_choices':
								$choices = implode('<br>', add_alphas(unserialize($row->$val),'. '));
								$cell[$val] = $choices;
							break;
							case 'toqq_answer':
								$answer = ( $row->toqq_type == 'Enumeration' || $row->toqq_type == 'Identification Plus' ) ? implode('<br>', add_alphas(unserialize($row->$val),'. ')) : $row->$val;
								$cell[$val] = $answer;
							break;
							case 'updated_date':
								$cell[$val] = date('Y-m-d', strtotime($row->$val));
							break;
							default: $cell[$val] = $row->$val; break;
						}
					}
					
					$responce->rows[$i]['id']=$row->$sortname;
					$responce->rows[$i++]['cell']=$cell;
				break;
				case 'online_quiz':
					foreach ( $fields as $val ) {
						switch($val){
							case 'taker':
								$quiz_status = ( $row->toq_quiz_status == 'Published' ) ? 'info' : 'warning';
								$quiz_title = ( $row->toq_quiz_status == 'Published' ) ? 'Published' : 'Not Publish';
								$examinees = $this->main_model->get_table('COUNT(toqr_id)','tbl_online_quiz_results','WHERE toqr_quiz_id = "'.$row->$val.'"','single');
								$cell[$val] = '<a onclick="show_examinees('.$row->toq_id.',\''.$row->toq_name.'\')" ><span class="ace-icon cursor badge badge-'.$quiz_status.'" title="'.$quiz_title.'">'.$examinees.'</span></a>';
							break;
							case 'not_taker':
								$examinees = $this->main_model->get_table('COUNT(student_id)','tbl_students','single');
								$cell[$val] = '<a onclick="show_not_examinees('.$row->toq_id.',\''.$row->toq_name.'\')" ><span class="ace-icon cursor badge badge-grey">'.examinees.'</span></a>';
							break;
							case 'updated_date':
								$cell[$val] = date('Y-m-d', strtotime($row->$val));
							break;
							case 'training_materials':
								$tags = explode(',',$row->$val);
								$tchmaterials = '';
								foreach ($tags as $m) {
									if(!empty($m)) {
										$tchmaterials .= '<span class="ace-icon badge badge-info"> '.$m. '</span><br>';
									}
								}
								$cell[$val] = $tchmaterials;
							break;
							case 'score':
								$partialScore = $this->main_model->get_table( 'toqr_quiz_id,toqr_emp_id,toqr_partial_score','tbl_online_quiz_results','WHERE toqr_quiz_id = "'.$row->toq_id.'" AND toqr_emp_id = "'.$this->session->userdata('user_id').'"','object' );
								$score = $partialScore[0]->toqr_partial_score;
								$cell[$val] = $score;		
							break;
							default: $cell[$val] = $row->$val; break;
						}
					}

				$responce->rows[$i]['id']=$row->$sortname;
				$responce->rows[$i++]['cell']=$cell;
				break;
				case 'online_quiz_examinees':
					$total_score = $this->main_model->get_table('SUM(toqq_possible_score)','tbl_online_quiz_questions','WHERE online_quiz_id = "'.$row->toqr_quiz_id.'"','single');
					foreach ( $fields as $val ) {
						switch($val){
							case 'toqr_partial_score':
								$cell[$val] = round( ( $row->$val / $total_score ) * 100 ).' %';
							break;
							case 'toqr_final_score':
								$cell[$val] = round( ( $row->$val / $total_score ) * 100 ).' %';
							break;
							case 'toqr_date':
								$cell[$val] = date('Y-m-d',strtotime($row->$val));
							break;
							case 'toqr_status':
								if( $row->$val == 'Pending' ){
									$cell[$val] = '<span class="label label-warning arrowed arrowed-right">Pending</span>';
								}else{
									$cell[$val] = '<span class="label label-success arrowed arrowed-right">Complete</span>';
								}
							break;
							default: $cell[$val] = $row->$val; break;
						}
					}
					
					$responce->rows[$i]['id']=$row->$sortname;
					$responce->rows[$i++]['cell']=$cell;
				break;
				default:
					foreach ( $fields as $val ) {

						switch($val){
							default: $cell[$val] = $row->$val; break;
						}
					}
					$responce->rows[$i]['id']=$row->$sortname;
					$responce->rows[$i++]['cell']=$cell;
				break;
			}
			
		}
		echo json_encode($responce);
	
	}

	
	
}