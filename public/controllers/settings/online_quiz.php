<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Online_quiz extends CI_Controller {

var $data = array();
    
    public function __construct()
    {
        parent::__construct();		
        checklogin();
        $this->data = array(
            'page_title' =>  strtoupper(''),
        );
    }
    
	public function index()
	{
		$this->benchmark->mark('code_start'); // For benchmarking		
        $this->data['content'] = $this->load->view('settings/online_quiz_view', $this->data ,TRUE);				
        $this->load->view('template', $this->data);
		$this->benchmark->mark('code_end'); // End Benchmarking     
	}

	public function add_quiz(){
		$insert_array = array(
			'toq_name' => $this->input->post('quiz_name'),
			'created_by' => $this->session->userdata('employee_id'),
			'updated_by' => $this->session->userdata('employee_id'),
			'updated_date' => date('Y-m-d H:i:s')
			);
		if( $this->db->insert( 'tbl_online_quiz',$insert_array ) ){
			echo 'success';
		}else{
			echo 'error';
		}
	}

	public function delete_quiz(){
		$update_array = array(
			'updated_by' => $this->session->userdata('user_id'),
			'updated_date' => date('Y-m-d H:i:s'),
			'toq_status' => 1
			);
		$this->db->update('tbl_online_quiz',$update_array,array('toq_id'=>$this->input->post('id')));
		return true;
	}

}
