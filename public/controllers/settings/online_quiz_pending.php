<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Online_quiz_pending extends CI_Controller {

var $data = array();
    
    public function __construct()
    {
        parent::__construct();		
        checklogin();
        $this->data = array(
            'page_title' =>  strtoupper(''),
        );
    }
    
	public function index()
	{
		$this->benchmark->mark('code_start'); // For benchmarking
		$quiz_id = $this->input->post('quiz_id');
		if( $quiz_id ){
			$result_id = $this->input->post('result_id');
			$this->data['emp_name'] = $this->input->post('emp_name');
			$this->data['result_id'] = $result_id;
			$this->data['essays'] = $this->create_essay($quiz_id,$result_id);
			$this->data['quiz_name'] = $this->main_model->get_table( 'toq_name','tbl_online_quiz','WHERE toq_id = "'.$quiz_id.'" AND toq_status != 1','single' );
	        $this->data['content'] = $this->load->view('settings/online_quiz_pending_view', $this->data ,TRUE);
	    }else{
	    	$this->data['content'] = $this->load->view('settings/online_quiz_view', $this->data ,TRUE);
	    } 
	    $this->load->view('template', $this->data);
		$this->benchmark->mark('code_end'); // End Benchmarking     
	}

	public function create_essay($quiz_id,$result_id){
		$essays = $this->main_model->get_table( 'toqq_id,toqq_question,toqq_possible_score','tbl_online_quiz_questions','WHERE online_quiz_id = "'.$quiz_id.'" AND toqq_type = "Essay" AND toqq_status != 1 ','object' );
		$essay_answers = $this->main_model->get_table( 'toqr_essays','tbl_online_quiz_results','WHERE toqr_id = "'.$result_id.'"','single' );
		$essay_answers = unserialize($essay_answers);
		$res_html = '';
		foreach ($essays as $key => $val) {
			$res_html .= '
				<div class="space"></div>
				<div class="question clearfix">
					<div class="essay col-xs-12">
						<h4>'.($key+1).'. '.$val->toqq_question.'</h4>
						<div class="col-xs-6">
							'.$essay_answers[$val->toqq_id].'
						</div>
					</div>
					<div class="margin-top-l margin-left-m col-xs-12">
						<input name="essay-'.$val->toqq_id.'" data-score="'.$val->toqq_possible_score.'" class="input-small input_essay" type="text"> <span class="text-muted margin-left-m"> Highest possible score is '.$val->toqq_possible_score.' </span>
					</div>
				</div>
				';
		}

		return $res_html;
	}

	public function calculate_final(){
		$data = $this->input->post();
		$res_id = $data['result_id'];
		unset($data['result_id']);
		$res = 0;
		foreach ($data as $key => $value) {
			$res += $value;
		}
		$quiz = $this->main_model->get_table( 'toqr_partial_score,toqr_quiz_id','tbl_online_quiz_results','WHERE toqr_id = "'.$res_id.'"','row_array' );
		$final = $quiz['toqr_partial_score'] + $res;
		$update_array = array(
			'toqr_final_score' => $final,
			'toqr_status' => 'Completed',
			'updated_by' => $this->session->userdata('employee_id'),
			'updated_date' => date('Y-m-d H:i:s')
			);

		$this->db->update('tbl_online_quiz_results',$update_array,array( 'toqr_id'=>$res_id ));
		redirect('agent/online_quiz_sheet');
	}

}
