<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Point_system extends CI_Controller {

var $data = array();
    
    public function __construct()
    {
        parent::__construct();		
        checklogin();
		$this->load->model('settings/jqGrid_settings_model','setmod');
        $this->data = array(
            'page_title' =>  strtoupper(''),
            'advance_search' =>  array(
                'settings' => true,
                'display' => array('search','date-range','field'),
                'buttons' => array('search'),
                ),
			'hide_show_column' =>  array(
                'settings' => true               	
                )
        );
    }
    
	public function index()
	{
		$this->benchmark->mark('code_start'); // For benchmarking		
        $this->data['content'] = $this->load->view('settings/point_system_view', $this->data ,TRUE);				
        $this->load->view('template', $this->data);
		$this->benchmark->mark('code_end'); // End Benchmarking     
	}
}
