<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Export_to_excel extends CI_Controller {
	public function __construct()
    {
		parent::__construct();
		//$this->load->model('export_to_excel_model','exportExcel');
		$this->load->library('excel');
	}
	public function index()	{
		
	}
	
	/*==============================================================================================================
									EXPORT TO EXCEL USING DATA FROM POST ARRAY by Rey
	===============================================================================================================*/
	function create_excel_file() {	
		
		$filename = $this->input->post('_defaultFilename');				
		// ADD CASE FOR YOUR EXCEL SETTINGS
	
		//set name on worksheet number 1		
		//$this->excel->getActiveSheet()->setTitle('Activity Log');	
		
		// set excel file content header
		$header_text = $this->input->post('_excelHeaders');		
		
		// set excel file content data
		foreach($this->input->post('_excelData') as $data){
			$return_data[] = $data;
		}	

		/*==============================================================================================================
													DON'T CHANGE ANYTHING BELOW
		===============================================================================================================*/
		// WRITE THE EXCEL CONTENT HEADER TEXT AND STYLE
		
		// abort if no records
		if(sizeof($return_data)==0){
			echo "Cannot Export due to No Records Found! Please press back button!";
			exit();
		}
		
		//activate worksheet number 1
		$this->excel->setActiveSheetIndex(0);
		
		$row = 1;
		$col = 0;	
		$colomn_letter = 'A';
	
		
		foreach($header_text as $header) {					
			// excel doc set header
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $header);	
			
			// get the letter of first cell of header text - for styling
			if($col == 0)
				$start_column = $colomn_letter;
			
			// get the last letter of cell of header text - for styling
			$end_column = $colomn_letter;	
			
			$colomn_letter++;	
			$col++;
		}	
		// set header style
		$header_text_style = array(
							'alignment' => array(
												'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
												),
							'fill' => array(
											'type' => PHPExcel_Style_Fill::FILL_SOLID,
											'startcolor' => array(
												'rgb' => 'CCCCCC',
												),
											),
							'font' => array(
								'bold' => true
							)
						);
		// style the header text to bold;
		$this->excel->getActiveSheet()->getStyle($start_column.'1'.":".$end_column.'1')->applyFromArray($header_text_style);
		
		// WRITE THE EXCEL CONTENT DATA -------------------------------------------------------------------------------------
		
		$col = 0;			
		$array_index = 0;	
		for($row = 1;$row <= sizeof($return_data);$row++){
			foreach ($return_data[$array_index] as $key => $val) {				
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row+1, $val);		
				$col++;				
			}	
			$this->excel->getActiveSheet()->getColumnDimensionByColumn($row-1)->setAutoSize(true);
			$col = 0;
			$array_index++;
		}	
		
		// apply default alignment for the content data 
		$this->excel->getDefaultStyle()
					->getAlignment()
					->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		
		
		$filename = $filename.'.xlsx'; //save our workbook as this file name
		// header('Content-Type: application/vnd.ms-excel'); //mime type
		// header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		// header('Cache-Control: max-age=0'); //no cache
					 
		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');  
		$response = array(
			 'success' => true,
			 'url' => $this->saveExcelToLocalFile($objWriter,$filename)
		 );
			echo json_encode($response);
			
		//force user to download the Excel file without writing it to server's HD
		//$this->load->view($objWriter->save('php://output'));
	}
	function saveExcelToLocalFile($objWriter,$filename){
		// make sure you have permission to write to directory
		$filePath = "temp/$filename";
		$objWriter->save($filePath);
		return $filePath;
	}
}
