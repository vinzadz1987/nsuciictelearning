<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

var $data = array();
    
    public function __construct()
    {
        parent::__construct();
        $this->load->library('input');
        $this->load->model('login_model','login');

        $this->data = array(
            'page_title' => 'Login Page',
        );

    }
    
	public function index()
	{
		$this->benchmark->mark('code_start'); // For benchmarking
        $this->load->view('login_view', $this->data);
		$this->benchmark->mark('code_end'); // End Benchmarking     
	}

	public function dologout()
    {
        $logged_data = array(
                   'user_id'            => '',
                   'username'           => '',
                   'last_login'         => '',
                   'logged_in'          => '',
                   'access_level'       => '',
                   'default_controller' => ''
                );
        
        $this->session->unset_userdata($logged_data);
        $this->session->sess_destroy();
        redirect('login','refresh');
    }
    
    public function dologin()
    {
        
        if ($this->input->post()):
            
        $userid = $this->input->post('userid');
        $password = $this->input->post('password');
            if ($this->login->verifylogin($userid, $password) != ''):
                echo $this->session->userdata('default_controller');
            endif;
        
        endif;
       
    }
    
    function getSubjects_Choice(){
        $this->login->getsubjecschoice($this->input->post('sem'),$this->input->post('ylevel'),$this->input->post('course'));
    }
    
    function getCourse(){
        echo $this->login->get_course();
    }
    
    function checkIDifExist(){
        echo $this->login->check_id_exist($this->input->post('idnumber'));
    }
	
	function checkIDifExistTeach(){
        echo $this->login->check_id_exist_teach($this->input->post('idnumber'));
    }
    
    public function registerUser(){
        echo  $this->login->register_user($this->input->post('idnumber'),
                $this->input->post('fname'),$this->input->post('lname'),$this->input->post('mname'),
                $this->input->post('pw'),$this->input->post('course'),$this->input->post('sem'),
                $this->input->post('level'),$this->input->post('selected_subjects'),$this->input->post('contnum'));
    }
    
    public function registerUserTeacher(){
        echo $this->login->register_tuser($this->input->post('idnumber'),
             $this->input->post('fname'),$this->input->post('lname'),
             $this->input->post('mname'),$this->input->post('pw'),
             $this->input->post('designation'), $this->input->post('teachmobile'));
    }
    
    
    
}

/* End of file login.php */
/* Location: ./application/controllers/home.php */