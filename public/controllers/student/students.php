	<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Students extends CI_Controller {

		public function __construct()
		{
			parent::__construct();
			checklogin();
			$this->load->model('student/student_model', 'studmodel');
		}

		public function index()
		{
			$this->benchmark->mark('code_start'); // For benchmarking
			$this->data['content'] = $this->load->view('student/student_subject_list_view', $this->data ,TRUE);
			$this->load->view('template', $this->data);
			$this->benchmark->mark('code_end'); // End Benchmarking     
		}

		function student_lessons()
		{
			$this->benchmark->mark('code_start'); // For benchmarking
			$this->data['params'] = $this->input->post('params');
			$this->data['content'] = $this->load->view('student/student_lessons_list_view', $this->data ,TRUE);
			$this->load->view('template', $this->data);
			$this->benchmark->mark('code_end'); // End Benchmarking     
		}

		function print_prospectus(){
			$this->benchmark->mark('code_start'); // For benchmarking
			$this->data['params'] = $this->input->post('params');
			$this->data['content'] = $this->load->view('student/student_prospectus_view', $this->data ,TRUE);
			$this->load->view('template', $this->data);
		}

		function getPrintProspectus(){
			$this->studmodel->get_subjects();
		}

		function getApproved(){
			echo $this->studmodel->get_approved($this->input->post('id'),  $this->input->post('studid'),  $this->input->post('firstname'), $this->input->post('lastname'));
		}

		function getSubjects(){
			echo $this->studmodel->get_subjects();
		}

		function enrolledSubjects(){
			echo $this->studmodel->enrolled_subjects($this->input->post('subid'),  $this->input->post('insid'));
		}

		function getSubjectGrades(){
			echo $this->studmodel->get_subject_grades($this->input->post('subid'));
		}

		function getSubjectTotalGrades(){
			$data = $this->input->post();
			$dataArray = [
				'studid' => $data['studid'],
				'year' => $data['year'],
				'sem' => $data['sem']
			];
			echo $this->studmodel->get_subject_total_grades($dataArray);
		}

	}

	/* End of file students.php */
	/* Location: ./application/controllers/home.php */