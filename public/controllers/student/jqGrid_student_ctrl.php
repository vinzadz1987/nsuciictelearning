<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class JqGrid_student_ctrl extends CI_Controller {
	
	public function __construct()
        {
		parent::__construct();
		$this->load->model('student/jqGrid_student_model','jam');
	}
	public function load_data(){
		$module = $this->input->post('module'); // module use for case
		$module_data = $this->input->post('module_data'); // data of each module
		$page = $this->input->post('page'); // get the requested page
		$loadonce = $this->input->post('loadonce'); // get loadonce
		$limit = $this->input->post('rows'); // get how many rows we want to have into the grid
		$sidx = $this->input->post('sidx'); // get index row - i.e. user click to sort
		$sord = $this->input->post('sord'); // get the direction
		$sortname = $this->input->post('sortname'); // get the direction
		$fields = $this->input->post('fields'); // index of fields
		$start = $limit*$page - $limit; 
                $start = ($start<0)?0:$start; 
		$filters = $this->input->post('filters'); // get filters

		$where = get_filter($filters); // call from function helper
		$query = $this->jam->getAllData($start,NULL,$sidx,$sord,$where,$module,$module_data);
		    $count = count($query); 
		    if( $count > 0 ) 
		        $total_pages = ceil($count/$limit);
		    else
		    	$total_pages = 0;
		    if ($page > $total_pages) 
		        $page=$total_pages;
		if( $loadonce != 'true' ){
			$query = $this->jam->getAllData($start,$limit,$sidx,$sord,$where,$module,$module_data);
			
		}
		$responce = new stdClass();
		$responce->page = $page;
		$responce->total = $total_pages;
		$responce->records = $count;
	    $i=0; 
	    $flag = true;



		foreach($query as $row){
			$cell = array();
			switch($module){
                            
                                case 'student_list':				

					foreach ( $fields as $val ) {
						switch($val){
                                                        case 'firstname':
                                                             $cell[$val] = str_replace('�','Ñ',$row->$val);
                                                             break;
                                                        case 'lastname':
                                                             $cell[$val] = str_replace('�','Ñ',$row->$val);
                                                             break;
							case 'middlename':
								$cell[$val] = str_replace('�','Ñ',$row->$val);
								break;
							default: $cell[$val] = $row->$val; break;
						}
					}

					$responce->rows[$i]['id']=$row->$sortname;
					$responce->rows[$i++]['cell']=$cell;

					break;

//				case 'raw_data':
//					foreach ( $fields as $val ) {
//
//						switch($val){
//							case 'sales':
//							case 'assisted':
//							case 'total_login':
//								$cell[$val] = number_format( $row->$val, 2,'.','' );
//								break;
//							default: $cell[$val] = $row->$val; break;
//						}
//					}
//
//					$responce->rows[$i]['id']=$row->$sortname;
//					$responce->rows[$i++]['cell']=$cell;
//
//					break;

				case 'sup_stack_rank':				
				case 'agent_stack_rank':				

					foreach ( $fields as $val ) {

						switch($val){
							case 'acvn':
							case 'sph':
							case 'nps_1':
							case 'nps_2':
								$cell[$val] = number_format( $row->$val, 2,'.','' );
								break;
							default: $cell[$val] = $row->$val; break;
						}
					}

					$responce->rows[$i]['id']=$row->$sortname;
					$responce->rows[$i++]['cell']=$cell;

					break;
                                    
                                    

				case 'agent_stack_rankx':				

					foreach ( $fields as $val ) {

						switch($val){
							case 'acvn':
								$cell[$val] = number_format( (($row->sales_clean + $row->sales_ism) / $row->assisted)*100, 2,'.','' );
								break;
							case 'acvn_rank':
								$cell[$val] = number_format( (($row->sales_clean + $row->sales_ism) / $row->assisted)*100, 2,'.','' );
								break;
							case 'sph':
								$cell[$val] = number_format( (($row->sales_clean + $row->sales_ism) / $row->assisted)*100,2,'.','');
								break;
							case 'nps_1':
								$cell[$val] = number_format( ($row->nps1_top-$row->nps1_bot)/$row->nps_total ,2,'.','' );
								break;
							case 'nps_2':
								$cell[$val] = number_format( ($row->nps2_top-$row->nps2_bot)/$row->nps_total ,2,'.','' );
								break;
							default: $cell[$val] = $row->$val; break;
						}
					}

					$responce->rows[$i]['id']=$row->$sortname;
					$responce->rows[$i++]['cell']=$cell;

					break;
					
				default:
					foreach ( $fields as $val ) {

						switch($val){
							default: $cell[$val] = $row->$val; break;
						}
					}
					$responce->rows[$i]['id']=$row->$sortname;
					$responce->rows[$i++]['cell']=$cell;
				break;
			}
			
		}
		echo json_encode($responce);
	
	}

	function rankData($array,$name){		
		$maxValue = 0;
		$rank = 0;
		$rank_name = $name.'_rank';
		foreach ($array as $key => $value) {
			if( $maxValue != $value->$name )
				$rank ++;
			$array[$key]->$rank_name = $rank;
			$maxValue = $value->$name;
		}
		return $array;
	}
        
	function deleteAllData(){
		$this->jam->deleteAllDataFromDBTable('tbl_agent_roster');
	}

	function deleteAllDataStackRank(){
		$this->jam->deleteAllDataFromDBTable('tbl_stack_rank');
	}
        
        
        function getStudentData(){
		echo $this->jam->getStudentData();
	}
        
}