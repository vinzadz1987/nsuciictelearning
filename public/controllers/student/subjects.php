<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Subjects extends CI_Controller {

var $data = array();
    
    public function __construct()
    {
        parent::__construct();
        checklogin();
        $this->load->model('student/subject_model', 'submodel');
    }
    
    public function index()
    {
            $this->benchmark->mark('code_start'); // For benchmarking
            $this->data['content'] = $this->load->view('student/subject_list_view', $this->data ,TRUE);
            $this->load->view('template', $this->data);
            $this->benchmark->mark('code_end'); // End Benchmarking     
    }
    
    function lessons()
    {
            $this->benchmark->mark('code_start'); // For benchmarking
            $this->data['params'] = $this->input->post('params');
            $this->data['content'] = $this->load->view('student/lessons_list_view', $this->data ,TRUE);
            $this->load->view('template', $this->data);
            $this->benchmark->mark('code_end'); // End Benchmarking     
    }
    
    function downloads()
    {
            $this->benchmark->mark('code_start'); // For benchmarking
            $this->data['params'] = $this->input->post('params');
            $this->data['content'] = $this->load->view('student/downloads_view', $this->data ,TRUE);
            $this->load->view('template', $this->data);
            $this->benchmark->mark('code_end'); // End Benchmarking     
    }
    
    function getSubjects(){
        echo $this->submodel->get_subjects();
    }
	
	function getSubjectsUpdate(){
        echo $this->submodel->get_subjects_update($this->input->post('subid'));
    }
    
    function getInstructor(){
        echo $this->submodel->get_instructor();
    }
    
    function getLessons(){
        echo $this->submodel->get_lessons($this->input->post('id'));
    }
    
    function getDownloads(){
        echo $this->submodel->get_downloads($this->input->post('download_id'));
    }
            
    function getLessonsCount(){
        echo $this->submodel->get_lessons_count($this->input->post('id'));
    }
    
    function getCourse(){
        echo $this->submodel->get_course();
    }
            
	function addSubject(){
		$data = $this->input->post();
		$this->submodel->add_subject(   
			$data['subjectid'],
			$data['description'],
			$data['units'],
			$data['year'],
			$data['sem'],
			$data['instructor'],
			$data['course'],
			$data['pre_requisites'],
			$data['lec_hrs'],
			$data['lab_hrs']
		);
	}

	function UpdateSubject(){
		$data = $this->input->post();
		$this->submodel->update_subject($data);
	}
    
	
    function addLessons(){
        $this->submodel->add_lessons($this->input->post('subjectid'),$this->input->post('name'),$this->input->post('description'));
    }
            
    function removeSubject(){
            $this->submodel->remove_subject($this->input->post('id'));
    }
    
    
}

/* End of file subjects.php */
/* Location: ./application/controllers/home.php */