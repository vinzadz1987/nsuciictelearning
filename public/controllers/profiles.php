<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profiles extends CI_Controller {

var $data = array();
    
    public function __construct()
    {
        parent::__construct();
        checklogin();
		$this->load->model('profiles_model','ProfileModel');
    }

    public function index()
    {
        // $this->data['page_title'] = 'User Profile';
        // $this->data['content'] = $this->load->view('profile/user_view', $this->data ,TRUE);
        // $this->load->view('template', $this->data);
    }
    
    public function edit_profile()
    {
        $this->data['page_title'] = 'User Profile';
        $this->data['content'] = $this->load->view('profile/user_view', $this->data ,TRUE);
        $this->load->view('template', $this->data);
    }
    
    public function edit_profile2()
    {
        $this->data['page_title'] = 'User Profile';
        $this->data['content'] = $this->load->view('profile/user_view2', $this->data ,TRUE);
        $this->load->view('template', $this->data);
    }
    
    public function timeline()
    {
    	$this->data['page_title'] = 'Timeline';
        $this->data['content'] = $this->load->view('profile/timeline_view', $this->data ,TRUE);
        $this->load->view('template', $this->data);
    }
    
	public function calendar()
	{
		$this->data['page_title'] = 'Calendar';
        $this->data['content'] = $this->load->view('profile/calendar_view', $this->data ,TRUE);
        $this->load->view('template', $this->data);  
	}
	public function save_event(){		
		$event_plugin_id = $this->input->post('_event_plugin_id');
		$event_name = $this->input->post('_event_name');
		$event_details = $this->input->post('_event_details');
		$event_where = $this->input->post('_event_where');
		$event_startDate = date('Y-m-d',strtotime($this->input->post('_event_startDate')));
		$event_endDate = date('Y-m-d',strtotime($this->input->post('_event_endDate')));
		$event_startTime = $this->input->post('_event_startTime');
		$event_endTime = $this->input->post('_event_endTime');
		$event_type = $this->input->post('_event_type');
		$current_user = $this->session->userdata('employee_id');		
		
        echo $this->ProfileModel->save_event_Qry($event_plugin_id,$event_name,$event_details,$event_where,$event_startDate,$event_endDate,$event_startTime,$event_endTime,$current_user,$event_type);
	}
	
	public function get_event_more_info(){		
		$event_plugin_id = $this->input->post('_event_id');
        $res['event_info'] = $this->ProfileModel->get_event_more_info_Qry($event_plugin_id);
		if($res){
			echo json_encode($res);
		}
	}

	public function load_all_active_events(){
		$this->ProfileModel->load_all_active_events_Qry();
	}
        
  public function options(){
      $segment = $this->uri->segment(3);
      switch($segment){
          case 'location':
              $this->ProfileModel->options2();
              break;
          case 'position':
              $this->ProfileModel->options_position();
              break;
          case 'supervisor':
              $this->ProfileModel->options_supervisor();
              break;
          case 'qa':
              $this->ProfileModel->options_qa();
              break;
      }
    }
                    
    function update_agent(){
        $uid = $this->input->post('uid');
        if($uid != ''){
            $this->ProfileModel->update_agent($this->input->post('uid'),  $this->input->post('att_uid'),  $this->input->post('first_name'),  $this->input->post('last_name'), $this->input->post('location'),  $this->input->post('position'),  $this->input->post('supervisor'),  $this->input->post('qa'), $this->input->post('rt_id'),  $this->input->post('complete_name'), $this->input->post('tenurity'));
        }else{
            $this->ProfileModel->insert_agent($this->input->post('att_uid'),  $this->input->post('first_name'),  $this->input->post('last_name'), $this->input->post('location'),  $this->input->post('position'),  $this->input->post('supervisor'),  $this->input->post('qa'), $this->input->post('rt_id'),  $this->input->post('complete_name'), $this->input->post('tenurity'));
        }
     }

    function  updateInfo(){
        echo $this->ProfileModel->update_info($this->input->post('id'),  $this->input->post('val'),  $this->input->post('col'));
    }
    
    function getProfileInfo(){
        if($this->input->post('access_name') == 'Students'){
            echo $this->ProfileModel->get_profile_info2();
        }else{
            echo $this->ProfileModel->get_profile_info();
        }
    }
    
    function saveProfileImage(){
        if($this->session->userdata('access_name') == 'Students'){
            echo $this->ProfileModel->get_profile_update($this->input->post('imgsrc'));
        }else{
            echo $this->ProfileModel->get_profile_update2($this->input->post('imgsrc'));
        }
    }
    
}
