<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sessions extends CI_Controller {

var $data = array();
    
    public function __construct()
    {
        parent::__construct();
        checklogin();
		$this->load->helper('menu_helper');
       
    }
    
	public function index()
	{
		
	}
	function save_session(){
		$array = array('campaign_id' => $this->input->post('_campaignIdSession'),'campaign_name' => $this->input->post('_campaignNameSession'),'business_id' => $this->input->post('_businessIdSession'),'business_name' => $this->input->post('_businessNameSession'));
        $this->session->set_userdata($array);  
		
		// echo json_encode($data);
		$data['default_campaign_id'] =  $this->input->post('_campaignIdSession');
		$data['default_campaign_name'] = $this->input->post('_campaignNameSession');
		$data['default_business_id'] = $this->input->post('_businessIdSession');
		$data['default_business_name'] = $this->input->post('_businessNameSession');
		
		$this->session->set_userdata('campaign_id',$this->input->post('_campaignIdSession'));
		$this->session->set_userdata('campaign_name',$this->input->post('_campaignNameSession'));
		$this->session->set_userdata('business_id',$this->input->post('_businessIdSession'));
		$this->session->set_userdata('business_name',$this->input->post('_businessNameSession'));
		
		$data['campaign'] = show_campaign(); 
		$data['business'] = show_business($data['default_campaign_id']);  
		echo json_encode($data);
	}
	function retrieve_session(){
		$data['default_campaign_id'] = ($this->session->userdata('campaign_id') != false) ? $this->session->userdata('campaign_id'):  default_campaign('id');
		$data['default_campaign_name'] = ($this->session->userdata('campaign_name') != false) ? $this->session->userdata('campaign_name'):  default_campaign('name');
		$data['default_business_id'] = ($this->session->userdata('business_id') != false) ? $this->session->userdata('business_id'):  default_business('id',$data['default_campaign_id']);
		$data['default_business_name'] = ($this->session->userdata('business_name') != false) ? $this->session->userdata('business_name'):  default_business('name',$data['default_campaign_id']);
		
		$this->session->set_userdata('campaign_id', $data['default_campaign_id']);
		$this->session->set_userdata('campaign_name',$data['default_campaign_name']);
		$this->session->set_userdata('business_id',$data['default_business_id']);
		$this->session->set_userdata('business_name',$data['default_business_name']);
				
		$data['campaign'] = show_campaign(); 
		$data['business'] = show_business($data['default_campaign_id']);  

		echo json_encode($data);
	}
	function getBusiness(){		
		$this->session->set_userdata('campaign_id', $this->input->post('_campaignId'));
		$this->session->set_userdata('campaign_name',$this->input->post('_campaignName'));

		$data['business'] = show_business($this->input->post('_campaignId'));
		$b = show_business($this->input->post('_campaignId'));
		$c = 0;
		$id = '';
		$name = '';
		foreach( $b as $row){
			if($c == 0){
				$id = $row->business_id;
				$name = $row->business_name;
			}
			$c++;			
		}
		$this->session->set_userdata('business_id',$id);
		$this->session->set_userdata('business_name',$name);
		$data['default_business_id'] = $this->session->userdata('business_id');
		$data['default_business_name'] = $this->session->userdata('business_name');
	   echo json_encode($data);
	}
	function save_bussiness_session(){		
		$this->session->set_userdata('campaign_id', $this->input->post('_campaignId'));
		$this->session->set_userdata('campaign_name',$this->input->post('_campaignName'));		
		$this->session->set_userdata('business_id',$this->input->post('_businessId'));
		$this->session->set_userdata('business_name',$this->input->post('_businessName'));
		
		$data['default_campaign_id'] = $this->session->userdata('campaign_id');
		$data['default_campaign_name'] = $this->session->userdata('campaign_name');
		$data['default_business_id'] = $this->session->userdata('business_id');
		$data['default_business_name'] = $this->session->userdata('business_name');
	    echo json_encode($data);
	}
	function keep_alive(){
		$response = $this->load->view('keep_alive_view',"", TRUE);
		if($response != 'OK')
			redirect('/login/dologout');
		else 
			print $response;
	}

}

