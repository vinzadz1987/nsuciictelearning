<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Audit extends CI_Controller {

var $data = array();
    
    public function __construct()
    {
        parent::__construct();
        checklogin();
        $this->data = array(
            'page_title' =>  'Audit Report',
        );
    }
    
	public function index()
	{
		$this->benchmark->mark('code_start'); // For benchmarking
                $this->data['content'] = $this->load->view('quality/summary_view', $this->data ,TRUE);
                $this->load->view('template', $this->data);
		$this->benchmark->mark('code_end'); // End Benchmarking     
	}

	

}

/* End of file home.php */
/* Location: ./application/controllers/home.php */