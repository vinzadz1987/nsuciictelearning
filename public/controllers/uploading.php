<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Uploading extends CI_Controller {

var $data = array();
    
    public function __construct()
    {
        parent::__construct();
        checklogin();
        $this->load->model('uploading_model', 'model');
        $this->load->library('csvimport');
    }
    
//    public function index()
//    {
//            $this->benchmark->mark('code_start'); // For benchmarking
//            $this->data['content'] = $this->load->view('uploading/upload_training_materialsview', $this->data ,TRUE);
//            $this->load->view('template', $this->data);
//            $this->benchmark->mark('code_end'); // End Benchmarking     
//    }
//    function upload_materials(){
//            $this->benchmark->mark('code_start'); // For benchmarking
//            $this->data['content'] = $this->load->view('agent/training_materials_view', $this->data ,TRUE);
//            $this->load->view('template', $this->data);
//            $this->benchmark->mark('code_end'); // End Benchmarking
//    }
//    function agents_scores(){
//        $this->benchmark->mark('code_start'); // For benchmarking
//        $this->data['content'] = $this->load->view('uploading/agents_score_cardsview', $this->data ,TRUE);
//        $this->load->view('template', $this->data);
//        $this->benchmark->mark('code_end'); // End Benchmarking
//    }
    function student_roster(){
        $this->benchmark->mark('code_start'); // For benchmarking
        $this->data['content'] = $this->load->view('student/student_list_view', $this->data ,TRUE);
        $this->load->view('template', $this->data);
        $this->benchmark->mark('code_end'); // End Benchmarking
    }
    function getCSVformat(){
        $this->model->getCSVformatTable();
    }
//    function importcsv() {
//            $uri = $this->uri->uri_to_assoc(2);
//            //$data['addressbook'] = $this->model->get_addressbook();
//            //$data['error'] = ''; //initialize image upload error array to empty
//            $config['upload_path'] = './uploads/';
//            $config['allowed_types'] = '*';
//            $config['max_size'] = '999999';
//             /*print_r($_FILES);
//             EXIT;*/
//            $this->load->library('upload', $config);
//            // If upload failed, display error
//            if (!$this->upload->do_upload()) {
//                    echo $data['error'] = $this->upload->display_errors();
//            } else {
//                    $file_data = $this->upload->data();
//                    $file_path = './uploads/'.$file_data['file_name'];
//                    if ($this->csvimport->get_array($file_path)) {
//                            $csv_array = $this->csvimport->get_array($file_path);
//                            switch($uri['importcsv']){
//                                    case 'online_quiz':
//                                            $data_save = $this->model->qa360($csv_array);
//                                            if($data_save){
//                                                     $this->session->set_flashdata('msg', 'Csv Data Imported Successfully');
//                                                     redirect('uploading/online_quiz');
//                                            }
//                                    break;
//                            }
//
//                    } else {
//                            $data['error'] = "Error occured";
//                    }
//            }
//    }
    function test(){
            $file_path = './uploads/comcast_update.csv';
            if ($this->csvimport->get_array($file_path)) {
                    $csv_array = $this->csvimport->get_array($file_path);
                    $this->model->tcportal($csv_array);
            } else {
                    $data['error'] = "Error occured";
                    //$this->load->view('csvindex', $data);
            }
    }

    function uploadFiles(){
        $fileName = $this->uri->segment(3);
        switch($fileName){
//            case 'nps':
//                   $data = $this->input->post('data');
//                   $this->model->uploadFilesExtract($data, 1);
//                break;
//            case 'agentroster':
//                   $data = $this->input->post('data');
//                   $this->model->uploadFilesExtract($data, 2);
//                break;
            case 'students':
                   $data = $this->input->post('data');
                   $this->model->uploadFilesExtract($data, 1);
                break;
        }
    }

//    function get_employee_data(){
//       echo $this->model->getEmployeeData();
//    }

    function uploadPhysicalFiles(){
        $target = "uploads/lessons/";
        $target = $target . str_replace(' ','_',basename ( $_FILES['files']['name']));
        if(move_uploaded_file($_FILES['files']['tmp_name'], $target)) {
            $this->model->getFileNameUpload(basename ( $_FILES['files']['name']),$target,$_POST['upload_name']);

//            $subjectURL = explode(',', $_POST['params'])[0]."_".explode(',', $_POST['params'])[1].'_'.explode(',', $_POST['params'])[2];
//            $spec_chars = array ('&',"(",")");
//            $subject = str_replace($spec_chars, '_', $subjectURL);
//            echo $subject;
//            redirect( "teacher/teacher_subjects/teacher_lessons/".$subject."");
            redirect( "teacher/teacher_subjects");
            
        }else{
            
//            $subjectURL = explode(',', $_POST['params'])[0]."_".explode(',', $_POST['params'])[1].'_'.explode(',', $_POST['params'])[2];
//            $spec_chars = array ('&',"(",")");
//            $subject = str_replace($spec_chars, '_', $subjectURL);
//            echo $subject;
//            redirect( "teacher/teacher_subjects/teacher_lessons/".$subject."");
            redirect( "teacher/teacher_subjects");
        }

    }
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */