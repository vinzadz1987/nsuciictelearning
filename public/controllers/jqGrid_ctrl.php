<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class JqGrid_ctrl extends CI_Controller {
	
	public function __construct()
        {
		parent::__construct();
		$this->load->model('jqGrid_model','smod');

		// Test model only
		$this->load->model('one_model','one');
		$this->month = date('Y-m-d');
	}

	public function load_jq_data(){
		$kind = ($this->input->post('data_case'))?$this->input->post('data_case'):1;
		if($kind == 'local')
                                    return true;
		$data = ($this->input->post('data'))?$this->input->post('data'):NULL;
		$ops = ($this->input->post('ops'))?$this->input->post('ops'):NULL;
		$adv = ($this->input->post('adv'))?$this->input->post('adv'):NULL;
		$page = isset($_POST['page'])?$_POST['page']:1; // get the requested page
		$limit = isset($_POST['rows'])?$_POST['rows']:10; // get how many rows we want to have into the grid
		$sidx = isset($_POST['sidx'])?$_POST['sidx']:'id'; // get index row - i.e. user click to sort
		$sord = isset($_POST['sord'])?$_POST['sord']:''; // get the direction
		$start = $limit*$page - $limit; 
                                     $start = ($start<0)?0:$start; 
		$filters = isset($_POST['filters'])?$_POST['filters']:''; // get filters
		$where = $groupOp = ""; 
		$this->month = ($ops) ? $ops : date('Y-m-d').' - '.date('Y-m-d');

		$query = $this->smod->getAllData($start,NULL,$sidx,$sord,$where,$kind,$data,$ops,$adv);
		// print_r($this->db->last_query());
		// exit();
		if(!$sidx) 
	        $sidx =1;
	    $count = count($query); 
	    if( $count > 0 ) {
	        $total_pages = ceil($count/$limit);    
	    } else {
	        $total_pages = 0;
	    }
		
	    if ($page > $total_pages) 
	        $page=$total_pages;
		
		$query = $this->smod->getAllData($start,$limit,$sidx,$sord,$where,$kind,$data,$ops,$adv);
		
		$responce = new stdClass();
		$responce->page = $page;
		$responce->total = $total_pages;
		$responce->records = $count;
		$fields = $this->get_fields($kind);
                                    $i=0; 
		$ctr = 1;
                                    $flag = true;
		foreach($query as $row){
			
			$responce->rows[$i]['id']=$row->$fields['sortname'];
			$cell = array();
			switch($kind){
				default:
					foreach ( explode(',', $fields['fields']) as $val ) {
						$cell[] = $row->$val;
					}
				break;
	            case 'training_materials':
	                foreach (explode(',', $fields['fields']) as $val){
	                    switch($val){
	                        default:
	                            $cell[$val] = $row->$val;
	                            break;
	                    }
	                }
	            break;
	            case 'agent_list':
	                foreach (explode(',', $fields['fields']) as $val){
	                    switch($val){
	                        default:
	                            $cell[$val] = $row->$val;
	                            break;
	                    }
	                }
	                $cell['last_name'] = str_replace('�','Ñ',$row->last_name);
	                $cell['first_name'] = str_replace('�','Ñ',$row->first_name);
	                $cell['location'] = str_replace('�','Ñ',$row->location);
	                $cell['supervisor'] = str_replace('�','Ñ',$row->supervisor);
	            break;
	            case 'stack_rank':
	                foreach (explode(',', $fields['fields']) as $val){
	                    switch($val){
	                        default:
	                            $cell[$val] = $row->$val;
	                            break;
	                    }
	                }
	                $cell['last_name'] = str_replace('�','Ñ',$row->last_name);
	                $cell['first_name'] = str_replace('�','Ñ',$row->first_name);
	                $cell['location'] = str_replace('�','Ñ',$row->location);
	                $cell['supervisor'] = str_replace('�','Ñ',$row->supervisor);
	            break;
				}
			$ctr++;
			$responce->rows[$i++]['cell']=$cell;
		}
		
		echo json_encode($responce);
	
	}

	function get_fields($kind) {
		switch ($kind){
			default:
				$data['sortname'] = $this->input->post('sortname');
				$data['fields'] = $this->input->post('fields');
			break;
		}
		return $data;
	}

	// Create jqGrid Container
	function jqgrid_container()
	{
		$data_case = $this->input->post('data_case'); //use for switch cases
		$header = $this->input->post('headers'); //header for the table
		$names = $this->input->post('names'); //use for modifaction and functons. assign names for data in a row.
		$index = $this->input->post('index'); //use for query. assign index for data in a row. 
		$widths = $this->input->post('widths'); //specific width of every column
		$sortname = $this->input->post('sortname'); //use to initialize sort

		$data = $this->input->post('data');
		$ops = $this->input->post('ops');
		$adv = $this->input->post('adv');
		
		foreach ($index as $key => $val) {
			$headers[] = $header[$key];
			$test = new stdClass();
			$test->name = $names[$key];
			$test->index = $index[$key];
			$test->width = (isset($widths[$key])) ? $widths[$key]:'80';
			$test->align = 'center';
			
			switch($data_case){
				case 'setting_comcast':
                    switch($key){
						case 0:
							$test->formatter = 'actions';
							$test->editable = false;
							$test->formatoptions = array('keys'=>true);
						break;					
						case 3:
							$editoptions = new stdClass();
							$editoptions->value = $this->smod->static_goal_type();
							$test->edittype = 'select';
							$test->editoptions = $editoptions;
							$test->editable = true;
						break;
						case 4:case 5:
							$test->editable = true;
							$test->unformat = 'pickDate';
						break;					
						case 6:
							$test->editable = false;
						break;
						default:
							$test->editable = true;
						break;
					}                       
				break;
				case 'sanction_settings':
                    switch($key){
						case 0:
							$test->formatter = 'actions';
							$test->editable = false;
							$test->formatoptions = array('keys'=>true);
						break;
						default:
							$test->editable = true;
						break;
					}                       
				break;
				case 'point_system':
					
                    switch($key){
						case 0:
							$test->formatter = 'actions';
							$test->editable = false;
							$test->formatoptions = array('keys'=>true);
							$test->hidden = true;
						break;
						case 1:case 2:case 3:case 4:case 5:case 14:
							$test->editable = false;
						break;
						case 6:
							$test->sortable = false;
						break;
						case 7:case 8:
							$test->hidden = true;
						break;
						default:
							if($ops!='')
								$test->editable = false;
							else
								$test->editable = true;
						break;
					}                       
				break;
				case 'weight_settings':
                    switch($key){
						case 0:
							$test->formatter = 'actions';
							$test->editable = false;
							$test->formatoptions = array('keys'=>true);
						break;					
						case 1:
							$editoptions = new stdClass();
							$editoptions->value = $this->smod->static_select_account();
							$test->edittype = 'select';
							$test->editoptions = $editoptions;
							$test->editable = true;
						break;					
						case 2:
							$editoptions = new stdClass();
							$editoptions->value = $this->smod->static_select_wtype();
							$test->edittype = 'select';
							$test->editoptions = $editoptions;
							$test->editable = true;
						break;
						default:
							$test->editable = true;
						break;
					}                       
				break;
				case 'metric_settings':
                    switch($key){
						case 0:
							$test->formatter = 'actions';
							$test->editable = false;
							$test->formatoptions = array('keys'=>true);
						break;					
						case 1:
							$editoptions = new stdClass();
							$editoptions->value = $this->smod->static_select_account();
							$test->edittype = 'select';
							$test->editoptions = $editoptions;
							$test->editable = true;
						break;					
						case 2:
							$editoptions = new stdClass();
							$editoptions->value = $this->smod->static_select_wtype();
							$test->edittype = 'select';
							$test->editoptions = $editoptions;
							$test->editable = true;
						break;					
						case 6:
							$editoptions = new stdClass();
							$editoptions->value = $this->smod->static_select_stats();
							$test->edittype = 'select';
							$test->editoptions = $editoptions;
							$test->editable = true;
						break;
						default:
							$test->editable = true;
						break;
					}                       
				break;
				default:
					$test->search = false;
					break;
			}
			$models[] = $test;
		}

		$samp = new stdClass();
		$postData = new stdClass();
		$postData->fields = implode(',', $index);
		$postData->sortname = $sortname;
		$postData->data_case = $data_case;
		$postData->data = $data;
		$postData->ops = $ops;
		$postData->adv = $adv;

		$samp->postData = $postData;
		$samp->colmodel = $models;
		$samp->colnames = $headers;
		echo json_encode($samp);
	}
	public function edit_row(){
		$cases = $this->uri->segment(4);
		if($cases=='point_system') $_POST['point_date'] = $this->month;
        if($this->smod->editRow($cases,$_POST))
            return true;
           
		return false;
    }
	
}