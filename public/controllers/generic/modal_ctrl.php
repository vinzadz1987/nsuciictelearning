<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Modal_ctrl extends CI_Controller {

    var $data = array();
    
    public function __construct()
    {
        parent::__construct();
        $this->data = array();
    }
    
    public function modal()
    {
        $this->benchmark->mark('code_start'); // For benchmarking
        $page = ($_POST['data_case']) ? $_POST['data_case'] : $_GET['location'];
        $postData = $_POST['data'];
        $this->data['data'] = $this->getData($page,$postData);
        if(!empty($_GET['location'])){
            $this->load->view($page,$this->data);
        }else echo json_encode($this->load->view($page,$this->data,TRUE));
        
        $this->benchmark->mark('code_end'); // End Benchmarking      
    }

    function getData($page,$data){
        $location = explode('?', $page)[0];
        $page_data = '';
        switch ($location) {
            case 'modals/top_up_modal':
                $page_data = array(
                    'data_case' => $data
                );
                break;
            case 'modals/advance_search_modal':
                $page_data = array(
                    'supervisor_dropDown' => $this->main_model->supervisorDropdown(),
                    'date' => $_GET['date']
                );
                break;
            case 'modals/volumes_hours_requested_view':
                 $page_data = array(
                    'getid_data' => $this->main_model->get_table('anothersub_id actions, anothersub_id, subcat_id, subcat_id catname, anothersub_name, anothersub_name volumes, anothersub_goals goals, subcat_id JANUARY, subcat_id FEBRUARY, subcat_id MARCH, subcat_id APRIL, subcat_id MAY, subcat_id JUNE, subcat_id JULY, subcat_id AUGUST, subcat_id SEPTEMBER, subcat_id OCTOBER, subcat_id NOVEMBER, subcat_id DECEMBER, subcat_id YTD' ,'nick_tbl_anothersub_cat' ,'WHERE subcat_id = "'.$_GET['data'].'"')
                );
                break;
            case 'modals/top_list_modal':
                 $page_data = array(
                    'list_data' => $this->main_model->serializeToArray($data)
                );
                break;
            case 'charts/overall_summary_lifelock':
            case 'charts/reports_kpi_chart':
                $page_data = array(
                    'chartData' => $this->main_model->get_table('`ID_#` id_num, RT_Login rt_login','rdata_qfn_agentlist','WHERE `ID_#` = "'.$_GET['data'].'"'),
                    'data_case' => $_GET['data_case']
                );
                break;
            break;
            case 'charts/summary_kpi_chart':
                $page_data = array(
                    'data_case' => $_POST['data']
                );
            break;
			case 'charts/overall_summary_chart':
				$page_data = array(
					'row_data' => $data
				);
			break;
            default:
                # code...
                break;
        }

        return $page_data;
    }

    function get_weekDate(){
        $year = $this->input->post('year');
        $week_start = $this->input->post('week_start');
        $week_end = $this->input->post('weel_end');

        echo json_encode($week_start);
    }

    function get_quarterDate(){
        $year = $this->input->post('year');
        $date_res = date(''.$year.'-01-01').' - '.date(''.$year.'-12-t');
        echo json_encode($date_res);
    }
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */