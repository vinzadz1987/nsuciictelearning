<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Scorecards extends CI_Controller {
	
var $data = array();
    
    public function __construct()
    {
        parent::__construct();
        checklogin();
		$this->load->model('scorecard_model','scmod');
        $this->data = array(
            'page_title' =>  strtoupper(''),
            'advance_search' =>  array(
                'settings' => true,
                'display' => array('search','date-range'),
                'buttons' => array('search','advance'),
                'auto_close' => true
                ),
            'hide_show_column' =>  array(
                'settings' => true                  
                )
        );
    }
    
	public function index()
	{
		$this->benchmark->mark('code_start'); // For benchmarking
        $this->data['content'] = $this->load->view('scorecard_view', $this->data ,TRUE);
        $this->load->view('template', $this->data);
		$this->benchmark->mark('code_end'); // End Benchmarking     
	}
	
	function getsupervisors(){
		echo json_encode($this->scmod->getsupervisor());
	}
	function getmanagers(){
		echo json_encode($this->scmod->getmanager());
	}
	function get_agent_info(){
		echo json_encode($this->scmod->getagentinfo());
	}
	function get_scorecard(){
		echo json_encode($this->scmod->getscorecard());
	}
	function showmetric(){
		return $this->scmod->showmetricpreview($this->input->post('account'));
	}
}
