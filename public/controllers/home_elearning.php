<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home_elearning extends CI_Controller {

var $data = array();
    
    public function __construct()
    {
        parent::__construct();
		
        checklogin();
        $this->data = array(
            'page_title' =>  ucwords($this->session->userdata('access_name')).' Homepage',
        );
    }
    
	public function index()
	{	

		$this->benchmark->mark('code_start'); // For benchmarking
        $this->data['content'] = $this->load->view('home_view', $this->data ,TRUE);
        $this->load->view('template', $this->data);
		$this->benchmark->mark('code_end'); // End Benchmarking     
	}

}