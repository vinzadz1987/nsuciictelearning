<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mail extends CI_Controller {

    
    public function __construct()
    {
        parent::__construct();
        checklogin();
        $this->load->model('teacher/teacher_model', 'teachmodel');
    }
    
    public function index()
    {
            $this->benchmark->mark('code_start'); // For benchmarking
            $this->data['params'] = $this->input->post('params');
            $this->data['content'] = $this->load->view('teacher/mail_view', $this->data ,TRUE);
            $this->load->view('template', $this->data);
            $this->benchmark->mark('code_end'); // End Benchmarking   
    }
    
    function getSelect(){
        $stype = $this->uri->segment(4);
        switch ($stype){
            case 1:
                echo $this->teachmodel->get_select($stype);
                break;
            case 2:
                echo $this->teachmodel->get_select($stype);
                break;
        }
    } 
    
    function getMailList(){
        echo $this->teachmodel->getMailList();
    }
    
    function getinboxmessages(){
        $stype = $this->uri->segment(4);
        switch ($stype){
            case 1:
//                echo $this->teachmodel->get_select($stype);
                
                echo $this->teachmodel->get_inboxmessages($stype);
                break;
//            case 2:
//                echo $this->teachmodel->get_inboxmessages($stype);
//                break;
        }
//        echo $this->teachmodel->get_inboxmessages();
    }
    
    function getsentmessages(){
        echo $this->teachmodel->get_sentmessages();
    }
            
    function sendMessage(){
        echo $this->teachmodel->send_message($this->input->post('data'), $this->input->post('messages'));
    }
    
     function getStudents(){
        echo $this->teachmodel->get_students();
    } 
    
    
}

/* End of file mail.php */