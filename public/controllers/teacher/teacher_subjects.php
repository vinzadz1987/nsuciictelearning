<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Teacher_subjects extends CI_Controller {

    var $data = array();
    
    public function __construct()
    {
        parent::__construct();
        checklogin();
        $this->load->model('teacher/teacher_model', 'teachmodel');
    }
    
    public function index()
    {
            $this->benchmark->mark('code_start'); // For benchmarking
            $this->data['params'] = $this->input->post('params');
            $this->data['content'] = $this->load->view('teacher/teacher_subject_list_view', $this->data ,TRUE);
            $this->load->view('template', $this->data);
            $this->benchmark->mark('code_end'); // End Benchmarking  
    }
    
    function teacher_lessons()
    {
            $this->benchmark->mark('code_start'); // For benchmarking
            $this->data['params'] = $this->input->post('params');
            $this->data['content'] = $this->load->view('teacher/teacher_lessons_list_view', $this->data ,TRUE);
            $this->load->view('template', $this->data);
            $this->benchmark->mark('code_end'); // End Benchmarking     
    }
    
    function getSubjects(){
        echo $this->teachmodel->get_subjects();
    }
    
    function getEnrolledStudents(){
        echo $this->teachmodel->get_enrolled_students($this->input->post('subject_id'));
    }
    
    
    function getNotes(){
       echo $this->teachmodel->get_notes($this->input->post('id')); 
    }
    
     function getVideos(){
       echo $this->teachmodel->get_videos($this->input->post('id')); 
    }
    
    function approveStudent(){
        $this->teachmodel->approve_students($this->input->post('param'));
    }
    
    
    function addNotes(){
        $this->teachmodel->add_notes($this->input->post('subjectid'),$this->input->post('notes_title'),$this->input->post('notes_desc'));
    }
    
    function addVideo(){
        $this->teachmodel->add_videos($this->input->post('subjectid'),$this->input->post('video_title'),$this->input->post('video_desc'),$this->input->post('video_embed'));
    }
    
    function addGrades(){
        $this->teachmodel->add_grades($this->input->post('subjectid'),$this->input->post('grades'));
    }
    
    
}

/* End of file teachers subjects.php */
/* Location: ./application/controllers/home.php */