<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Online_quiz_questions extends CI_Controller {

var $data = array();
    
    public function __construct()
    {
        parent::__construct();		
        checklogin();
        $this->data = array(
            'page_title' =>  strtoupper(''),
        );
    }
    
	public function index()
	{
		$this->benchmark->mark('code_start'); // For benchmarking
		$this->data['title'] = $this->input->post('title');
		$this->data['quiz_id'] = $this->input->post('quiz_id');
		$this->data['status'] = $this->main_model->get_table( 'toq_quiz_status','tbl_online_quiz','WHERE toq_id = "'.$this->data['quiz_id'].'" AND toq_status != 1','single' );
        $this->data['content'] = $this->load->view('settings/online_quiz_questions_view', $this->data ,TRUE);				
        $this->load->view('template', $this->data);
		$this->benchmark->mark('code_end'); // End Benchmarking     
	}

	public function add_question(){
		$form_data = serializeToArray($this->input->post('form_data'));
		$multiChoiceData = $this->input->post('m_data');
		$identificationPlusData = json_decode($this->input->post('i_plus'));
		$quiz_id = $this->input->post('quiz_id');
		$insert_data = array(
			'online_quiz_id' => $quiz_id,
			'toqq_question' => $form_data['quiz_question'],
			'toqq_type' => $form_data['quiz_type'],
			'updated_by' => $this->session->userdata('user_id'),
			'updated_date' => date('Y-m-d H:i:s')
			);

		if( $form_data['quiz_type'] == 'Enumeration' ){
			// set choices and answer
			unset($insert_data['toqq_choices']);
			$insert_data['toqq_answer'] = serialize($multiChoiceData);

			// set possible score
			$possible_score = ( count($multiChoiceData) >= 3 ) ? 3 : count($multiChoiceData);
			$insert_data['toqq_possible_score'] = $possible_score;
		}else if( $form_data['quiz_type'] == 'Identification Plus' ){
			// set choices and answer
			$question = array();
			$answer = array();
			foreach ($identificationPlusData as $key => $value) {
				array_push($question, $key);
				array_push($answer, $value);
			}
			$insert_data['toqq_choices'] = serialize($question);
			$insert_data['toqq_answer'] = serialize($answer);
			// set possible score
			$insert_data['toqq_possible_score'] = count((array)$identificationPlusData); // cast an object as an array to enable count.
		}else{
			// set choices and answer
			$insert_data['toqq_choices'] = serialize($multiChoiceData);
			$insert_data['toqq_answer'] = $form_data['quiz_answer'];

			// set possible score
			if( $form_data['quiz_type'] == 'Essay' ){
				$insert_data['toqq_possible_score'] = $form_data['quiz_answer'];
			}else{
				$insert_data['toqq_possible_score'] = 1;
			}
		}
		
		if( $this->db->insert('tbl_online_quiz_questions',$insert_data) ){
			echo 'success';
		}else echo 'error';
	}

	public function update_question(){
		$form_data = serializeToArray($this->input->post('form_data'));
		$multiChoiceData = $this->input->post('m_data');
		$identificationPlusData = json_decode($this->input->post('i_plus'));
		$question_id = $this->input->post('question_id');
		$update_data = array(
			'toqq_question' => $form_data['quiz_question'],
			'toqq_type' => $form_data['quiz_type'],
			'updated_by' => $this->session->userdata('user_id'),
			'updated_date' => date('Y-m-d H:i:s')
			);

		if( $form_data['quiz_type'] == 'Enumeration' ){
			// set choices and answer
			unset($update_data['toqq_choices']);
			$update_data['toqq_answer'] = serialize($multiChoiceData);

			// set possible score
			$possible_score = ( count($multiChoiceData) >= 3 ) ? 3 : count($multiChoiceData);
			$update_data['toqq_possible_score'] = $possible_score;
		}else if( $form_data['quiz_type'] == 'Identification Plus' ){
			// set choices and answer
			$question = array();
			$answer = array();
			foreach ($identificationPlusData as $key => $value) {
				array_push($question, $key);
				array_push($answer, $value);
			}
			$update_data['toqq_choices'] = serialize($question);
			$update_data['toqq_answer'] = serialize($answer);
			// set possible score
			$update_data['toqq_possible_score'] = count((array)$identificationPlusData); // cast an object as an array to enable count.
		}else{
			// set choices and answer
			$update_data['toqq_choices'] = serialize($multiChoiceData);
			$update_data['toqq_answer'] = $form_data['quiz_answer'];

			// set possible score
			if( $form_data['quiz_type'] == 'Essay' ){
				$update_data['toqq_possible_score'] = $form_data['quiz_answer'];
			}else{
				$update_data['toqq_possible_score'] = 1;
			}
		}

		if( $this->db->update('tbl_online_quiz_questions',$update_data,array('toqq_id'=>$question_id)) ){
			echo 'success';
		}else echo 'error';
	}

	public function edit_title(){
		$title = $this->input->post('title');
		$quiz_id = $this->input->post('quiz_id');

		$this->db->update( 'tbl_online_quiz',array('toq_name'=>$title),array('toq_id'=>$quiz_id) );
		echo 'success';
	}

	public function delete_quiz_item(){
		$update_array = array(
			'updated_by' => $this->session->userdata('employee_id'),
			'updated_date' => date('Y-m-d H:i:s'),
			'toqq_status' => 1
			);
		$this->db->update('tbl_online_quiz_questions',$update_array,array('toqq_id'=>$this->input->post('id')));
		return true;
	}

	public function publish_quiz(){
		$quiz_id = $this->input->post('quiz_id');
		$title = $this->input->post('title');
		$this->db->update( 'tbl_online_quiz',array('toq_quiz_status'=>'Published'),array('toq_id'=>$quiz_id) );

		$this->data['title'] = $title;
		$this->data['status'] = 'Published';
		$this->data['quiz_id'] = $quiz_id;
        $this->data['content'] = $this->load->view('settings/online_quiz_questions_view', $this->data ,TRUE);				
        $this->load->view('template', $this->data);
	}

}
