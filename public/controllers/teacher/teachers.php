<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Teachers extends CI_Controller {

var $data = array();
    
    public function __construct()
    {
        parent::__construct();
        checklogin();
        $this->load->model('teacher/teacher_model', 'teachmodel');
    }
    
    public function index()
    {
            $this->benchmark->mark('code_start'); // For benchmarking
            $this->data['content'] = $this->load->view('teacher/teachers_list_view', $this->data ,TRUE);
            $this->load->view('template', $this->data);
            $this->benchmark->mark('code_end'); // End Benchmarking     
    }
    
    function lessons()
    {
            $this->benchmark->mark('code_start'); // For benchmarking
            $this->data['params'] = $this->input->post('params');
            $this->data['content'] = $this->load->view('student/lessons_list_view', $this->data ,TRUE);
            $this->load->view('template', $this->data);
            $this->benchmark->mark('code_end'); // End Benchmarking     
    }
    
    function downloads()
    {
            $this->benchmark->mark('code_start'); // For benchmarking
            $this->data['params'] = $this->input->post('params');
            $this->data['content'] = $this->load->view('student/downloads_view', $this->data ,TRUE);
            $this->load->view('template', $this->data);
            $this->benchmark->mark('code_end'); // End Benchmarking     
    }
    
    function mystudents()
    {
            $this->benchmark->mark('code_start'); // For benchmarking
            $this->data['params'] = $this->input->post('params');
            $this->data['content'] = $this->load->view('teacher/my_student_list_view', $this->data ,TRUE);
            $this->load->view('template', $this->data);
            $this->benchmark->mark('code_end'); // End Benchmarking     
    }
    
    function subjects()
    {
            $this->benchmark->mark('code_start'); // For benchmarking
            $this->data['params'] = $this->input->post('params');
            $this->data['content'] = $this->load->view('teacher/teacher_subject_list_view', $this->data ,TRUE);
            $this->load->view('template', $this->data);
            $this->benchmark->mark('code_end'); // End Benchmarking     
    }
    
    function getTeachers(){
        echo $this->teachmodel->get_teachers();
    } 
    
    function getStudents(){
        echo $this->teachmodel->get_students();
    } 
    
    function getQuizTime(){
        echo $this->teachmodel->get_quiz_time($this->input->post('id'));
    } 
    
    function addTeacher(){
        $this->teachmodel->add_teacher($this->input->post('teacherid'),$this->input->post('firstname'),$this->input->post('lastname'),$this->input->post('middlename'));
    }
    
    function addTime(){
        $this->teachmodel->add_time($this->input->post('hrs'),$this->input->post('min'),$this->input->post('id'));
    }
    
    function updateTeacher(){
        $this->teachmodel->update_teacher($this->input->post('tblinsid'),$this->input->post('instructor_id'),$this->input->post('firstname'),$this->input->post('middlename'),$this->input->post('lastname'));
    }
    
    function confirmInstructor(){
        echo $this->teachmodel->approve_instructor($this->input->post('id'));
    }
    
    function removeTeacher(){
        $this->teachmodel->remove_teachers($this->input->post('id'));
    }
    
    
}

/* End of file teachers.php */
/* Location: ./application/controllers/home.php */