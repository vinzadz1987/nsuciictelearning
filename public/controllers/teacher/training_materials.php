<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Training_materials extends CI_Controller {
	
    var $data = array();

    public function __construct()
    {
                parent::__construct();
                checklogin();
                $this->load->model('teacher/teacher_student_model','am');
    }
    
	public function index()
	{
		$this->benchmark->mark('code_start'); // For benchmarking
        $this->data['content'] = $this->load->view('agent/training_materials_view', $this->data ,TRUE);
        $this->load->view('template', $this->data);
		$this->benchmark->mark('code_end'); // End Benchmarking     
	}
    function getTrainingList(){
        echo $this->am->getTrainingList();
    }
    function getAgentRead(){
        echo $this->am->getAgentRead($this->input->post('_id'));
    }
    function getAgentUnread(){
        echo $this->am->getAgentUnread($this->input->post('_id'));
    }
    function getEveryIdCount(){
        echo $this->am->getEveryIdCount($this->input->post('id'));
    }
    function whoseReadAgent(){
        echo $this->am->whoseReadAgent($this->input->post('_id'),$this->session->userdata('employee_id'));
    }
    function getTrainingMaterialSelect(){
        echo $this->am->get_training_materials_select();
    }
    function removeTrainMaterial(){
        $this->am->removeTrainingList($this->input->post('_id'));
    }
    function savedTrainingmaterials(){
        // echo $this->input->post('data');
        // echo $this->input->post('id');
        echo $this->am->savedtrainingmaterials($this->input->post('data'),$this->input->post('id'));
    }
}
