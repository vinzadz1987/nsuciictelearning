<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Conference extends CI_Controller {

    
    public function __construct()
    {
        parent::__construct();
        checklogin();
        $this->load->model('teacher/teacher_model', 'teachmodel');
    }
    
    public function index()
    {
            $this->benchmark->mark('code_start'); // For benchmarking
            $this->data['params'] = $this->input->post('params');
            $this->data['content'] = $this->load->view('teacher/conference_view', $this->data ,TRUE);
            $this->load->view('template', $this->data);
            $this->benchmark->mark('code_end'); // End Benchmarking   
    }
    
    function getStudents(){
        echo $this->teachmodel->get_students();
    } 
    
    function saveInvite(){
        $this->teachmodel->add_invites($this->input->post('data'),$this->input->post('title'),$this->input->post('image'));
    }
    
    function conferenceList(){
        $this->teachmodel->conference_list();
    }
    
    function studentConfeStat(){
        $this->teachmodel->studentconfe_stat($this->input->post('studid'),$this->input->post('confeID'));
    }
    
    
}

/* End of file mystudents.php */