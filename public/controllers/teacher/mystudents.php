<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mystudents extends CI_Controller {

    
    public function __construct()
    {
        parent::__construct();
        checklogin();
        $this->load->model('teacher/teacher_model', 'teachmodel');
    }
    
    public function index()
    {
            $this->benchmark->mark('code_start'); // For benchmarking
            $this->data['params'] = $this->input->post('params');
            $this->data['content'] = $this->load->view('teacher/my_student_list_view', $this->data ,TRUE);
            $this->load->view('template', $this->data);
            $this->benchmark->mark('code_end'); // End Benchmarking   
    }
    
    function getStudents(){
        echo $this->teachmodel->get_students();
    } 
    
    function enrolledStudent(){
        $this->teachmodel->enrolled_students($this->input->post('studentid'),$this->input->post('subjectid'),$this->input->post('enrollstudent'),$this->input->post('enrollid'));
    }
    
    
}

/* End of file mystudents.php */