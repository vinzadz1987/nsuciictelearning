<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Online_quiz_sheet extends CI_Controller {

var $data = array();
    
    public function __construct()
    {
        parent::__construct();		
        checklogin();
        $this->data = array(
            'page_title' =>  strtoupper(''),
        );
    }
    
	public function index()
	{
		$quiz_id = $this->input->post('quiz_id');
		$emp_id = $this->session->userdata('user_id');
        $access_name = $this->session->userdata('access_name');
		$done_quiz = $this->main_model->row_exist( array('toqr_quiz_id'=>$quiz_id,'toqr_emp_id'=>$emp_id),'tbl_online_quiz_results' );

		if( in_array($access_name, array('Teacher'))  ){ //in_array($emp_id, array('F-11111','1077'))
			if( ! $done_quiz ){
				$this->benchmark->mark('code_start'); // For benchmarking
				$view = ( $quiz_id ) ? 'teacher/online_quiz_sheet_view' : 'settings/online_quiz_view';
				$this->data['quizes'] = $this->creat_quiz($quiz_id);
				$this->data['title'] = $this->input->post('title');
				$this->data['quiz_id'] = $quiz_id;
                $this->data['content'] = $this->load->view($view, $this->data ,TRUE);
                $this->load->view('template', $this->data);
				$this->benchmark->mark('code_end'); // End Benchmarking
			}else{
				$this->data['message'] = 'You already have finished this quiz.';
				$this->data['content'] = $this->load->view('settings/errors/quiz_success_view', $this->data ,TRUE);
				$this->load->view('template', $this->data);
			}
		}else{ //toqr_emp_id
			redirect('teacher/online_quiz_list');
		}
	}

	public function add_quiz(){
                //echo $this->session->userdata('user_id');
		$insert_array = array(
			'toq_name' => $this->input->post('quiz_name'),
			'created_by' => $this->session->userdata('user_id'),
			'updated_by' => $this->session->userdata('user_id'),
			'updated_date' => date('Y-m-d H:i:s')
			);
		if( $this->db->insert( 'tbl_online_quiz',$insert_array ) ){
			echo 'success';
		}else{
			echo 'error';
		}
	}

	public function delete_quiz(){
		$update_array = array(
			'updated_by' => $this->session->userdata('user_id'),
			'updated_date' => date('Y-m-d H:i:s'),
			'toq_status' => 1
			);
		$this->db->update('tbl_online_quiz',$update_array,array('toq_id'=>$this->input->post('id')));
		return true;
	}

	public function calculate_partial(){
		$quiz_answers = $this->input->post();
		$quiz_data = $this->main_model->get_table( 
			'toqq_id,toqq_type,toqq_choices,toqq_answer,toqq_possible_score',
			'tbl_online_quiz_questions',
			'WHERE online_quiz_id = "'.$quiz_answers['quiz_id'].'" AND toqq_status != 1',
			'object'
			);
		$score = 0;
		foreach ($quiz_data as $key => $val) {
			$quiz_answer = $quiz_answers['quiz-'.$val->toqq_id];
			switch ($val->toqq_type) {
				case 'Identification Plus':
					$iPlus_answer = unserialize( $val->toqq_answer );
					foreach ($iPlus_answer as $i_key => $i_val) {
						if( strcasecmp( trim($i_val),trim($quiz_answer[$i_key]) ) == 0 )
							$score++;
					}
				break;
				case 'Identification':
				case 'Multiple Choice':
				case 'True or False':
					if( strcasecmp( trim($val->toqq_answer),trim($quiz_answer) ) == 0 )
						$score++;
					break;
				case 'Enumeration':
					$enu_answer = unserialize( $val->toqq_answer );
					$q_answer = explode('|', $quiz_answer );
					$posible_score = $val->toqq_possible_score; // allow only this posible score.
					foreach ($enu_answer as $e_key => $e_val) {						
						if( in_array_case_insensitive($e_val, $q_answer) && $posible_score != 0 ){
							$score++;
							$posible_score--;
						}						
					}
					break;
				
				default:
					# code...
					break;
			}
		}
		$insert_array = array(
			'toqr_quiz_id' => $quiz_answers['quiz_id'],
			'toqr_emp_id' => $this->session->userdata('user_id'),
			'toqr_partial_score' => $score,
			'toqr_status' => 'Pending'
			);
		$has_essay = $this->main_model->row_exist( array('toqq_type'=>'Essay','online_quiz_id'=>$quiz_answers['quiz_id']),'tbl_online_quiz_questions' );
		if( ! $has_essay ){ // Check if quiz has essay. if no. set quiz to complete and final score.
			$essays = array();
			$insert_array['toqr_final_score'] = $score;
			$insert_array['toqr_status'] = 'Completed';
		}else{
			foreach ($quiz_answers as $essay_key => $essay_val) {
				if( strpos($essay_key,'essay') !== false ){
					$essay_id = explode('-', $essay_key)[1];
					$essays[$essay_id] = $essay_val;
				}

			}
			$insert_array['toqr_essays'] = serialize($essays);
		}

		$done_quiz = $this->main_model->row_exist( array('toqr_quiz_id'=>$quiz_answers['quiz_id'],'toqr_emp_id'=>$this->session->userdata('user_id')),'tbl_online_quiz_results' );
		if( ! $done_quiz ){ // check if user already done this quiz.
			if( $this->db->insert( 'tbl_online_quiz_results',$insert_array ) ){
				$this->data['message'] = '
					Your partial score is <b>'.$score.'</b> You have successfully finish this quiz.
				';
			}
		}else{
			$this->data['message'] = '
				Your partial score is <span class="green"> '.$score.' </span>, you have successfully finish this quiz.
			';
		}

		$this->data['content'] = $this->load->view('settings/errors/quiz_success_view', $this->data ,TRUE);
		$this->load->view('template', $this->data);

	}

	public function creat_quiz($quiz_id){
		$questions = $this->main_model->get_table( 'toqq_id,toqq_question,toqq_type,toqq_choices','tbl_online_quiz_questions','WHERE online_quiz_id = "'.$quiz_id.'" AND toqq_status != 1','object' );
		$res_html = '';
		foreach ($questions as $key => $val) {

			$res_html .= '
				<div class="space"></div>
				<div class="question clearfix">
					<h4>'.($key+1).'. '.$val->toqq_question.'</h4>
					<div class="answer col-xs-6">
						'.$this->get_answer_type($val).'
					</div>
				</div>
			';
		}
		return $res_html;
	}

	public function get_answer_type($data){
		$res_html = '';
		switch ($data->toqq_type) {
			case 'Identification':
				$res_html .= '<input name="quiz-'.$data->toqq_id.'" class="lined-input col-xs-6" type="text" placeholder="Answer..." autocomplete="off">';
				break;
			case 'Identification Plus':
				$choices = unserialize($data->toqq_choices);
				foreach ($choices as $key => $value) {
					$res_html .= '
						<div class="margin-bottom-m">
							<label>'.$value.'</label>
							<input name="quiz-'.$data->toqq_id.'['.$key.']" class="lined-input margin-left-m" type="text" placeholder="Answer here">
						</div>
					';
				}
				break;
			case 'Enumeration':
				$res_html .= '<input name="quiz-'.$data->toqq_id.'" class="enumeration" type="text" placeholder="Enter to separate answer" />';
				break;
			case 'Multiple Choice':
				$choices = unserialize($data->toqq_choices);
				foreach ($choices as $key => $value) {
					$res_html .= '
						<div>
							<label>
								<input class="ace" type="radio" name="quiz-'.$data->toqq_id.'" value="'.$value.'">
								<span class="lbl lighter smaller-90"> '.$value.'</span>
							</label>
						</div>
					';
				}
				break;
			case 'True or False':
				$res_html = '
					<label>
						<input class="ace" type="radio" name="quiz-'.$data->toqq_id.'" value="True">
						<span class="lbl lighter smaller-90"> True</span>
					</label>
					<label class="margin-left-xl">
						<input class="ace" type="radio" name="quiz-'.$data->toqq_id.'" value="False">
						<span class="lbl lighter smaller-90"> False</span>
					</label>
					';
				break;

			case 'Essay':
				$res_html .= '<textarea name="essay-'.$data->toqq_id.'" class="col-xs-6" placeholder="Answer here.." style="resize:none"></textarea>';
				break;
		}
		return $res_html;
	}

}
