<?php if ( !defined('BASEPATH')) exit('No direct script access allowed');

class Main_ctrl extends CI_Controller {

	function __construct()
    {
        parent::__construct();
//        $this->load->model('highChart_model','hm');
//        $this->load->model('highStock_model','hsm');
    }

    public function ajaxMake_select(){
        $ops = $this->input->post('ops');
        $selected = $this->input->post('selected');
        $type = $this->input->post('type');
        $data = $this->input->post('data');
        echo $this->main_model->dropDown($ops,$selected,$type,$data);
    }

    public function setHighChart(){
        echo json_encode($this->hm->getHighChart($_POST));
    }

    public function setHighStock(){
    	echo json_encode($this->hsm->getHighStock($_POST));
    }

    public function jqGrid_caption(){
        echo json_encode($this->main_model->search_caption($_POST));
    }

    public function get_dropDown_local(){
        echo json_encode($this->main_model->dropDown_local($_POST));
    }
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */