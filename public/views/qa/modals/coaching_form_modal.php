<?php
//print_r($result['resData']);
?>
<div class="page-header" align="center">
	<h4>Coaching Forms</h4>
	<h4><i>Account Name</i> : <?php echo $result['resData']['account'];?></h4>
</div>
<div class="row">
	<div class="col-sm-6">
			<form role="form" class="form-horizontal">
				<div class="form-group">
					<label for="form-field-1" class="col-sm-3 control-label no-padding-right"> Agent Name: </label>

					<div class="col-sm-9">
						<input type="text" class="col-xs-10 col-sm-9" value="<?php echo $result['resData']['agentname']?>" readonly>
					</div>
				</div>
				<div class="form-group">
					<label for="form-field-1" class="col-sm-3 control-label no-padding-right"> Supervisor Name: </label>

					<div class="col-sm-9">
						<input type="text" class="col-xs-10 col-sm-9" value="<?php echo $result['resData']['supervisor']?>" readonly>
					</div>
				</div>
				<div class="form-group">
					<label for="form-field-1" class="col-sm-3 control-label no-padding-right"> CSAT score (if applicable): </label>

					<div class="col-sm-9">
						<input type="text" class="col-xs-10 col-sm-9" value="<?php echo $result['resData']['csat_score']?>" readonly>
					</div>
				</div>
				<div class="form-group">
					<label for="form-field-1" class="col-sm-3 control-label no-padding-right"> Overall Summary of Chat Interaction: </label>

					<div class="col-sm-9">
						<textarea class="form-control col-xs-10 col-sm-9"readonly><?php echo $result['resData']['overall_summary']?></textarea>
					</div>
				</div>
				<div class="form-group">
					<label for="form-field-1" class="col-sm-3 control-label no-padding-right"> Areas of Success: </label>

					<div class="col-sm-9">
						<textarea class="form-control col-xs-10 col-sm-9"readonly><?php echo $result['resData']['area_of_success']?></textarea>
					</div>
				</div>
			</form>
	</div>

	<div class="col-sm-6">
		<form class="form-horizontal">
			<div class="form-group">
				<label for="form-field-1" class=" inline col-sm-3 control-label no-padding-right"> Coaching Date: </label>

				<div class="col-sm-9">
					<input type="text" class="col-xs-10 col-sm-9" value="<?php echo $result['resData']['coaching_date']?>" readonly>
				</div>
			</div>
			<div class="form-group">
				<label for="form-field-1" class=" inline col-sm-3 control-label no-padding-right"> Coaching Type: </label>

				<div class="col-sm-9">
					<input type="text" class="col-xs-10 col-sm-9" value="<?php echo $result['resData']['coaching_type']?>" readonly>
				</div>
			</div>
			<div class="form-group">
				<label for="form-field-1" class=" inline col-sm-3 control-label no-padding-right"> External score (if applicable): </label>

				<div class="col-sm-9">
					<input type="text" class="col-xs-10 col-sm-9" value="<?php echo $result['resData']['ext_score']?>" readonly>
				</div>
			</div>
			<div class="form-group">
				<label for="form-field-1" class=" inline col-sm-3 control-label no-padding-right"> Chat ID: </label>

				<div class="col-sm-9">
					<textarea class="form-control col-xs-10 col-sm-9"readonly><?php echo $result['resData']['chat_id']?></textarea>
				</div>
			</div>
			<div class="form-group">
				<label for="form-field-1" class=" inline col-sm-3 control-label no-padding-right"> Focus Areas of Development: </label>

				<div class="col-sm-9">
					<textarea class="form-control col-xs-10 col-sm-9"readonly><?php echo $result['resData']['overall_summary']?></textarea>
				</div>
			</div>
		</form>
	</div>
</div>
<div class="space-6"></div>
<div align="center">
Agent's Action Plan and Commitment
<div class="space-2"></div>
<?php echo $result['resData']['agent_action_plan']?>
<div class="space-6"></div>
Agent’s Signature: _____________________ Supervisor’s Signature:_______________________
</div>
<div class="space-6"></div>