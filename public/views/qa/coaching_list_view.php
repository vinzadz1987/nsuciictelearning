<div class="col-sm-12">
    <!--h3 class="row header smaller lighter green">
        <span class="col-sm-8">
            Online Quiz
            <span class="comment-count">2</span>
        </span>
        <span class="col-sm-4">
            <button id="add_question" class="btn btn-sm btn-success pull-right">
                <i class="ace-icon fa fa-plus bigger-110"></i>
                Add New
            </button>
        </span>
    </h3-->
    <div class="page-header">
        <h1>
            QA List
            <small>
                <i class="ace-icon fa fa-angle-double-right"></i>
                Coaching List
            </small>
        </h1>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div id="grid-name" class="grid-wrap col-xs-12 compress head-primary">
                    <table id="grid-table"></table>
                    <div id="grid_pager"></div>
                </div>
            </div>
        </div><!-- /.col -->
    </div><!-- /.row -->
</div>

<!-- page specific plugin scripts -->
<script src="assets/js/jqGrid/jquery.jqGrid.preset.js"></script>
<script src="assets/js/jqGrid/jquery.jqGrid.min.js"></script>
<script src="assets/js/jqGrid/i18n/grid.locale-en.js"></script>
<!-- chosen plugin -->
<script src="assets/js/chosen.jquery.min.js"></script>

<script type="text/javascript">
    jQuery(function($) {

        

        $('#grid-table').wd_jqGrid({
            //url: 'settings/jqGrid_settings_ctrl/load_data',
            url: 'qa/jqGrid_coaching_ctrl/load_data',
            //module: 'online_quiz',
            module: 'coaching_list_view',
            //module_data: {ops:'l_name',status:0},
            //colNames: ['ID','Quiz Name','Author','<span class="badge badge-inverse">&nbsp;&nbsp;</span>','Date','Actions'],
            colNames: ['Actions','ID','Account','Agent Name','Supname','Coaching Date','Coaching Type','Csat Score','Ext Score','Overall Summry','Chat Id','Area of Success','Area of Development','Agent Action Plan'],
            colModel: [ {name:'actions',index:'actions', width:80, fixed:true, sortable:false, resize:false, align: 'center',
                            formatter:'actions', 
                            formatoptions:{ 
                                keys:true,
                                //delbutton: false,//disable delete button
                                delOptions:{recreateForm: true, beforeShowForm:beforeDeleteCallback},
                                editbutton:false
                            }
                        },
                        {name:'coaching_id',index:'coaching_id', width:0, sorttype:"int",hidden:true},
                        {name:'account',index:'account',width:90, editable:true, cellattr: function() { return 'style="white-space: normal;"' }},
                        {name:'agentname',index:'agentname', width:90, editable: true, align: 'center'},
                        {name:'supervisor',index:'supervisor', width:90, editable: true, align: 'center'},
                        {name:'coaching_date',index:'coaching_date', width:80, editable: true, align: 'center'},
                        {name:'coaching_type',index:'coaching_type', width:80, editable: true, align: 'center'},
                        {name:'csat_score',index:'csat_score', width:80, editable: true, align: 'center'},
                        {name:'ext_score',index:'ext_score', width:80, editable: true, align: 'center'},
                        {name:'overall_summary',index:'overall_summary', width:90, editable: true, align: 'center'},
                        {name:'chat_id',index:'chat_id', width:80, editable: true, align: 'center'},
                        {name:'area_of_success',index:'area_of_success', width:90, editable: true, align: 'center'},
                        {name:'area_of_development',index:'area_of_development', width:90, editable: true, align: 'center'},
                        {name:'agent_action_plan',index:'agent_action_plan', width:90, editable: true, align: 'center'},
                    ],
            sortname: 'coaching_id',
            //editurl: 'settings/online_quiz/delete_quiz',
            editurl: 'qa/coaching_forms_list/delete_form_list',
            caption: false,
            height: 400,
            altRows: true,
            ondblClickRow: function(rowid,iRow,iCol,e){
                var rowData = $(this).jqGrid('getRowData',rowid);
                //$.redirect('settings/online_quiz_questions', {quiz_id:rowData.toq_id, title:rowData.toq_name});
                $.redirect('settings/online_quiz_questions1', {quiz_id:rowData.toq_id, title:rowData.toq_name});
            },
            loadComplete: function(data){
                var grid = $(this);

                // add custom button in action field
                
                setTimeout(function(){
                    styleCheckbox(grid);
                    
                    updateActionIcons(grid);
                    updatePagerIcons(grid);
                    enableTooltips(grid);
                  }, 0);
                var iCol = $.getColumnIndexByName(grid,'actions');
                grid.find(">tbody>tr.jqgrow>td:nth-child(" + (iCol + 1) + ")")
                  .each(function() {
                      $("<div>", {
                          title: "View Form",
                          mouseover: function() {
                              $(this).addClass('ui-state-hover');
                          },
                          mouseout: function() {
                              $(this).removeClass('ui-state-hover');
                          },
                          click: function(e) {
                            var rowid = $(e.target).closest("tr.jqgrow").attr("id");
                            var rowData = grid.jqGrid('getRowData',rowid);
                            //$.redirect('agent/online_quiz_sheet', {quiz_id:rowData.toq_id, title:rowData.toq_name});
                             $('.ajaxModal').modalBox({
                                        modal_view:'coaching_form_modal',
                                        modal_data:{resData:rowData},
                                        title: false,
                                        width: 1200,
                                        button_cancel: 'Cancel',
                                        button_ok: 'Print',
                             });
                             $(".btn_ok_mdl").click(function(){
                                    window.print();
                            });
                          }
                      }
                    ).css({"margin-left": "5px", float: "left", cursor: "pointer"})
                     .addClass("ui-pg-div ui-inline-custom")
                     .append('<span class="ui-icon ui-icon-share green"></span>')
                     .appendTo($(this).children("div"));
                });
            }
        });
    });

    // $('#add_question').click(function(){
    //     $('.ajaxModal').modalBox({
    //         modal_view:'add_quiz_modal',
    //         title: false,
    //         width: 400,
    //         button_cancel: 'Cancel',
    //         button_ok: 'save',
    //     });
    // })

</script>