<script src="assets/js/coaching_forms.js"></script>
<div class="page-header">
	<h1>
		QA Forms
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			Coaching Forms
		</small>
	</h1>
</div>
<div class="row">
	<div class="col-xs-12">
		<!-- PAGE CONTENT BEGINS -->
		<form role="form" class="form-horizontal">
			<!-- #section:elements.form -->
			<div class="form-group">
				<label for="form-field-1" class="col-sm-3 control-label no-padding-right"> Account Name: </label>

				<div class="col-sm-9">
					<input type="text" class="col-xs-10 col-sm-5" placeholder="Account" id="account" name="account">
				</div>
			</div>
			
			<div class="form-group">
				<label for="form-field-1" class="col-sm-3 control-label no-padding-right"> Agent Name: </label>

				<div class="col-sm-9">
					<select multiple="multiple" class="col-xs-10 col-sm-5" id="agent_id" name="agent_id">
						<?php foreach ($agentfullname as $item):?>
						<option value="<?php echo $item['agent_id'];?>"><?php echo $item['agentfullname'];?></option>
						<?php endforeach;?>
					</select>
				</div>
			</div>

			<div class="form-group">
				<label for="form-field-1" class="col-sm-3 control-label no-padding-right"> Coaching Date: </label>

				<div class="col-sm-9">
					<input type="text" placeholder="yyyy-mm-dd" data-date-format="yyyy-mm-dd" id="coaching_date" name="coaching_date" class="date-picker col-xs-10 col-sm-5">
						<button type="button" class="btn btn-sm btn-default" disabled>
							<i class="ace-icon fa fa-calendar bigger-110"></i>
						</button>
				</div>
				
			</div>

			<div class="form-group">
				<label for="form-field-1" class="col-sm-3 control-label no-padding-right"> Supervisor Name: </label>

				<div class="col-sm-9">
					<select multiple="multiple" class="col-xs-10 col-sm-5" id="sup_id" name="sup_id">
						<?php foreach ($supfullname as $item):?>
						<option value="<?php echo $item['supervisor_id'];?>"><?php echo $item['supfullname'];?></option>
						<?php endforeach;?>
					</select>
				</div>
			</div>
			
			<div class="form-group">
				<label for="form-field-1" class="col-sm-3 control-label no-padding-right"> Coaching Type: </label>

				<div class="col-sm-9">
					<select multiple="multiple" class="col-xs-10 col-sm-5" id="coaching_type" name="coaching_type">
						<option value="CT">Connecticut</option>
						<option value="DE">Delaware</option>
						<option value="FL">Florida</option>
						<option value="GA">Georgia</option>
						<option value="HI">Hawaii</option>
						<option value="ID">Idaho</option>
					</select>
				</div>
			</div>

			<div class="form-group">
				<label for="form-field-1" class="col-sm-3 control-label no-padding-right"> CSAT Score (if applicable): </label>

				<div class="col-sm-9">
					<input type="text" class="col-xs-10 col-sm-5" placeholder="CSAT score" id="csat_score" name="csat_score">
				</div>
			</div>

			<div class="form-group">
				<label for="form-field-1" class="col-sm-3 control-label no-padding-right"> External Score (if applicable): </label>

				<div class="col-sm-9">
					<input type="text" class="col-xs-10 col-sm-5" placeholder="External score" id="ext_score" name="ext_score">
				</div>
			</div>

			<div class="form-group">
				<label for="form-field-1" class="col-sm-3 control-label no-padding-right"> Overall Summary of Chat Interaction: </label>

				<div class="col-sm-9">
					<textarea class="col-xs-10 col-sm-5" placeholder="Overall summary of chat interaction" id="overall_summary" name="overall_summary"></textarea>
				</div>
			</div>

			<div class="form-group">
				<label for="form-field-1" class="col-sm-3 control-label no-padding-right"> Chat ID: </label>

				<div class="col-sm-9">
					<select multiple="multiple" class="col-xs-10 col-sm-5" id="chat_id" name="chat_id">
						<option value="IL">Illinois</option>
						<option value="IN">Indiana</option>
						<option value="IA">Iowa</option>
						<option value="KS">Kansas</option>
						<option value="KY">Kentucky</option>
						<option value="LA">Louisiana</option>
					</select>
				</div>
			</div>

			<div class="form-group">
				<label for="form-field-1" class="col-sm-3 control-label no-padding-right"> Areas of Success: </label>

				<div class="col-sm-9">
					<textarea class="col-xs-10 col-sm-5" placeholder="Areas of success" id="area_of_success" name="area_of_success"></textarea>
				</div>
			</div>

			<div class="form-group">
				<label for="form-field-1" class="col-sm-3 control-label no-padding-right"> Focus Areas of Development: </label>

				<div class="col-sm-9">
					<textarea class="col-xs-10 col-sm-5" placeholder="Focus areas of development" id="area_of_development" name="area_of_development"></textarea>
				</div>
			</div>

			<div class="form-group">
				<label for="form-field-1" class="col-sm-3 control-label no-padding-right"> Agent's Action Plan and Commitment: </label>

				<div class="col-sm-9">
					<textarea class="col-xs-10 col-sm-5" placeholder="Agent's action plan and commitment" id="agent_action_plan" name="agent_action_plan"></textarea>
				</div>
			</div>

			<div class="space-4"></div>
			<div class="clearfix form-actions">
				<div class="col-md-offset-3 col-md-9">
					<button type="button" class="btn btn-info" id="addformsubmit">
						<i class="ace-icon fa fa-check bigger-110"></i>
						Submit
					</button>

					&nbsp; &nbsp; &nbsp;
					<button type="reset" class="btn">
						<i class="ace-icon fa fa-undo bigger-110"></i>
						Reset
					</button>
				</div>
			</div>
		</form>
	</div><!-- /.col -->
</div>
<script>
$(document).ready(function(){
	$.getJSON("qa/coaching_forms/getAgent", function( data ) {
        var items = '';
        var items2 = '';
        $.each( data, function( key, val ) {
			switch(val.position){
				case 'Agent':
					items += "<option value="+val.att_uid+">"+val.first_name+" "+val.last_name+"</option>";

				break;
				case 'Supervisor':
					items2 += "<option value="+val.att_uid+">"+val.first_name+" "+val.last_name+"</option>";
				break;
			}                        	
			$("#agent_id").html(items);
			$("#sup_id").html(items2);
        });
    });

}); 
</script>
