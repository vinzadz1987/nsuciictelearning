<div class="col-sm-12">
	<div class="row">
		<div class="col-xs-12">
                    <table>
                        <tr>
                            <td><input id="uploadFile" placeholder="Chosen Filename" disabled="disabled" class="form-control"/></td>
                            <td>
                                <div class="fileUpload btn btn-primary">
                                    <span>Choose File</span>
                                    <input id="uploadBtn" type="file" class="upload" />
                                </div>
                            </td>
                            <td>
                                <button id="uploadExcel" type="button" class="btn btn-success">Upload</button>
                            </td>
                        </tr>
                    </table>
				<div class="widget-box transparent">
					<div class="widget-box">
						<div class="widget-header">
							<h4 class="widget-title lighter smaller">
								<i class="ace-icon fa fa-cloud-upload blue"></i>
								NPS CSV Format
							</h4>
						</div>
						<div class="widget-body">
							<div class="widget-main no-padding">
								<table class="table table-bordered table-striped">
									<thead class="thin-border-bottom">
									<tr>
										<th>ChatID</th>
										<th>agentID</th>
										<th>Submitdate</th>
										<th>recommend_rank_nps1</th>
										<th>recommend_rank_nps1</th>
									</tr>
									</thead>
									<tbody>
									<tr>
										<td>xxx</td><td>xxx</td><td>xxx</td><td>xxx</td><td>xxx</td>
									</tr>

									</tbody>
								</table>
							</div><!-- /.widget-main -->
						</div><!-- /.widget-body -->
					</div>
				</div><!-- /.widget-box -->
			</div><!-- /.col -->
<!--			<div id="grid-name" class="grid-wrap col-xs-12 compress head-primary">-->
<!--				<table id="grid-table"></table>-->
<!--				<div id="grid_pager"></div>-->
<!--			</div>-->
		</div>
	</div><!-- /.col -->

	<br><br><br>
	<br><br><br>

</div><!-- /.row -->
</div>

<script src="assets/js/chosen.jquery.min.js"></script>
<script src="assets/js/jPages.js"></script>
<script src="assets/js/jquery.slimscroll.min.js"></script>


<script src="assets/js/jqGrid/jquery.jqGrid.preset.js"></script>
<script src="assets/js/jqGrid/jquery.jqGrid.min.js"></script>
<script src="assets/js/jqGrid/i18n/grid.locale-en.js"></script>
<script src="assets/js/bootbox.min.js"></script>
<script src="assets/js/ace-elements.min.js"></script>
<script src="assets/js/ace.min.js"></script>
<script src="assets/js/nps.js"></script>