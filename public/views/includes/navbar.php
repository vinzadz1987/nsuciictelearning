<script type="text/javascript">
	try{ace.settings.check('navbar' , 'fixed')}catch(e){}
</script>
<div class="navbar-container" id="navbar-container container">
	<!-- #section:basics/sidebar.mobile.toggle -->
	<button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler">
		<span class="sr-only">Toggle sidebar</span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
	</button>
	<!-- /section:basics/sidebar.mobile.toggle -->
	<div class="navbar-header pull-left">
		<!-- #section:basics/navbar.layout.brand -->
		<a href="<?php print $this->session->userdata('default_controller')?>" class="navbar-brand">
			<small>
			<i class="fa fa-leaf"></i>
			<?php print ucwords($this->session->userdata('access_name')).' Dashboard'; ?>
			</small>
		</a>
		<!-- /section:basics/navbar.toggle -->
	</div>
	<!-- #section:basics/navbar.dropdown -->
	<div class="navbar-buttons navbar-header pull-right" role="navigation">
		<ul class="nav ace-nav">
			<!-- #section:basics/navbar.user_menu -->
			<li class="light-blue">
				<a data-toggle="dropdown" href="#" class="dropdown-toggle">
					<img id="nav-user-photo" class="nav-user-photo" alt="Photo" src="assets/avatars/small_loader.gif"  style="margin: -4px 8px 0 0; max-width: 30px; border-radius: 0%;border:0px solid #FFF;" /> 
					<span class="user-info">
						<small>Welcome</small>
						<?php print $this->session->userdata('firstname'); ?>
					</span>
					<i class="ace-icon fa fa-caret-down"></i>
				</a>

				<ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
					<li><a href="javascript:void(0)"><i class="ace-icon fa fa-cog"></i>Settings</a></li>
					<li><a href="javascript:void(0)"><i class="ace-icon fa fa-user"></i>Profile</a></li>
					<li class="divider"></li>
					<li><a href="login/doLogout"><i class="ace-icon fa fa-power-off"></i>Logout</a></li>
				</ul>
			</li>
			<!-- /section:basics/navbar.user_menu -->
		</ul>
	</div>
	<!-- /section:basics/navbar.dropdown -->
</div><!-- /.navbar-container -->