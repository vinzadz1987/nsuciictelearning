<?php if(!empty($advance_search['settings'])){ ?>
<div class="ace-settings-container" id="ace-settings-container">
	<div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
		<i class="ace-icon fa fa-search bigger-150"></i>
	</div>
	<div class="ace-settings-box clearfix" id="ace-settings-box">
		<div class="row">
			<div class="col-xs-13">			
				<div class="col-sm-13">
					<div class="tabbable">
						<ul id="myTab4" class="nav nav-tabs padding-12 tab-color-blue background-blue">
							<li class="active">								
								<a href="#search" data-toggle="tab">
									<i class="green ace-icon fa fa-search bigger-120"></i>
									Search
								</a>
							</li>
							<?php if(!empty($hide_show_column['settings'])){ ?>
							<li class="">
								<a href="#hide_show_columns" data-toggle="tab">
									<i class="green ace-icon fa fa-cog bigger-120"></i>
									Show/Hide Columns
								</a>
							</li>
							<?php } ?>
						</ul>

						<div class="tab-content no_border" data-table-name=''>
							<!-- SEARCH TAB CONTENT -->
							<div class="tab-pane active" id="search">
								<!-- <h3 class="header smaller lighter blue">Search</h3> -->
								<form class="form-horizontal" role="form">

									<!-- Search -->
									<?php 
										if(in_array('search', $advance_search['display'])){
									?>
									<div class="form-group">
										<label class="col-sm-4 control-label" for="grid_search"> Search </label>
										<div class="col-sm-8">
											<input type="text" id="grid_search" placeholder="Name" class="col-xs-8 col-sm-12" />
										</div>
									</div>
									<?php } ?>

									<?php 
										if(in_array('date-range', $advance_search['display'])){
									?>
									<!-- Date-range -->
									<div class="form-group">
										<label class="col-sm-4 control-label" for="grid_byDate"> By Date </label>
										<div class="col-sm-8">
											<input class="form-control date_range-picker" type="text" name="date-range-picker" id="grid_byDate" />
										</div>
									</div>
									<?php } ?>

									<?php 
										if(in_array('date', $advance_search['display'])){
									?>
									<!-- Date -->
									<div class="form-group">
										<label class="col-sm-4 control-label" for="grid_byDate_only"> By Date </label>
										<div class="col-sm-8">
											<input class="form-control date-picker" type="text" name="date-only" id="grid_byDate_only" />
										</div>
									</div>
									<?php } ?>
									
									<?php 
										if(in_array('field', $advance_search['display'])){
									?>
									<!-- field -->
									<div class="form-group">
										<label class="col-sm-4 control-label" for="grid_advance"> Field </label>
										<div class="col-sm-8">
											<select class="form-control" id="grid_advance">
												<option value=""></option>
												<option value="ID_#">ID number</option>
												<option value="Last_Name">Last Name</option>
												<option value="First_Name">First Name</option>
											</select>
										</div>
									</div>
									<?php } ?>
					
									<?php if(in_array('search', $advance_search['buttons'])){?>
									<button id="btn_search" class="btn btn-warning margin-left-m <?php print (!empty($advance_search['auto_close']) && $advance_search['auto_close'] == true) ? 'btn_auto_close' : '';?> pull-right">Search</button>
									<?php } ?>
									<?php if(in_array('advance', $advance_search['buttons'])){?>
									<button id="btn_advance" class="btn btn-warning margin-left-m pull-right <?php print (!empty($advance_search['auto_close']) && $advance_search['auto_close'] == true) ? 'btn_auto_close' : '';?>">Advance</button>
									<?php } ?>
								</form>
							</div>
							
							
							<?php if(!empty($hide_show_column['settings'])){ ?>
							<!-- HIDE/SHOW COLUMNS TAB CONTENT -->
							<div class="tab-pane" id="hide_show_columns">				
								<div class="row margin_left_10 hide_show_columns_content">Loading...																					
								</div>						
							</div>
							<?php } ?>
							
							
						</div>
					</div>
				</div>
				<div class="space-6"></div>
			</div>
		</div>
		<div class="space-6"></div>
	</div><!-- /.ace-settings-box -->
</div><!-- /.ace-settings-container -->
<?php } ?>

<script src="assets/js/jqGrid/i18n/grid.locale-en.js"></script>
<script>
jQuery(function(){	
	//datepicker plugin
	//link
	$('.date-picker').datepicker({
		autoclose: true,
		todayHighlight: true
	})
	//to translate the daterange picker, please copy the "examples/daterange-fr.js" contents here before initialization
	$('.date_range-picker').daterangepicker({
		'applyClass' : 'btn-sm btn-success',
		'cancelClass' : 'btn-sm btn-default',
		locale: {
			applyLabel: 'Apply',
			cancelLabel: 'Cancel',
		}
	})

	$('.btn_auto_close').on('click',function(){
		advanceSearch('close');
	});

	$('#btn_advance').on('click',function(e){
		e.preventDefault();
		dialogBox(
			model = {
				type: 'url',
				data: 'generic/modal_ctrl/modal/?location=modals/advance_search_modal&date='+<?php echo json_encode((isset($advance_search['date'])) ? $advance_search['date'] : '') ?>,
				title: 'Advance Search',
				title_icon: 'fa-search',
				width: 600,
				height: 600,
				buttons: [
					{
						'html': "<i class='ace-icon fa fa-times'></i> Cancel",
						'class': 'btn_cancel btn btn-xs',
						click: function(){
							$(this).dialog('close');
						}
					},
					{
						'html': "<i class='ace-icon fa fa-check'></i> Search",
						'class': 'btn_submit btn btn-primary btn-xs',
						click: function(){
							//$(this).dialog('close');
						}
					}
				]
			}
		)
	});


		
});		
</script>