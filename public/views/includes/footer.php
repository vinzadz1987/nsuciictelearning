﻿
<!-- #section:basics/footer -->
<div class="footer-content">
	<span class="">
		<span class="blue bolder">CIICT E-Learning v 1.0 </span>
		| &copy;Copyright 2014<?php (date('Y') == '2014') ? "" : print "-".date('Y')?> 
	</span>

	&nbsp; &nbsp;
	<span class="action-buttons">
	<!--	<a href="#">
			<i class="ace-icon fa fa-twitter-square light-blue bigger-150"></i>
		</a>

		<a href="#">
			<i class="ace-icon fa fa-facebook-square text-primary bigger-150"></i>
		</a>

	•	<a href="#">
			<i class="ace-icon fa fa-rss-square orange bigger-150"></i>
		</a> -->
	</span>
	<span class="blue bolder">Designed and Developed by</span> <a href='#' class="orange bold">CIICT 4rth Year Student - NSU </a>
</div>

<!-- /section:basics/footer -->