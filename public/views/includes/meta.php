	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta charset="utf-8" />
	<title><?php print $this->config->item('site_url').' | '. $page_title; ?></title>

	<meta name="description" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

	<!-- bootstrap & fontawesome -->
	<link rel="stylesheet" href="assets/css/bootstrap.min.css" />
	<link rel="stylesheet" href="assets/css/font-awesome46/css/font-awesome.css">

	<!-- page specific plugin styles -->
	<link rel="stylesheet" href="assets/css/fullcalendar.css" />
	<link rel="stylesheet" href="assets/css/jquery-ui.min.css" />
	<link rel="stylesheet" href="assets/css/datepicker.css" />
	<link rel="stylesheet" href="assets/css/daterangepicker.css" />
	<link rel="stylesheet" href="assets/css/ui.jqgrid.css" />
	<link rel="stylesheet" href="assets/css/select2.css" />
	<link rel="stylesheet" href="assets/css/chosen.css" />

	<link href="assets/nprogress/nprogress.css" rel="stylesheet" />

	<!-- text fonts -->
	<link rel="stylesheet" href="assets/css/ace-fonts.css" />

	<!-- ace styles -->
	<link rel="stylesheet" href="assets/css/ace.min.css" />
	<link rel="stylesheet" href="assets/css/main.css" />

	<link rel="stylesheet" href="assets/css/ace-skins.min.css" />
	<link rel="stylesheet" href="assets/css/ace-rtl.min.css" />



	<!--[if lte IE 9]>
	<link rel="stylesheet" href="assets/css/ace-ie.min.css" />
	<![endif]-->
	<!-- ace settings handler -->
	<script src="assets/js/ace-extra.min.js"></script>


	<!-- basic scripts -->		
	<!--[if !IE]> -->
	<script type="text/javascript">
	window.jQuery || document.write("<script src='assets/js/jquery.min.js'>"+"<"+"/script>");
	</script>
	<!-- <![endif]-->

	<!-- Custom functions -->

	<script src="assets/js/main.js"></script>

	<script type="text/javascript">
	if('ontouchstart' in document.documentElement) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
	</script>
	<script src="assets/js/bootstrap.min.js"></script>	

	<script src="assets/js/jquery-ui.min.js"></script>
	<script src="assets/js/jquery.ui.touch-punch.min.js"></script>

	<link rel="stylesheet" href="assets/css/datepicker.css" />		
	<link rel="stylesheet" href="assets/css/daterangepicker.css" />

	<script src="assets/js/date-time/bootstrap-datepicker.min.js"></script>
	<script src="assets/js/date-time/bootstrap-timepicker.min.js"></script>
	<script src="assets/js/date-time/moment.min.js"></script>
	<script src="assets/js/date-time/daterangepicker.min.js"></script>
	<script src="assets/js/date-time/bootstrap-datetimepicker.min.js"></script>

	<script src="assets/js/jquery-idle-timeout-master/jquery.idletimeout.js"></script>	
	<script src="assets/js/jquery-idle-timeout-master/jquery.idletimer.js"></script>	
	<script src="assets/js/battatech_excelexport-master/Scripts/jquery.battatech.excelexport.js"></script>	
	<script src="assets/js/battatech_excelexport-master/Scripts/base64.js"></script>	
	<script src="assets/js/jquery.redirect.js"></script>	
	<script src="assets/js/bootbox.min.js"></script>

	<script type="text/javascript" src="assets/js/timersetter/js/jquery.timesetter.js"></script>

	<script>
		$(document).ready(function(){
		//NProgress.done(true);
		});
	</script>