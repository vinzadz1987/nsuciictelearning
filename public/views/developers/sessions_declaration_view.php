<link rel="stylesheet" href="assets/css/jquery-ui.custom.min.css" />
		<link rel="stylesheet" href="assets/css/jquery.gritter.css" />
<div class="row">
	<div class="col-xs-12">		
		<div class="row">
			<div class="col-xs-12">
				<div id="recent-box" class="widget-box transparent">
					<div class="widget-header">
						<h4 class="widget-title lighter smaller">
							<i class="ace-icon fa fa-rss orange"></i>Developers - Development References
						</h4>
					</div>
					<!-- #section:elements.tab -->
					
					<!-- /section:elements.tab -->								
				</div>
			</div>			
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="row">
					<div class="tabbable">
						<ul id="myTab" class="nav nav-tabs">
							<li class="active">
								<a href="#session_tab" data-toggle="tab" class='rememberTab'>
									<i class="green ace-icon fa fa-clock-o bigger-120"></i>
									Sessions
								</a>
							</li>
							<li>
								<a href="#declaration_tab" data-toggle="tab" class='rememberTab'>
									<i class="green ace-icon fa fa-book  bigger-120"></i>
									Declaration
								</a>
							</li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="session_tab">
								<div class="row">
									<div class="col-xs-12">
										<ul class="list-unstyled spaced2">
											<li>
												<i class="ace-icon fa fa-circle green"></i>
												Sessions used in this project; <?php //echo $this->session->userdata('default_controller');?>
											</li>															
											<li>
												<ul class="list-unstyled">
													<li>
														<i class="ace-icon fa fa-angle-right blue"></i>
														<span class='orange'>ip_address</span> - IP address of the current client PC.
													</li>
													<li>
														<i class="ace-icon fa fa-angle-right blue"></i>
														<span class='orange'>employee_id</span> - ID of the current user.
													</li>
													<li>
														<i class="ace-icon fa fa-angle-right blue"></i>
														<span class='orange'>firstname</span> - First name of the current user.
													</li>
													<li>
														<i class="ace-icon fa fa-angle-right blue"></i>
														<span class='orange'>middlename</span> - Middle name of the current user.
													</li>
													<li>
														<i class="ace-icon fa fa-angle-right blue"></i>
														<span class='orange'>lastname</span> - Last name of the current user.
													</li>
													<li>
														<i class="ace-icon fa fa-angle-right blue"></i>
														<span class='orange'>last_login</span> - Previous date when current user logged in.
													</li>
													<li>
														<i class="ace-icon fa fa-angle-right blue"></i>
														<span class='orange'>default_controller</span> - Controller name of the current page.
													</li>
													<li>
														<i class="ace-icon fa fa-angle-right blue"></i>
														<span class='orange'>logged_in</span> - Flag if user is still logged in.
													</li>
													<li>
														<i class="ace-icon fa fa-angle-right blue"></i>
														<span class='orange'>center_id</span> - center ID where the current user belong to.
													</li>
													<li>
														<i class="ace-icon fa fa-angle-right blue"></i>
														<span class='orange'>menu_sets</span> - set of menu IDs which the current user is allowed to access.
													</li>
													<li>
														<i class="ace-icon fa fa-angle-right blue"></i>
														<span class='orange'>campaign_id</span> - ID of the selected campaign buttton located on the right-top of the page.
													</li>
													<li>
														<i class="ace-icon fa fa-angle-right blue"></i>
														<span class='orange'>campaign_name</span> - Name(of string type) of the selected campaign buttton located on the right-top of the page.
													</li>
													<li>
														<i class="ace-icon fa fa-angle-right blue"></i>
														<span class='orange'>business_id</span> - ID of the selected business buttton.
													</li>
													<li>
														<i class="ace-icon fa fa-angle-right blue"></i>
														<span class='orange'>business_name</span> -  Name of the selected business buttton..
													</li>
												</ul>
											</li>				
										</ul>
									</div>
								</div>
								
								<!-- Functions and Line of Code -->
								<div class="row">
									<div class="col-xs-12">
										<ul class="list-unstyled spaced2">
											<li>
												<i class="ace-icon fa fa-circle green"></i>
												Functions and Line/s of Code; <?php //echo $this->session->userdata('default_controller');?>
											</li>															
											<li>
												<ul class="list-unstyled">
													<li>
														<i class="ace-icon fa fa-angle-right blue"></i>
														<span class='orange'>jQuery function to show "export to file" button</span>
														<code>
														function loadComplete_global(model,data){
															setWidgetToolbarItems({
																exportToFile:
																	{
																		excel:[model,data,'json']
																	}, 
																widgetButton:
																	[
																		{id:'graph1', classes:'', icon:'fa-bar-chart-o', text:'Show Graph'}
																	]		
															});															
														}
														</code>
													</li>
												</ul>
											</li>				
										</ul>
									</div>
								</div>	
							</div>
							<div class="tab-pane" id="declaration_tab">
								<div class="row">
									<div class="col-xs-12">
										<h3>Functions</h3>
										<h5>Location: <strong>Main_model</strong></h5>
										<ul class="list-unstyled">
											<li>
												<span class='orange'>get_week_range('ops','date','format')</span>
											</li>
											<li>
												<ul class="list-unstyled">
													<li>
														<i class="ace-icon fa fa-angle-right blue"></i>
														<a href="#" data-content="<?php echo implode(', ', $this->main_model->get_week_range()); ?>" data-placement="right" data-rel="popover" title="" >
															<code> get_week_range(); </code>
														</a>
													</li>
													<li>
														<i class="ace-icon fa fa-angle-right blue"></i>
														<a href="#" data-content="<?php echo implode(', ', $this->main_model->get_week_range('now')); ?>" data-placement="right" data-rel="popover" title="" >
															<code> get_week_range('now'); </code>
														</a>
													</li>
													<li>
														<i class="ace-icon fa fa-angle-right blue"></i>
														<a href="#" data-content="<?php echo implode(', ', $this->main_model->get_week_range('now','2013-09-06','m-d-Y')); ?>" data-placement="right" data-rel="popover" title="" >
															<code> get_week_range('now','2013-09-03','m-d-Y'); </code>
														</a>
													</li>
												</ul>
											</li>

											<li>
												<span class='orange'>get_daysInweek('ops',date','format')</span>
											</li>
											<li>
												<ul class="list-unstyled">
													<li>
														<i class="ace-icon fa fa-angle-right blue"></i>
														<a href="#" data-content="<?php echo implode(', ', $this->main_model->get_daysInweek()); ?>" data-placement="right" data-rel="popover" title="" >
															<code> get_daysInweek(); </code>
														</a>
													</li>
													<li>
														<i class="ace-icon fa fa-angle-right blue"></i>
														<a href="#" data-content="<?php echo implode(', ', $this->main_model->get_daysInweek('now')); ?>" data-placement="right" data-rel="popover" title="" >
															<code> get_daysInweek('now'); </code>
														</a>
													</li>
													<li>
														<i class="ace-icon fa fa-angle-right blue"></i>
														<a href="#" data-content="<?php echo implode(', ', $this->main_model->get_daysInweek('now','2013-09-06','m-d-Y')); ?>" data-placement="right" data-rel="popover" title="" >
															<code> get_daysInweek('now','2013-09-03','m-d-Y'); </code>
														</a>
													</li>
												</ul>
											</li>

											<li>
												<span class='orange'>get_Month_range('ops','date','format')</span>
											</li>

											<li>
												<ul class="list-unstyled">
													<li>
														<i class="ace-icon fa fa-angle-right blue"></i>
														<a href="#" data-content="<?php echo implode(', ', $this->main_model->get_Month_range()); ?>" data-placement="right" data-rel="popover" title="" >
															<code> get_Month_range(); </code>
														</a>
													</li>
													<li>
														<i class="ace-icon fa fa-angle-right blue"></i>
														<a href="#" data-content="<?php echo implode(', ', $this->main_model->get_Month_range('now')); ?>" data-placement="right" data-rel="popover" title="" >
															<code> get_Month_range('now'); </code>
														</a>
													</li>
													<li>
														<i class="ace-icon fa fa-angle-right blue"></i>
														<a href="#" data-content="<?php echo implode(', ', $this->main_model->get_Month_range('now','2014-08-03','m-d-Y')); ?>" data-placement="right" data-rel="popover" title="" >
															<code> get_Month_range('','2014-08-03','m-d-Y'); </code>
														</a>
													</li>
												</ul>
											</li>
											<li>
												<span class='orange'>get_daysInMonth('date','format')</span>
											</li>

											<li>
												<ul class="list-unstyled">
													<li>
														<i class="ace-icon fa fa-angle-right blue"></i>
														<a href="#" data-content="<?php echo implode(', ', $this->main_model->get_daysInMonth()); ?>" data-placement="right" data-rel="popover" title="" >
															<code> get_daysInMonth(); </code>
														</a>
													</li>
													<li>
														<i class="ace-icon fa fa-angle-right blue"></i>
														<a href="#" data-content="<?php echo implode(', ', $this->main_model->get_daysInMonth('2014-08-03','m-d-Y')); ?>" data-placement="right" data-rel="popover" title="" >
															<code> get_daysInMonth('2014-08-03','m-d-Y'); </code>
														</a>
													</li>
												</ul>
											</li>
											<li>
												<span class='orange'>get_Quarter_range('year','format')</span>
											</li>

											<li>
												<ul class="list-unstyled">
													<li>
														<i class="ace-icon fa fa-angle-right blue"></i>
														<a href="#" data-content="<?php echo implode(', ', $this->main_model->get_firstQuarter_range()); ?>" data-placement="right" data-rel="popover" title="" >
															<code> get_firstQuarter_range(); </code>
														</a>
													</li>
													<li>
														<i class="ace-icon fa fa-angle-right blue"></i>
														<a href="#" data-content="<?php echo implode(', ', $this->main_model->get_secondQuarter_range()); ?>" data-placement="right" data-rel="popover" title="" >
															<code> get_secondQuarter_range(); </code>
														</a>
													</li>
													<li>
														<i class="ace-icon fa fa-angle-right blue"></i>
														<a href="#" data-content="<?php echo implode(', ', $this->main_model->get_thirdQuarter_range()); ?>" data-placement="right" data-rel="popover" title="" >
															<code> get_thirdQuarter_range(); </code>
														</a>
													</li>
													<li>
														<i class="ace-icon fa fa-angle-right blue"></i>
														<a href="#" data-content="<?php echo implode(', ', $this->main_model->get_fourthQuarter_range('2013','d-m-Y')); ?>" data-placement="right" data-rel="popover" title="" >
															<code> get_fourthQuarter_range('2013','d-m-Y'); </code>
														</a>
													</li>
												</ul>
											</li>

											<li>
												<span class='orange'>get_daysInQuarter('ops','year','format')</span>
											</li>

											<li>
												<ul class="list-unstyled">
													<li>
														<i class="ace-icon fa fa-angle-right blue"></i>
														<a href="#" data-content="<?php echo implode(', ', $this->main_model->get_daysInQuarter('first')); ?>" data-placement="right" data-rel="popover" title="" >
															<code> get_daysInQuarter('first'); </code>
														</a>
													</li>
													<li>
														<i class="ace-icon fa fa-angle-right blue"></i>
														<a href="#" data-content="<?php echo implode(', ', $this->main_model->get_daysInQuarter('second')); ?>" data-placement="right" data-rel="popover" title="" >
															<code> get_daysInQuarter('second'); </code>
														</a>
													</li>
													<li>
														<i class="ace-icon fa fa-angle-right blue"></i>
														<a href="#" data-content="<?php echo implode(', ', $this->main_model->get_daysInQuarter('third')); ?>" data-placement="right" data-rel="popover" title="" >
															<code> get_daysInQuarter('third'); </code>
														</a>
													</li>
													<li>
														<i class="ace-icon fa fa-angle-right blue"></i>
														<a href="#" data-content="<?php echo implode(', ', $this->main_model->get_daysInQuarter('fourth','2013','m-d-Y')); ?>" data-placement="right" data-rel="popover" title="" >
															<code> get_daysInQuarter('fourth','2013','m-d-Y'); </code>
														</a>
													</li>
												</ul>
											</li>
										</ul>
									</div>					
								</div>					
							</div>
						</div>
					</div>
				</div>
			</div><!-- /.col -->
		</div>
		
	</div>
</div>

<!-- page specific plugin scripts -->
<script src="assets/js/bootstrap.min.js"></script>

<!-- ace scripts -->
<script src="assets/js/ace-elements.min.js"></script>
<script src="assets/js/ace.min.js"></script>

<script>
jQuery(function($) {
	
});

$('[data-rel=popover]').popover({html:true});

$('a[href=#]').on('click', function (e) {
    e.preventDefault();
})

</script>
<!-- page specific plugin scripts -->
