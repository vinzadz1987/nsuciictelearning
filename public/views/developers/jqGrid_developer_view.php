<div class="row">
	<div class="col-xs-12">
		<!-- PAGE CONTENT BEGINS -->
		<!-- <div class="well well-sm">
			test
		</div> -->
		<div class="row">
			<div class="col-xs-12">
				<table id="grid_table"></table>
				<div id="grid_pager"></div>
			</div>
		</div>



		<script type="text/javascript">
			var $path_base = "..";//this will be used for editurl parameter
		</script>

		<!-- PAGE CONTENT ENDS -->
	</div><!-- /.col -->
</div><!-- /.row -->


<!-- page specific plugin scripts -->
<script src="assets/js/date-time/bootstrap-datepicker.min.js"></script>
<script src="assets/js/jqGrid/jquery.jqGrid.min.js"></script>
<script src="assets/js/jqGrid/i18n/grid.locale-en.js"></script>

<!-- ace scripts -->
<script src="assets/js/ace-elements.min.js"></script>
<script src="assets/js/ace.min.js"></script>

<script>
	$(document).ready(function(){
		jqGrid_control(jqGrid_data());
	});

	//==================================//
	// jqGrid Function Start            //
	//==================================//
	function jqGrid_data(){
		model = {
			data_case:'sample_data', //use for switch cases
			data: 'sample', // any Data
			ops: '', // operation/option data
			adv: '', // advance data
			table: '#grid_table',
			pager: '#grid_pager',
			container_url: 'developers/jqGrid_developer_ctrl/jqgrid_container',
			gridlocation: 'developers/jqGrid_developer_ctrl/load_jq_data/local', // location of controller for gathering data from database
			headers: ['Action','ID','Name','Checkbox','Date','Dropdown', 'Status', 'test'], //header for the table
			names: ['actions','developer_id','name','checkbox','date','dropdown', 'status', 'test'], //use for modifaction and functons. assign names for data in a row.
			index: ['actions','developer_id','name','checkbox','date','dropdown', 'status', 'test'], //set true if index is equal to name. use for query. assign index for data in a row.
			widths: [,,,,,,], //specific width of every column
			sortname: 'developer_id', //use to initialize sort
			//donot add other jqgrid function here. only in jqGridOptions
			jqGridOptions: { // other functions of jqGrid. if not set default will be use. === refer to main.js ===
				// boolean value must be string e.g. 'true' or 'false'
				caption: 'Sample Caption',
				rowList: [20,30,50],
				height : 400,
				sortorder: 'asc',
				rowNum: 20 ,
				scroll: 0,
				editurl: 'developers/jqGrid_sample/edit_row',
				toolbarEdit: 'true',
				toolbarAdd: 'true',
				toolbarDel: 'true',
				toolbarSearch: 'true',
				toolbarRefresh: 'true',
				toolbarView: 'true'
			}
		};
		return model;
	}

	

	function loadComplete_global(table,data,ops){
		$('.btn_option').click(function (e) {
			alert(table+' - '+data);
		});
	}

	//overide function if not set. default will be use. === refer to main.js ===
	function ondblClickRow_global(table,rowId,iRow,iCol,e){
		alert(table+' - '+rowId+' - '+iRow+' - '+iCol+' - '+e);
	}

	//overide function if not set. default will be use. === refer to main.js ===
	function style_edit_form(form) {

		//enable datepicker on "sdate" field and switches for "stock" field
		form.find('input[name=date]').datepicker({format:'yyyy-mm-dd' , autoclose:true})
			.end().find('input[role=checkbox]')
				.addClass('ace ace-switch ace-switch-5').after('<span class="lbl"></span>');
				   //don't wrap inside a label element, the checkbox value won't be submitted (POST'ed)
				  //.addClass('ace ace-switch ace-switch-5').wrap('<label class="inline" />').after('<span class="lbl"></span>');
	
		style_form_with_ui(form);
		//update buttons classes
		style_form_update_buttons(form);	

	}

	function edit_afterSubmit_global(table,response,postdata){
		//Reload Grid after submit.
		alert('Successful');
		$(model.table).jqGrid('setGridParam',{datatype:'json'}).trigger('reloadGrid');
	};

	//==================================//
	// jqGrid Function End              //
	//==================================//

</script>
