<div class="col-xs-12">
	<div class="row">
		<div class="widget-box">
			<div class="widget-header">
				<h5 class="widget-title">Default Widget Box</h5>

				<!-- #section:custom/widget-box.toolbar -->
				<div class="widget-toolbar">

					<a href="#" data-action="collapse">
						<i class="ace-icon fa fa-chevron-up"></i>
					</a>
				</div>

				<!-- /section:custom/widget-box.toolbar -->
			</div>

			<div class="widget-body">
				<div class="widget-main">
					<div class="row">
						<div class="col-xs-4">
							<!-- PAGE CONTENT BEGINS -->
							<form class="form-horizontal" role="form">
								<!-- #section:elements.form -->
								<div class="form-group">
									<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Text Field </label>

									<div class="col-sm-9">
										<input type="text" id="form-field-1" placeholder="Username" class="col-xs-10 col-sm-5" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Text Field </label>

									<div class="col-sm-9">
										<input type="text" id="form-field-1" placeholder="Username" class="col-xs-10 col-sm-5" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Text Field </label>

									<div class="col-sm-9">
										<input type="text" id="form-field-1" placeholder="Username" class="col-xs-10 col-sm-5" />
									</div>
								</div>
							</form>
						</div>
						<div class="col-xs-4">
							<!-- PAGE CONTENT BEGINS -->
							<form class="form-horizontal" role="form">
								<!-- #section:elements.form -->
								<div class="form-group">
									<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Text Field </label>

									<div class="col-sm-9">
										<input type="text" id="form-field-1" placeholder="Username" class="col-xs-10 col-sm-5" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Text Field </label>

									<div class="col-sm-9">
										<input type="text" id="form-field-1" placeholder="Username" class="col-xs-10 col-sm-5" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Text Field </label>

									<div class="col-sm-9">
										<input type="text" id="form-field-1" placeholder="Username" class="col-xs-10 col-sm-5" />
									</div>
								</div>
							</form>
						</div>
						<div class="col-xs-4">
							<!-- PAGE CONTENT BEGINS -->
							<form class="form-horizontal" role="form">
								<!-- #section:elements.form -->
								<div class="form-group">
									<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Text Field </label>

									<div class="col-sm-9">
										<input type="text" id="form-field-1" placeholder="Username" class="col-xs-10 col-sm-5" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Text Field </label>

									<div class="col-sm-9">
										<input type="text" id="form-field-1" placeholder="Username" class="col-xs-10 col-sm-5" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Text Field </label>

									<div class="col-sm-9">
										<input type="text" id="form-field-1" placeholder="Username" class="col-xs-10 col-sm-5" />
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- page specific plugin scripts -->

<!-- ace scripts -->
<script src="assets/js/ace-elements.min.js"></script>
<script src="assets/js/ace.min.js"></script>