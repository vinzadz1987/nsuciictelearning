<div class="row">
	<div class="col-xs-12">
		<!-- PAGE CONTENT BEGINS -->
		<!-- <div class="well well-sm">
			test
		</div> -->

		<div id="default-buttons" class="col-sm-6">
			<h3 class="row header smaller lighter green">
				<span class="col-sm-6"> jqGrid Actions </span>
			</h3>

			<div class="btn-group margin-right-xl">
				<button id="btn_multiSelect" class="btn btn-success">Multi Select</button>
				<button class="btn dropdown-toggle btn-success" data-toggle="dropdown">
				<span class="ace-icon fa fa-caret-down icon-only"></span>
				</button>
				<ul class="dropdown-menu">
					<li>
						<a id="select_all" href="#">Select All</a>
					</li>
					<li>
						<a id="select_one" href="#">Select Row 5</a>
					</li>
					<li>
						<a id="unselect_all" href="#">Unselect All</a>
					</li>
					<li class="divider"></li>
					<li>
						<a id="mselect_code" href="#">View Code</a>
					</li>
				</ul>
			</div>

			<div class="btn-group margin-right-xl">
				<button id="btn_addRow" class="btn btn-primary">Add row</button>
				<button class="btn dropdown-toggle btn-primary" data-toggle="dropdown">
				<span class="ace-icon fa fa-caret-down icon-only"></span>
				</button>
				<ul class="dropdown-menu">
					<li>
						<a id="built-in_addRow" href="#">Built-in add row</a>
					</li>
					<li class="divider"></li>
					<li>
						<a id="mselect_code" href="#">View Code</a>
					</li>
				</ul>
			</div>

			<div class="btn-group margin-right-xl">
				<button id="btn_editRow" class="btn btn-warning">Edit row</button>
				<button class="btn dropdown-toggle btn-warning" data-toggle="dropdown">
				<span class="ace-icon fa fa-caret-down icon-only"></span>
				</button>
				<ul class="dropdown-menu">
					<li>
						<a id="built-in_editRow" href="#">Built-in edit row</a>
					</li>
					<li>
						<a id="editRow_5" href="#">Edit row 5</a>
					</li>
					<li class="divider"></li>
					<li>
						<a id="mselect_code" href="#">View Code</a>
					</li>
				</ul>
			</div>

			<div class="btn-group margin-right-xl">
				<button id="btn_removeRow" class="btn btn-danger">Remove row</button>
				<button class="btn dropdown-toggle btn-danger" data-toggle="dropdown">
				<span class="ace-icon fa fa-caret-down icon-only"></span>
				</button>
				<ul class="dropdown-menu">
					<li>
						<a id="built-in_removeRow" href="#">Built-in remove row</a>
					</li>
					<li class="divider"></li>
					<li>
						<a id="mselect_code" href="#">View Code</a>
					</li>
				</ul>
			</div>

		</div>

		<div class="col-sm-6">
			<h3 class="row header smaller lighter purple">
				<span class="col-sm-6"> jqGrid Search Functions </span>
			</h3>

			<div class="btn-group col-sm-12">
				<div class="input-group">
					<input id="search_input" class="form-control search-query" type="text" placeholder="Search...">
					<span class="input-group-btn">
						<button id="btn_search" class="btn btn-purple btn-sm margin-right-xs" type="button">Search
						<i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
						</button>
						<button class="btn dropdown-toggle btn-purple btn-sm" data-toggle="dropdown">
							<span class="ace-icon fa fa-caret-down icon-only"></span>
						</button>
						<ul class="dropdown-menu">
						<li>
							<a id="built-in_removeRow" href="#">Built-in remove row</a>
						</li>
						<li class="divider"></li>
						<li>
							<a id="mselect_code" href="#">View Code</a>
						</li>
					</ul>
					</span>
				</div>
			</div>
			
		</div>

		<div class="row">
			<div class="col-xs-12">
				<br><br>
				<table id="grid_table"></table>
				<div id="grid_pager"></div>
			</div>
		</div>

		<script type="text/javascript">
			var $path_base = "..";//this will be used for editurl parameter
		</script>

		<!-- PAGE CONTENT ENDS -->
	</div><!-- /.col -->
</div><!-- /.row -->


<!-- page specific plugin scripts -->
<script src="assets/js/date-time/bootstrap-datepicker.min.js"></script>
<script src="assets/js/jqGrid/jquery.jqGrid.min.js"></script>
<script src="assets/js/jqGrid/i18n/grid.locale-en.js"></script>
<script src="assets/js/bootbox.min.js"></script>
<script src="assets/js/prettify.js"></script>

<!-- ace scripts -->
<script src="assets/js/ace-elements.min.js"></script>
<script src="assets/js/ace.min.js"></script>

<script>
	$(document).ready(function(){
		jqGrid_control(jqGrid_data());
	});

	//==================================//
	// jqGrid Function Start            //
	//==================================//
	function jqGrid_data(){
		model = {
			data_case:'sample_functions_data', //use for switch cases
			data: 'sample_functions', // any Data
			ops: '', // operation/option data
			adv: '', // advance data
			table: '#grid_table',
			pager: '#grid_pager',
			container_url: 'developers/jqGrid_developer_ctrl/jqgrid_container',
			gridlocation: 'developers/jqGrid_developer_ctrl/load_jq_data', // location of controller for gathering data from database
			headers: ['ID','Name','Checkbox','Date','Dropdown', 'Status', 'test'], //header for the table
			names: ['developer_id','name','checkbox','date','dropdown', 'status', 'test'], //use for modifaction and functons. assign names for data in a row.
			index: ['developer_id','name','checkbox','date','dropdown', 'status', 'test'], //set true if index is equal to name. use for query. assign index for data in a row.
			widths: [,,,,,,], //specific width of every column
			sortname: 'developer_id', //use to initialize sort
			autoresize: true,
			//donot add other jqgrid function here. only in jqGridOptions
			jqGridOptions: { // other functions of jqGrid. if not set default will be use. === refer to main.js ===
				// boolean value must be string e.g. 'true' or 'false'
				caption: 'Sample Caption',
				rowList: [20,30,50],
				height: 400,
				sortorder: 'asc',
				rowNum: 10,
				scroll: 0,
				editurl: 'developers/jqGrid_sample/edit_row',
				toolbarRefresh: 'true',
				multiselect: 'true'
			}
		};
		return model;
	}

	

	function loadComplete_global(table,aRowids,status){
		$('.btn_option').click(function (e) {
			bootbox.alert(table+' - '+aRowids,function(){});
		});

		//for multiselect
		 $.each(idsOfSelectedRows,function(i,v){
			jQuery(table).jqGrid('setSelection', idsOfSelectedRows[i], false);
		})
	}

	//overide function if not set. default will be use. === refer to main.js ===
	function ondblClickRow_global(tabel,rowId,iRow,iCol,e){
		bootbox.alert(table+' - '+rowId+' - '+iRow+' - '+iCol+' - '+e,function(){});
	}

	//overide function if not set. default will be use. === refer to main.js ===
	function style_edit_form(form) {

		//enable datepicker on "sdate" field and switches for "stock" field
		form.find('input[name=date]').datepicker({format:'yyyy-mm-dd' , autoclose:true})
			.end().find('input[role=checkbox]')
				.addClass('ace ace-switch ace-switch-5').after('<span class="lbl"></span>');
				   //don't wrap inside a label element, the checkbox value won't be submitted (POST'ed)
				  //.addClass('ace ace-switch ace-switch-5').wrap('<label class="inline" />').after('<span class="lbl"></span>');
		
		style_form_with_ui(form);
		//update buttons classes
		style_form_update_buttons(form);

	}

	function edit_afterSubmit_global(table,response,postdata){
		//Reload Grid after submit.
		bootbox.alert('Successful',function(){})
		reloadGrid(model.table);//main.js
	};

	//==================================//
	// jqGrid Function End              //
	//==================================//

	//==================================//
	// jqGrid Action Functions Start    //
	//==================================//

	$('a[href=#]').on('click', function (e) {
        e.preventDefault();
    })

    //Muliselect start
	
	$('#btn_multiSelect').click(function(e){
		var ids = idsOfSelectedRows;//main.js
		bootbox.alert(ids,function(){});
	})

	$('#select_all').click(function(e){
		var ids = $("#grid_table").jqGrid('getGridParam','_index');
		jQuery("#grid_table").jqGrid('resetSelection');
		$.each(ids,function(i,v){
			jQuery("#grid_table").jqGrid('setSelection',i);
			updateIdsOfSelectedRows(i,true);//main.js
		})		
	})

	$('#select_one').click(function(e){
		jQuery("#grid_table").jqGrid('setSelection',"5");
	})

	$('#unselect_all').click(function(e){
		updateIdsOfSelectedRows('clear');//main.js
		jQuery("#grid_table").jqGrid('resetSelection');
	})

	$('#mselect_code').click(function(e){
		e.preventDefault();
		dialogBox(
			model = {
				type: 'url',
				data: 'developers/modal_ctrl/modal/multiselect_modal',
				title: 'Multi Select',
				title_icon: 'fa-check-square-o',
				width: 800,
				height: 600,
				buttons: [
					{
						text: "OK",
						"class" : "btn btn-primary btn-xs",
						click: function() {
							$( this ).dialog( "close" ); 
						} 
					}
				]
			}
		)
	})

	//Multiselect end

	//Add row start
	$('#built-in_addRow').click(function(e){
		$('#grid_table').jqGrid("editGridRow", "new",{beforeShowForm:function(e){var form = $(e[0]); style_edit_form(form)}});
	})

	$('#btn_addRow').click(function(e){
		e.preventDefault();
		dialogBox(
			model = {
				type: 'url',
				data: 'developers/modal_ctrl/modal/jqgrid_addRow_modal',
				title: 'Add Row',
				title_icon: 'fa-plus',
				width: 300,
				buttons: [
					{
						'html': "<i class='ace-icon fa fa-check'></i> Submit",
						'class': 'btn_submit btn btn-primary btn-xs',
						click: function(){
							//$(this).dialog('close');
						}
					},
					{
						'html': "<i class='ace-icon fa fa-times'></i> Cancel",
						'class': 'btn_cancel btn btn-xs',
						click: function(){
							$(this).dialog('close');
						}
					}
				]
			}
		)
	})

	//Add row end

	//Edit row start

	$('#built-in_editRow').click(function(e){
		var selected_row = jQuery("#grid_table").jqGrid('getGridParam','selarrrow');
		if(selected_row.length==1){
			$('#grid_table').jqGrid("editGridRow", selected_row,{
				beforeShowForm:function(e){var form = $(e[0]); style_edit_form(form)}
			});
		}else bootbox.alert('Please select only one row');
	})

	$('#editRow_5').click(function(e){
		$('#grid_table').jqGrid("editGridRow", 5,{
			beforeShowForm:function(e){var form = $(e[0]); style_edit_form(form)}
		});
	})

	$('#btn_editRow').click(function(e){
		e.preventDefault();
		var selected_row = jQuery("#grid_table").jqGrid('getGridParam','selarrrow');
		if(selected_row.length==1){
			dialogBox(
				model = {
					type: 'url',
					data: 'developers/modal_ctrl/modal/jqgrid_editRow_modal/'+selected_row.join(),
					title: 'Edit Row',
					title_icon: 'fa-pencil',
					width: 300,
					buttons: [
						{
							'html': "<i class='ace-icon fa fa-check'></i> Submit",
							'class': 'btn_submit btn btn-primary btn-xs',
							click: function(){
								//$(this).dialog('close');
							}
						},
						{
							'html': "<i class='ace-icon fa fa-times'></i> Cancel",
							'class': 'btn_cancel btn btn-xs',
							click: function(){
								$(this).dialog('close');
							}
						}
					]
				}
			)
		}else bootbox.alert('Please select only one row');
	})

	//Edit row end

	//Remove row start
	$('#built-in_removeRow').click(function(e){
		var selected_row = jQuery("#grid_table").jqGrid('getGridParam','selarrrow');
		$('#grid_table').jqGrid('delGridRow',selected_row,{
			beforeShowForm:function(e){var form = $(e[0]); style_edit_form(form)}
		});
	})

	$('#btn_removeRow').click(function(e){
		var selected_row = jQuery("#grid_table").jqGrid('getGridParam','selarrrow');
		bootbox.confirm("Remove row "+selected_row,function(res){
			if(res){
				$.ajax({
					url: 'developers/jqgrid_external_function/remove_row',
					type: 'POST',
					data: {ids:selected_row},
					dataType: 'json',
					success: function(res){
						bootbox.alert('Row Removed');
						reloadGrid('#grid_table');//main.js
					},
					error: function(res){
						bootbox.alert('Error! try again');
					}
				})
			}
		});
	});

	//Remove row end

    //==================================//
	// jqGrid Action Functions End      //
	//==================================//

    //==================================//
	// jqGrid Search Functions Start    //
	//==================================//

	$('#btn_search').click(function(e){
	})

	function jqgridSearch(data,ops){

	}


    //==================================//
	// jqGrid Search Functions End      //
	//==================================//

</script>
