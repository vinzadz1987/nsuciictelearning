<div class="modal-body">
	<div class="scroll-track">
		<div class="scroll-bar"></div>
	</div>
	<div class="scroll-content">
		<div class="onpage-help-content">
			<div class="help-content">
				<div class="widget-box transparent collapsed">
					<div class="widget-header">
						<h3 class="info-title smaller widget-title"><a data-action="collapse" href="#"><span class="ace-icon">1. Multi Select (view selected)</span></a></h3></h3>
						<div class="widget-toolbar no-border">
							<a data-action="collapse" href="#">
							<i class="ace-icon fa fa-plus" data-icon-show="fa-plus" data-icon-hide="fa-minus"></i>
							</a>
						</div>
					</div>
					<div class="widget-body">
						<div class="widget-body-inner" style="display: block;">
							<div class="widget-main">
								<div class="info-section">
									<ul class="info-list list-unstyled">
										<li>
											<ol>
												<li>
													View all data selected, including other pages.
												</li>
												<li>
													<b>idsOfSelectedRows</b>: contains ids of all selected rows. (<b>found</b>:@ <code class="open-file" data-open-file="html"><span class="brief-show">assets/js/</span>main.js</code>)
												</li>
											</ol>
											
											<div class="space-4"></div>
<pre data-language="javascript" class="rainbow">
$('#btn_multiSelect').click(function(e){
  var ids = idsOfSelectedRows;
  bootbox.alert(ids,function(){});
})
</pre>
										</li>
									 </ul><!-- /.info-list -->

								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="widget-box transparent collapsed">
					<div class="widget-header">
						<h3 class="info-title smaller widget-title"><a data-action="collapse" href="#"><span class="ace-icon">2. Select All</span></a></h3>
						<div class="widget-toolbar no-border">
							<a data-action="collapse" href="#">
							<i class="ace-icon fa fa-plus" data-icon-show="fa-plus" data-icon-hide="fa-minus"></i>
							</a>
							</div>
					</div>
					<div class="widget-body">
						<div class="widget-body-inner" style="display: block;">
							<div class="widget-main">
								<div class="info-section">
									<ul class="info-list list-unstyled">
										<li>
											<ol>
												<li>
													Select all data in grid, including other pages.
												</li>
												<li>
													<code>$("#grid_table").jqGrid('getGridParam','_index');</code>: get all ids in grid.
												</li>
												<li>
													<b>resetSelection</b>: reset all selected row to none in grid.
												</li>
												<li>
													<b>updateIdsOfSelectedRows</b>: Update <b>IdsOfSelectedRows</b> (<b>found</b>:@ <code class="open-file" data-open-file="html"><span class="brief-show">assets/js/</span>main.js</code>)
												</li>
											</ol>
											
											<div class="space-4"></div>
<pre data-language="javascript" class="rainbow">
$('#select_all').click(function(e){
  var ids = $("#grid_table").jqGrid('getGridParam','_index');
  jQuery("#grid_table").jqGrid('resetSelection');
  $.each(ids,function(i,v){
    jQuery("#grid_table").jqGrid('setSelection',i);
	updateIdsOfSelectedRows(i,true);//main.js
  })		
})
</pre>
										</li>
									 </ul><!-- /.info-list -->

								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="widget-box transparent collapsed">
					<div class="widget-header">
						<h3 class="info-title smaller widget-title"><a data-action="collapse" href="#"><span class="ace-icon">3. Unselect All</span></a></h3>
						<div class="widget-toolbar no-border">
							<a data-action="collapse" href="#">
							<i class="ace-icon fa fa-plus" data-icon-show="fa-plus" data-icon-hide="fa-minus"></i>
							</a>
							</div>
					</div>
					<div class="widget-body">
						<div class="widget-body-inner" style="display: block;">
							<div class="widget-main">
								<div class="info-section">
									<ul class="info-list list-unstyled">
										<li>
											<ol>
												<li>
													Unelect all data in grid, including other pages.
												</li>
												<li>
													<code>updateIdsOfSelectedRows('clear');</code>: Clear all selected rows (<b>found</b>:@ <code class="open-file" data-open-file="html"><span class="brief-show">assets/js/</span>main.js</code>).
												</li>
												<li>
													<b>resetSelection</b>: reset all selected row to none in grid.
												</li>
											</ol>
											
											<div class="space-4"></div>
<pre data-language="javascript" class="rainbow">
$('#unselect_all').click(function(e){
  updateIdsOfSelectedRows('clear');//main.js
  jQuery("#grid_table").jqGrid('resetSelection');
})
</pre>
										</li>
									 </ul><!-- /.info-list -->

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>