<div class="row">
	<div class="col-xs-12">
		<form id="addRow_form" class="form-horizontal" role="form">
			<div class="form-group">
				<label class="col-sm-4 control-label no-padding-right" for="name"> Name </label>
				<div class="col-sm-7">
					<input class="form-control" type="text" id="name" name="name" placeholder="Name" />
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-4 control-label no-padding-right" for="checkbox"> Checkbox </label>
				<div class="col-sm-7">
					<label>
						<input class="ace ace-switch ace-switch-4" type="checkbox" name="checkbox" value="xx">
						<span class="lbl" data-lbl="YES  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NO"></span>
					</label>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-4 control-label no-padding-right" for="date"> Date </label>
				<div class="col-sm-7">
					<input class="form-control date-picker" type="text" id="date" name="date" placeholder="2014-06-04" data-date-format="yyyy-mm-dd" />
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-4 control-label no-padding-right" for="date"> Dropdown </label>
				<div class="col-sm-7">
					<select class="form-control" id="dropdown_id" name="dropdown_id">
						<?php echo($drop_down); ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-4 control-label no-padding-right" for="date"> Status </label>
				<div class="col-sm-7">
					<input class="form-control" type="text" id="status" name="status" placeholder="normal"/>
				</div>
			</div>
		</form>
	</div>
</div>

<script>
	$('.date-picker').datepicker({
		autoclose: true,
		todayHighlight: true
	})

	$('.btn_submit').click(function(e){
		e.preventDefault();
		addRow();
	})

	function addRow(){
		$.ajax({
			url: 'developers/jqgrid_external_function/add_row',
			type: 'POST',
			data: {data:encodeURIComponent($('#addRow_form').serialize())},
			dataType: 'json',
			success: function(res){
				bootbox.alert('Row Added');
				closeDialog();
				reloadGrid('#grid_table');//main.js
			},
			error: function(res){
				bootbox.alert('Row Already exist');
			}
		})
	}
</script>