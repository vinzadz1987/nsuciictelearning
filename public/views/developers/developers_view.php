<div class="row">
	<div class="col-xs-12">		
		<div class="row">
			<div class="col-xs-12">
				<div id="recent-box" class="widget-box transparent">
					<div class="widget-header">
						<h4 class="widget-title lighter smaller">
							<i class="ace-icon fa fa-rss orange"></i>Developers - Development References
						</h4>
					</div>
					<!-- #section:elements.tab -->
					
					<!-- /section:elements.tab -->								
				</div>
			</div>			
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="row">
					<div class="tabbable">
						<ul id="myTab" class="nav nav-tabs">
							<li class="active">
								<a href="#developers_guide" data-toggle="tab" class='rememberTab'>
									<i class="green ace-icon fa fa-user bigger-120"></i>
									Developer's Guide
								</a>
							</li>
							<li>
								<a href="#add_access" data-toggle="tab" class='rememberTab'>
									<i class="green ace-icon fa fa-clock-o bigger-120"></i>
									Add Access
								</a>
							</li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="developers_guide">
								<div class="row">
									<div class="col-xs-12">
										<ul class="list-unstyled spaced2">
											<li>
												<i class="ace-icon fa fa-circle green"></i>
												Sessions used in this project; <?php //echo $this->session->userdata('default_controller');?>
											</li>															
											<li>
												<ul class="list-unstyled">
													<li>
														<i class="ace-icon fa fa-angle-right blue"></i>
														<span class='orange'>ip_address</span> - IP address of the current client PC.
													</li>
													<li>
														<i class="ace-icon fa fa-angle-right blue"></i>
														<span class='orange'>employee_id</span> - ID of the current user.
													</li>
													<li>
														<i class="ace-icon fa fa-angle-right blue"></i>
														<span class='orange'>firstname</span> - First name of the current user.
													</li>
													<li>
														<i class="ace-icon fa fa-angle-right blue"></i>
														<span class='orange'>middlename</span> - Middle name of the current user.
													</li>
													<li>
														<i class="ace-icon fa fa-angle-right blue"></i>
														<span class='orange'>lastname</span> - Last name of the current user.
													</li>
													<li>
														<i class="ace-icon fa fa-angle-right blue"></i>
														<span class='orange'>last_login</span> - Previous date when current user logged in.
													</li>
													<li>
														<i class="ace-icon fa fa-angle-right blue"></i>
														<span class='orange'>default_controller</span> - Controller name of the current page.
													</li>
													<li>
														<i class="ace-icon fa fa-angle-right blue"></i>
														<span class='orange'>logged_in</span> - Flag if user is still logged in.
													</li>
													<li>
														<i class="ace-icon fa fa-angle-right blue"></i>
														<span class='orange'>center_id</span> - center ID where the current user belong to.
													</li>
													<li>
														<i class="ace-icon fa fa-angle-right blue"></i>
														<span class='orange'>menu_sets</span> - set of menu IDs which the current user is allowed to access.
													</li>
													<li>
														<i class="ace-icon fa fa-angle-right blue"></i>
														<span class='orange'>campaign_id</span> - ID of the selected campaign buttton located on the right-top of the page.
													</li>
													<li>
														<i class="ace-icon fa fa-angle-right blue"></i>
														<span class='orange'>campaign_name</span> - Name(of string type) of the selected campaign buttton located on the right-top of the page.
													</li>
													<li>
														<i class="ace-icon fa fa-angle-right blue"></i>
														<span class='orange'>business_id</span> - ID of the selected business buttton.
													</li>
													<li>
														<i class="ace-icon fa fa-angle-right blue"></i>
														<span class='orange'>business_name</span> -  Name of the selected business buttton..
													</li>
												</ul>
											</li>				
										</ul>
									</div>
								</div>
								
								<!-- Functions and Line of Code -->
								<div class="row">
									<div class="col-xs-12">
										<ul class="list-unstyled spaced2">
											<li>
												<i class="ace-icon fa fa-circle green"></i>
												Functions and Line/s of Code; <?php //echo $this->session->userdata('default_controller');?>
											</li>															
											<li>
												<ul class="list-unstyled">
													<li>
														<i class="ace-icon fa fa-angle-right blue"></i>
														<span class='orange'>jQuery function to show "export to file" button</span>
														<code>
														function loadComplete_global(model,data){
															setWidgetToolbarItems({
																exportToFile:
																	{
																		excel:[model,data,'json']
																	}, 
																widgetButton:
																	[
																		{id:'graph1', classes:'', icon:'fa-bar-chart-o', text:'Show Graph'}
																	]		
															});															
														}
														</code>
													</li>
												</ul>
											</li>				
										</ul>
									</div>
								</div>	
							</div>
							<div class="tab-pane" id="add_access">
														
							</div>
						</div>
					</div>
				</div>
			</div><!-- /.col -->
		</div>
		
	</div>
</div>
<script>
$(document).ready(function(){
	// baseUrl = "";
	// baseUrl =  window.location.href.split('/?')[0]; 
	// $('.rememberTab').on('click',function(){ //alert(baseUrl);
		// var stateObject = {};
		// tabId = $(this).attr('href').substring(1,$(this).attr('href').length);
		// window.history.pushState(stateObject,'',baseUrl+'/?tabRef='+tabId);
	// });
	// var hasParam = (document.URL).indexOf("tabRef") > -1; 
		// if(hasParam == true){
			// tab = getUrlParameters("tabRef", "", true);		//alert('#'+tab);
			// $('.rememberTab').each(function(){
				// if($(this).attr('href') == '#'+tab){
					// $(this).parent().addClass('active').siblings().removeClass('active');;
				// }
			// });
			// $('.tab-pane').each(function(){
				// if($(this).attr('id') == tab){
					// $(this).addClass('active');
				// }else {
					// $(this).removeClass('active');
				// }
			// });
		// }	
	

});
</script>
<!-- page specific plugin scripts -->
