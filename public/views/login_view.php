<!DOCTYPE html>
<base href="<?php print base_url(); ?>" />
<html lang="en">
<head>
<link rel="icon" href="https://upload.wikimedia.org/wikipedia/en/thumb/d/df/Naval_State_University.png/200px-Naval_State_University.png">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta charset="utf-8" />
<title><?php print $page_title; ?></title>

<meta name="description" content="User login page" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

<!-- bootstrap & fontawesome -->
<link rel="stylesheet" href="assets/css/bootstrap.min.css" />
<link rel="stylesheet" href="assets/css/font-awesome.min.css" />

<!-- text fonts -->
<link rel="stylesheet" href="assets/css/ace-fonts.css" />

<!-- ace styles -->
<link rel="stylesheet" href="assets/css/ace.min.css" />
<link rel="stylesheet" href="assets/css/login.css" />
    
<link rel="stylesheet" href="assets/css/ace-rtl.min.css" />

<link rel="stylesheet" href="assets/css/ace-skins.min.css" />
<link rel="stylesheet" href="assets/css/ace.onpage-help.css" />

<script src="assets/js/ace-extra.min.js"></script>
</head>

<body class="login-layout light-login">
<div class="main-container">
<div class="main-content">
<div class="row login_form_container">
<div class="col-sm-10 col-sm-offset-1 ">
<div class="login-container">
<div class="center">
<h1>
<i class="ace-icon fa fa-lightbulb-o green"></i>
<span class="orange">CIICT</span>
<span class="white" id="id-text2">ONLINE LEARNING
<span class="blue">HUB</span></span>
</h1>
</div>
<div class="space-6"></div>
<div class="position-relative">
<div id="login-box" class="login-box widget-box no-border visible">
<div class="widget-body">
<div class="widget-main">
<h4 class="header blue lighter bigger">
<i class="ace-icon fa fa-coffee green"></i>
Please Enter Your Information
</h4>

<div class="space-6"></div>

	<form>
		<fieldset>
			<label class="block clearfix">
				<span class="block input-icon input-icon-right">
					<input class="form-control" id="suserid" placeholder="Student ID" type="text">
					<i class="ace-icon fa fa-user"></i>
				</span>
				<span class="input-icon input-icon-right hidden">
					<input class="form-control" id="tuserid" placeholder="Username" type="text">
					<i class="ace-icon fa fa-user"></i>
				</span>
			</label>

			<label class="block clearfix">
				<span class="block input-icon input-icon-right">
					<input class="form-control" id="password" placeholder="Password" type="password">
					<i class="ace-icon fa fa-lock"></i>
				</span>
			</label>

			<label class="block clearfix">
				<span class="block input-icon input-icon-right">
					<select class="form-control" id="accesstype" style="position: absolute;">
						<option value="">Select Access Type</option>
						<option value="1" selected="selected">Student</option>
						<option value="2">Teacher</option>
					</select>
				</span>
			</label>

			<div style="height: 30px"></div>

			<div class="clearfix">
				<button type="button" id="btn-login" class="width-35 pull-right btn btn-sm btn-primary">
					<i class="ace-icon fa fa-key"></i>
					<span class="bigger-110">Login</span>
				</button>
			</div>
		</fieldset>
	</form>
</div><!-- /.widget-main -->

<div class="toolbar clearfix">
	<div>
		<a href="#" data-target="#forgot-box" class="forgot-password-link">
			<i class="ace-icon fa fa-arrow-left"></i>
			I forgot my password
		</a>
	</div>
	<div>
		<a href="#" data-target="#signup-box" class="user-signup-link">
			I want to register
			<i class="ace-icon fa fa-arrow-right"></i>
		</a>
	</div>
</div>
</div><!-- /.widget-body -->
</div><!-- /.login-box -->

<div id="forgot-box" class="forgot-box widget-box no-border">
<div class="widget-body">
<div class="widget-main">
<h4 class="header red lighter bigger">
<i class="ace-icon fa fa-key"></i>
Retrieve Password
</h4>

<div class="space-6"></div>
<p>                                         
Report to Administrator.
<br>Email us at <a href='#'>ciictoelh@nsu.edu.ph</br>
</p>
</div><!-- /.widget-main -->    

<div class="toolbar center">
<a href="#" data-target="#login-box" class="back-to-login-link">
Back to login
<i class="ace-icon fa fa-arrow-right"></i>
</a>
</div>
</div><!-- /.widget-body -->
</div><!-- /.forgot-box -->

<div id="signup-box" class="signup-box widget-box no-border">
<div class="widget-body">
<div class="widget-main">
<h4 class="header green lighter bigger">
<i class="ace-icon fa fa-users blue"></i>
New User Registration 
                               
</h4>
<div class="space-6"></div>

<div class="tabbable">
<ul class="nav nav-tabs" id="myTab">
<li class="active">
<a data-toggle="tab" href="#home">
<i class="green ace-icon fa fa-user bigger-120"></i>
Student
</a>
</li>
<li>
<a data-toggle="tab" href="#profile">
<i class="green ace-icon fa fa-user bigger-120"></i>
Teacher
</a>
</li>
</ul>

<div class="tab-content">
<div id="home" class="tab-pane in active">
<div id="studentRegistration">
	<center>
		<i class="ace-icon fa fa-user red"></i>
		Please Authenticate yourself 
		<input class="form-control" id="magic_words_stud" placeholder="" type="text" style="text-align: center">
		<span id="errorAuthenticStud"></span>
	</center>	
	<div style="height: 10px;"></div>
	<div id="studentSignup">      
		<p class="center"><h4> Enter Your Details To Begin </h4> </p>                 									
			<form>
				<fieldset>
					<label class="block clearfix">
						<span class="block input-icon input-icon-right">
							<input class="form-control" id="student_id_number" placeholder="Student ID Number" type="text">
							<i class="ace-icon fa fa-user"></i>
							<center><span> e.g ##-#-##### </span></center>
						</span>
					</label>

					<label class="block clearfix">
						<span class="block input-icon input-icon-right">
							<input class="form-control" id="fname" placeholder="First Name" type="text">
							<i class="ace-icon fa fa-user"></i>
						</span>
					</label>


					<label class="block clearfix">
						<span class="block input-icon input-icon-right">
							<input class="form-control" id="mname" placeholder="Middle Name" type="text">
							<i class="ace-icon fa fa-user"></i>
						</span>
					</label>

					<label class="block clearfix">
						<span class="block input-icon input-icon-right">
							<input class="form-control" id="lname" placeholder="Last Name" type="text">
							<i class="ace-icon fa fa-user"></i>
						</span>
					</label>

					<label class="block clearfix">
						<span class="block input-icon input-icon-right">
							<input class="form-control" id="contactnum" placeholder="+63" type="text">
							<i class="ace-icon fa fa-user"></i>
						</span>
					</label>

					<label class="block clearfix">
						<span class="block input-icon input-icon-right">
							<input class="form-control" id="pw" placeholder="Password" type="password">
							<i class="ace-icon fa fa-lock"></i>
						</span>
					</label>

					<label class="block clearfix">
						<span class="block input-icon input-icon-right">
							<input class="form-control" id="pw2" placeholder="Repeat Password" type="password" onchange="checkPasswordMatch()">
							<i class="ace-icon fa fa-retweet"></i>
						</span>
						<span class="pull-left" style="color:red; font-size: small" id="divCheckPasswordMatch"></span>
					</label>

					<label class="block clearfix">
						<span class="block input-icon input-icon-right">
							<select class="form-control" id="course"></select>
						</span>
					</label>

					<label class="block clearfix">
						<span class="block input-icon input-icon-right">
							<select class="form-control" id="semester">
								<option value=""> Semester </option>
								<option value="1">First Semester</option>
								<option value="2">Second Semester</option>
							</select>
						</span>
					</label>

					<label class="block clearfix">
						<span class="block input-icon input-icon-right">
						<select class="form-control" id="year_level">
							<option value=""> Year Level </option>
							<option value="1">First Year</option>
							<option value="2">Second Year</option>
							<option value="3">Third Year</option>
							<option value="4">Fourth Year</option>
						</select>
						</span>
					</label>

					<label class="block clearfix" id="subjectList"></label>
					<div class="" id="error_message"></div>
					<div class="space-10"></div>
					<div class="clearfix">
						<button type="reset" class="width-12 pull-left btn btn-sm">
							<i class="ace-icon fa fa-refresh"></i>
							<span class="bigger-110">Reset</span>
						</button>
						<button id="register_user" type="button" class="width-60 pull-right btn btn-sm btn-success">
							<span class="bigger-110">Register</span>
							<i class="ace-icon fa fa-arrow-right icon-on-right"></i>
						</button>
					</div>									
				</fieldset>
			</form>
		</div>
	</div>
</div>

<div id="profile" class="tab-pane">    
<center>
<i class="ace-icon fa fa-user red"></i>
Please Authenticate yourself 
<input class="form-control" id="magic_words_teach" placeholder="" type="text" style="text-align: center">
<span id="errorAuthenticTeach"></span>
</center>
<div id="teacherSignup"> 
<p class="center">TEACHER - Enter your details to begin: </p>
<form>
<fieldset>
<label class="block clearfix">
<center>
<table>
<tr>
<td>
F
</td>
<td style="padding: 10px;"> - </td>
<td>
<input class="form-control" id="idnumberTeach" placeholder="00000" type="text" style="width:100px; text-align: center;">
</td>
</tr>
<tr>
<td colspan="5" class="sm" id="errorIDMessageTeach" style="text-align: center; color: red;"><i class="ace-icon fa fa-user red"></i> Input ID Number</td>
</tr>
</table>
</center>
</label>
<label class="block clearfix">
<span class="block input-icon input-icon-right">
<input class="form-control" id="tfname" placeholder="First Name" type="text">
<i class="ace-icon fa fa-user"></i>
</span>
</label>

<label class="block clearfix">
<span class="block input-icon input-icon-right">
<input class="form-control" id="tmname" placeholder="Middle Name" type="text">
<i class="ace-icon fa fa-user"></i>
</span>
</label>

<label class="block clearfix">
<span class="block input-icon input-icon-right">
<input class="form-control" id="tlname" placeholder="Last Name" type="text">
<i class="ace-icon fa fa-user"></i>
</span>
</label>

<label class="block clearfix">
<span class="block input-icon input-icon-right">
<input class="form-control" id="teacher_mobile_number" placeholder="+63" type="text">
<i class="ace-icon fa fa-user"></i>
</span>
</label>

<label class="block clearfix">
<span class="block input-icon input-icon-right">
<input class="form-control" id="pwt" placeholder="Password" type="password">
<i class="ace-icon fa fa-lock"></i>
</span>
</label>

<label class="block clearfix">
<span class="block input-icon input-icon-right">
<input class="form-control" id="pw2t" placeholder="Repeat Password" type="password" onchange="checkPasswordMatch2()">
<i class="ace-icon fa fa-retweet"></i>
</span>
<span class="pull-left" style="color:red; font-size: small" id="divCheckPasswordMatch2"></span>
</label>

<label class="block clearfix">
<span class="block input-icon input-icon-right">
<select class="form-control" id="designation">
<option value=""> Designation </option>
<option value="Part Time">Part Time</option>
<option value="Contract">Contract</option>
<option value="Regular">Regular</option>
</select>
</span>
</label>

<div class="" id="error_message_2"></div>

<div class="space-10"></div>

<div class="clearfix">
<button type="reset" class="width-12 pull-left btn btn-sm">
<i class="ace-icon fa fa-refresh"></i>
<span class="bigger-110">Reset</span>
</button>

<button id="register_tuser" type="button" class="width-60 pull-right btn btn-sm btn-success">
<span class="bigger-110">Register</span>
<i class="ace-icon fa fa-arrow-right icon-on-right"></i>
</button>
</div>
</fieldset>
</form>
</div>
</div>
</div>
</div>
</div>

<div class="toolbar center">
<a href="#" data-target="#login-box" class="back-to-login-link">
<i class="ace-icon fa fa-arrow-left"></i>
Back to login
</a>
</div>
</div><!-- /.widget-body -->
</div><!-- /.signup-box -->
</div><!-- /.position-relative -->
</div>

<span class="white" style='width:100%;display:block;text-align:center;'>Designed and Developed by</span><a href='#' class="orange bolder" style='width:100%;display:block;text-align:center;'>CIICT Student - NSU </a>

</div><!-- /.col -->
</div><!-- /.row -->
</div><!-- /.main-content -->
</div><!-- /.main-container -->

	<!--[if !IE]> -->
	<script type="text/javascript">
		window.jQuery || document.write("<script src='assets/js/jquery.min.js'>"+"<"+"/script>");
	</script>

	<script type="text/javascript">
	if('ontouchstart' in document.documentElement) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
	</script>

	<!-- inline scripts related to this page -->
	<script type="text/javascript">

		// For Page Submission
	$(document).ready(function(){ 
		getCourse();
		$("#suserid").mask("99-9-99999");
		$("#student_id_number").mask("99-9-99999");
		$("#tuserid").mask("F-99999");
		$("#contactnum").mask("+63999999999");
		$("#studentSignup").addClass("hidden");
		$("#teacherSignup").addClass("hidden");

		$("#magic_words_stud").keyup(function(){
			if($("#magic_words_stud").val() == ""){
				$("#errorAuthenticStud").html("Authentication required.");
				$("#studentSignup").addClass("hidden");
			}else if($("#magic_words_stud").val() !== '14344'){
				$("#errorAuthenticStud").html("Something wrong with the input");
				$("#studentSignup").addClass("hidden");
			}else{
				$("#errorAuthenticStud").html("");
				$("#studentSignup").removeClass("hidden");
			}
		});
	                                            
		$("#magic_words_teach").keyup(function(){
			if($("#magic_words_teach").val() == ""){
				$("#errorAuthenticTeach").html("Authentication required.");
				$("#teacherSignup").addClass("hidden");
			}else if($("#magic_words_teach").val() !== '14345'){
				$("#errorAuthenticTeach").html("Something wrong with the input");
				$("#teacherSignup").addClass("hidden");
			}else{
				$("#errorAuthenticTeach").html("");
				$("#teacherSignup").removeClass("hidden");
			}
		});

		$("#idnumberTeach").keyup(function(){
			var string = $(this).val();
			var result = /^[0-9]{1,5}$/.test(string);  
			if(result == true){
				$("#errorIDMessageTeach").html("");
				CheckifExistIDTeach();
			}else{
				$("#errorIDMessageTeach").html("<i class='ace-icon fa fa-times red'></i>  Please Input Five Digit Number.");
				$(this).val("");
			}
		});

		var items = ["b1","b2","b3","b5","b7","b9","ciict","b6","b11"];
		var rand = items[Math.floor(Math.random() * items.length)];
		$.backstretch("assets/images/background/"+rand+".jpg");

		var items2 = ["btn-warning","btn-danger","btn-inverse","btn-pink","btn-default","btn-info","btn-success"];
		var rand2 = items2[Math.floor(Math.random() * items2.length)];
		$("#btnStudent").addClass(rand2);
		var items3 = ["btn-warning","btn-danger","btn-inverse","btn-pink","btn-default","btn-info","btn-success"];
		var rand3 = items2[Math.floor(Math.random() * items3.length)];
		$("#btnTeacher").addClass(rand3);

		$("#btnTeacher").click(function(){
			$("#teacherRegistration").fadeIn();
			$("#studentRegistration").fadeOut();
		});

		$("#btnStudent").click(function(){
			$("#studentRegistration").fadeIn();
			$("#teacherRegistration").fadeOut();
		});
                    
                        
		$(document).on('click', '.toolbar a[data-target]', function(e) {
			e.preventDefault();
			var target = $(this).data('target');
			$('.widget-box.visible').removeClass('visible');//hide others
			$(target).addClass('visible');//show target
		});
                    

		$('#userid').focus();
		$(document).keypress(function(e) {
			if(e.which === 13) {
				$("#btn-login").click();
				e.preventDefault();
			}
		});
                    
		$("#pw2").keyup(checkPasswordMatch);
		$("#pw2t").keyup(checkPasswordMatch2);

		$("#accesstype").on('change', function(){
			if( $(this).val() == 1 ){
				$("#tuserid").val("");
				$("#password").val("");
				$("#suserid").parent().removeClass('hidden').addClass('block');
				$("#tuserid").parent().addClass('hidden').removeClass('block');
				$("#suserid").attr('placeholder','Student ID');
			}else{
				$("#suserid").val("");
				$("#password").val("");
				$("#tuserid").parent().removeClass('hidden').addClass('block');
				$("#suserid").parent().addClass('hidden').removeClass('block');
				$("#tuserid").attr('placeholder','Teacher ID');
			}
		});

		$("#course").on('change',function(){
			var y_level = $("#year_level").val();
			var sem = $("#semester").val();
			if(y_level == ""){
				$("#error_message").addClass('alert alert-block alert-danger');
				error = '<i class="ace-icon fa fa-times red"></i> Select Year Level';
				$("#error_message").html( error );
				$("#subjectList").addClass('hide');
			}else if(sem == ""){
				$("#error_message").addClass('alert alert-block alert-danger');
				error = '<i class="ace-icon fa fa-times red"></i> Select Year Level';
				$("#error_message").html( error );
				$("#subjectList").addClass('hide');
			}else{
				$("#subjectList").removeClass('hide');
				$("#error_message").removeClass('alert alert-block alert-danger');
				$("#error_message").html("");
				getSubjects(sem+'_'+y_level+'_'+$(this).val());
			}
		});

		$('select#semester').on('change', function() {
			var y_level = $("#year_level").val();
			var course = $("#course").val();
			if(y_level == ""){
				$("#error_message").addClass('alert alert-block alert-danger');
				error = '<i class="ace-icon fa fa-times red"></i> Select Year Level';
				$("#error_message").html( error );
				$("#subjectList").addClass('hide');
			}else if(course == ""){
				$("#error_message").addClass('alert alert-block alert-danger');
				error = '<i class="ace-icon fa fa-times red"></i> Select Course';
				$("#error_message").html( error );
				$("#subjectList").addClass('hide');
			}else{
				$("#subjectList").removeClass('hide');
				$("#error_message").removeClass('alert alert-block alert-danger');
				$("#error_message").html("");
				getSubjects($(this).val() +'_'+y_level+'_'+course);
			}
		});
                      
		$('select#year_level').on('change', function() {
			var sem = $("#semester").val();
			var course = $("#course").val();
			if(sem == ""){
				$("#error_message").addClass('alert alert-block alert-danger');
				error = '<i class="ace-icon fa fa-times red"></i> Select Semester';
				$("#error_message").html( error );
				$("#subjectList").addClass('hide');
			}else if(course == ""){
				$("#error_message").addClass('alert alert-block alert-danger');
				error = '<i class="ace-icon fa fa-times red"></i> Select Course';
				$("#error_message").html( error );
				$("#subjectList").addClass('hide');
			}else{
				$("#subjectList").removeClass('hide');
				$("#error_message").removeClass('alert alert-block alert-danger');
				$("#error_message").html("");
				getSubjects(sem+'_'+$(this).val()+'_'+course);
			}
		});
                    
		$("#register_user").on("click", function(){

			var idnumber = $("#student_id_number").val();
			var fname = $("#fname").val();
			var mname = $("#mname").val();
			var lname = $("#lname").val();
			var sem = $("#semester").val();
			var contact = $("#contactnum").val();
			var y_level = $("#year_level").val();
			var course = $("#course").val();
			var pw = $("#pw").val();


			var val = [];
			$(':checkbox:checked').each(function(i){
				val[i] = $(this).val();
			});

			if(idnumber == "" || fname == "" || lname == "" || mname == "" || sem == "" || y_level == "" || pw == ""){
				errorHandler("Required Fields is Empty","error_message");
				return false;
			}
			$.ajax({
				type:'post',
				url:'login/registerUser',
				data:{idnumber:idnumber, fname:fname, lname:lname, mname:mname, pw:pw, course: course, sem: sem, level:y_level, contnum: contact , selected_subjects: val },
				success: function(res){
					$("#student_id_number").val("");
					$("#fname").val("");
					$("#mname").val("");
					$("#lname").val("");
					$("#semester").val("");
					$("#year_level").val("");
					$("#course").val("");
					$("#pw").val("");
					$("#pw2").val("");
					$("#contactnum").val("");
					$("#divCheckPasswordMatch").addClass('hidden');
					$("#subjectList").addClass("hidden");
					$("#subjectList").removeClass("block");
					alert(res);
				}
			});

		});

                    
		$("#register_tuser").on("click", function(){
			if($("#idnumberTeach").val() == "" ){
				errorHandler("Required Fields is Empty","error_message_2");
				idnumberValueTeach = "";
			}else{
				idnumberValueTeach = 'F-'+$("#idnumberTeach").val();
			}
			var idnumber = idnumberValueTeach;
			var fname = $("#tfname").val();
			var mname = $("#tmname").val();
			var lname = $("#tlname").val();
			var desig = $("#designation").val();
			var pw = $("#pwt").val();
			var tmobile = $("#teacher_mobile_number").val();

			if(idnumber == "" || fname == "" || lname == "" || mname == "" || pw == "" || desig == ""){
				errorHandler("Required Fields is Empty","error_message_2");
				return false;
			}
			$.ajax({
				type:'post',
				url:'login/registerUserTeacher',
				data:{idnumber:idnumber, fname:fname, lname:lname, mname:mname, pw:pw, designation:desig, teachmobile: tmobile },
				success: function(res){
					$("#tidnumber").val("");
					$("#tfname").val("");
					$("#tmname").val("");
					$("#tlname").val("");
					$("#designation").val("");
					$("#pwt").val("");
					$("#pw2t").val("");
					$("#teacher_mobile_number").val("");
					alert(res);
				}
			});
		});

		$("#suserid").focus();
		$("#btn-login").on("click", function(){
			var userid= ($("#suserid").val() == "") ? $("#tuserid").val() : $("#suserid").val();	
			var password=$("#password").val();
			var accesstype = $("#accesstype").val();
			if (userid === '' || password === '' || accesstype == "")
			{
				return false;
			}
			$.ajax({
				type: "POST",
				url: "login/dologin",
				data: ({userid: userid, password: password}),
				success: function(response){
					if(response !== '')
					{
						window.location.href = response;
					}
						else
					{
						$(".no-margin").html('Invalid id number and password. Please try again.');
						$("#suserid").focus();
						bootbox.dialog({
								message: "<span class='bigger-110'>Invalid id number or password. Please try again.</span>",
								buttons:
							{
							"danger" :
							{
								"label" : "Ok I understand!",
								"className" : "btn-sm btn-danger",
							}
							}
						});
					}
				}
			});
		});
                    
		$("#contactnum").keydown(function (e) {
			numberOnly(e);
		});

		$("#teacher_mobile_number").keydown(function (e) {
			numberOnly(e);
		});

		// $("#student_id_number").on("keyup", function(){
		// 	console.log($(this).val());
		// });

	// 	function CheckifExistID(){
	// 	if($("#idnumber1").val() == "" || $("#idnumber2").val() == "" || $("#idnumber3").val() == ""){
	// 		$("#errorIDMessage").html("<i class='ace-icon fa fa-times red'></i> All fields required.");
	// 	}else{
	// 		idnumber = $("#idnumber1").val()+'-'+$("#idnumber2").val()+'-'+$("#idnumber3").val();
	// 		$.ajax({
	// 			type:'post',
	// 			url:'login/checkIDifExist',
	// 			data:{ idnumber:idnumber },
	// 				success: function(res){
	// 					$("#errorIDMessage").html(res);
	// 				}
	// 		});
	// 	}
	// }

});
				
	function CheckifExistIDTeach(){
		if($("#idnumberTeach").val() == "" ){
			$("#errorIDMessageTeach").html("<i class='ace-icon fa fa-times red'></i> All fields required.");
		}else{
			idnumber = 'F-'+$("#idnumberTeach").val();
			$.ajax({
				type:'post',
				url:'login/checkIDifExistTeach',
				data:{idnumber:idnumber },
				success: function(res){
					$("#errorIDMessageTeach").html(res);
				}
			});
		}
	}
                
	function numberOnly(e){
		// Allow: backspace, delete, tab, escape, enter and .
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
		// Allow: Ctrl+A, Command+A
		(e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
		// Allow: home, end, left, right, down, up
		(e.keyCode >= 35 && e.keyCode <= 40)) {
		// let it happen, don't do anything
		return;
		}
		// Ensure that it is a number and stop the keypress
		if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
		e.preventDefault();
		}
	}
                
	function checkPasswordMatch() {
		var password = $("#pw").val();
		var confirmPassword = $("#pw2").val();

		if (password != confirmPassword)
			$("#divCheckPasswordMatch").html("Passwords do not match!");
		else
			$("#divCheckPasswordMatch").html("Passwords match.");
	}

	function checkPasswordMatch2() {
		var password = $("#pwt").val();
		var confirmPassword = $("#pw2t").val();

		if (password != confirmPassword)
			$("#divCheckPasswordMatch2").html("Passwords do not match!");
		else
			$("#divCheckPasswordMatch2").html("Passwords match.");
	}
                
                function errorHandler(message,divid){
                    $("#"+divid).addClass('alert alert-block alert-danger');
                    error = '<i class="ace-icon fa fa-times red"></i> '+message+'';
                    $("#"+divid).html( error );
                }
                
                function getSubjects(param){
                       sem = param.split('_')[0];
                       yl = param.split('_')[1];
                       crs = param.split('_')[2];
                        $.ajax({
                           url:'login/getSubjects_Choice',
                           type:'post',
                           dataType:'json',
                           data:{sem:sem, ylevel:yl, course: crs},
                           success: function(data){
                               var list = '<div class="control-group label-primary white">\n\
                                                  <div class="space-6"></div>\n\
                                            <label class="control-label bolder"> &nbsp;&nbsp;&nbsp; Select Subject/s</label>';
                               $.each(data, function(key, val){
                                   list += '<div class="checkbox">\n\
                                                    <label>\n\
                                                            <input name="selector[]" id="ad_checkbox'+val.prospectid+'" class="ace" type="checkbox" value="'+val.prospectid+'_'+val.instructor+'">\n\
                                                            <span class="lbl"> '+val.subject_code+' <small>'+val.description+'</small> </span>\n\
                                                    </label>\n\
                                            </div>';
                                });
                               $("#subjectList").html(list+'<div class="space-6"></div></div>');
                           }
                        });
                }
                
                function getCourse(){
                        $.ajax({
                           url:'login/getCourse',
                           type:'post',
                           dataType:'json',
                           success: function(data){
                               var option = '<option value="">Course</option>';
                               $.each(data, function(key, val){
                                   option += '<option value="'+val.course_id+'"> '+val.course_name+'</option>';
                                });
                               $("#course").html(option);
                           }
                        });
                }

	</script>
	<!--[if !IE]> -->
	<script type="text/javascript">
		window.jQuery || document.write("<script src='assets/js/jquery.min.js'>"+"<"+"/script>");
	</script>

	<script type="text/javascript">
	if('ontouchstart' in document.documentElement) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
	</script>
	<script src="assets/js/bootstrap.min.js"></script>

	<!-- page specific plugin scripts -->

	<script src="assets/js/jquery-ui.custom.min.js"></script>
	<script src="assets/js/jquery.ui.touch-punch.min.js"></script>
	<script src="assets/js/bootbox.min.js"></script>
	<script src="assets/js/jquery.easypiechart.min.js"></script>
	<script src="assets/js/jquery.gritter.min.js"></script>
	<script src="assets/js/spin.min.js"></script>

	<script src="assets/js/jquery.maskedinput.js" type="text/javascript"></script>


	<script src="assets/js/jquery.backstretch.min.js"></script>

	<!-- ace scripts -->
	<script src="assets/js/ace-elements.min.js"></script>
	<script src="assets/js/ace.min.js"></script>
	<link rel="stylesheet" href="assets/css/ace.onpage-help.css" />
	<link rel="stylesheet" href="docs/assets/js/themes/sunburst.css" />

	<script type="text/javascript"> ace.vars['base'] = '..'; </script>
	<script src="assets/js/ace/ace.onpage-help.js"></script>
	<script src="docs/assets/js/rainbow.js"></script>
	<script src="docs/assets/js/language/generic.js"></script>
	<script src="docs/assets/js/language/html.js"></script>
	<script src="docs/assets/js/language/css.js"></script>
	<script src="docs/assets/js/language/javascript.js"></script>
</body>
</html>
