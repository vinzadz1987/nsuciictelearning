<div class="row">
	<div class="col-xs-12">
		<!-- PAGE CONTENT BEGINS
		<div class="page-header">
			<h1>
				Tables
				<small>
					<i class="ace-icon fa fa-angle-double-right"></i>
					Static &amp; Dynamic Tables
				</small>
			</h1>
		</div> /.page-header -->
			
			<div id="chartContainer" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
			
				<div class="row">
				<div class="col-xs-12">
					<div id="recent-box" class="widget-box transparent">
						<div class="widget-header">
							<h4 class="widget-title lighter smaller">
								<i class="ace-icon fa fa-rss orange"></i>QA Daily Scores
							</h4>
							
						</div>

						<div class="widget-body">
							<div class="widget-main padding-4">
								<div class="tab-content padding-8">
									<div class="tab-pane active" id="xfinityhome-tab">
										<!-- #section:pages/dashboard.xfinityhome -->
										<ul class="item-list ui-sortable" id="xfinityhome">
										<!-- PAGE CONTENT BEGINS -->
										
										<!-- #section:elements.tab -->
										<div class="tabbable">
											<ul id="myTab" class="nav nav-tabs">
												<li class="active">
													<a href="#months" data-toggle="tab">
														<i class="green ace-icon fa fa-home bigger-120"></i>
														Months
													</a>
												</li>

												<li class="">
													<a href="#weeks" data-toggle="tab">
														Weeks
														<span class="badge badge-danger"></span>
													</a>
												</li>

												<li class="">
														<div class="col-xs-8 col-sm-12">
															<!-- #section:plugins/date-time.daterangepicker -->
															<div class="input-group">
																<span class="input-group-addon">
																	<i class="fa fa-calendar bigger-110"></i>
																</span>

																<input type="text" id="id-date-range-picker-1" name="date-range-picker" class="form-control">
															</div>

															<!-- /section:plugins/date-time.daterangepicker -->
														</div>
												</li>
												
											</ul>

											<div class="tab-content">
												<div class="tab-pane active" id="months">
													<div class="col-xs-12">
													<table class="table table-striped table-bordered table-hover" id="sample-table-1">
														<thead>
															<tr>
																<th class="center">
																	<label class="position-relative">
																		<input type="checkbox" class="ace">
																		<span class="lbl"></span>
																	</label>
																</th>
																<th>Domain</th>
																<th>Price</th>
																<th class="hidden-480">Clicks</th>

																<th>
																	<i class="ace-icon fa fa-clock-o bigger-110 hidden-480"></i>
																	Update
																</th>
																<th class="hidden-480">Status</th>

																<th></th>
															</tr>
														</thead>

														<tbody>
															<tr>
																<td class="center">
																	<label class="position-relative">
																		<input type="checkbox" class="ace">
																		<span class="lbl"></span>
																	</label>
																</td>

																<td>
																	<a href="#">ace.com</a>
																</td>
																<td>$45</td>
																<td class="hidden-480">3,330</td>
																<td>Feb 12</td>

																<td class="hidden-480">
																	<span class="label label-sm label-warning">Expiring</span>
																</td>

																<td>
																	<div class="hidden-sm hidden-xs btn-group">
																		<button class="btn btn-xs btn-success">
																			<i class="ace-icon fa fa-check bigger-120"></i>
																		</button>

																		<button class="btn btn-xs btn-info">
																			<i class="ace-icon fa fa-pencil bigger-120"></i>
																		</button>

																		<button class="btn btn-xs btn-danger">
																			<i class="ace-icon fa fa-trash-o bigger-120"></i>
																		</button>

																		<button class="btn btn-xs btn-warning">
																			<i class="ace-icon fa fa-flag bigger-120"></i>
																		</button>
																	</div>

																	<div class="hidden-md hidden-lg">
																		<div class="inline position-relative">
																			<button data-position="auto" data-toggle="dropdown" class="btn btn-minier btn-primary dropdown-toggle">
																				<i class="ace-icon fa fa-cog icon-only bigger-110"></i>
																			</button>

																			<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
																				<li>
																					<a title="" data-rel="tooltip" class="tooltip-info" href="#" data-original-title="View">
																						<span class="blue">
																							<i class="ace-icon fa fa-search-plus bigger-120"></i>
																						</span>
																					</a>
																				</li>

																				<li>
																					<a title="" data-rel="tooltip" class="tooltip-success" href="#" data-original-title="Edit">
																						<span class="green">
																							<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
																						</span>
																					</a>
																				</li>

																				<li>
																					<a title="" data-rel="tooltip" class="tooltip-error" href="#" data-original-title="Delete">
																						<span class="red">
																							<i class="ace-icon fa fa-trash-o bigger-120"></i>
																						</span>
																					</a>
																				</li>
																			</ul>
																		</div>
																	</div>
																</td>
															</tr>

															<tr>
																<td class="center">
																	<label class="position-relative">
																		<input type="checkbox" class="ace">
																		<span class="lbl"></span>
																	</label>
																</td>

																<td>
																	<a href="#">base.com</a>
																</td>
																<td>$35</td>
																<td class="hidden-480">2,595</td>
																<td>Feb 18</td>

																<td class="hidden-480">
																	<span class="label label-sm label-success">Registered</span>
																</td>

																<td>
																	<div class="hidden-sm hidden-xs btn-group">
																		<button class="btn btn-xs btn-success">
																			<i class="ace-icon fa fa-check bigger-120"></i>
																		</button>

																		<button class="btn btn-xs btn-info">
																			<i class="ace-icon fa fa-pencil bigger-120"></i>
																		</button>

																		<button class="btn btn-xs btn-danger">
																			<i class="ace-icon fa fa-trash-o bigger-120"></i>
																		</button>

																		<button class="btn btn-xs btn-warning">
																			<i class="ace-icon fa fa-flag bigger-120"></i>
																		</button>
																	</div>

																	<div class="hidden-md hidden-lg">
																		<div class="inline position-relative">
																			<button data-position="auto" data-toggle="dropdown" class="btn btn-minier btn-primary dropdown-toggle">
																				<i class="ace-icon fa fa-cog icon-only bigger-110"></i>
																			</button>

																			<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
																				<li>
																					<a title="" data-rel="tooltip" class="tooltip-info" href="#" data-original-title="View">
																						<span class="blue">
																							<i class="ace-icon fa fa-search-plus bigger-120"></i>
																						</span>
																					</a>
																				</li>

																				<li>
																					<a title="" data-rel="tooltip" class="tooltip-success" href="#" data-original-title="Edit">
																						<span class="green">
																							<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
																						</span>
																					</a>
																				</li>

																				<li>
																					<a title="" data-rel="tooltip" class="tooltip-error" href="#" data-original-title="Delete">
																						<span class="red">
																							<i class="ace-icon fa fa-trash-o bigger-120"></i>
																						</span>
																					</a>
																				</li>
																			</ul>
																		</div>
																	</div>
																</td>
															</tr>

															<tr>
																<td class="center">
																	<label class="position-relative">
																		<input type="checkbox" class="ace">
																		<span class="lbl"></span>
																	</label>
																</td>

																<td>
																	<a href="#">max.com</a>
																</td>
																<td>$60</td>
																<td class="hidden-480">4,400</td>
																<td>Mar 11</td>

																<td class="hidden-480">
																	<span class="label label-sm label-warning">Expiring</span>
																</td>

																<td>
																	<div class="hidden-sm hidden-xs btn-group">
																		<button class="btn btn-xs btn-success">
																			<i class="ace-icon fa fa-check bigger-120"></i>
																		</button>

																		<button class="btn btn-xs btn-info">
																			<i class="ace-icon fa fa-pencil bigger-120"></i>
																		</button>

																		<button class="btn btn-xs btn-danger">
																			<i class="ace-icon fa fa-trash-o bigger-120"></i>
																		</button>

																		<button class="btn btn-xs btn-warning">
																			<i class="ace-icon fa fa-flag bigger-120"></i>
																		</button>
																	</div>

																	<div class="hidden-md hidden-lg">
																		<div class="inline position-relative">
																			<button data-position="auto" data-toggle="dropdown" class="btn btn-minier btn-primary dropdown-toggle">
																				<i class="ace-icon fa fa-cog icon-only bigger-110"></i>
																			</button>

																			<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
																				<li>
																					<a title="" data-rel="tooltip" class="tooltip-info" href="#" data-original-title="View">
																						<span class="blue">
																							<i class="ace-icon fa fa-search-plus bigger-120"></i>
																						</span>
																					</a>
																				</li>

																				<li>
																					<a title="" data-rel="tooltip" class="tooltip-success" href="#" data-original-title="Edit">
																						<span class="green">
																							<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
																						</span>
																					</a>
																				</li>

																				<li>
																					<a title="" data-rel="tooltip" class="tooltip-error" href="#" data-original-title="Delete">
																						<span class="red">
																							<i class="ace-icon fa fa-trash-o bigger-120"></i>
																						</span>
																					</a>
																				</li>
																			</ul>
																		</div>
																	</div>
																</td>
															</tr>

															<tr>
																<td class="center">
																	<label class="position-relative">
																		<input type="checkbox" class="ace">
																		<span class="lbl"></span>
																	</label>
																</td>

																<td>
																	<a href="#">best.com</a>
																</td>
																<td>$75</td>
																<td class="hidden-480">6,500</td>
																<td>Apr 03</td>

																<td class="hidden-480">
																	<span class="label label-sm label-inverse arrowed-in">Flagged</span>
																</td>

																<td>
																	<div class="hidden-sm hidden-xs btn-group">
																		<button class="btn btn-xs btn-success">
																			<i class="ace-icon fa fa-check bigger-120"></i>
																		</button>

																		<button class="btn btn-xs btn-info">
																			<i class="ace-icon fa fa-pencil bigger-120"></i>
																		</button>

																		<button class="btn btn-xs btn-danger">
																			<i class="ace-icon fa fa-trash-o bigger-120"></i>
																		</button>

																		<button class="btn btn-xs btn-warning">
																			<i class="ace-icon fa fa-flag bigger-120"></i>
																		</button>
																	</div>

																	<div class="hidden-md hidden-lg">
																		<div class="inline position-relative">
																			<button data-position="auto" data-toggle="dropdown" class="btn btn-minier btn-primary dropdown-toggle">
																				<i class="ace-icon fa fa-cog icon-only bigger-110"></i>
																			</button>

																			<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
																				<li>
																					<a title="" data-rel="tooltip" class="tooltip-info" href="#" data-original-title="View">
																						<span class="blue">
																							<i class="ace-icon fa fa-search-plus bigger-120"></i>
																						</span>
																					</a>
																				</li>

																				<li>
																					<a title="" data-rel="tooltip" class="tooltip-success" href="#" data-original-title="Edit">
																						<span class="green">
																							<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
																						</span>
																					</a>
																				</li>

																				<li>
																					<a title="" data-rel="tooltip" class="tooltip-error" href="#" data-original-title="Delete">
																						<span class="red">
																							<i class="ace-icon fa fa-trash-o bigger-120"></i>
																						</span>
																					</a>
																				</li>
																			</ul>
																		</div>
																	</div>
																</td>
															</tr>

															<tr>
																<td class="center">
																	<label class="position-relative">
																		<input type="checkbox" class="ace">
																		<span class="lbl"></span>
																	</label>
																</td>

																<td>
																	<a href="#">pro.com</a>
																</td>
																<td>$55</td>
																<td class="hidden-480">4,250</td>
																<td>Jan 21</td>

																<td class="hidden-480">
																	<span class="label label-sm label-success">Registered</span>
																</td>

																<td>
																	<div class="hidden-sm hidden-xs btn-group">
																		<button class="btn btn-xs btn-success">
																			<i class="ace-icon fa fa-check bigger-120"></i>
																		</button>

																		<button class="btn btn-xs btn-info">
																			<i class="ace-icon fa fa-pencil bigger-120"></i>
																		</button>

																		<button class="btn btn-xs btn-danger">
																			<i class="ace-icon fa fa-trash-o bigger-120"></i>
																		</button>

																		<button class="btn btn-xs btn-warning">
																			<i class="ace-icon fa fa-flag bigger-120"></i>
																		</button>
																	</div>

																	<div class="hidden-md hidden-lg">
																		<div class="inline position-relative">
																			<button data-position="auto" data-toggle="dropdown" class="btn btn-minier btn-primary dropdown-toggle">
																				<i class="ace-icon fa fa-cog icon-only bigger-110"></i>
																			</button>

																			<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
																				<li>
																					<a title="" data-rel="tooltip" class="tooltip-info" href="#" data-original-title="View">
																						<span class="blue">
																							<i class="ace-icon fa fa-search-plus bigger-120"></i>
																						</span>
																					</a>
																				</li>

																				<li>
																					<a title="" data-rel="tooltip" class="tooltip-success" href="#" data-original-title="Edit">
																						<span class="green">
																							<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
																						</span>
																					</a>
																				</li>

																				<li>
																					<a title="" data-rel="tooltip" class="tooltip-error" href="#" data-original-title="Delete">
																						<span class="red">
																							<i class="ace-icon fa fa-trash-o bigger-120"></i>
																						</span>
																					</a>
																				</li>
																			</ul>
																		</div>
																	</div>
																</td>
															</tr>
														</tbody>
													</table>
													</div>
												</div>

												<div class="tab-pane" id="weeks">
													<div class="col-xs-12">
														<table class="table table-striped table-bordered table-hover" id="sample-table-1">
															<thead>
																<tr>
																	<th class="center">
																		<label class="position-relative">
																			<input type="checkbox" class="ace">
																			<span class="lbl"></span>
																		</label>
																	</th>
																	<th>Domain</th>
																	<th>Price</th>
																	<th class="hidden-480">Clicks</th>

																	<th>
																		<i class="ace-icon fa fa-clock-o bigger-110 hidden-480"></i>
																		Update
																	</th>
																	<th class="hidden-480">Status</th>

																	<th></th>
																</tr>
															</thead>

															<tbody>
																<tr>
																	<td class="center">
																		<label class="position-relative">
																			<input type="checkbox" class="ace">
																			<span class="lbl"></span>
																		</label>
																	</td>

																	<td>
																		<a href="#">base.com</a>
																	</td>
																	<td>$35</td>
																	<td class="hidden-480">2,595</td>
																	<td>Feb 18</td>

																	<td class="hidden-480">
																		<span class="label label-sm label-success">Registered</span>
																	</td>

																	<td>
																		<div class="hidden-sm hidden-xs btn-group">
																			<button class="btn btn-xs btn-success">
																				<i class="ace-icon fa fa-check bigger-120"></i>
																			</button>

																			<button class="btn btn-xs btn-info">
																				<i class="ace-icon fa fa-pencil bigger-120"></i>
																			</button>

																			<button class="btn btn-xs btn-danger">
																				<i class="ace-icon fa fa-trash-o bigger-120"></i>
																			</button>

																			<button class="btn btn-xs btn-warning">
																				<i class="ace-icon fa fa-flag bigger-120"></i>
																			</button>
																		</div>

																		<div class="hidden-md hidden-lg">
																			<div class="inline position-relative">
																				<button data-position="auto" data-toggle="dropdown" class="btn btn-minier btn-primary dropdown-toggle">
																					<i class="ace-icon fa fa-cog icon-only bigger-110"></i>
																				</button>

																				<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
																					<li>
																						<a title="" data-rel="tooltip" class="tooltip-info" href="#" data-original-title="View">
																							<span class="blue">
																								<i class="ace-icon fa fa-search-plus bigger-120"></i>
																							</span>
																						</a>
																					</li>

																					<li>
																						<a title="" data-rel="tooltip" class="tooltip-success" href="#" data-original-title="Edit">
																							<span class="green">
																								<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
																							</span>
																						</a>
																					</li>

																					<li>
																						<a title="" data-rel="tooltip" class="tooltip-error" href="#" data-original-title="Delete">
																							<span class="red">
																								<i class="ace-icon fa fa-trash-o bigger-120"></i>
																							</span>
																						</a>
																					</li>
																				</ul>
																			</div>
																		</div>
																	</td>
																</tr>

																<tr>
																	<td class="center">
																		<label class="position-relative">
																			<input type="checkbox" class="ace">
																			<span class="lbl"></span>
																		</label>
																	</td>

																	<td>
																		<a href="#">max.com</a>
																	</td>
																	<td>$60</td>
																	<td class="hidden-480">4,400</td>
																	<td>Mar 11</td>

																	<td class="hidden-480">
																		<span class="label label-sm label-warning">Expiring</span>
																	</td>

																	<td>
																		<div class="hidden-sm hidden-xs btn-group">
																			<button class="btn btn-xs btn-success">
																				<i class="ace-icon fa fa-check bigger-120"></i>
																			</button>

																			<button class="btn btn-xs btn-info">
																				<i class="ace-icon fa fa-pencil bigger-120"></i>
																			</button>

																			<button class="btn btn-xs btn-danger">
																				<i class="ace-icon fa fa-trash-o bigger-120"></i>
																			</button>

																			<button class="btn btn-xs btn-warning">
																				<i class="ace-icon fa fa-flag bigger-120"></i>
																			</button>
																		</div>

																		<div class="hidden-md hidden-lg">
																			<div class="inline position-relative">
																				<button data-position="auto" data-toggle="dropdown" class="btn btn-minier btn-primary dropdown-toggle">
																					<i class="ace-icon fa fa-cog icon-only bigger-110"></i>
																				</button>

																				<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
																					<li>
																						<a title="" data-rel="tooltip" class="tooltip-info" href="#" data-original-title="View">
																							<span class="blue">
																								<i class="ace-icon fa fa-search-plus bigger-120"></i>
																							</span>
																						</a>
																					</li>

																					<li>
																						<a title="" data-rel="tooltip" class="tooltip-success" href="#" data-original-title="Edit">
																							<span class="green">
																								<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
																							</span>
																						</a>
																					</li>

																					<li>
																						<a title="" data-rel="tooltip" class="tooltip-error" href="#" data-original-title="Delete">
																							<span class="red">
																								<i class="ace-icon fa fa-trash-o bigger-120"></i>
																							</span>
																						</a>
																					</li>
																				</ul>
																			</div>
																		</div>
																	</td>
																</tr>

																<tr>
																	<td class="center">
																		<label class="position-relative">
																			<input type="checkbox" class="ace">
																			<span class="lbl"></span>
																		</label>
																	</td>

																	<td>
																		<a href="#">best.com</a>
																	</td>
																	<td>$75</td>
																	<td class="hidden-480">6,500</td>
																	<td>Apr 03</td>

																	<td class="hidden-480">
																		<span class="label label-sm label-inverse arrowed-in">Flagged</span>
																	</td>

																	<td>
																		<div class="hidden-sm hidden-xs btn-group">
																			<button class="btn btn-xs btn-success">
																				<i class="ace-icon fa fa-check bigger-120"></i>
																			</button>

																			<button class="btn btn-xs btn-info">
																				<i class="ace-icon fa fa-pencil bigger-120"></i>
																			</button>

																			<button class="btn btn-xs btn-danger">
																				<i class="ace-icon fa fa-trash-o bigger-120"></i>
																			</button>

																			<button class="btn btn-xs btn-warning">
																				<i class="ace-icon fa fa-flag bigger-120"></i>
																			</button>
																		</div>

																		<div class="hidden-md hidden-lg">
																			<div class="inline position-relative">
																				<button data-position="auto" data-toggle="dropdown" class="btn btn-minier btn-primary dropdown-toggle">
																					<i class="ace-icon fa fa-cog icon-only bigger-110"></i>
																				</button>

																				<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
																					<li>
																						<a title="" data-rel="tooltip" class="tooltip-info" href="#" data-original-title="View">
																							<span class="blue">
																								<i class="ace-icon fa fa-search-plus bigger-120"></i>
																							</span>
																						</a>
																					</li>

																					<li>
																						<a title="" data-rel="tooltip" class="tooltip-success" href="#" data-original-title="Edit">
																							<span class="green">
																								<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
																							</span>
																						</a>
																					</li>

																					<li>
																						<a title="" data-rel="tooltip" class="tooltip-error" href="#" data-original-title="Delete">
																							<span class="red">
																								<i class="ace-icon fa fa-trash-o bigger-120"></i>
																							</span>
																						</a>
																					</li>
																				</ul>
																			</div>
																		</div>
																	</td>
																</tr>

																<tr>
																	<td class="center">
																		<label class="position-relative">
																			<input type="checkbox" class="ace">
																			<span class="lbl"></span>
																		</label>
																	</td>

																	<td>
																		<a href="#">pro.com</a>
																	</td>
																	<td>$55</td>
																	<td class="hidden-480">4,250</td>
																	<td>Jan 21</td>

																	<td class="hidden-480">
																		<span class="label label-sm label-success">Registered</span>
																	</td>

																	<td>
																		<div class="hidden-sm hidden-xs btn-group">
																			<button class="btn btn-xs btn-success">
																				<i class="ace-icon fa fa-check bigger-120"></i>
																			</button>

																			<button class="btn btn-xs btn-info">
																				<i class="ace-icon fa fa-pencil bigger-120"></i>
																			</button>

																			<button class="btn btn-xs btn-danger">
																				<i class="ace-icon fa fa-trash-o bigger-120"></i>
																			</button>

																			<button class="btn btn-xs btn-warning">
																				<i class="ace-icon fa fa-flag bigger-120"></i>
																			</button>
																		</div>

																		<div class="hidden-md hidden-lg">
																			<div class="inline position-relative">
																				<button data-position="auto" data-toggle="dropdown" class="btn btn-minier btn-primary dropdown-toggle">
																					<i class="ace-icon fa fa-cog icon-only bigger-110"></i>
																				</button>

																				<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
																					<li>
																						<a title="" data-rel="tooltip" class="tooltip-info" href="#" data-original-title="View">
																							<span class="blue">
																								<i class="ace-icon fa fa-search-plus bigger-120"></i>
																							</span>
																						</a>
																					</li>

																					<li>
																						<a title="" data-rel="tooltip" class="tooltip-success" href="#" data-original-title="Edit">
																							<span class="green">
																								<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
																							</span>
																						</a>
																					</li>

																					<li>
																						<a title="" data-rel="tooltip" class="tooltip-error" href="#" data-original-title="Delete">
																							<span class="red">
																								<i class="ace-icon fa fa-trash-o bigger-120"></i>
																							</span>
																						</a>
																					</li>
																				</ul>
																			</div>
																		</div>
																	</td>
																</tr>
															</tbody>
														</table>
													</div>
												</div>

												<div class="tab-pane" id="dropdown1">
													<p>Etsy mixtape wayfarers, ethical wes anderson tofu before they sold out mcsweeney's organic lomo retro fanny pack lo-fi farm-to-table readymade.</p>
												</div>

												<div class="tab-pane" id="dropdown2">
													<p>Trust fund seitan letterpress, keytar raw denim keffiyeh etsy art party before they sold out master cleanse gluten-free squid scenester freegan cosby sweater. Fanny pack portland seitan DIY, art party locavore wolf cliche high life echo park Austin.</p>
												</div>
											</div>
										</div>

									<!-- /section:elements.tab -->

										<!-- PAGE CONTENT ENDS -->
										</ul>

										<!-- /section:pages/dashboard.xfinityhome -->
									</div>

									<div class="tab-pane" id="member-tab">
										<!-- #section:pages/dashboard.members -->
										<div class="clearfix">
											<div class="itemdiv memberdiv">
												<div class="user">
													<img src="../assets/avatars/user.jpg" alt="Bob Doe's avatar">
												</div>

												<div class="body">
													<div class="name">
														<a href="#">Bob Doe</a>
													</div>

													<div class="time">
														<i class="ace-icon fa fa-clock-o"></i>
														<span class="green">20 min</span>
													</div>

													<div>
														<span class="label label-warning label-sm">pending</span>

														<div class="inline position-relative">
															<button data-position="auto" data-toggle="dropdown" class="btn btn-minier btn-yellow btn-no-border dropdown-toggle">
																<i class="ace-icon fa fa-angle-down icon-only bigger-120"></i>
															</button>

															<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
																<li>
																	<a title="" data-rel="tooltip" class="tooltip-success" href="#" data-original-title="Approve">
																		<span class="green">
																			<i class="ace-icon fa fa-check bigger-110"></i>
																		</span>
																	</a>
																</li>

																<li>
																	<a title="" data-rel="tooltip" class="tooltip-warning" href="#" data-original-title="Reject">
																		<span class="orange">
																			<i class="ace-icon fa fa-times bigger-110"></i>
																		</span>
																	</a>
																</li>

																<li>
																	<a title="" data-rel="tooltip" class="tooltip-error" href="#" data-original-title="Delete">
																		<span class="red">
																			<i class="ace-icon fa fa-trash-o bigger-110"></i>
																		</span>
																	</a>
																</li>
															</ul>
														</div>
													</div>
												</div>
											</div>

											<div class="itemdiv memberdiv">
												<div class="user">
													<img src="../assets/avatars/avatar2.png" alt="Joe Doe's avatar">
												</div>

												<div class="body">
													<div class="name">
														<a href="#">Joe Doe</a>
													</div>

													<div class="time">
														<i class="ace-icon fa fa-clock-o"></i>
														<span class="green">1 hour</span>
													</div>

													<div>
														<span class="label label-warning label-sm">pending</span>

														<div class="inline position-relative">
															<button data-position="auto" data-toggle="dropdown" class="btn btn-minier btn-yellow btn-no-border dropdown-toggle">
																<i class="ace-icon fa fa-angle-down icon-only bigger-120"></i>
															</button>

															<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
																<li>
																	<a title="" data-rel="tooltip" class="tooltip-success" href="#" data-original-title="Approve">
																		<span class="green">
																			<i class="ace-icon fa fa-check bigger-110"></i>
																		</span>
																	</a>
																</li>

																<li>
																	<a title="" data-rel="tooltip" class="tooltip-warning" href="#" data-original-title="Reject">
																		<span class="orange">
																			<i class="ace-icon fa fa-times bigger-110"></i>
																		</span>
																	</a>
																</li>

																<li>
																	<a title="" data-rel="tooltip" class="tooltip-error" href="#" data-original-title="Delete">
																		<span class="red">
																			<i class="ace-icon fa fa-trash-o bigger-110"></i>
																		</span>
																	</a>
																</li>
															</ul>
														</div>
													</div>
												</div>
											</div>

											<div class="itemdiv memberdiv">
												<div class="user">
													<img src="../assets/avatars/avatar.png" alt="Jim Doe's avatar">
												</div>

												<div class="body">
													<div class="name">
														<a href="#">Jim Doe</a>
													</div>

													<div class="time">
														<i class="ace-icon fa fa-clock-o"></i>
														<span class="green">2 hour</span>
													</div>

													<div>
														<span class="label label-warning label-sm">pending</span>

														<div class="inline position-relative">
															<button data-position="auto" data-toggle="dropdown" class="btn btn-minier btn-yellow btn-no-border dropdown-toggle">
																<i class="ace-icon fa fa-angle-down icon-only bigger-120"></i>
															</button>

															<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
																<li>
																	<a title="" data-rel="tooltip" class="tooltip-success" href="#" data-original-title="Approve">
																		<span class="green">
																			<i class="ace-icon fa fa-check bigger-110"></i>
																		</span>
																	</a>
																</li>

																<li>
																	<a title="" data-rel="tooltip" class="tooltip-warning" href="#" data-original-title="Reject">
																		<span class="orange">
																			<i class="ace-icon fa fa-times bigger-110"></i>
																		</span>
																	</a>
																</li>

																<li>
																	<a title="" data-rel="tooltip" class="tooltip-error" href="#" data-original-title="Delete">
																		<span class="red">
																			<i class="ace-icon fa fa-trash-o bigger-110"></i>
																		</span>
																	</a>
																</li>
															</ul>
														</div>
													</div>
												</div>
											</div>

											<div class="itemdiv memberdiv">
												<div class="user">
													<img src="../assets/avatars/avatar5.png" alt="Alex Doe's avatar">
												</div>

												<div class="body">
													<div class="name">
														<a href="#">Alex Doe</a>
													</div>

													<div class="time">
														<i class="ace-icon fa fa-clock-o"></i>
														<span class="green">3 hour</span>
													</div>

													<div>
														<span class="label label-danger label-sm">blocked</span>
													</div>
												</div>
											</div>

											<div class="itemdiv memberdiv">
												<div class="user">
													<img src="../assets/avatars/avatar2.png" alt="Bob Doe's avatar">
												</div>

												<div class="body">
													<div class="name">
														<a href="#">Bob Doe</a>
													</div>

													<div class="time">
														<i class="ace-icon fa fa-clock-o"></i>
														<span class="green">6 hour</span>
													</div>

													<div>
														<span class="label label-success label-sm arrowed-in">approved</span>
													</div>
												</div>
											</div>

											<div class="itemdiv memberdiv">
												<div class="user">
													<img src="../assets/avatars/avatar3.png" alt="Susan's avatar">
												</div>

												<div class="body">
													<div class="name">
														<a href="#">Susan</a>
													</div>

													<div class="time">
														<i class="ace-icon fa fa-clock-o"></i>
														<span class="green">yesterday</span>
													</div>

													<div>
														<span class="label label-success label-sm arrowed-in">approved</span>
													</div>
												</div>
											</div>

											<div class="itemdiv memberdiv">
												<div class="user">
													<img src="../assets/avatars/avatar4.png" alt="Phil Doe's avatar">
												</div>

												<div class="body">
													<div class="name">
														<a href="#">Phil Doe</a>
													</div>

													<div class="time">
														<i class="ace-icon fa fa-clock-o"></i>
														<span class="green">2 days ago</span>
													</div>

													<div>
														<span class="label label-info label-sm arrowed-in arrowed-in-right">online</span>
													</div>
												</div>
											</div>

											<div class="itemdiv memberdiv">
												<div class="user">
													<img src="../assets/avatars/avatar1.png" alt="Alexa Doe's avatar">
												</div>

												<div class="body">
													<div class="name">
														<a href="#">Alexa Doe</a>
													</div>

													<div class="time">
														<i class="ace-icon fa fa-clock-o"></i>
														<span class="green">3 days ago</span>
													</div>

													<div>
														<span class="label label-success label-sm arrowed-in">approved</span>
													</div>
												</div>
											</div>
										</div>

										<div class="space-4"></div>

										<div class="center">
											<i class="ace-icon fa fa-users fa-2x green middle"></i>

											&nbsp;
											<a class="btn btn-sm btn-white btn-info" href="#">
												See all members &nbsp;
												<i class="ace-icon fa fa-arrow-right"></i>
											</a>
										</div>

										<div class="hr hr-double hr8"></div>

										<!-- /section:pages/dashboard.members -->
									</div><!-- /.#member-tab -->

									<div class="tab-pane" id="comment-tab">
										<!-- #section:pages/dashboard.comments -->
										<div class="comments ace-scroll" style="position: relative;"><div class="scroll-track" style="display: none;"><div class="scroll-bar"></div></div><div class="scroll-content" style="max-height: 300px;">
											<div class="itemdiv commentdiv">
												<div class="user">
													<img src="../assets/avatars/avatar.png" alt="Bob Doe's Avatar">
												</div>

												<div class="body">
													<div class="name">
														<a href="#">Bob Doe</a>
													</div>

													<div class="time">
														<i class="ace-icon fa fa-clock-o"></i>
														<span class="green">6 min</span>
													</div>

													<div class="text">
														<i class="ace-icon fa fa-quote-left"></i>
														Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque commodo massa sed ipsum porttitor facilisis …
													</div>
												</div>

												<div class="tools">
													<div class="inline position-relative">
														<button data-toggle="dropdown" class="btn btn-minier bigger btn-yellow dropdown-toggle">
															<i class="ace-icon fa fa-angle-down icon-only bigger-120"></i>
														</button>

														<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
															<li>
																<a title="" data-rel="tooltip" class="tooltip-success" href="#" data-original-title="Approve">
																	<span class="green">
																		<i class="ace-icon fa fa-check bigger-110"></i>
																	</span>
																</a>
															</li>

															<li>
																<a title="" data-rel="tooltip" class="tooltip-warning" href="#" data-original-title="Reject">
																	<span class="orange">
																		<i class="ace-icon fa fa-times bigger-110"></i>
																	</span>
																</a>
															</li>

															<li>
																<a title="" data-rel="tooltip" class="tooltip-error" href="#" data-original-title="Delete">
																	<span class="red">
																		<i class="ace-icon fa fa-trash-o bigger-110"></i>
																	</span>
																</a>
															</li>
														</ul>
													</div>
												</div>
											</div>

											<div class="itemdiv commentdiv">
												<div class="user">
													<img src="../assets/avatars/avatar1.png" alt="Jennifer's Avatar">
												</div>

												<div class="body">
													<div class="name">
														<a href="#">Jennifer</a>
													</div>

													<div class="time">
														<i class="ace-icon fa fa-clock-o"></i>
														<span class="blue">15 min</span>
													</div>

													<div class="text">
														<i class="ace-icon fa fa-quote-left"></i>
														Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque commodo massa sed ipsum porttitor facilisis …
													</div>
												</div>

												<div class="tools">
													<div class="action-buttons bigger-125">
														<a href="#">
															<i class="ace-icon fa fa-pencil blue"></i>
														</a>

														<a href="#">
															<i class="ace-icon fa fa-trash-o red"></i>
														</a>
													</div>
												</div>
											</div>

											<div class="itemdiv commentdiv">
												<div class="user">
													<img src="../assets/avatars/avatar2.png" alt="Joe's Avatar">
												</div>

												<div class="body">
													<div class="name">
														<a href="#">Joe</a>
													</div>

													<div class="time">
														<i class="ace-icon fa fa-clock-o"></i>
														<span class="orange">22 min</span>
													</div>

													<div class="text">
														<i class="ace-icon fa fa-quote-left"></i>
														Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque commodo massa sed ipsum porttitor facilisis …
													</div>
												</div>

												<div class="tools">
													<div class="action-buttons bigger-125">
														<a href="#">
															<i class="ace-icon fa fa-pencil blue"></i>
														</a>

														<a href="#">
															<i class="ace-icon fa fa-trash-o red"></i>
														</a>
													</div>
												</div>
											</div>

											<div class="itemdiv commentdiv">
												<div class="user">
													<img src="../assets/avatars/avatar3.png" alt="Rita's Avatar">
												</div>

												<div class="body">
													<div class="name">
														<a href="#">Rita</a>
													</div>

													<div class="time">
														<i class="ace-icon fa fa-clock-o"></i>
														<span class="red">50 min</span>
													</div>

													<div class="text">
														<i class="ace-icon fa fa-quote-left"></i>
														Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque commodo massa sed ipsum porttitor facilisis …
													</div>
												</div>

												<div class="tools">
													<div class="action-buttons bigger-125">
														<a href="#">
															<i class="ace-icon fa fa-pencil blue"></i>
														</a>

														<a href="#">
															<i class="ace-icon fa fa-trash-o red"></i>
														</a>
													</div>
												</div>
											</div>
										</div></div>

										<div class="hr hr8"></div>

										<div class="center">
											<i class="ace-icon fa fa-comments-o fa-2x green middle"></i>

											&nbsp;
											<a class="btn btn-sm btn-white btn-info" href="#">
												See all comments &nbsp;
												<i class="ace-icon fa fa-arrow-right"></i>
											</a>
										</div>

										<div class="hr hr-double hr8"></div>

										<!-- /section:pages/dashboard.comments -->
									</div>
								</div>
							</div><!-- /.widget-main -->
						</div><!-- /.widget-body -->
					</div><!-- /.widget-box -->
				</div><!-- /.span -->
			</div><!-- /.row -->
			
							
		<!-- PAGE CONTENT ENDS -->
	</div><!-- /.col -->
</div><!-- /.row -->
							
<script type="text/javascript">
$(function () {
        $('#chartContainer').highcharts({
            title: {
                text: 'Monthly Average Temperature',
                x: -20 //center
            },
            subtitle: {
                text: 'Source: WorldClimate.com',
                x: -20
            },
            xAxis: {
                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
            },
            yAxis: {
                title: {
                    text: 'Temperature (°C)'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                valueSuffix: '°C'
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            },
            series: [{
                name: 'Tokyo',
                data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
            }, {
                name: 'New York',
                data: [-0.2, 0.8, 5.7, 11.3, 17.0, 22.0, 24.8, 24.1, 20.1, 14.1, 8.6, 2.5]
            }, {
                name: 'Berlin',
                data: [-0.9, 0.6, 3.5, 8.4, 13.5, 17.0, 18.6, 17.9, 14.3, 9.0, 3.9, 1.0]
            }, {
                name: 'London',
                data: [3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]
            }]
        });
    });
</script>