<div class="row">
	<div class="col-xs-12">
		<div class="row">
				<div id="recent-box" class="widget-box transparent">
					<div class="widget-header">
						<h4 class="widget-title lighter smaller">
							<i class="ace-icon fa fa-rss orange"></i>Overall Summary Report
						</h4>
					</div>
				</div>
			<div class="tabbable">
				<ul class="nav nav-tabs" id="myTab">
					<li class="">
						<a data-toggle="tab" href="#monthly_tab">Monthly</a>
					</li>

					<li class="active">
						<a data-toggle="tab" href="#weekly_tab">Weekly</a>
					</li>

					<li class="dropdown">
						<a data-toggle="tab" href="#daily_tab">Daily</a>
					</li>
				</ul>
				<div class="tab-content">
					<div id="monthly_tab" class="tab-pane">

					</div>
					<div id="weekly_tab" class="tab-pane in active">
						<table id="grid_table"></table>
						<div id="grid_pager"></div>
					</div>
					<div id="daily_tab" class="tab-pane">
						daily
					</div>
				</div>
			</div>
		</div>


		<script type="text/javascript">
			var $path_base = "..";//this will be used for editurl parameter
		</script>

		<!-- PAGE CONTENT ENDS -->
	</div><!-- /.col -->
</div><!-- /.row -->

<style>
	.ui-th-div-ie {
		white-space:normal !important;
		height:auto !important;
	}
	th.ui-th-column div{
		white-space:normal !important;
		height:auto !important;
		padding:2px;
	}
</style>

<!-- page specific plugin scripts -->
<script src="assets/js/date-time/bootstrap-datepicker.min.js"></script>
<script src="assets/js/jqGrid/jquery.jqGrid.min.js"></script>
<script src="assets/js/jqGrid/i18n/grid.locale-en.js"></script>
<script src="assets/js/date-time/bootstrap-datepicker.min.js"></script>
<script src="assets/js/date-time/daterangepicker.min.js"></script>
<script src="assets/js/date-time/bootstrap-datetimepicker.min.js"></script>

<script>
$(document).ready(function(){
			jqGrid_control(jqGrid_data());	
			$(window).resize(function(){
				var _height = window.innerHeight - 325;		
				jQuery("#grid_table").setGridHeight(_height);		
			});
	});

	//==================================//
	// jqGrid Function Start            //
	//==================================//

	function jqGrid_data(data,ops,adv){
	mHeight = window.innerHeight - 325;
		model = {
			data_case:'daily_wfu', //use for switch cases
			data: data||'', // any Data
			ops: ops||'', // operation/option data
			adv: adv||'', // advance data
			table: '#grid_table',
			pager: '#grid_pager',
			container_url: 'reports/jqGrid_reports_ctrl/jqgrid_container',
			gridlocation: 'reports/jqGrid_reports_ctrl/load_jq_data/daily_wfu', // location of controller for gathering data from database
			headers: ['ID #','RT Login','Last Name','First Name','Supervisor','Account','Scheduled Hours','Billed Hours','Productivity','Avail Time + Busy While Chatting','Billed Hourds','WF Utilizaion','Avail Time','Billed Hourds','WF Utilizaion'], //header for the table
			names: ['uid','rt_login','lname','fname','supervisor','account','scheduled_hours','billed_hours','productivity','abwc','billed_hours2','wf_utilization','avail_time','billed_hours3','wf_utilization2'], //use for modifaction and functons. assign names for data in a row.
			index: 'true', //set true if index is equal to name. use for query. assign index for data in a row.
			widths: [80,80,150,150,150,150], //specific width of every column
			sortname: 'uid', //use to initialize sort
			//donot add other jqgrid function here. only in jqGridOptions
			jqGridOptions: { // other functions of jqGrid. if not set default will be use. === refer to main.js ===
				// boolean value must be string e.g. 'true' or 'false'
				editurl: 'reports/jqGrid_sample/edit_row',
				shrinkToFit:'false',
				altRows: 'true',
				height: mHeight				
			}
		};
		return model;
	}

	//==================================//
	// jqGrid Function End              //
	//==================================//

	jQuery(function($) {
	
		//datepicker plugin
		//link
		$('.date-picker').datepicker({
			autoclose: true,
			todayHighlight: true
		})
		//to translate the daterange picker, please copy the "examples/daterange-fr.js" contents here before initialization
		$('input[name=date-range-picker]').daterangepicker({
			'applyClass' : 'btn-sm btn-success',
			'cancelClass' : 'btn-sm btn-default',
			locale: {
				applyLabel: 'Apply',
				cancelLabel: 'Cancel',
			}
		})
	});

	function jqGrid_extension(table){
		setLabelColor('grid_table','uid,rt_login,lname,fname,supervisor,account','bg-warning');//main.js
		jQuery("#grid_table").jqGrid('setFrozenColumns');
		/*jQuery("#grid_table").jqGrid('setGroupHeaders', {
		  useColSpanStyle: true, 
		  groupHeaders:[
			{startColumnName: 'lname', numberOfColumns: 2, titleText: '<em>Price</em>'}
		  ]
		});*/
	}
	

	/*=======================================================================
				Search functions
	========================================================================*/

	$('#btn_search').click(function(e){
		e.preventDefault();

		var search = ($('#grid_search').val())?$('#grid_search').val():'';
		var byDate = ($('#grid_byDate').val())?$('#grid_byDate').val():'';
		var advance = ($('#grid_advance').val())?$('#grid_advance').val():'';

		gridData = {data:search,ops:byDate,adv:advance};
		searchOnGrid('reports/jqGrid_reports_ctrl','grid_table','daily_wfu',gridData);
		advanceSearch('close');
	})

	/*=======================================================================
				Chart Display Functions
	========================================================================*/
	$(function () {
        $('#chartContainer').highcharts({
            title: {
                text: 'QA Monthly Result',
                x: -20 //center
            },
            subtitle: {
                text: 'Source: WorldClimate.com',
                x: -20
            },
            xAxis: {
                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
            },
            yAxis: {
                title: {
                    text: 'Temperature (°C)'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                valueSuffix: '°C'
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            },
            series: [{
                name: 'Tokyo',
                data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
            }, {
                name: 'New York',
                data: [-0.2, 0.8, 5.7, 11.3, 17.0, 22.0, 24.8, 24.1, 20.1, 14.1, 8.6, 2.5]
            }, {
                name: 'Berlin',
                data: [-0.9, 0.6, 3.5, 8.4, 13.5, 17.0, 18.6, 17.9, 14.3, 9.0, 3.9, 1.0]
            }, {
                name: 'London',
                data: [3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]
            }]
        });
    });

</script>