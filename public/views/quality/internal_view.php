﻿<div class="row">
	<div class="col-xs-12">
		<div class="row">
			<table id="grid_table"></table>
			<div id="grid_pager"></div>
		</div>
		<script type="text/javascript">
			var $path_base = "..";//this will be used for editurl parameter
		</script>

		<!-- PAGE CONTENT ENDS -->
	</div><!-- /.col -->
</div><!-- /.row -->
<!-- page specific plugin scripts -->
<script src="assets/js/date-time/bootstrap-datepicker.min.js"></script>
<script src="assets/js/jqGrid/jquery.jqGrid.min.js"></script>
<script src="assets/js/jqGrid/i18n/grid.locale-en.js"></script>
<script src="assets/js/date-time/bootstrap-datepicker.min.js"></script>
<script src="assets/js/date-time/daterangepicker.min.js"></script>
<script src="assets/js/date-time/bootstrap-datetimepicker.min.js"></script>


<script>
	$(document).ready(function(){
		jqGrid_control(jqGrid_data());
	});
	//==================================//
	// jqGrid Function Start            //
	//==================================//
	function jqGrid_data(data,ops,adv){
		model = {
			data_case:'internal', //use for switch cases
			data: data||'', // any Data
			ops: ops||'', // operation/option data
			adv: adv||'', // advance data
			table: '#grid_table',
			//pager: '#grid_pager',
			container_url: 'reports/jqGrid_reports_ctrl/jqgrid_container',
			gridlocation: 'reports/jqGrid_reports_ctrl/load_jq_data/internal', // location of controller for gathering data from database
			headers: ['ID #','Agent Name','Monitoring Date','Evaluator','Supervisor','Flag','Chat ID','Program','Agent Tenure','Pages Names','Method','Reviewed','Opening Procedure','Complete and Accurate Information','Grammar / Word Choice','Tone / Enthusiasm','Chat Management','Follow Process and Procedure','Rebuttals','Sales Transistion','Chat Technique','Closing Procedure','Total Score'], //header for the table
			names: ['empid','Agent_Name','Monitor_Date','Evaluator','Supervisor','Flag','Chat_ID','Program','Agent_Tenure','Page_Names','`Method_(Side-by-Side)`','Reviewed','Opening_Procedure','Complete_and_Accurate_Information','Grammar_/_Word_Choice','Tone_/_Enthusiasm','Chat_Management','Follow_Process_and_Procedure','Rebuttals','Sales_Transition','Chat_Technique','Closing_Procedure','Total_Score'], //use for modifaction and functons. assign names for data in a row.
			index: ['empid','agent','mdate','evaluation','sup','flag','chat_id','program','tenure','pnames','method','reviewed','opening','complete','grammar','tone','management','follow','rebuttals','salestrans','chattech','closing','total'], //set true if index is equal to name. use for query. assign index for data in a row.
			widths: [80,200,180,180,180,80,80,80,120,140,150], //specific width of every column
			sortname: 'empid', //use to initialize sort
			autoHeight: 'true',
			//donot add other jqgrid function here. only in jqGridOptions
			jqGridOptions: { // other functions of jqGrid. if not set default will be use. === refer to main.js ===
				// boolean value must be string e.g. 'true' or 'false'
				shrinkToFit:'false',
				height: window.innerHeight - 325,
				altRows: 'true',
				rowNum: -1,
				caption:'QA Internal - '+<?php echo json_encode($this->main_model->account_title()); ?>,
				/*footerrow: 'true',
				userDataOnFooter: 'true'*/
			}
		};
		_grid_model_berfore = model;
		return model;
	}

	function beforeRequest_global(model){
		NProgress.start();
	}
	function loadComplete_global(model,data){
		setWidgetToolbarItems({
			exportToFile:
				{
					excel:[model,data,'json']
				}, 
			widgetButton:
				[
					{id:'graph1', classes:'', icon:'fa-bar-chart-o', text:'Show Graph'}
				]		
		});
		NProgress.done();
		_grid_model_loaded = model;
	}
	
	/*function loadComplete_global(tableName){
		NProgress.done();

		sched_hr= jQuery("#grid_table").jqGrid('getCol', 'scheduled_hours', false, 'sum');
		bild_hr= jQuery("#grid_table").jqGrid('getCol', 'billed_hours', false, 'sum');
		jQuery("#grid_table").jqGrid('footerData', 'set', { scheduled_hours: sched_hr});
		jQuery("#grid_table").jqGrid('footerData', 'set', { billed_hours: bild_hr});
	}*/

	//==================================//
	// jqGrid Function End              //
	//==================================//

	

	function jqGrid_extension(table){
		setLabelColor('grid_table','`id_#`,RT_Login,Last_Name,First_Name,supervisor,`Tenure_in_(YY/MM/DD)`,shift','bg-warning');//main.js
		jQuery("#grid_table").jqGrid('setFrozenColumns');		
	}
	

	/*=======================================================================
				Search functions
	========================================================================*/

	$('#btn_search').click(function(e){
		e.preventDefault();

		var search = ($('#grid_search').val())?$('#grid_search').val():'';
		var byDate = ($('#grid_byDate').val())?$('#grid_byDate').val():'';
		var advance = ($('#grid_advance').val())?$('#grid_advance').val():'';

		gridData = {data:search,ops:byDate,adv:advance};
		searchOnGrid('reports/jqGrid_reports_ctrl','grid_table','daily_wfu',gridData);
		
		searchData = {
			grid: 'grid_table',
			title: 'WFU - '+<?php echo json_encode($this->main_model->account_title()); ?>,
			search: search,
			date: byDate,
			field: advance,
		}
		//main.js
		jqGrid_caption(searchData);
	})



</script>
