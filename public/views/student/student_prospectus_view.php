<script type="text/javascript" src="assets/js/tableExport/tableExport.js"></script>
<script type="text/javascript" src="assets/js/tableExport/jquery.base64.js"></script>
<script type="text/javascript" src="assets/js/xepOnline.jqPlugin.js"></script>

<div class="col-sm-12">
<!-- <a href="#"  onclick="return xepOnline.Formatter.Format('prospectusTab',{render:'download'});"><span class="pull-left"><img src="http://letsemeng.gov.za/wp-content/uploads/2016/01/pdfIcon.png" style="height: 40px; width: 40px"></span></a> -->
<a href="javascript:void(0)" id="printMe"><span class="pull-left"><img src="http://findicons.com/files/icons/2152/snowish/128/printer.png" style="height: 40px; width: 40px"></span></a>
<div class="row">
<div id="prospectusTab">
	<center>
		<div>
			<table>
				<tr>
					<td>
						<img src="<?php echo base_url() ?>assets/images/Naval_State_University.png" style="height: 100px; width: 100px">
					</td>	
					<td style="text-align: center;">
						<h5>Republic of the Philippines<br><span style="font-family: Old English Text MT, Engravers Old English BT, Old English, Collins Old English, New Old English, serif; ">Naval State University</span><br>Naval, Biliran</h5>
						<h4>BACHELOR OF SCIENCE IN COMPUTER SCIENCE</h4>
					</td>	
					<td>
						<img src="<?php echo base_url() ?>assets/images/nsuciictlogo.jpg" style="height: 100px; width: 100px">
					</td>	
				</tr>
			</table>
		</div>
	</center>
    <div id='prospectusTable'>
	<div id="widget-container-col-1" class="col-xs-12 col-sm-12">
		<div style="float: left;">Revision No. 2</div>
		<div style="float: right;">Revised 2009-2010</div><br>
		<span><h4>Name: <span style="text-decoration: underline;"><?php print $this->session->userdata('firstname').' '.$this->session->userdata('middlename').' '.$this->session->userdata('lastname'); ?></span></h4></span>
			<div class="widget-box ui-sortable-handle" id="widget-box-2">
				<div class="widget-body">
					<div class="widget-main no-padding">
						<div class="col-sm-12">
							<div class="widget-box transparent">
								<div class="widget-header widget-header-flat">
									<h4 class="widget-title lighter blue">First Year - First Semester</h4>
								</div>

								<div class="widget-body">
									<div class="widget-main no-padding">
										<table class="table table-bordered table-striped" id="tbl_fyfs">
											<tr>
												<td><img src="assets/img/loading.gif"></td>
											</tr>
										</table>
									</div><!-- /.widget-main -->
								</div><!-- /.widget-body -->
							</div><!-- /.widget-box -->
						</div>

						<div class="col-sm-12">
							<div class="widget-box transparent">
								<div class="widget-header widget-header-flat">
									<h4 class="widget-title lighter blue">First Year - Second Semester</h4>
								</div>

								<div class="widget-body">
									<div class="widget-main no-padding">
										<table class="table table-bordered table-striped" id="tbl_fyss">
											<tr>
												<td><img src="assets/img/loading.gif"></td>
											</tr>
										</table>
									</div><!-- /.widget-main -->
								</div><!-- /.widget-body -->
							</div><!-- /.widget-box -->
						</div>
					</div>
				</div>
			</div>
		</div>

		<div id="widget-container-col-1" class="col-xs-12 col-sm-12 widget-container-col ui-sortable">
			<div class="widget-box ui-sortable-handle" id="widget-box-2">
				<div class="widget-body">
					<div class="widget-main no-padding">
						<div class="col-sm-12">
							<div class="widget-box transparent">
								<div class="widget-header widget-header-flat">
									<h4 class="widget-title lighter">Second Year - First Semester</h4>
								</div>

								<div class="widget-body">
								<div class="widget-main no-padding">
								<table class="table table-bordered table-striped" id="tbl_syfs">
									<tr>
										<td><img src="assets/img/loading.gif" ></td>
									</tr>
								</table>
								</div><!-- /.widget-main -->
								</div><!-- /.widget-body -->
							</div><!-- /.widget-box -->
						</div>
						<div class="col-sm-12">
							<div class="widget-box transparent">
								<div class="widget-header widget-header-flat">
									<h4 class="widget-title lighter green">Second Year - Second Semester</h4>
								</div>

								<div class="widget-body">
									<div class="widget-main no-padding">
										<table class="table table-bordered table-striped" id="tbl_syss">
											<tr>
												<td><img src="assets/img/loading.gif"></td>
											</tr>
										</table>
									</div><!-- /.widget-main -->
								</div><!-- /.widget-body -->
							</div><!-- /.widget-box -->
						</div>
					</div>
				</div>
			</div>
		</div>


		<div id="widget-container-col-1" class="col-xs-12 col-sm-12 widget-container-col ui-sortable">
			<div class="widget-box ui-sortable-handle" id="widget-box-2">
				<div class="widget-body">
					<div class="widget-main no-padding">
						<div class="col-sm-12">
							<div class="widget-box transparent">
								<div class="widget-header widget-header-flat">
									<h4 class="widget-title lighter">Third Year - First Semester</h4>
								</div>

								<div class="widget-body">
									<div class="widget-main no-padding">
										<table class="table table-bordered table-striped" id="tbl_tyfs">
											<tr>
												<td><img src="assets/img/loading.gif"></td>
											</tr>
										</table>
									</div><!-- /.widget-main -->
								</div><!-- /.widget-body -->
							</div><!-- /.widget-box -->
						</div>

					<div class="col-sm-12">
						<div class="widget-box transparent">
							<div class="widget-header widget-header-flat">
								<h4 class="widget-title lighter">Third Year -  Second Semester</h4>
							</div>

							<div class="widget-body">
								<div class="widget-main no-padding">
									<table class="table table-bordered table-striped" id="tbl_tyss">
										<tr>
											<td><img src="assets/img/loading.gif"></td>
										</tr>
									</table>
								</div><!-- /.widget-main -->
							</div><!-- /.widget-body -->
						</div><!-- /.widget-box -->
					</div>

					</div>
				</div>
			</div>
		</div>

		<div id="widget-container-col-1" class="col-xs-12 col-sm-12 widget-container-col ui-sortable">
			<div class="widget-box ui-sortable-handle" id="widget-box-2">
				<div class="widget-body">
					<div class="widget-main no-padding">

						<div class="col-sm-12">
							<div class="widget-box transparent">
								<div class="widget-header widget-header-flat">
									<h4 class="widget-title lighter">Fourth Year - First Semester</h4>
								</div>

								<div class="widget-body">
									<div class="widget-main no-padding">
										<table class="table table-bordered table-striped" id="tbl_fryfs">
											<tr>
												<td><img src="assets/img/loading.gif"></td>
											</tr>
										</table>
									</div><!-- /.widget-main -->
								</div><!-- /.widget-body -->
							</div><!-- /.widget-box -->
						</div>

						<div class="col-sm-12">
							<div class="widget-box transparent">
								<div class="widget-header widget-header-flat">
									<h4 class="widget-title lighter">Fourth Year - Second Semester</h4>
								</div>

								<div class="widget-body">
									<div class="widget-main no-padding">
										<table class="table table-bordered table-striped" id="tbl_fryss">
											<tr>
												<td><img src="assets/img/loading.gif"></td>
											</tr>
										</table>
									</div><!-- /.widget-main -->
								</div><!-- /.widget-body -->
							</div><!-- /.widget-box -->
						</div>

					</div>
				</div>
			</div>
		</div>

<div class="col-sm-4">
	<h5>General Education <span class="pull-right">- 64</span></h5>
	<h5>Basic ITE Core Courses <span class="pull-right">- 32</span></h5>
	<h5>ITE Professional Courses <span class="pull-right">- 48</span></h5>
	<h5>Electives <span class="pull-right">- 15</span></h5>
	<h5>OJT <span class="pull-right"> - 6</span></h5>
	<h5>Free Electives <span class="pull-right"> - 6</span></h5>
	<h5>P.E <span class="pull-right">- 8</span></h5>
	<h5>NSTP <span class="pull-right">- (6)</span></h5>
	<hr>
	<h5>Total No. of Units <span class="pull-right">177</span></h5>
</div>
</div>
</div>
        
        
<div id="lessonTab" style="display: none">
	<div class="col-xs-12">
		<div class="widget-box transparent">
			<div class="widget-header widget-header-flat">
				<h4 class="widget-title lighter">
					<i class="ace-icon fa fa-bookmark blue"></i>
					<span id="lesson_name"><span id="subjectCode"></span> <small id="subjectDesc"></small> </span>
				</h4>
				<div class="widget-toolbar">
					<a href="#" data-action="collapse">
						<i class="ace-icon fa fa-chevron-up"></i>
					</a>
				</div>
			</div>
		<div class="space-6"></div>
		<div class="space-6"></div>
			<div class="widget-body">
				<div class="widget-body-inner" style="display: block;">
					<div class="widget-main no-padding">
						<div id="faq-tab-1" class="tab-pane fade in active">
							<div id="faq-list-1" class="panel-group accordion-style1 accordion-style2"></div>
						</div>
					</div>
					<!-- /.widget-main -->
				</div>
			</div><!-- /.widget-body -->
		</div>
	</div>
</div>
</div>
<!-- /.col -->
<div id="editor"></div>
<script>
	$(document).ready(function(){
		$("#create_subject").click(function(){
			dialogBoxModalAdd('Create Subjects');
		});
		$("#printMe").click(function(){
			$(this).hide();
			window.print();
		});
		$("#navbar, #breadcrumbs, #sidebar, .footer-content").addClass('hidden').attr('style','position:absolute');
		$(".page-content").attr('style','position: absolute; left: 0px');
		yoursubjects();

		// var doc = new jsPDF();
		// var specialElementHandlers = {
		//     '#editor': function (element, renderer) {
		//         return true;
		//     }
		// };

		// $('#pdfMe').click(function () {
		//     doc.fromHTML($('#prospectusTab').html(), 15, 15, {
		//         'width': 170,
		//             'elementHandlers': specialElementHandlers
		//     });
		//     doc.save('prospectus.pdf');
		// });


	});

	function yoursubjects(){
		$.ajax({
			url:'student/students/getSubjects',
			type:'post',
			dataType:'json',
			success:function(data){
				var thead = '<thead class="thin-border-bottom success">\n\
							<tr>\n\
								<th>SUBJECTNO</th>\n\
								<th>DESCRIPTIVE_TITLE</th>\n\
								<th>PRE_REQUISITE</th>\n\
								<th>LEC</th>\n\
								<th>LAB</th>\n\
								<th>UNITS</th>\n\
								<th>RATINGS</th>\n\
							</tr>\n\
				</thead>';
				var trcontent1 = thead +'<tbody>';  
				var trcontent2 = thead +'<tbody>';
				var trcontent3 = thead +'<tbody>';
				var trcontent4 = thead +'<tbody>'; 
				var trcontent5 = thead +'<tbody>'; 
				var trcontent6 = thead +'<tbody>';
				var trcontent7 = thead +'<tbody>'; 
				var trcontent8 = thead +'<tbody>';
				var totalUnitsFirstFirst = totalUnitsFirstSecond = totalUnitsSecondFirst = totalUnitsSecondSecond = totalUnitsThirdFirst = totalUnitsThirdSecond = totalUnitsFourthFirst = totalUnitsFourthSecond = 0; 
				$.each(data, function (key, val){
					var snw = 'style="width:150px"'; 
					var dtw = 'style="width:400px"';
					if(val.year == 1 && val.sem == 1){
						identsubs = subjectenrolled(val.students_id);
						if(identsubs !=""){
							hasactions = actions(1,val.countlessons,val.prospectid,val.instructor,val.subject_code,val.description, val.status);
						}else{
							hasactions = actions(2,val.countlessons,val.prospectid,val.instructor,val.subject_code,val.description, val.status,val.students_id);
						}
						trcontent1 += insideTable(val,identsubs,hasactions,snw,dtw,val.photo);
						totalUnitsFirstFirst += parseInt(val.units);
                        showGrades(val.prospectid,val.year, val.sem);
					}else if(val.year == 1 && val.sem == 2){
						identsubs = subjectenrolled(val.students_id);
						if(identsubs !=""){
							hasactions = actions(1,val.countlessons,val.prospectid,val.instructor,val.subject_code,val.description, val.status);
						}else{
							hasactions = actions(2,val.countlessons,val.prospectid,val.instructor,val.subject_code,val.description, val.status,val.students_id);
						}
						trcontent2 += insideTable(val,identsubs,hasactions,snw,dtw,val.photo);
						totalUnitsFirstSecond += parseInt(val.units);
                        showGrades(val.prospectid,val.year, val.sem);
					}else if(val.year == 2 && val.sem == 1){
						identsubs = subjectenrolled(val.students_id);
						if(identsubs !=""){
							hasactions = actions(1,val.countlessons,val.prospectid,val.instructor,val.subject_code,val.description, val.status);
						}else{
							hasactions = actions(2,val.countlessons,val.prospectid,val.instructor,val.subject_code,val.description, val.status,val.students_id);
						}
						trcontent3 += insideTable(val,identsubs,hasactions,snw,dtw,val.photo);
						totalUnitsSecondFirst += parseInt(val.units);
					}else if(val.year == 2 && val.sem == 2){
						identsubs = subjectenrolled(val.students_id);
						if(identsubs !=""){
							hasactions = actions(1,val.countlessons,val.prospectid,val.instructor,val.subject_code,val.description, val.status);
						}else{
							hasactions = actions(2,val.countlessons,val.prospectid,val.instructor,val.subject_code,val.description, val.status,val.students_id);
						}
						trcontent4 += insideTable(val,identsubs,hasactions,snw,dtw,val.photo);
						totalUnitsSecondSecond += parseInt(val.units);
					}else if(val.year == 3 && val.sem == 1){
						identsubs = subjectenrolled(val.students_id);
						if(identsubs !=""){
							hasactions = actions(1,val.countlessons,val.prospectid,val.instructor,val.subject_code,val.description, val.status);
						}else{
							hasactions = actions(2,val.countlessons,val.prospectid,val.instructor,val.subject_code,val.description, val.status,val.students_id);
						}
						trcontent5 += insideTable(val,identsubs,hasactions,snw,dtw,val.photo);
						totalUnitsThirdFirst += parseInt(val.units);
					}else if(val.year == 3 && val.sem == 2){
						identsubs = subjectenrolled(val.students_id);
						if(identsubs !=""){
							hasactions = actions(1,val.countlessons,val.prospectid,val.instructor,val.subject_code,val.description, val.status);
						}else{
							hasactions = actions(2,val.countlessons,val.prospectid,val.instructor,val.subject_code,val.description, val.status,val.students_id);
						}
						trcontent6 += insideTable(val,identsubs,hasactions,snw,dtw,val.photo);
						totalUnitsThirdSecond += parseInt(val.units);
					}else if(val.year == 4 && val.sem == 1){
						identsubs = subjectenrolled(val.students_id);
						if(identsubs !=""){
							hasactions = actions(1,val.countlessons,val.prospectid,val.instructor,val.subject_code,val.description, val.status);
						}else{
							hasactions = actions(2,val.countlessons,val.prospectid,val.instructor,val.subject_code,val.description, val.status,val.students_id);
						}
						trcontent7 += insideTable(val,identsubs,hasactions,snw,dtw,val.photo);
						totalUnitsFourthFirst += parseInt(val.units);
					}else if(val.year == 4 && val.sem == 2){
						identsubs = subjectenrolled(val.students_id);
						if(identsubs !=""){
							hasactions = actions(1,val.countlessons,val.prospectid,val.instructor,val.subject_code,val.description, val.status);
						}else{
							hasactions = actions(2,val.countlessons,val.prospectid,val.instructor,val.subject_code,val.description, val.status,val.students_id);
						}
						trcontent8 += insideTable(val,identsubs,hasactions,snw,dtw,val.photo);
						totalUnitsFourthSecond += parseInt(val.units);
					}
				});

				$('table#tbl_fyfs').html(trcontent1 +'</tbody>').append('<tfoot class="thin-border-bottom success">\n\
							<tr>\n\
								<th style="width: 100px;" class="hidden-480"></th>\n\
								<th></th>\n\
								<th>SUBTOTAL</th>\n\
								<th></th>\n\
								<th></th>\n\
								<th id="countUnitsFisFis">Total UNITS</th>\n\
								<th></th>\n\
								</tr>\n\
							</tfoot>');
                        $('table#tbl_fyfs tfoot').find('th#countUnitsFisFis').text(totalUnitsFirstFirst);
                        $('table#tbl_fyss').html(trcontent2 +'</tbody>').append('<tfoot class="thin-border-bottom success">\n\
							<tr>\n\
								<th style="width: 100px;" class="hidden-480"></th>\n\
								<th></th>\n\
								<th>SUBTOTAL</th>\n\
								<th></th>\n\
								<th></th>\n\
								<th id="countUnitsFisSec">Total UNITS</th>\n\
								<th></th>\n\
								</tr>\n\
							</tfoot>');
                        $('table#tbl_fyss tfoot').find('th#countUnitsFisSec').text(totalUnitsFirstSecond);
                        $('table#tbl_syfs').html(trcontent3 +'</tbody>').append('<tfoot class="thin-border-bottom success">\n\
							<tr>\n\
								<th style="width: 100px;" class="hidden-480"></th>\n\
								<th></th>\n\
								<th>SUBTOTAL</th>\n\
								<th></th>\n\
								<th></th>\n\
								<th id="countUnitsSecondFirst">Total UNITS</th>\n\
								<th></th>\n\
								</tr>\n\
							</tfoot>');
                        $('table#tbl_syfs tfoot').find('th#countUnitsSecondFirst').text(totalUnitsSecondFirst);
                        $('table#tbl_syss').html(trcontent4 +'</tbody>').append('<tfoot class="thin-border-bottom success">\n\
							<tr>\n\
								<th style="width: 100px;" class="hidden-480"></th>\n\
								<th></th>\n\
								<th>SUBTOTAL</th>\n\
								<th></th>\n\
								<th></th>\n\
								<th id="countUnitsSecondSecond">Total UNITS</th>\n\
								<th></th>\n\
								</tr>\n\
							</tfoot>');
                        $('table#tbl_syss tfoot').find('th#countUnitsSecondSecond').text(totalUnitsSecondSecond);
                        $('table#tbl_tyfs').html(trcontent5 +'</tbody>').append('<tfoot class="thin-border-bottom success">\n\
							<tr>\n\
								<th style="width: 100px;" class="hidden-480"></th>\n\
								<th></th>\n\
								<th>SUBTOTAL</th>\n\
								<th></th>\n\
								<th></th>\n\
								<th id="totalUnitsThirdFirst">Total UNITS</th>\n\
								<th></th>\n\
								</tr>\n\
							</tfoot>');
                        $('table#tbl_tyfs tfoot').find('th#totalUnitsThirdFirst').text(totalUnitsThirdFirst);
                        $('table#tbl_tyss').html(trcontent6 +'</tbody>').append('<tfoot class="thin-border-bottom success">\n\
							<tr>\n\
								<th style="width: 100px;" class="hidden-480"></th>\n\
								<th></th>\n\
								<th>SUBTOTAL</th>\n\
								<th></th>\n\
								<th></th>\n\
								<th id="totalUnitsThirdSecond">Total UNITS</th>\n\
								<th></th>\n\
								</tr>\n\
							</tfoot>');
                        $('table#tbl_tyss tfoot').find('th#totalUnitsThirdSecond').text(totalUnitsThirdSecond);
                        $('table#tbl_fryfs').html(trcontent7 +'</tbody>').append('<tfoot class="thin-border-bottom success">\n\
							<tr>\n\
								<th style="width: 100px;" class="hidden-480"></th>\n\
								<th></th>\n\
								<th>SUBTOTAL</th>\n\
								<th></th>\n\
								<th></th>\n\
								<th id="totalUnitsFourthFirst">Total UNITS</th>\n\
								<th></th>\n\
								</tr>\n\
							</tfoot>');
                        $('table#tbl_fryfs tfoot').find('th#totalUnitsFourthFirst').text(totalUnitsFourthFirst);
                        $('table#tbl_fryss').html(trcontent8 +'</tbody>').append('<tfoot class="thin-border-bottom success">\n\
							<tr>\n\
								<th style="width: 100px;" class="hidden-480"></th>\n\
								<th></th>\n\
								<th>SUBTOTAL</th>\n\
								<th></th>\n\
								<th></th>\n\
								<th id="totalUnitsFourthSecond">Total UNITS</th>\n\
								<th></th>\n\
								</tr>\n\
							</tfoot>');
                        $('table#tbl_fryss tfoot').find('th#totalUnitsFourthSecond').text(totalUnitsFourthSecond);

			}
		});
	}
    
	function insideTable(val,identsubs,hasactions,snw,dtw,photo){
		return '<tr id="'+val.prospectid+'">\n\
					<td '+snw+' class="'+identsubs+'"><b>'+val.subject_code+'</b></td>\n\
					<td '+dtw+' class="'+identsubs+'">'+val.description+'</td>\n\
					<td class="'+identsubs+'">\n\
					</td>\n\
					<td class="'+identsubs+'">\n\
					<b class="green"></b>\n\
					</td>\n\
					<td class="'+identsubs+'">\n\
					<b class="green"></b>\n\
					</td>\n\
					<td class="'+identsubs+'">'+val.units+'</td>\n\
					<td class="'+identsubs+'" id="grade'+val.prospectid+'"></td>\n\
				</tr>';
	}
    
    function actions(act,countlesson,subid,insid,sub_code,description,status,studid){
        var items = ["label-success"];
        var rand = items[Math.floor(Math.random() * items.length)];
        if(act == 1){
            showGrades(subid);
            return'<div class="action-buttons">\n\
                    <a href="javascript:void(0)" class="green" onclick="showLessons(\''+subid+','+sub_code+','+description+'\')">\n\
                            <span class="label '+rand+' arrowed-in"><i class="ace-icon fa fa-bookmark bigger-100"></i> topics </span>\n\
                    </a>\n\
                    </div>';
        }else{
            if(status == '0'){
                status = "Pending";
                label = 'label-danger';
            }else{
                status = "Endroll";
                label = 'label-primary';
            }
            return'<div class="action-buttons">\n\
                        <a href="javascript:void(0)" class="green">\n\
                                <span class="label '+label+'" onclick="dialogBoxModal(\''+subid+'_'+insid+'\')"> '+status+' </span>\n\
                        </a>\n\
                     </div>';
        }
    }
    
    
    function subjectenrolled(studid){
        var has_subject = studid.split(',');
        var yoursub;
        $.each(has_subject, function(subs){
            if('<?php echo $this->session->userdata('user_id')?>' == has_subject[subs]){
                yoursub += " label-yellow";
            }else{
                yoursub += "";
            }
        });
        return identsubs = yoursub.replace(/undefined/g,"");
    }
    
    function viewDataTable(){
        $.ajax({
            url:'student/subjects/getSubjects',
            type:'post',
            dataType:'json',
            success:function(data){
                $.each(data, function (key, val){
                    $('table#dataTable_view').dataTable().fnAddData( [
                        val.subject_code,
                        val.description,
                        val.units,
                        val.year,
                        val.sem,
                        '<a href="">123</a>',
                        '<a class="green " href="javascript:void(0)" title="Lessons" style="text-decoration: none" \n\
                            onclick="showLessons(\''+val.prospectid+','+val.subject_code+','+val.description+'\')">\n\
                                <i class="ace-icon fa fa-bookmark bigger-100"></i>\n\
                                    <span id="lesson_count'+val.prospectid+'" \n\
                                        '+getLessons(val.prospectid,val.subject_code)+'\
                                    </span> \n\
                        </a>',
                        val.firstname+' '+val.lastname,
                        '<div class="hidden-xs hidden-xs action-buttons">\n\
                                <a class="green" href="javascript:void(0)" title="Edit">\n\
                                        <i class="ace-icon fa fa-pencil bigger-100"></i>\n\
                                </a>\n\
                                <a class="red" href="javascript:void(0)" title="Delete" onclick="removeSubject('+val.prospectid+')">\n\
                                        <i class="ace-icon fa fa-trash-o bigger-100"></i>\n\
                                </a>\n\
                        </div>'
                    ]);
                });
            }
        });
    }
    
    function getLessons(id){
        $.ajax({
                url:'student/subjects/getLessonsCount',
                type:'post',
                dataType:'json',
                data:{id:id},
                success:function(data){
                        var clesson = "";
                        $.each(data, function (key, val){
                                clesson += val.count_lesson;
                        });
                        $("span#lesson_count"+id).html(clesson);
                }
        });
    }
    
    function showLessons(params){
        $("#prospectusTab").fadeOut();
        $("#lessonTab").fadeIn();
        $("#subjectCode").html(params.split(',')[1]);
        $("#subjectDesc").html(params.split(',')[2]);
        $("#subjectID4Create").html(params.split(',')[0]);
        subjectID = params.split(',')[0];
        lessonsList(subjectID);
    }
    
    function lessonsList(id){
        $.ajax({
            url:'student/subjects/getLessons',
            type:'post',
            dataType:'json',
            data:{id:id},
            success:function(data){
                var list = '';
                $.each(data, function (key, val){
                    
                    var items = ["label-success",
                                 "label-light","label-yellow","label-primary white"];
                    var rand = items[Math.floor(Math.random() * items.length)];
                    list += '<div class="panel panel-default">\n\
                            <div class="panel-heading" onclick="'+showDownloads(val.lesson_id)+'">\n\
                                    <a href="#faq-1-'+val.lesson_id+'" data-parent="#faq-list-'+val.lesson_id+'" data-toggle="collapse" class="accordion-toggle '+rand+' collapsed">\n\
                                            <i class="ace-icon fa fa-chevron-left pull-right" data-icon-hide="ace-icon fa fa-chevron-down" data-icon-show="ace-icon fa fa-chevron-left"></i>\n\
                                            <i class="ace-icon white fa fa-cloud-upload pull-right bigger-150" data-icon-hide="ace-icon fa fa-cloud-upload" data-icon-show="ace-icon fa fa-cloud-upload" title="Upload materials" onclick="dialogUploadModal(\''+val.lesson_id+'_'+val.lesson_name+'\')"></i>\n\
                                            &nbsp;<span></span> <span class="bold"><i class="ace-icon fa fa-files-o bigger-130"></i> '+val.lesson_name.toUpperCase()+'</span><small> '+val.lesson_description+' </small>\n\
                                    </a>\n\
                            </div>\n\
                            <div class="panel-collapse collapse" id="faq-1-'+val.lesson_id+'">\n\
                                    <div class="panel-body">\n\
                                            <div id="downloadlist'+val.lesson_id+'">\n\
                                            </div>\n\
                                    </div>\n\
                            </div>\n\
                    </div>';
                });
                
                $("#faq-list-1").html(list);
            }
        });
    }
    
	function exportPDF(p){
		if(p==1){
			$('#prospectusTable').tableExport({type:'pdf',pdfFontSize:'7',escape:'false',htmlContent:'false',bootstap:'true'});
		}
	}

    function showDownloads(id){
        $.ajax({
            url:'student/subjects/getDownloads',
            type:'post',
            dataType:'json',
            data:{download_id:id}, 
            success:function(data){
                upload = '<div class="col-xs-12 col-sm-12 widget-container-col ui-sortable">\n\
                                <div class="widget-header widget-header-flat">\n\
                                        <h5 class="widget-title lighter">\n\
                                                <i class="ace-icon fa fa-star orange"></i>\n\
                                                Documents \n\
                                        </h5>\n\
                                        <div class="widget-toolbar">\n\
                                        </div>\n\
                                </div><div class="space-4"></div><p>';
                $.each(data, function (key, val){
                    var items = ["btn-success","btn-danger","btn-info","btn-inverse"," btn-pink","btn-yellow","btn-purple"];
                    var rand = items[Math.floor(Math.random() * items.length)];
                    var filename = val.uploaded_filename.split('.')[1];
                    if(filename == 'docx' || filename == 'doc'){
                        var icon = 'word_icon';
                        btn_color = rand;
                    }else if(filename == 'xlsx'){ 
                         icon = 'excel-icon';
                         btn_color = rand;
                    }else if(filename == 'pdf'){
                         icon = 'pdf_icon';
                         btn_color = rand;
                    }else{
                        icon = 'powerpoint-icon';
                        btn_color = rand;
                    }
                    upload +=  '\n\
                                  <span id=fileShow'+val.uploaded_id+' title='+val.uploaded_path+' class="btn btn-app '+btn_color+' btn-sm no-radius" style="padding-right: 10px; width:202px;" onclick=downloadFile('+val.uploaded_id+')>\n\
                                          <img src="assets/img/'+icon+'.png" style="width:30px; height: 30px; border: 0px; border-radius: 0px"/>\n\
                                          <small title='+val.uploaded_filename.split('.')[0]+'>'+val.uploaded_filename.split('.')[0].substring(0,9)+'..</small>\n\
                                  </span>';
                });
                    //////////////// NOTES //////////////////////
                    upload += '</p></div><div class="space-20"></div><div class="col-xs-12 col-sm-12 widget-container-col ui-sortable"><div class="widget-header widget-header-flat">\n\
                                        <h5 class="widget-title ">\n\
                                                <i class="ace-icon fa fa-file-archive-o orange"></i>\n\
                                                Notes \n\
                                        </h5>\n\
                                        <div class="widget-toolbar">\n\
                                        </div>\n\
                                </div><div class="space-4"></div><p id="notes'+id+'">';
                                                    
                    upload +=getnotes(id)+'</p></div>'; 
                    
                    //////////////// END NOTES //////////////////////
                    
                    //////////////// VIDEOS //////////////////////
                
                    upload += '<br><div class="col-xs-12 col-sm-12 widget-container-col ui-sortable"><div class="widget-header widget-header-flat">\n\
                                        <h5 class="widget-title lighter">\n\
                                                <i class="ace-icon fa fa-film orange"></i>\n\
                                                Video \n\
                                        </h5>\n\
                                        <div class="widget-toolbar">\n\
                                        </div>\n\
                                </div><div class="space-4"></div><p id="videos'+id+'">';
                                                    
                    upload +=getVideos(id)+'</p></div>'; 
                    
                    //////////////// END NOTES //////////////////////
                
                $("#downloadlist"+id).html(upload);
            }
        });

    }
    
    function showGrades(val){
            gradesModal(val);
    }

    function gradesModal(val){
        $.ajax({
            url:'student/students/getSubjectGrades',
            type:'post',
            dataType:'json',
            data:{subid:val},
            success:function(data){
                var list = '';
                $.each(data, function (key, val){
                    if(val.grades == ""){
                        igrades = "NG";
                    }else{
                        igrades = val.grades;
                    }
                    list += igrades;
                });
                $("#grade"+val).html(list);
            }
        });
    }
    function downloadFile(id){
            $.fileDownload($("#fileShow"+id).attr('title'),{});
    }
    
</script>

<script src="assets/js/jPages.js"></script>
<script src="assets/js/jquery.slimscroll.min.js"></script>
<script src="assets/js/bootbox.min.js"></script>
<script src="assets/js/ace-elements.min.js"></script>
<!--<script src="assets/js/ace.min.js"></script>-->

<script src="assets/js/jquery.dataTables.min.js"></script>
<script src="assets/js/jquery.dataTables.bootstrap.js"></script>

<script src="assets/js/jquery.fileDownload.js"></script>
	