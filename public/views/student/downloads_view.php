<div class="col-sm-12">
    <div class="row">
        
            <div class="col-xs-12">
                
                
                    <div class="widget-box transparent">
                        <div class="widget-header widget-header-flat">
                                <h4 class="widget-title lighter">
                                        <i class="ace-icon fa fa-bookmark blue"></i>
                                        <span id="lesson_name"><?php echo explode(',', $params)[1]; ?> <small><?php echo explode(',', $params)[2]; ?></small> </span>
                                </h4>

                                <div class="widget-toolbar">
                                        <a href="#" data-action="collapse">
                                                <i class="ace-icon fa fa-chevron-up"></i>
                                        </a>
                                </div>
                        </div>
                        <div class="space-6"></div>
                        <div class="row">
                            <div class="col-xs-12">                    
                                <div class="form-group">
                                    <button id="create_lesson" type="button" class="btn btn-primary btn-sm pull-right"><i class="fa fa-plus"></i> Create Lesson </button>
                                </div>
                            </div>
                        </div>
                        <div class="space-6"></div>

                        <div class="widget-body">
                                <div class="widget-body-inner" style="display: block;">
                                    
                                            <div class="widget-main no-padding">
                                                <div class="space-2"></div>
                                                <table id="dataTable_view2" class="table table-striped table-bordered table-hover">
                                                    <thead>
                                                    <tr>
                                                        <th>Lesson name</th>
                                                        <th>Description</th>
                                                        <th>Download Materials</th>
                                                        <th style="width:140px;">Action</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div><!-- /.widget-main -->
                                    </div>
                        </div><!-- /.widget-body -->
                    </div>

                
                    
            </div>
        
            <div id="dialog-message" class="hide">
                    <div id="insideModal"></div>
            </div>
            <div id="dialog-message2" class="hide">
                <div id="insideModal2">

                    <div class="space-4"></div>
                    <form class="form-horizontal" role="form">
                            <!-- #section:elements.form -->
                            <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Lesson name </label>

                                    <div class="col-sm-9">
                                            <input id="lesson_name_input" placeholder="Lesson name" class="col-xs-12" type="text">
                                    </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="form-field-1-1"> Description</label>

                                    <div class="col-sm-9">
                                        &nbsp;<textarea placeholder="Description" id="description_textarea"></textarea>
                                    </div>
                            </div>


                    </form>
                </div>
            </div>
            </div>
        <!--</div>-->
</div><!-- /.col -->

<script>
    $(document).ready(function(){
        
         $('table#dataTable_view').dataTable({"scrollX": true});
         viewDataTable();
         
        
         $("#create_lesson").click(function(){
             dialogBoxModalAdd('Create Lesson');
         });
         
         
    });
    
    
    function viewDataTable(){
        $.ajax({
            url:'student/subjects/getLessons',
            type:'post',
            dataType:'json',
            data:{id:<?php echo explode(',', $params)[0]; ?>},
            success:function(data){
                $.each(data, function (key, val){
                    $('table#dataTable_view2').dataTable().fnAddData( [
                        val.lesson_name,
                        val.lesson_description,
                        '<div class="hidden-xs hidden-xs action-buttons">\n\
                                <a class="green" href="javascript:void(0)" title="Edit">\n\
                                        <i class="ace-icon fa fa-cloud-download  bigger-100"></i>\n\
                                            123\n\
                                </a>\n\
                        </div>',
                        '<div class="hidden-xs hidden-xs action-buttons">\n\
                                <a class="green" href="javascript:void(0)" title="Edit">\n\
                                        <i class="ace-icon fa fa-pencil bigger-100"></i>\n\
                                </a>\n\
                                <a class="red" href="javascript:void(0)" title="Delete" onclick="removeSubject('+val.prospectid+')">\n\
                                        <i class="ace-icon fa fa-trash-o bigger-100"></i>\n\
                                </a>\n\
                        </div>'
                    ]);
                });
            }
        });
    }
    
    function getLessons(id){
        $.ajax({
                url:'student/subjects/getLessonsCount',
                type:'post',
                dataType:'json',
                data:{id:id},
                success:function(data){
                        var clesson = "";
                        $.each(data, function (key, val){
                                clesson += val.count_lesson;
                        });
                        $("span#lesson_count"+id).html(clesson);
                }
        });
    }
    
    function showLessons(id,code){
        $.ajax({
            url:'student/subjects/getLessons',
            type:'post',
            dataType:'json',
            data:{id:id},
            success:function(data){
                $.each(data, function (key, val){
                    $('table#dataTable_view2').dataTable().fnAddData( [
                        val.lesson_name,
                        val.lesson_description,
                        '<div class="hidden-xs hidden-xs action-buttons">\n\
                                <a class="green" href="javascript:void(0)" title="Edit">\n\
                                        <i class="ace-icon fa fa-cloud-download  bigger-100"></i>\n\
                                </a>\n\
                        </div>',
                        '<div class="hidden-xs hidden-xs action-buttons">\n\
                                <a class="green" href="javascript:void(0)" title="Edit">\n\
                                        <i class="ace-icon fa fa-pencil bigger-100"></i>\n\
                                </a>\n\
                                <a class="red" href="javascript:void(0)" title="Delete" onclick="removeSubject('+val.prospectid+')">\n\
                                        <i class="ace-icon fa fa-trash-o bigger-100"></i>\n\
                                </a>\n\
                        </div>'
                    ]);
                });
            }
        });
    }
                
    function dialogBoxModalAdd(titleName){
            dialogBox(
                    model = {
                            type: 'local',
                            data: 'dialog-message2',
                            title: titleName,
                            title_icon: 'fa-ticket',
                            width: 300,
                            buttons: [
                                    {
                                            'html': "<i class='ace-icon fa fa-times'></i> Cancel",
                                            'class': 'btn_cancel btn btn-xs',
                                            click: function(){
                                                    $(this).dialog('close');
                                            }
                                    },
                                            {
                                            'html': "<i class='ace-icon fa fa-check'></i> Submit",
                                            'class': 'btn-primary btn btn-xs',
                                            click: function(){
                                                    var subid = <?php echo explode(',', $params)[0]; ?>;
                                                    var name = $("#lesson_name_input").val();
                                                    var descript = $("#description_textarea").val();
                                                    $.ajax({
                                                       url:'student/subjects/addLessons',
                                                       type:'post',
                                                       data:{subjectid:subid,name:name,description: descript},
                                                       success:function(){
                                                           location.reload();
                                                       }
                                                    });
                                            }
                                    }
                            ]
                    }
            );
    }
    
    function removeSubject(id){
            bootbox.confirm("Are you sure you want to remove this subject?", function(confirmed) {
                    if(confirmed === true){
                            $.ajax({
                                    url:'student/subjects/removeSubject',
                                    type:'post',
                                    data:{id:id},
                                    success:function(){
                                            location.reload();
                                    }
                            });
                    }
            });
    }
    
    
</script>

<script src="assets/js/jPages.js"></script>
<script src="assets/js/jquery.slimscroll.min.js"></script>
<script src="assets/js/bootbox.min.js"></script>
<script src="assets/js/ace-elements.min.js"></script>
<!--<script src="assets/js/ace.min.js"></script>-->

<script src="assets/js/jquery.dataTables.min.js"></script>
<script src="assets/js/jquery.dataTables.bootstrap.js"></script>
