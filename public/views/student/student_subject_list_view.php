<script type="text/javascript" src="assets/js/tableExport/tableExport.js"></script>
<script type="text/javascript" src="assets/js/tableExport/jquery.base64.js"></script>
<script type="text/javascript" src="assets/js/tableExport/jspdf/libs/sprintf.js"></script> 
<script type="text/javascript" src="assets/js/tableExport/jspdf/jspdf.js"></script>
<script type="text/javascript" src="assets/js/tableExport/jspdf/libs/base64.js"></script>
<script src="http://doersguild.github.io/jQuery.print/jQuery.print.js"></script>
<style type="text/css">
	div.item {
	    /* To correctly align image, regardless of content height: */
	    vertical-align: top;
	    display: inline-block;
	    /* To horizontally center images and caption */
	    text-align: center;
	    /* The width of the container also implies margin around the images. */
	    width: 60px;
	}
	img.lessons {
	    width: 100px;
	    height: 100px;
	    /*background-color: grey;*/
	}
	.caption {
	    /* Make the caption a block so it occupies its own line. */
	    display: block;
	}
</style>
<div class="col-sm-12">
<div class="row">
	<div id="prospectusTab">
		<div class="col-sm-12 center">
			<table class="table table-striped">
				<tr>
					<td>
						<img src="<?php echo base_url() ?>assets/images/Naval_State_University.png" style="height: 100px; width: 100px">
					</td>   
					<td style="text-align: center;">
						<h5>Republic of the Philippines<br><span style="font-family: Old English Text MT, Engravers Old English BT, Old English, Collins Old English, New Old English, serif; ">Naval State University</span><br>Naval, Biliran</h5>
						<h4>BACHELOR OF SCIENCE IN COMPUTER SCIENCE</h4>
					</td>   
					<td>
						<img src="<?php echo base_url() ?>assets/images/nsuciictlogo.jpg" style="height: 100px; width: 100px">
					</td>   
				</tr>
			</table>
		</div>
		<br>
		<div class="col-sm-12">
			<div class="alert alert-warning">
				<strong>NOTICE :</strong>
				This is not valid for transfer. This is not an official copy of your transcript.
				The Curriculum contents are subject to change based on Course Equivalencies.
				This will be your guide for selecting subjects to be enrolled. 
				<br>
			</div>
			<div class="label label-default">
				<strong>No yet taken.</strong>
				<br>
			</div>
			<div class="label label-yellow">
				<strong>Currently taken.</strong>
				<br>
			</div>
			<div class="label label-primary">
				<strong>Already taken.</strong>
				<br>
			</div>
			<div class="label label-info">
				<strong>Currently enrolled for next semester.</strong>
				<br>
			</div>
			<button class="btn btn-success pull-right" onclick="window.open('student/students/print_prospectus','popUpWindow','height=1000,width=600,left=10,top=10,,scrollbars=yes,menubar=no'); return false;">Print Prospectus</button>
			<div class="space-4"></div>
		</div>
    
    
    <div id='prospectusTable'>
		<div id="widget-container-col-1" class="col-xs-12 col-sm-12 widget-container-col ui-sortable">
			<div class="widget-box widget-color-blue ui-sortable-handle" id="widget-box-2">
				<div class="widget-header">
					<h5 class="widget-title bigger lighter">FIRST YEAR</h5>
				</div>
				<div class="widget-body">
					<div class="widget-main no-padding">
						<div class="col-sm-12">
							<div class="widget-box transparent">
								<div class="widget-header widget-header-flat">
									<h4 class="widget-title lighter blue">
										<i class="ace-icon glyphicon glyphicon-file"></i> First Semester
									</h4>

									<div class="widget-toolbar">
										<a href="#" data-action="collapse">
											<i class="ace-icon fa fa-chevron-up"></i>
										</a>
									</div>
								</div>

								<div class="widget-body">
									<div class="widget-main no-padding">
										<table class="table table-bordered table-striped" id="tbl_fyfs">
											<tr>
												<td><img src="assets/img/loading.gif" style="position: absolute;"></td>
											</tr>
										</table>
									</div><!-- /.widget-main -->
								</div><!-- /.widget-body -->
							</div><!-- /.widget-box -->
						</div>

						<div class="col-sm-12">
							<div class="widget-box transparent">
								<div class="widget-header widget-header-flat">
									<h4 class="widget-title lighter blue">
										<i class="ace-icon glyphicon glyphicon-file"></i> Second Semester
									</h4>

									<div class="widget-toolbar">
										<a href="#" data-action="collapse">
											<i class="ace-icon fa fa-chevron-up"></i>
										</a>
									</div>
								</div>

								<div class="widget-body">
									<div class="widget-main no-padding">
										<table class="table table-bordered table-striped" id="tbl_fyss">
											<tr>
												<td><img src="assets/img/loading.gif" style="position: absolute;"></td>
											</tr>
										</table>
									</div><!-- /.widget-main -->
								</div><!-- /.widget-body -->
							</div><!-- /.widget-box -->
						</div>
					</div>
				</div>
			</div>
		</div>


<div id="widget-container-col-1" class="col-xs-12 col-sm-12 widget-container-col ui-sortable">
    <div class="widget-box widget-color-blue ui-sortable-handle" id="widget-box-2">
            <div class="widget-header">
                    <h5 class="widget-title bigger lighter">
                                <i class="ace-icon fa  fa-user"></i> SECOND YEAR
                    </h5>
            </div>
            <div class="widget-body">
                    <div class="widget-main no-padding">
                            <div class="col-sm-12">
                                    <div class="widget-box transparent">
                                            <div class="widget-header widget-header-flat">
                                                    <h4 class="widget-title lighter green">
                                                            <i class="ace-icon glyphicon glyphicon-file"></i> First Semester
                                                    </h4>

                                                    <div class="widget-toolbar">
                                                            <a href="#" data-action="collapse">
                                                                    <i class="ace-icon fa fa-chevron-up"></i>
                                                            </a>
                                                    </div>
                                            </div>

                                            <div class="widget-body">
                                                
                                                    <div class="widget-main no-padding">
                                                            <table class="table table-bordered table-striped" id="tbl_syfs">
                                                            	<tr>
												<td><img src="assets/img/loading.gif" style="position: absolute;"></td>
											</tr>
                                                            </table>
                                                    </div><!-- /.widget-main -->
                                            </div><!-- /.widget-body -->
                                    </div><!-- /.widget-box -->
                            </div>
                            <div class="col-sm-12">
                                    <div class="widget-box transparent">
                                            <div class="widget-header widget-header-flat">
                                                    <h4 class="widget-title lighter green">
                                                            <i class="ace-icon glyphicon glyphicon-file"></i> Second Semester
                                                    </h4>

                                                    <div class="widget-toolbar">
                                                            <a href="#" data-action="collapse">
                                                                    <i class="ace-icon fa fa-chevron-up"></i>
                                                            </a>
                                                    </div>
                                            </div>

                                            <div class="widget-body">
                                                    <div class="widget-main no-padding">
                                                            <table class="table table-bordered table-striped" id="tbl_syss"></table>
                                                    </div><!-- /.widget-main -->
                                            </div><!-- /.widget-body -->
                                    </div><!-- /.widget-box -->
                            </div>
                    </div>
        </div>
    </div>
</div>


<div id="widget-container-col-1" class="col-xs-12 col-sm-12 widget-container-col ui-sortable">
    <div class="widget-box widget-color-blue ui-sortable-handle" id="widget-box-2">
            <div class="widget-header">
                    <h5 class="widget-title bigger lighter">
                            <i class="ace-icon fa  fa-user"></i> THIRD YEAR
                    </h5>
            </div>
            <div class="widget-body">
                    <div class="widget-main no-padding">
                            
                        <div class="col-sm-12">
                                <div class="widget-box transparent">
                                        <div class="widget-header widget-header-flat">
                                                <h4 class="widget-title lighter purple">
                                                       <i class="ace-icon glyphicon glyphicon-file"></i> First Semester
                                                </h4>

                                                <div class="widget-toolbar">
                                                        <a href="#" data-action="collapse">
                                                                <i class="ace-icon fa fa-chevron-up"></i>
                                                        </a>
                                                </div>
                                        </div>

                                        <div class="widget-body">
                                                <div class="widget-main no-padding">
                                                        <table class="table table-bordered table-striped" id="tbl_tyfs"></table>
                                                </div><!-- /.widget-main -->
                                        </div><!-- /.widget-body -->
                                </div><!-- /.widget-box -->
                        </div>

                        <div class="col-sm-12">
                                <div class="widget-box transparent">
                                        <div class="widget-header widget-header-flat">
                                                <h4 class="widget-title lighter purple">
                                                         <i class="ace-icon glyphicon glyphicon-file"></i> Second Semester
                                                </h4>

                                                <div class="widget-toolbar">
                                                        <a href="#" data-action="collapse">
                                                                <i class="ace-icon fa fa-chevron-up"></i>
                                                        </a>
                                                </div>
                                        </div>

                                        <div class="widget-body">
                                                <div class="widget-main no-padding">
                                                        <table class="table table-bordered table-striped" id="tbl_tyss">
                                                        	<tr>
												<td><img src="assets/img/loading.gif" style="position: absolute;"></td>
											</tr>
                                                        </table>
                                                </div><!-- /.widget-main -->
                                        </div><!-- /.widget-body -->
                                </div><!-- /.widget-box -->
                        </div>
                        
                    </div>
        </div>
    </div>
</div>


<div id="widget-container-col-1" class="col-xs-12 col-sm-12 widget-container-col ui-sortable">
    <div class="widget-box widget-color-blue ui-sortable-handle" id="widget-box-2">
            <div class="widget-header">
                    <h5 class="widget-title bigger lighter">
                            <i class="ace-icon fa  fa-user"></i> FOURTH YEAR
                    </h5>
            </div>
            <div class="widget-body">
                    <div class="widget-main no-padding">
                            
                        <div class="col-sm-12">
                                <div class="widget-box transparent">
                                        <div class="widget-header widget-header-flat">
                                                <h4 class="widget-title lighter red">
                                                       <i class="ace-icon glyphicon glyphicon-file"></i> First Semester
                                                </h4>

                                                <div class="widget-toolbar">
                                                        <a href="#" data-action="collapse">
                                                                <i class="ace-icon fa fa-chevron-up"></i>
                                                        </a>
                                                </div>
                                        </div>

                                        <div class="widget-body">
                                                <div class="widget-main no-padding">
                                                        <table class="table table-bordered table-striped" id="tbl_fryfs">
                                                        	<tr>
																<td><img src="assets/img/loading.gif" style="position: absolute;"></td>
															</tr>
                                                        </table>
                                                </div><!-- /.widget-main -->
                                        </div><!-- /.widget-body -->
                                </div><!-- /.widget-box -->
                        </div>

                        <div class="col-sm-12">
                                <div class="widget-box transparent">
                                        <div class="widget-header widget-header-flat">
                                                <h4 class="widget-title lighter red">
                                                        <i class="ace-icon glyphicon glyphicon-file"></i> Second Semester
                                                </h4>

                                                <div class="widget-toolbar">
                                                        <a href="#" data-action="collapse">
                                                                <i class="ace-icon fa fa-chevron-up"></i>
                                                        </a>
                                                </div>
                                        </div>

                                        <div class="widget-body">
                                                <div class="widget-main no-padding">
                                                        <table class="table table-bordered table-striped" id="tbl_fryss">
                                                        	<tr>
												<td><img src="assets/img/loading.gif" style="position: absolute;"></td>
											</tr>
                                                        </table>
                                                </div><!-- /.widget-main -->
                                        </div><!-- /.widget-body -->
                                </div><!-- /.widget-box -->
                        </div>
                        
                    </div>
        </div>
    </div>
</div>
<div class="col-sm-4">
	<h5>General Education <span class="pull-right">- 64</span></h5>
	<h5>Basic ITE Core Courses <span class="pull-right">- 32</span></h5>
	<h5>ITE Professional Courses <span class="pull-right">- 48</span></h5>
	<h5>Electives <span class="pull-right">- 15</span></h5>
	<h5>OJT <span class="pull-right"> - 6</span></h5>
	<h5>Free Electives <span class="pull-right"> - 6</span></h5>
	<h5>P.E <span class="pull-right">- 8</span></h5>
	<h5>NSTP <span class="pull-right">- (6)</span></h5>
	<hr>
	<h5>Total No. of Units <span class="pull-right">177</span></h5>
</div>
</div>
</div>
        </div>
        
        
<div id="lessonTab" style="display: none">
	<div class="col-xs-12">
		<div class="widget-box transparent">
			<div class="widget-header widget-header-flat">
				<h4 class="widget-title lighter">
					<i class="ace-icon fa fa-bookmark blue"></i>
					<span id="lesson_name"><span id="subjectCode"></span> <small id="subjectDesc"></small> </span>
				</h4>
				<div class="widget-toolbar">
					<a href="#" data-action="collapse">
						<i class="ace-icon fa fa-chevron-up"></i>
					</a>
				</div>
			</div>
		<div class="space-6"></div>
		<div class="space-6"></div>
			<div class="widget-body">
				<div class="widget-body-inner" style="display: block;">
					<div class="widget-main no-padding">
						<div id="faq-tab-1" class="tab-pane fade in active">
							<div id="faq-list-1" class="panel-group accordion-style1 accordion-style2"></div>
						</div>
					</div>
					<!-- /.widget-main -->
				</div>
			</div><!-- /.widget-body -->
		</div>
	</div>
</div>

<div id="dialog-message" class="hide">
	<div id="insideModal"></div>
</div>

<div id="dialog-message-showGrades" class="hide">
	<div id="insideModalshowGrades">
		Teacher
		Grade
	</div>
</div>
            
            <div id="dialog-message2" class="hide">
                <div id="insideModal2">

                    <div class="space-4"></div>
                    <form class="form-horizontal" role="form">
                            <!-- #section:elements.form -->
                            <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Subject Code </label>

                                    <div class="col-sm-9">
                                            <input id="subjectid" placeholder="Subject code" class="col-xs-12" type="text">
                                    </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="form-field-1-1"> Description</label>

                                    <div class="col-sm-9">
                                        &nbsp;<textarea placeholder="Description" id="description"></textarea>
                                    </div>
                            </div>

                            <!-- /section:elements.form -->
                            <div class="space-4"></div>

                            <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-2"> Units </label>

                                    <div class="col-sm-9">
                                            <input placeholder="Units" id="units" class="col-xs-10 col-sm-5" type="text">
                                    </div>
                            </div>

                            <div class="space-4"></div>

                            <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-input-readonly"> Year </label>

                                    <div class="col-sm-9">
                                            <select class="form-control" id="year">
                                                <option value=""> Select </option>
                                                <option value="1">1st Year</option>
                                                <option value="2">2nd Year</option>
                                                <option value="3">3rd Year</option>
                                                <option value="4">4rth Year</option>
                                            </select>
                                    </div>
                            </div>

                            <div class="space-2"></div>

                            <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-4">Semester</label>

                                    <div class="col-sm-9">
                                            <select class="form-control" id="sem">
                                                <option value=""> Select </option>
                                                <option value="1">1st Semester</option>
                                                <option value="2">2nd Semester</option>
                                            </select>

                                    </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="form-field-5">Instructor</label>

                                    <div class="col-sm-9">
                                            <div class="clearfix">
                                                    <select class="form-control" id="instructor"></select>
                                            </div>

                                     </div>
                            </div>

                            </div><!-- /.row -->


                    </form>
                </div>
            </div>
            </div>
<!-- /.col -->

<script>
	$(document).ready(function(){
		$("#create_subject").click(function(){
			dialogBoxModalAdd('Create Subjects');
		});
		yoursubjects();
		$("#exportButton").click(function () {
			$("#prospectusTable").print({
				//Use Global styles
				globalStyles : true,
				//Add link with attrbute media=print
				mediaPrint : false,
				//Custom stylesheet
				stylesheet : "http://fonts.googleapis.com/css?family=Inconsolata",
				//Print in a hidden iframe
				iframe : false,
				//Don't print this
				noPrintSelector : ".avoid-this",
				//Add this at top
				prepend : "PROSPECTUS YEAR 2017",
				//Add this on bottom
				append : "<br/>END",
				//Log to console when printing is done via a deffered callback
				deferred: $.Deferred().done(function() { console.log('Printing done', arguments); })
			});
		});
	});
    function yoursubjects(){
        $.ajax({
                url:'student/students/getSubjects',
                type:'post',
                dataType:'json',
                success:function(data){
					var trcontent = [];
					var trcontent1 = trcontent2 = trcontent3 = trcontent4 = trcontent5 = trcontent6 = trcontent7 = trcontent8 = studentID = null;
						// for(i=1;i<=8;i++){
					thead ='<thead class="thin-border-bottom success">\n\
								<tr>\n\
								<th style="width: 100px;" class="hidden-480"></th>\n\
								<th>SUBJECTNO</th>\n\
								<th>DESCRIPTIVE_TITLE</th>\n\
								<th>PRE_REQUISITE</th>\n\
								<th>LEC</th>\n\
								<th>LAB</th>\n\
								<th>UNITS</th>\n\
								<th>GRADES</th>\n\
								<th>INSTRUCTOR</th>\n\
								</tr>\n\
							</thead><tbody>';
						// }

						var trcontent1 = thead +'<tbody>';  
						var trcontent2 = thead +'<tbody>';
						var trcontent3 = thead +'<tbody>';
						var trcontent4 = thead +'<tbody>'; 
						var trcontent5 = thead +'<tbody>'; 
						var trcontent6 = thead +'<tbody>';
						var trcontent7 = thead +'<tbody>'; 
						var trcontent8 = thead +'<tbody>';
                        var totalUnitsFirstFirst = totalUnitsFirstSecond = totalUnitsSecondFirst = totalUnitsSecondSecond = totalUnitsThirdFirst = totalUnitsThirdSecond = totalUnitsFourthFirst = totalUnitsFourthSecond = 0;                        
                        $.each(data, function (key, val){
								var snw = 'style="width:150px"'; 
								var dtw = 'style="width:400px"';
								var hasactionsNotEmpty = {
									'countlessons': val.countlessons,
									'prospectid': val.prospectid,
									'instructor': val.instructor,
									'subject_code': val.subject_code,
									'description': val.description,
									'status': val.status,
									'students_id': val.students_id
								};
								var hasactionsEmpty = {
									'countlessons': val.countlessons,
									'prospectid': val.prospectid,
									'instructor': val.instructor,
									'subject_code': val.subject_code,
									'description': val.description,
									'status': val.status,
									'students_id': val.students_id
								};
                                if(val.year == 1 && val.sem == 1){
                                	identsubs = subjectenrolled(val.students_id);
                                    if(identsubs !=""){
                                        hasactions = actions(1,hasactionsNotEmpty);
                                    }else{
                                        hasactions = actions(2,hasactionsEmpty);
                                    }
                                    trcontent1 += insideTable(val,identsubs,hasactions,snw,dtw,val.photo);
                                    totalUnitsFirstFirst += parseInt(val.units);
                                    showGrades(val.prospectid,val.year, val.sem);
                                }else if(val.year == 1 && val.sem == 2){
                                    identsubs = subjectenrolled(val.students_id);
                                    if(identsubs !=""){
                                       hasactions = actions(1,hasactionsNotEmpty);
                                    }else{
                                       hasactions = actions(2,hasactionsEmpty);
                                    }
                                    trcontent2 += insideTable(val,identsubs,hasactions,snw,dtw,val.photo);
                                    totalUnitsFirstSecond += parseInt(val.units);
                                    showGrades(val.prospectid,val.year, val.sem);
                                }else if(val.year == 2 && val.sem == 1){
                                    identsubs = subjectenrolled(val.students_id);
                                   if(identsubs !=""){
                                         hasactions = actions(1,hasactionsNotEmpty);
                                    }else{
                                        hasactions = actions(2,hasactionsEmpty);
                                    }
                                    trcontent3 += insideTable(val,identsubs,hasactions,snw,dtw,val.photo);
                                    totalUnitsSecondFirst += parseInt(val.units);
                                }else if(val.year == 2 && val.sem == 2){
                                    identsubs = subjectenrolled(val.students_id);
                                    if(identsubs !=""){
                                         hasactions = actions(1,hasactionsNotEmpty);
                                    }else{
                                        hasactions = actions(2,hasactionsEmpty);
                                    }
                                    trcontent4 += insideTable(val,identsubs,hasactions,snw,dtw,val.photo);
                                    totalUnitsSecondSecond += parseInt(val.units);
                                }else if(val.year == 3 && val.sem == 1){
                                    identsubs = subjectenrolled(val.students_id);
                                    if(identsubs !=""){
                                        hasactions = actions(1,hasactionsNotEmpty);
                                    }else{
                                        hasactions = actions(2,hasactionsEmpty);
                                    }
                                    trcontent5 += insideTable(val,identsubs,hasactions,snw,dtw,val.photo);
                                    totalUnitsThirdFirst += parseInt(val.units);
                                }else if(val.year == 3 && val.sem == 2){
                                    identsubs = subjectenrolled(val.students_id);
                                   if(identsubs !=""){
                                         hasactions = actions(1,hasactionsNotEmpty);
                                    }else{
                                        hasactions = actions(2,hasactionsEmpty);
                                    }
                                    trcontent6 += insideTable(val,identsubs,hasactions,snw,dtw,val.photo);
                                    totalUnitsThirdSecond += parseInt(val.units);
                                }else if(val.year == 4 && val.sem == 1){
                                    identsubs = subjectenrolled(val.students_id);
                                    if(identsubs !=""){
                                        hasactions = actions(1,hasactionsNotEmpty);
                                    }else{
                                        hasactions = actions(2,hasactionsEmpty);
                                    }
                                    trcontent7 += insideTable(val,identsubs,hasactions,snw,dtw,val.photo);
                                    totalUnitsFourthFirst += parseInt(val.units);
                                }else if(val.year == 4 && val.sem == 2){
                                    identsubs = subjectenrolled(val.students_id);
                                    if(identsubs !=""){
                                         hasactions = actions(1,hasactionsNotEmpty);
                                    }else{
                                        hasactions = actions(2,hasactionsEmpty);
                                    }
                                    trcontent8 += insideTable(val,identsubs,hasactions,snw,dtw,val.photo);
                                    totalUnitsFourthSecond += parseInt(val.units);
                                }
                        });
						
                        $('table#tbl_fyfs').html(trcontent1 +'</tbody>').append('<tfoot class="thin-border-bottom success">\n\
							<tr>\n\
								<th style="width: 100px;" class="hidden-480"></th>\n\
								<th></th>\n\
								<th></th>\n\
								<th>SUBTOTAL</th>\n\
								<th></th>\n\
								<th></th>\n\
								<th id="countUnitsFisFis">Total UNITS</th>\n\
								<th></th>\n\
								<th></th>\n\
								</tr>\n\
							</tfoot>');
                        $('table#tbl_fyfs tfoot').find('th#countUnitsFisFis').text(totalUnitsFirstFirst);
                        $('table#tbl_fyss').html(trcontent2 +'</tbody>').append('<tfoot class="thin-border-bottom success">\n\
							<tr>\n\
								<th style="width: 100px;" class="hidden-480"></th>\n\
								<th></th>\n\
								<th></th>\n\
								<th>SUBTOTAL</th>\n\
								<th></th>\n\
								<th></th>\n\
								<th id="countUnitsFisSec">Total UNITS</th>\n\
								<th></th>\n\
								<th></th>\n\
								</tr>\n\
							</tfoot>');
                        $('table#tbl_fyss tfoot').find('th#countUnitsFisSec').text(totalUnitsFirstSecond);
                        $('table#tbl_syfs').html(trcontent3 +'</tbody>').append('<tfoot class="thin-border-bottom success">\n\
							<tr>\n\
								<th style="width: 100px;" class="hidden-480"></th>\n\
								<th></th>\n\
								<th></th>\n\
								<th>SUBTOTAL</th>\n\
								<th></th>\n\
								<th></th>\n\
								<th id="countUnitsSecondFirst">Total UNITS</th>\n\
								<th></th>\n\
								<th></th>\n\
								</tr>\n\
							</tfoot>');
                        $('table#tbl_syfs tfoot').find('th#countUnitsSecondFirst').text(totalUnitsSecondFirst);
                        $('table#tbl_syss').html(trcontent4 +'</tbody>').append('<tfoot class="thin-border-bottom success">\n\
							<tr>\n\
								<th style="width: 100px;" class="hidden-480"></th>\n\
								<th></th>\n\
								<th></th>\n\
								<th>SUBTOTAL</th>\n\
								<th></th>\n\
								<th></th>\n\
								<th id="countUnitsSecondSecond">Total UNITS</th>\n\
								<th></th>\n\
								<th></th>\n\
								</tr>\n\
							</tfoot>');
                        $('table#tbl_syss tfoot').find('th#countUnitsSecondSecond').text(totalUnitsSecondSecond);
                        $('table#tbl_tyfs').html(trcontent5 +'</tbody>').append('<tfoot class="thin-border-bottom success">\n\
							<tr>\n\
								<th style="width: 100px;" class="hidden-480"></th>\n\
								<th></th>\n\
								<th></th>\n\
								<th>SUBTOTAL</th>\n\
								<th></th>\n\
								<th></th>\n\
								<th id="totalUnitsThirdFirst">Total UNITS</th>\n\
								<th></th>\n\
								<th></th>\n\
								</tr>\n\
							</tfoot>');
                        $('table#tbl_tyfs tfoot').find('th#totalUnitsThirdFirst').text(totalUnitsThirdFirst);
                        $('table#tbl_tyss').html(trcontent6 +'</tbody>').append('<tfoot class="thin-border-bottom success">\n\
							<tr>\n\
								<th style="width: 100px;" class="hidden-480"></th>\n\
								<th></th>\n\
								<th></th>\n\
								<th>SUBTOTAL</th>\n\
								<th></th>\n\
								<th></th>\n\
								<th id="totalUnitsThirdSecond">Total UNITS</th>\n\
								<th></th>\n\
								<th></th>\n\
								</tr>\n\
							</tfoot>');
                        $('table#tbl_tyss tfoot').find('th#totalUnitsThirdSecond').text(totalUnitsThirdSecond);
                        $('table#tbl_fryfs').html(trcontent7 +'</tbody>').append('<tfoot class="thin-border-bottom success">\n\
							<tr>\n\
								<th style="width: 100px;" class="hidden-480"></th>\n\
								<th></th>\n\
								<th></th>\n\
								<th>SUBTOTAL</th>\n\
								<th></th>\n\
								<th></th>\n\
								<th id="totalUnitsFourthFirst">Total UNITS</th>\n\
								<th></th>\n\
								<th></th>\n\
								</tr>\n\
							</tfoot>');
                        $('table#tbl_fryfs tfoot').find('th#totalUnitsFourthFirst').text(totalUnitsFourthFirst);
                        $('table#tbl_fryss').html(trcontent8 +'</tbody>').append('<tfoot class="thin-border-bottom success">\n\
							<tr>\n\
								<th style="width: 100px;" class="hidden-480"></th>\n\
								<th></th>\n\
								<th></th>\n\
								<th>SUBTOTAL</th>\n\
								<th></th>\n\
								<th></th>\n\
								<th id="totalUnitsFourthSecond">Total UNITS</th>\n\
								<th></th>\n\
								<th></th>\n\
								</tr>\n\
							</tfoot>');
                        $('table#tbl_fryss tfoot').find('th#totalUnitsFourthSecond').text(totalUnitsFourthSecond);
                            
                }
        });
    }
    
	function insideTable(val,identsubs,hasactions,snw,dtw,photo){
		if(photo == ''){
			photo = 'assets/img/default-user2.png';
		}
		return '<tr id="'+val.prospectid+'">\n\
					<td class="'+identsubs+'" '+snw+'>\n\
					'+hasactions+'\n\
					</td>\n\
					<td '+snw+' class="'+identsubs+'"><b>'+val.subject_code+'</b></td>\n\
					<td '+dtw+' class="'+identsubs+'">'+val.description+'</td>\n\
					<td class="'+identsubs+'">'+val.pre_requisites+'</td>\n\
					<td class="'+identsubs+'">'+val.lecture_hrs+'</td>\n\
					<td class="'+identsubs+'">'+val.lab_hrs+'</td>\n\
					<td class="'+identsubs+'">'+val.units+'</td>\n\
					<td class="'+identsubs+' yourGrades" id="grade'+val.prospectid+'"></td>\n\
					<td class="'+identsubs+'" '+snw+'><img class="pull-left img-circle" alt="" src="'+photo+'" style="height: 25px; width: 25px">&nbsp;'+val.firstname+' '+val.lastname+'</td>\n\
				</tr>';
	}
    
	function actions(act,params){ 
		var items = ["label-success"];
		var rand = items[Math.floor(Math.random() * items.length)];
		if(act == 1){
			return'<div class="action-buttons">\n\
					<a href="javascript:void(0)" class="green" onclick="showLessons(\''+params['prospectid']+','+params['subject_code']+','+params['description']+'\')">\n\
						<span class="label '+rand+' arrowed-in"><i class="ace-icon fa fa-bookmark bigger-100"></i> topics </span>\n\
					</a>\n\
				</div>';
		}else{
			if(params['status'] == '0'){
				clickable = '<span class="red"> Pending </span>';
			}else{
				clickable = '<a href="javascript:void(0)" class="green">\n\
								<span class="label label-primary arrowed-in" onclick="dialogBoxModal(\''+params['prospectid']+'_'+params['instructor']+'\')"><i class="ace-icon fa fa-user bigger-100"></i> Enroll </span>\n\
							</a>';
			}
			return'<div class="action-buttons">'+clickable+'</div>';
		}
	}
    
    
    function subjectenrolled(studid){
        var has_subject = studid.split(',');
        var yoursub;
        $.each(has_subject, function(subs){
            if('<?php echo $this->session->userdata('user_id')?>' == has_subject[subs]){
                yoursub += " label-yellow";
            }else{
                yoursub += "";
            }
        });
        return identsubs = yoursub.replace(/undefined/g,"");
    }
    
    function viewDataTable(){
        $.ajax({
            url:'student/subjects/getSubjects',
            type:'post',
            dataType:'json',
            success:function(data){
                $.each(data, function (key, val){
                    $('table#dataTable_view').dataTable().fnAddData( [
                        val.subject_code,
                        val.description,
                        val.units,
                        val.year,
                        val.sem,
                        '<a href="">123</a>',
                        '<a class="green " href="javascript:void(0)" title="Lessons" style="text-decoration: none" \n\
                            onclick="showLessons(\''+val.prospectid+','+val.subject_code+','+val.description+'\')">\n\
                                <i class="ace-icon fa fa-bookmark bigger-100"></i>\n\
                                    <span id="lesson_count'+val.prospectid+'" \n\
                                        '+getLessons(val.prospectid,val.subject_code)+'\
                                    </span> \n\
                        </a>',
                        val.firstname+' '+val.lastname,
                        '<div class="hidden-xs hidden-xs action-buttons">\n\
                                <a class="green" href="javascript:void(0)" title="Edit">\n\
                                        <i class="ace-icon fa fa-pencil bigger-100"></i>\n\
                                </a>\n\
                                <a class="red" href="javascript:void(0)" title="Delete" onclick="removeSubject('+val.prospectid+')">\n\
                                        <i class="ace-icon fa fa-trash-o bigger-100"></i>\n\
                                </a>\n\
                        </div>'
                    ]);
                });
            }
        });
    }
    
    function getLessons(id){
        $.ajax({
                url:'student/subjects/getLessonsCount',
                type:'post',
                dataType:'json',
                data:{id:id},
                success:function(data){
                        var clesson = "";
                        $.each(data, function (key, val){
                                clesson += val.count_lesson;
                        });
                        $("span#lesson_count"+id).html(clesson);
                }
        });
    }
    
    function showLessons(params){
        $("#prospectusTab").fadeOut();
        $("#lessonTab").fadeIn();
        $("#subjectCode").html(params.split(',')[1]);
        $("#subjectDesc").html(params.split(',')[2]);
        $("#subjectID4Create").html(params.split(',')[0]);
        subjectID = params.split(',')[0];
        lessonsList(subjectID);
    }
    
    function lessonsList(id){
        $.ajax({
            url:'student/subjects/getLessons',
            type:'post',
            dataType:'json',
            data:{id:id},
            success:function(data){
                var list = '';
                $.each(data, function (key, val){
                    
                    var items = ["label-success",
                                 "label-light","label-yellow","label-primary white"];
                    var rand = items[Math.floor(Math.random() * items.length)];
                    list += '<div class="panel panel-default">\n\
                            <div class="panel-heading" onclick="'+showDownloads(val.lesson_id)+'">\n\
                                    <a href="#faq-1-'+val.lesson_id+'" data-parent="#faq-list-'+val.lesson_id+'" data-toggle="collapse" class="accordion-toggle '+rand+' collapsed">\n\
                                            <i class="ace-icon fa fa-chevron-left pull-right" data-icon-hide="ace-icon fa fa-chevron-down" data-icon-show="ace-icon fa fa-chevron-left"></i>\n\
                                            <i class="ace-icon white fa fa-cloud-upload pull-right bigger-150" data-icon-hide="ace-icon fa fa-cloud-upload" data-icon-show="ace-icon fa fa-cloud-upload" title="Upload materials" onclick="dialogUploadModal(\''+val.lesson_id+'_'+val.lesson_name+'\')"></i>\n\
                                            &nbsp;<span></span> <span class="bold"><i class="ace-icon fa fa-files-o bigger-130"></i> '+val.lesson_name.toUpperCase()+'</span><small> '+val.lesson_description+' </small>\n\
                                    </a>\n\
                            </div>\n\
                            <div class="panel-collapse collapse" id="faq-1-'+val.lesson_id+'">\n\
                                    <div class="panel-body">\n\
                                            <div id="downloadlist'+val.lesson_id+'">\n\
                                            </div>\n\
                                    </div>\n\
                            </div>\n\
                    </div>';
                });
                
                $("#faq-list-1").html(list);
            }
        });
    }
    
    
	function exportPDF(p){
		if(p==1){
			$('#prospectusTable').tableExport({type:'pdf',pdfFontSize:'7',escape:'false',htmlContent:'false',bootstap:'true'});
		}
	}
    function showDownloads(id){
        $.ajax({
            url:'student/subjects/getDownloads',
            type:'post',
            dataType:'json',
            data:{download_id:id}, 
            success:function(data){
                upload = '<div class="col-xs-12 col-sm-12 widget-container-col ui-sortable">\n\
                                <div class="widget-header widget-header-flat">\n\
                                        <h5 class="widget-title lighter">\n\
                                                <i class="ace-icon fa fa-star orange"></i>\n\
                                                Documents \n\
                                        </h5>\n\
                                        <div class="widget-toolbar">\n\
                                        </div>\n\
                                </div><div class="space-4"></div><p>';
                $.each(data, function (key, val){
                    // var items = ["btn-success","btn-danger","btn-info","btn-inverse"," btn-pink","btn-yellow","btn-purple"];
                    // var rand = items[Math.floor(Math.random() * items.length)];
                    var filename = val.uploaded_filename.split('.')[1];
                    if(filename == 'docx' || filename == 'doc'){
                        var icon = 'word_icon';
                        // btn_color = rand;
                    }else if(filename == 'xlsx'){ 
                         icon = 'excel-icon';
                         // btn_color = rand;
                    }else if(filename == 'pdf'){
                         icon = 'pdf_icon';
                         // btn_color = rand;
                    }else{
                        icon = 'powerpoint-icon';
                        // btn_color = rand;
                    }
                    upload +=  '<div class="item" onclick="downloadFile('+val.uploaded_id+')" title='+val.uploaded_filename.split('.')[0]+'><img class="lessons" src="assets/img/'+icon+'.png" style="width:59px; height: 59px; border: 0px; border-radius: 0px"/>\n\
                                  <span class="caption" id=fileShow'+val.uploaded_id+' title='+val.uploaded_path+' ">\n\
                                          \n\
                                          <small title='+val.uploaded_filename.split('.')[0]+'>'+val.uploaded_filename.split('.')[0].substring(0,9)+'..</small>\n\
                                  </span>\n\
                                </div>';
                });
                    //////////////// NOTES //////////////////////
                    upload += '</p></div><div class="space-20"></div><div class="col-xs-12 col-sm-12 widget-container-col ui-sortable"><div class="widget-header widget-header-flat">\n\
                                        <h5 class="widget-title ">\n\
                                                <i class="ace-icon fa fa-file-archive-o orange"></i>\n\
                                                Notes \n\
                                        </h5>\n\
                                        <div class="widget-toolbar">\n\
                                        </div>\n\
                                </div><div class="space-4"></div><p id="notes'+id+'">';
                                                    
                    upload +=getnotes(id)+'</p></div>'; 
                    
                    //////////////// END NOTES //////////////////////
                    
                    //////////////// VIDEOS //////////////////////
                
                    upload += '<br><div class="col-xs-12 col-sm-12 widget-container-col ui-sortable"><div class="widget-header widget-header-flat">\n\
                                        <h5 class="widget-title lighter">\n\
                                                <i class="ace-icon fa fa-film orange"></i>\n\
                                                Video \n\
                                        </h5>\n\
                                        <div class="widget-toolbar">\n\
                                        </div>\n\
                                </div><div class="space-4"></div><p id="videos'+id+'">';
                                                    
                    upload +=getVideos(id)+'</p></div>'; 
                    
                    //////////////// END NOTES //////////////////////
                
                $("#downloadlist"+id).html(upload);
            }
        });

    }
    
    function getnotes(id){
        $.ajax({
            url:'teacher/teacher_subjects/getNotes',
            type:'post',
            dataType:'json',
            data:{id:id},
            success:function(data){
                var list = '';
                $.each(data, function (key, val){
                    list += '<div class="col-xs-12 col-sm-6 widget-container-col  ui-sortable">\n\
                                    <div class="widget-box ">\n\
                                            <div class="widget-header">\n\
                                                    <h5 class="widget-title smaller">'+val.notes_title+'</h5>\n\
                                                    <div class="widget-toolbar">\n\
                                                       <a href=""><i class="ace-icon fa fa-times"></i></a>\n\
                                                    </div>\n\
                                            </div>\n\
                                            <div class="widget-body">\n\
                                                    <div class="widget-main padding-6">\n\
                                                            <div class="alert alert-info"> '+val.notes_description +' </div>\n\
                                                    </div>\n\
                                            </div>\n\
                                    </div>\n\
                            </div>';
                });
                $("#downloadlist"+id).find('#notes'+id).html(list);  
            }
        });
         
    }
    
    function getVideos(id){
        $.ajax({
            url:'teacher/teacher_subjects/getVideos',
            type:'post',
            dataType:'json',
            data:{id:id},
            success:function(data){
                var list = '';
                $.each(data, function (key, val){
                    var items = ["widget-color-dark","widget-color-pink","widget-color-orange","widget-color-blue"," widget-color-dark"];
                    var rand = items[Math.floor(Math.random() * items.length)];
                    list += '<div class="col-xs-12 col-sm-12 widget-container-col  ui-sortable">\n\
                                    <div class="widget-box '+rand+'">\n\
                                            <div class="widget-header">\n\
                                                    <h5 class="widget-title smaller">'+val.video_name+' <small> '+val.video_description+' </small></h5>\n\
                                                    <div class="widget-toolbar">\n\
                                                       <a href=""><i class="ace-icon fa fa-times"></i></a>\n\
                                                    </div>\n\
                                            </div>\n\
                                            <div class="widget-body">\n\
                                                    <div class="widget-main padding-6">\n\
                                                          <center>   '+val.video_embed +' </center> \n\
                                                    </div>\n\
                                            </div>\n\
                                    </div>\n\
                            </div>';
                });
                $("#downloadlist"+id).find('#videos'+id).html(list);  
            }
        });
    }
    
    function dialogBoxModal(val){
            $("#insideModal").html("<h5>Are you sure you want to enroll this subject.</h5>");
            dialogBox(
                    model = {
                            type: 'local',
                            data: 'dialog-message',
                            title: 'Enroll This Subject',
                            title_icon: 'fa-user',
                            width: 500,
                            buttons: [
                                    {
                                            'html': "<i class='ace-icon fa fa-times white'></i> No",
                                            'class': 'btn_cancel btn btn-xs btn-danger',
                                            click: function(){
                                                    $(this).dialog('close');
                                            }
                                    },
                                            
                                    {
                                            'html': "<i class='ace-icon fa fa-check'></i> Yes",
                                            'class': 'btn-primary btn btn-xs',
                                            click: function(){
                                                    alert('SUBJECT FOR CONFIRMATION OR PLEASE CONTACT ADMINISTRATOR.');
                                                    $.ajax({
                                                       url:'student/students/enrolledSubjects',
                                                       type:'post',
                                                       data:{subid:val.split('_')[0],insid:val.split('_')[1]},
                                                       success:function(){
                                                           yoursubjects();
                                                       }
                                                    });
                                            }
                                    }
                            ]
                    }
            );
    }
    
	function showGrades(val,year,sem){
		$.ajax({
			url:'student/students/getSubjectGrades',
			type:'post',
			dataType:'json',
			data:{subid:val},
			success:function(data){
				var list = ''; mygrade = 0; totalGrade = 0;
				$.each(data, function (key, value){
					if(value.grades == ""){
						igrades = "NG";
					}else{
						igrades = value.grades;
					}
					list += igrades;
					studentID = value.student_id;
				});
				$("#grade"+val).html(list);
				totalallGrade(studentID,year,sem);
			}
		});
	}
    function totalallGrade(studid,year,sem) {
  //       $.ajax({
		// 	url:'student/students/getSubjectTotalGrades',
		// 	type:'post',
		// 	dataType:'json',
		// 	data:{studid:studid, year: year, sem: sem},
		// 	success:function(data){
		// 		if(year == 1 && sem == 1) {
		// 			countUnitsFisFis = $('table#tbl_fyfs tfoot').find('th#countUnitsFisFis').text();
		// 			calculateGradesFisFis = parseFloat(data[0]['mygrades']/countUnitsFisFis);
		// 			$('table#tbl_fyfs tfoot').find('th#countUnitsFisFis').next().html(calculateGradesFisFis.toFixed(2)); 

		// 		// console.log(data[0]['mygrades']);
		// 		} else if(year == 1 && sem == 2) {
		// 			countUnitsFisSec = $('table#tbl_fyss tfoot').find('th#countUnitsFisSec').text();
		// 			calculateGradesFisSec = parseFloat(data[0]['mygrades']/countUnitsFisSec);
		// 			$('table#tbl_fyss tfoot').find('th#countUnitsFisSec').next().html(calculateGradesFisSec.toFixed(2)); 

		// 		console.log(data[0]['mygrades']);
		// 		}
		// 	}
		// });
    }
    function downloadFile(id){
            $.fileDownload($("#fileShow"+id).attr('title'),{});
    }
 
    function removeSubject(id){
			bootbox.confirm("Are you sure you want to remove this subject?", function(confirmed) {
				if(confirmed === true){
					$.ajax({
						url:'student/subjects/removeSubject',
						type:'post',
						data:{id:id},
						success:function(){
							location.reload();
						}
					});
				}
			});
		}
                
                
    function selects(){
         $.ajax({
            url:'student/subjects/getInstructor',
            type:'post',
            dataType:'json',
            success: function(data){
                var option = '<option value=""> Select </option>';
                $.each(data, function(key, val){
                   option += '<option value="'+val.instructid+'">'+val.firstname+' '+val.lastname+'</option>'; 
                });
                $("#instructor").html(option);
                
            }
         });
    }
</script>

<script src="assets/js/jPages.js"></script>
<script src="assets/js/jquery.slimscroll.min.js"></script>
<script src="assets/js/bootbox.min.js"></script>
<script src="assets/js/ace-elements.min.js"></script>
<!--<script src="assets/js/ace.min.js"></script>-->

<script src="assets/js/jquery.dataTables.min.js"></script>
<script src="assets/js/jquery.dataTables.bootstrap.js"></script>

<script src="assets/js/jquery.fileDownload.js"></script>
