<div class="col-sm-12">
    <div class="row">
        
        
        
            <div class="col-xs-12">
                    <div class="widget-box transparent">
                        <div class="widget-header widget-header-flat">
                                <h4 class="widget-title lighter">
                                        <i class="ace-icon fa fa-bookmark blue"></i>
                                        <span id="lesson_name"><?php echo explode(',', $params)[1]; ?> <small><?php echo explode(',', $params)[2]; ?></small> </span>
                                </h4>

                                <div class="widget-toolbar">
                                        <a href="#" data-action="collapse">
                                                <i class="ace-icon fa fa-chevron-up"></i>
                                        </a>
                                </div>
                        </div>
                        <div class="space-6"></div>
<!--                        <div class="row">
                            <div class="col-xs-12">                    
                                <div class="form-group">
                                    <button id="create_lesson" type="button" class="btn btn-primary btn-sm pull-right"><i class="fa fa-plus"></i> Create Lesson </button>
                                </div>
                            </div>
                        </div>-->
                        <div class="space-6"></div>

                        <div class="widget-body">
                                <div class="widget-body-inner" style="display: block;">
                                    
                                            <div class="widget-main no-padding">
                                                 <div id="faq-tab-1" class="tab-pane fade in active">

                                                        <div id="faq-list-1" class="panel-group accordion-style1 accordion-style2"></div>
                                                </div>
                                            </div>
                                    <!-- /.widget-main -->
                                </div>
                        </div><!-- /.widget-body -->
                    </div>
            </div>
        
        
            <div id="dialog-message" class="hide">
                <div id="insideModal">
                    
                    <div class="col-sm-12">
                            <div class="widget-box transparent" id="recent-box">
                                    <div id="upload-tab" class="tab-pane">
                                            <h4 class="smaller lighter green">
                                                    <i class="ace-icon fa fa-upload"></i>
                                                    Upload materials
                                            </h4>

                                            <div class="panel-body">
                                                    <form enctype="multipart/form-data" action="uploading/uploadPhysicalFiles" method="post">
                                                            <input type="file" id="uploaded_file" name="files" />
                                                            <input type="hidden" name="upload_name" id="upload_id"/>
                                                            <input id="uploadExcel" type="submit" class="btn btn-success pull-right" value="Upload"/>
                                                    </form>
                                                    <div class="form-group">
                                                            Only accept .doc,.csv,.docx and .pdf files.
                                                    </div>
                                            </div>

                                    </div>
                            </div><!-- /.widget-box -->
                    </div>
                    
                    
                </div>
            </div>
            <div id="dialog-message2" class="hide">
                <div id="insideModal2">

                    <div class="space-4"></div>
                    <form class="form-horizontal" role="form">
                            <!-- #section:elements.form -->
                            <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Lesson name </label>

                                    <div class="col-sm-9">
                                            <input id="lesson_name_input" placeholder="Lesson name" class="col-xs-12" type="text">
                                    </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="form-field-1-1"> Description</label>

                                    <div class="col-sm-9">
                                        &nbsp;<textarea placeholder="Description" id="description_textarea"></textarea>
                                    </div>
                            </div>


                    </form>
                </div>
            </div>
        
            
<style>
.slide-container {
    display: none;
}
</style>
        
            <a href="#" id="start">Start</a>
            <div id="superContainer">
                <div class="slide-container">
                    <div class="question">Question 1</div>
                    <div><a href="#" class="next">next ...</a></div>
                </div>
                <div class="slide-container">
                    <div class="question">Question 2</div>
                    <div><a href="#" class="next">next ...</a></div>
                </div>
                <div class="slide-container">
                    <div class="question">Question 3</div>
                    <div><a href="#" class="next">next ...</a></div>
                </div>
            </div>


        
            </div>
        <!--</div>-->
</div><!-- /.col -->
<script>
    $(document).ready(function(){
        
        lessonsList();
         
        $('#uploaded_file').ace_file_input({
                style:'well',
                btn_choose:'Drop files here or click to choose',
                btn_change:null,
                no_icon:'ace-icon fa fa-cloud-upload',
                droppable:true,
                thumbnail:'small'
        }).on('change', function(){
                console.log($(this).data('ace_input_files'));
                console.log($(this).data('ace_input_method'));
                var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.csv|.doc|.docx |.pdf)$/;
                (regex.test($("#uploaded_file").val().toLowerCase())) ? '':bootbox.alert("you can only upload csv, doc and pdf.");
                $('.remove').attr('href','javascript:void(0)');
                $('.fa-times').attr('onclick','remove_upload()');
        });
        
         $("#create_lesson").click(function(){
             dialogBoxModal(2,'Create Lesson');
         });
    });
    
    
    var questionTimeout = null;

function goNext($el) {
    clearTimeout(questionTimeout);
    var $next = $el.next();
    $el.fadeOut(500, function() {
        if($next.length > 0) {
            $next.fadeIn(500, function() {
                questionTimeout = setTimeout(function() {
                    goNext($next);
                }, 5000);
            });
        }
        else {
            afterLastQuestion();
        }
    });
}
function afterLastQuestion(){
    alert("last question complete");
    $start.show();
}

var $superContainer = $("#superContainer").on('click', '.next', function() {
    goNext($(this).closest('.slide-container'));
    return false;
});

var $start = $("#start").on('click', function(){
    $(this).hide();
    $superContainer.find(".slide-container")
        .eq(0).clone(true,true)
        .prependTo(superContainer)
        .find(".next").trigger('click');
    return false;
});

    
    function lessonsList(){
        $.ajax({
            url:'student/subjects/getLessons',
            type:'post',
            dataType:'json',
            data:{id:<?php echo explode(',', $params)[0]; ?>},
            success:function(data){
                var list = '';
                $.each(data, function (key, val){
                    list += '<div class="panel panel-default">\n\
                            <div class="panel-heading" onclick="'+showDownloads(val.lesson_id)+'">\n\
                                    <a href="#faq-1-'+val.lesson_id+'" data-parent="#faq-list-'+val.lesson_id+'" data-toggle="collapse" class="accordion-toggle collapsed">\n\
                                            <i class="ace-icon fa fa-chevron-left pull-right" data-icon-hide="ace-icon fa fa-chevron-down" data-icon-show="ace-icon fa fa-chevron-left"></i>\n\
                                            <i class="ace-icon green fa fa-cloud-upload pull-right bigger-150" data-icon-hide="ace-icon fa fa-cloud-upload" data-icon-show="ace-icon fa fa-cloud-upload" title="Upload materials" onclick="dialogBoxModal(1,\''+val.lesson_id+'_'+val.lesson_name+'\')"></i>\n\
                                            &nbsp;<span class="green"></span> <span class="blue bold"><i class="ace-icon fa fa-files-o bigger-130"></i> '+val.lesson_name.toUpperCase()+'</span><small> '+val.lesson_description+' </small>\n\
                                    </a>\n\
                            </div>\n\
                            <div class="panel-collapse collapse" id="faq-1-'+val.lesson_id+'">\n\
                                    <div class="panel-body">\n\
                                            <div id="downloadlist'+val.lesson_id+'">\n\
                                            </div>\n\
                                    </div>\n\
                            </div>\n\
                    </div>';
                });
                
                $("#faq-list-1").html(list);
            }
        });
    }
    
    
    function getLessons(id){
        $.ajax({
                url:'student/subjects/getDownloads',
                type:'post',
                dataType:'json',
                success:function(data){
                        var upload = "";
                        $.each(data, function (key, val){
                                upload += val.uploaded_filename;
                        });
                        $("#insideModal"+id).html(upload);
                }
        });
    }
    
    function showDownloads(id){
        $.ajax({
            url:'student/subjects/getDownloads',
            type:'post',
            dataType:'json',
            data:{download_id:id}, 
            success:function(data){
                upload = '<div class="widget-header widget-header-flat">\n\
                                        <h5 class="widget-title lighter">\n\
                                                <i class="ace-icon fa fa-star orange"></i>\n\
                                                Documents \n\
                                        </h5>\n\
                                        <div class="widget-toolbar">\n\
                                        </div>\n\
                                </div><div class="space-4"></div><p>';
                $.each(data, function (key, val){
                    var filename = val.uploaded_filename.split('.')[1];
                    if(filename == 'docx' || filename == 'doc'){
                        var icon = 'word_icon';
                        btn_color = 'btn-info';
                    }else if(filename == 'xlsx'){ 
                         icon = 'excel-icon';
                         btn_color = 'btn-success';
                    }else if(filename == 'pdf'){
                         icon = 'pdf_icon';
                         btn_color = 'btn-purple';
                    }else{
                        icon = 'powerpoint-icon';
                        btn_color = 'btn-warning';
                    }
                    
                    

                      upload +=  '\n\
                                    <a id=fileShow'+val.uploaded_id+' title='+val.uploaded_path+' href="javascript:void(0)" class="btn btn-app '+btn_color+' btn-sm no-radius" style="padding-right: 10px; width:100px;" onclick=downloadFile('+val.uploaded_id+')>\n\
                                            <img src="assets/img/'+icon+'.png" style="width:30px; height: 30px; border: 0px; border-radius: 0px"/>\n\
                                            <small title='+val.uploaded_filename.split('.')[0]+'>'+val.uploaded_filename.split('.')[0].substring(0,5)+'..</small>\n\
                                    </a>';
                });
//                upload += '</p><div class="widget-header widget-header-flat">\n\
//                                        <h5 class="widget-title lighter">\n\
//                                                <i class="ace-icon fa fa-film orange"></i>\n\
//                                                Videos \n\
//                                        </h5>\n\
//                                        <div class="widget-toolbar">\n\
//                                        </div>\n\
//                                </div><div class="space-4"></div><p>';
//                upload +=' <iframe width="560" height="315" src="https://www.youtube.com/embed/Hl-zzrqQoSE?list=PLFE2CE09D83EE3E28" frameborder="0" allowfullscreen></iframe></p>'; 
                $("#downloadlist"+id).html(upload);
            }
        });
        

    }
                
    function dialogBoxModal(ident,titleName){
            if(ident == 1){
                var dialog_message = 'dialog-message';
                var dialog_width = '500';
                var header_name = titleName.split('_')[1];
                $("#upload_id").val(titleName.split('_')[0]);
//                $(".ui-dialog-buttonpane").removeClass('hide');
            }else{
                dialog_message = 'dialog-message2';
                dialog_width = '300';
                header_name = titleName;
                $("#upload_id").val(titleName.split('_')[0]);
//                $(".ui-dialog-buttonpane").addClass('hide');
            }
            dialogBox(
                    model = {
                            type: 'local',
                            data: dialog_message,
                            title: header_name,
                            title_icon: 'fa-ticket',
                            width: dialog_width,
                            buttons: [
                                    {
                                            'html': "<i class='ace-icon fa fa-times'></i> Cancel",
                                            'class': 'btn_cancel btn btn-xs',
                                            click: function(){
                                                    $(this).dialog('close');
                                            }
                                    },
                                    {
                                            'html': "<i class='ace-icon fa fa-check'></i> Submit",
                                            'class': 'btn-primary btn btn-xs',
                                            click: function(){
                                                    var subid = <?php echo explode(',', $params)[0]; ?>;
                                                    var name = $("#lesson_name_input").val();
                                                    var descript = $("#description_textarea").val();
                                                    if(name == "" || descript == ""){
                                                        return false;
                                                    }
                                                    $.ajax({
                                                       url:'student/subjects/addLessons',
                                                       type:'post',
                                                       data:{subjectid:subid,name:name,description: descript},
                                                       success:function(){
                                                             lessonsList();
                                                             $("#lesson_name_input").val("");
                                                             $("#description_textarea").val("");
                                                       }
                                                    });
                                            }
                                    }
                            ]
                    }
            );
    }
    function downloadFile(id){
            $.fileDownload($("#fileShow"+id).attr('title'),{});
    }
    function removeSubject(id){
            bootbox.confirm("Are you sure you want to remove this subject?", function(confirmed) {
                    if(confirmed === true){
                            $.ajax({
                                    url:'student/subjects/removeSubject',
                                    type:'post',
                                    data:{id:id},
                                    success:function(){
                                            location.reload();
                                    }
                            });
                    }
            });
    }
    
    
</script>
<script src="assets/js/jPages.js"></script>
<script src="assets/js/jquery.slimscroll.min.js"></script>
<script src="assets/js/bootbox.min.js"></script>
<script src="assets/js/ace-elements.min.js"></script>

<script src="assets/js/jquery.dataTables.min.js"></script>
<script src="assets/js/jquery.dataTables.bootstrap.js"></script>

<script src="assets/js/jquery.fileDownload.js"></script>
