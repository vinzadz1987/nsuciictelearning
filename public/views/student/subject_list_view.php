<div class="col-sm-12">
    <div class="page-header">
            <h1>
             All Subjects
            </h1>
    </div>
    <div class="row">
            <div class="form-group">
                    <div class="col-xs-12">
                        <button id="create_subject" type="button" class="btn btn-primary btn-sm pull-right"><i class="fa fa-plus"></i> Create Subject</button>
                    </div>
            </div>

            <div class="col-xs-12">
                    <div class="space-2"></div>
                    <table id="dataTable_view" class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th style="width:120px;">Subject Code</th>
                            <th style="width:150px;">Description</th>
                            <th style="width:80px;">Units</th>
                            <th style="width:80px;">Pre-requisites</th>
                            <th style="width:80px;">Lec Hours</th>
                            <th style="width:80px;">Lab Hours</th>
                            <th style="width:80px;">Year</th>
                            <th style="width:80px;">Semester</th>
                            <th style="width:80px;">Course</th>
                            <th style="width:30px;">Date</th>
                            <th style="width:100px;">Instructor</th>
                            <th style="width:50px;">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
            </div>
        
            <div id="dialog-message" class="hide">
                    <div id="insideModal"></div>
            </div>
			
			<div id="dialog-update" class="hide">
                <div id="insideModal2">

                    <div class="space-4"></div>
                    <form class="form-horizontal" role="form">
                            <!-- #section:elements.form -->
                            <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Subject Code* </label>

                                    <div class="col-sm-9">
                                            <input id="subjectid_edit" placeholder="Subject code" class="col-xs-12" type="text">
                                    </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="form-field-1-1"> Description* </label>

                                    <div class="col-sm-9">
                                        &nbsp;<textarea placeholder="Description" id="description-edit" class="form-control"></textarea>
                                    </div>
                            </div>

                            <!-- /section:elements.form -->
                            <div class="space-4"></div>

                            <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-2"> Pre-requisites </label>

                                    <div class="col-sm-9">
                                            <input placeholder="Pre-requisites" id="pre_requisites-edit" class="col-xs-10 col-sm-5 form-control" type="text">
                                    </div>
                            </div>

                            <div class="space-4"></div>

                            <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-2"> Lec Hours* </label>

                                    <div class="col-sm-9">
                                            <input placeholder="Lec Hours" id="lec_hours-edit" class="col-xs-10 col-sm-5 form-control" type="text">
                                    </div>
                            </div>

                            <div class="space-4"></div>

                            <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-2"> Lab Hours* </label>

                                    <div class="col-sm-9">
                                            <input placeholder="Lab Hours" id="lab_hrs-edit" class="col-xs-10 col-sm-5 form-control" type="text">
                                    </div>
                            </div>

                            <div class="space-4"></div>

                            <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-2"> Units* </label>

                                    <div class="col-sm-9">
                                            <input placeholder="Units" id="units-edit" class="col-xs-10 col-sm-5 form-control" type="text">
                                    </div>
                            </div>

                            <div class="space-4"></div>

                            <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-input-readonly"> Course* </label>

                                    <div class="col-sm-9">
                                            <select class="form-control" id="course-edit"></select>
                                    </div>
                            </div>
                                
                            <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-input-readonly"> Year* </label>

                                    <div class="col-sm-9">
                                            <select class="form-control" id="year-edit">
                                                <option value=""> Year </option>
                                                <option value="1">1st Year</option>
                                                <option value="2">2nd Year</option>
                                                <option value="3">3rd Year</option>
                                                <option value="4">4rth Year</option>
                                            </select>
                                    </div>
                            </div>

                            <div class="space-2"></div>

                            <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-4"> Semeste* </label>

                                    <div class="col-sm-9">
                                            <select class="form-control" id="sem-edit">
                                                <option value="">&nbsp;Semester </option>
                                                <option value="1">1st Semester</option>
                                                <option value="2">2nd Semester</option>
                                            </select>

                                    </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="form-field-5"> Instructor* </label>

                                    <div class="col-sm-9">
                                            <div class="clearfix">
                                                    <select class="form-control" id="instructor-edit"></select>
                                            </div>

                                     </div>
                            </div>

                            </div><!-- /.row -->


                    </form>
                </div>
			
			
            <div id="dialog-message2" class="hide">
                <div id="insideModal2">

                    <div class="space-4"></div>
                    <form class="form-horizontal" role="form">
                            <!-- #section:elements.form -->
                            <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Subject Code* </label>

                                    <div class="col-sm-9">
                                            <input id="subjectid" placeholder="Subject code" class="col-xs-12" type="text">
                                    </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="form-field-1-1"> Description* </label>

                                    <div class="col-sm-9">
                                        &nbsp;<textarea placeholder="Description" class="form-control" id="description"></textarea>
                                    </div>
                            </div>

                            <!-- /section:elements.form -->
                            <div class="space-4"></div>

                            <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-2"> Units* </label>

                                    <div class="col-sm-9">
                                            <input placeholder="Units" id="units" class="col-xs-10 col-sm-5 form-control" type="text">
                                    </div>
                            </div>

                            <div class="space-4"></div>

                            <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-2"> Pre-requisites </label>

                                    <div class="col-sm-9">
                                            <input placeholder="Pre-requisites" id="pre_requisites" class="col-xs-10 col-sm-5 form-control" type="text">
                                    </div>
                            </div>

                            <div class="space-4"></div>

                            <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-2"> Lec Hrs* </label>

                                    <div class="col-sm-9">
                                            <input placeholder="Lecture Hours" id="lec_hours" class="col-xs-10 col-sm-5 form-control" type="text">
                                    </div>
                            </div>
                    
                            <div class="space-4"></div>

                            <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-2"> Lab Hrs* </label>

                                    <div class="col-sm-9">
                                            <input placeholder="Lab Hours" id="lab_hours" class="col-xs-10 col-sm-5 form-control" type="text">
                                    </div>
                            </div>


                            <div class="space-4"></div>

                            <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-input-readonly"> Course* </label>

                                    <div class="col-sm-9">
                                            <select class="form-control" id="course"></select>
                                    </div>
                            </div>
                                
                            <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-input-readonly"> Year* </label>

                                    <div class="col-sm-9">
                                            <select class="form-control" id="year">
                                                <option value=""> Year </option>
                                                <option value="1">1st Year</option>
                                                <option value="2">2nd Year</option>
                                                <option value="3">3rd Year</option>
                                                <option value="4">4rth Year</option>
                                            </select>
                                    </div>
                            </div>

                            <div class="space-2"></div>

                            <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-4"> Semester* </label>

                                    <div class="col-sm-9">
                                            <select class="form-control" id="sem">
                                                <option value="">&nbsp;Semester </option>
                                                <option value="1">1st Semester</option>
                                                <option value="2">2nd Semester</option>
                                            </select>

                                    </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="form-field-5"> Instructor* </label>

                                    <div class="col-sm-9">
                                            <div class="clearfix">
                                                    <select class="form-control" id="instructor"></select>
                                            </div>

                                     </div>
                            </div>

                            </div><!-- /.row -->


                    </form>
                </div>
            </div>
            </div>
        <!--</div>-->
</div><!-- /.col -->

<script>
    $(document).ready(function(){

		var tables = $('table#dataTable_view').dataTable({
			"scrollX": true,
		});
         viewDataTable();
		 selects();
         courseSelect();
        
         $("#create_subject").click(function(){
             dialogBoxModalAdd('Create Subjects');
             
         });
         
         for (i = new Date().getFullYear(); i > 2013; i--)
        {
            $('#yearpicker').append($('<option />').val(i).html(i));
        }
         
				

         
    });
    
    
    function viewDataTable(){
        $.ajax({
            url:'student/subjects/getSubjects',
            type:'post',
            dataType:'json',
            success:function(data){
                var table = $('table#dataTable_view').DataTable({
                                 bDestroy: true,
                                 bLengthChange: false,
                                 paging: false
                              });
                             if (table) table.fnClearTable();
                $.each(data, function (key, val){
                     var photos = val.photo;
                    if(photos == ""){
                        photos = "assets/img/default-user2.png";
                    }
                    $('table#dataTable_view').dataTable().fnAddData( [
                        val.subject_code,
                        val.description,
                        val.units,
                        val.pre_requisites,
                        val.lecture_hrs,
                        val.lab_hrs,
                        val.year,
                        val.sem,
                        val.course_name,
                        val.date_added,
                        '<img class="pull-left img-circle" alt="" src="'+photos+'" style="height: 25px; width: 30px">&nbsp;'+val.firstname+' '+val.lastname,
                        '<div class="hidden-xs hidden-xs action-buttons">\n\
                                <a class="green" href="javascript:void(0)" title="Edit" \n\
                                            onclick="dialogBoxModalUpdate('+val.prospectid+')">\n\
                                        <i class="ace-icon fa fa-pencil bigger-100"></i>\n\
                                </a>\n\
                                <a class="red" href="javascript:void(0)" title="Delete" onclick="removeSubject1('+val.prospectid+')">\n\
                                        <i class="ace-icon fa fa-trash-o bigger-100"></i>\n\
                                </a>\n\
                        </div>'
                    ]);
                });
            }
        });
    }
    
	function removeSubject1(id){
		bootbox.confirm("Are you sure you want to remove this Subject?", function(confirmed) {
				if(confirmed === true){
					$.ajax({
						url:'student/subjects/removeSubject',
						type:'post',
						data:{id:id},
						success:function(){
                        viewDataTable();
						}
					});
				}
			});
    }
    
	
    function getLessons(id){
        $.ajax({
                url:'student/subjects/getLessonsCount',
                type:'post',
                dataType:'json',
                data:{id:id},
                success:function(data){
                        var clesson = "";
                        $.each(data, function (key, val){
                                clesson += val.count_lesson;
                        });
                        $("span#lesson_count"+id).html(clesson);
                }
        });
    }
    
    function showLessons(params){
        $.redirect('student/subjects/lessons', {params:params});
    }
    
    function dialogBoxModal(titleName){
			dialogBox(
				model = {
					type: 'local',
					data: 'dialog-message',
					title: titleName,
					title_icon: 'fa-plus',
					width: 800,
					buttons: [
						{
							'html': "<i class='ace-icon fa fa-times'></i> Cancel",
							'class': 'btn_cancel btn btn-xs',
							click: function(){
								$(this).dialog('close');
							}
						}
					]
				}
			);
		}
                
    function dialogBoxModalAdd(titleName){
            dialogBox(
                    model = {
                            type: 'local',
                            data: 'dialog-message2',
                            title: titleName,
                            title_icon: 'fa-ticket',
                            width: 500,
                            buttons: [
                                    {
                                            'html': "<i class='ace-icon fa fa-times'></i> Cancel",
                                            'class': 'btn_cancel btn btn-xs',
                                            click: function(){
                                                    $(this).dialog('close');
                                            }
                                    },
                                            {
                                            'html': "<i class='ace-icon fa fa-check'></i> Submit",
                                            'class': 'btn-primary btn btn-xs',
                                            click: function(){
                                                    var subid = $("#subjectid").val();
                                                    var desc = $("#description").val();
                                                    var units = $("#units").val();
                                                    var pre_requisites = $("#pre_requisites").val();
                                                    var lab_hrs = $("#lab_hours").val();
                                                    var lec_hrs = $("#lec_hours").val();
                                                    var year = $("#year").val();
                                                    var sem = $("#sem").val();
                                                    var inst = $("#instructor").val();
                                                    var course = $("#course").val();
                                                    if(subid == "" || desc == "" || units == "" || lab_hrs == "" || lec_hrs == "" || year == "" || sem == "" || inst == "" || course == ""){
                                                        alert("Required Fields is empty");
                                                        return false;
                                                    }
                                                    $.ajax({
                                                       url:'student/subjects/addSubject',
                                                       type:'post',
                                                       data:{subjectid:subid,description:desc,units: units,year:year,sem:sem,instructor:inst,course:course,pre_requisites,lab_hrs,lec_hrs},
                                                       success:function(){
                                                           alert("SUCCESFULLY ADDED SUBJECT");
                                                           // viewDataTable();
                                                       }
                                                    });
                                            }
                                    }
                            ]
                    }
            );
    }
	
	
	
	function dialogBoxModalUpdate(params){
			 $.ajax({
				url:'student/subjects/getSubjectsUpdate',
				type:'post',
				dataType:'json',
				data:{subid:params},
				success:function(data){
					var sc = un = desc = c = ins = sem = yr = pre_requisites = lec_hrs = lab_hrs = "";
					$.each(data, function (key, val){
						sc += val.subject_code;
						un += val.units;
						desc += val.description;
						c += val.course_name;
						ins += val.instructor_id;
						sem += val.sem;
						yr += val.year;
                        pre_requisites += val.pre_requisites;
                        lec_hrs += val.lecture_hrs;
                        lab_hrs += val.lab_hrs;
					});
					$("#subjectid_edit").val(sc);
					$("#units-edit").val(un);
					$("#description-edit").val(desc);
                    $("#pre_requisites-edit").val(pre_requisites);
                    $("#lec_hours-edit").val(lec_hrs);
                    $("#lab_hrs-edit").val(lab_hrs);
					courseSelectUpdate(c);
					instructor_idSelectUpdate(ins);
					semSelectUpdate(sem);
					yearSelectUpdate(yr);
					
				}
			});
            dialogBox(
                    model = {
                            type: 'local',
                            data: 'dialog-update',
                            title: "Update Subject",
                            title_icon: 'fa-ticket',
                            width: 400,
                            buttons: [
                                    {
                                            'html': "<i class='ace-icon fa fa-times'></i> Cancel",
                                            'class': 'btn_cancel btn btn-xs',
                                            click: function(){
                                                    $(this).dialog('close');
                                            }
                                    },
                                            {
                                            'html': "<i class='ace-icon fa fa-check'></i> Submit",
                                            'class': 'btn-primary btn btn-xs',
                                            click: function(){
                                                    var arrayData = 
                                                        {
                                                            id: params,
                                                            subid: $("#subjectid_edit").val(),
                                                            desc: $("#description-edit").val(),
                                                            units: $("#units-edit").val(),
                                                            year: $("#year-edit").val(),
                                                            sem: $("#sem-edit").val(),
                                                            inst: $("#instructor-edit").val(),
                                                            course: $("#course-edit").val(),
                                                            pre_requisites: $("#pre_requisites-edit").val(),
                                                            lec_hrs: $("#lec_hours-edit").val(),
                                                            lab_hrs: $("#lab_hrs-edit").val()
                                                        };
                                                    $.ajax({
                                                       url:'student/subjects/UpdateSubject',
                                                       type:'post',
													   data:{ data: arrayData },
                                                       success:function(){
                                                           alert("SUCCESFULLY UPDATED SUBJECT");
                                                           viewDataTable();
                                                       }
                                                    });
                                            }
                                    }
                            ]
                    }
            );
    }
	
	function yearSelectUpdate(yr){
		var option = '<option value="">&nbsp;Year </option>';
		for(i=1;i<=4;i++){
			if(yr == i){
				option+='<option value="'+i+'" selected>'+i+' Year</option>';
			}else{
				option+='<option value="'+i+'">'+i+' Year</option>';
			}
		}
		$("#year-edit").html(option);
	
	} 

	function semSelectUpdate(sem){
		var option = '<option value="">&nbsp;Semester </option>';
		for(i=1;i<=2;i++){
			if(sem == i){
				option+='<option value="'+i+'" selected>'+i+' Semester</option>';
			}else{
				option+='<option value="'+i+'">'+i+' Semester</option>';
			}
		}
		$("#sem-edit").html(option);
		
	}
    
    function removeSubject(id){
			bootbox.confirm("Are you sure you want to remove this subject?", function(confirmed) {
				if(confirmed === true){
					$.ajax({
						url:'student/subjects/removeSubject',
						type:'post',
						data:{id:id},
						success:function(){
							location.reload();
						}
					});
				}
			});
		}
                
                
    function selects(){
         $.ajax({
            url:'student/subjects/getInstructor',
            type:'post',
            dataType:'json',
            success: function(data){
                var option = '<option value="">&nbsp;Instructor </option>';
                $.each(data, function(key, val){
                   option += '<option value="'+val.instructor_id+'">'+val.firstname+' '+val.lastname+'</option>'; 
                });
                $("#instructor").html(option);
                $("#instructor-edit").html(option);
            }
         });
    }
	
	 function instructor_idSelectUpdate(ins){
		 //alert(ins);
         $.ajax({
            url:'student/subjects/getInstructor',
            type:'post',
            dataType:'json',
            success: function(data){
                var option = '<option value="">&nbsp;Instructor </option>';
                $.each(data, function(key, val){
					if(ins == val.instructor_id){
						option += '<option value="'+val.instructor_id+'" selected>'+val.firstname+' '+val.lastname+'</option>'; 
					}else{
						option += '<option value="'+val.instructor_id+'">'+val.firstname+' '+val.lastname+'</option>'; 
					}
                   //option += '<option value="'+val.instructor_id+'">'+val.firstname+' '+val.lastname+'</option>'; 
				   
					});
                $("#instructor").html(option);
                $("#instructor-edit").html(option);
            }
         });
    }
    
    function courseSelect(){
         $.ajax({
            url:'student/subjects/getCourse',
            type:'post',
            dataType:'json',
            success: function(data){
                var option = '<option value="">&nbsp;Course </option>';
                $.each(data, function(key, val){
                   option += '<option value="'+val.course_id+'">'+val.course_name+'</option>'; 
                });
                $("#course").html(option);
                //$("#course-edit").html(option);
            }
         });
    }
	
	function courseSelectUpdate(course){
		//alert(course);
         $.ajax({
            url:'student/subjects/getCourse',
            type:'post',
            dataType:'json',
            success: function(data){
                var option = '<option value="">&nbsp;Course </option>';
                $.each(data, function(key, val){
				   if(course == val.course_name){
					   option += '<option value="'+val.course_id+'" selected>'+val.course_name+'</option>'; 
				   }else{
					   option += '<option value="'+val.course_id+'">'+val.course_name+'</option>'; 
				   }
                   //option += '<option value="'+val.course_id+'">'+val.course_name+'</option>'; 
                });
                //$("#course").html(option);
                $("#course-edit").html(option);
            }
         });
    }
    
    
</script>

<script src="assets/js/jPages.js"></script>
<script src="assets/js/jquery.slimscroll.min.js"></script>
<script src="assets/js/bootbox.min.js"></script>
<script src="assets/js/ace-elements.min.js"></script>
<!--<script src="assets/js/ace.min.js"></script>-->

<script src="assets/js/jquery.dataTables.min.js"></script>
<script src="assets/js/jquery.dataTables.bootstrap.js"></script>
