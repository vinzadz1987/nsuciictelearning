<div class="col-sm-12">
    <div class="row">
        <div class="col-xs-12">
                <div class="space-2"></div>
                <table id="dataTable_view" class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <!--<th>Photo</th>-->
                        <th>Student ID</th>
                        <th>Firstname</th>
                        <th>Lastname</th>
                        <th>Middlename</th>
                        <th>Gender</th>
                        <th>Course</th>
                        <th>Year</th>
                        <th>Semester</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        
         $('table#dataTable_view').dataTable({"scrollX": true});
         viewDataTable();
        
        $('#uploaded_file').ace_file_input({
            style:'well',
            btn_choose:'Drop files here or click to choose',
            btn_change:null,
            no_icon:'ace-icon fa fa-cloud-upload',
            droppable:true,
            thumbnail:'small'
        }).on('change', function(){
            $('.remove').attr('href','javascript:void(0)');
            $('.fa-times').attr('onclick','remove_upload()');
            var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.csv)$/;
            (regex.test($("#uploaded_file").val().toLowerCase())) ? '':alert("please upload a valid CSV file.");
        });
        
        $("#uploadExcel").click(function(){
            uploadDataExcel();
        });
        
    });
    
    function viewDataTable(){
        $.ajax({
            url:'student/jqGrid_student_ctrl/getStudentData',
            type:'post',
            dataType:'json',
            success:function(data){
                $.each(data, function (key, val){
                    if(val.status == 0){
                        status = '<span class="label label-success arrowed-in arrowed-in-right">approved</span>';
                    }else{
                        status = '<span class="label label-danger arrowed" onclick="dialogBoxModal(\''+val.studids+'_'+val.student_id+'_'+val.firstname+'_'+val.lastname+'\')" style="cursor: pointer">pending</span>';
                    }
                    $('table#dataTable_view').dataTable().fnAddData( [
                        val.student_id,
                        val.firstname,
                        val.lastname,
                        val.middlename,
                        val.gender,
                        val.course,
                        val.curr_year,
                        val.semester,
                        status
                    ]);
                });
            }
        });
    }
    
    function uploadDataExcel(){
        var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.csv|.txt)$/;
        if (regex.test($("#uploaded_file").val().toLowerCase())) {
            if (typeof (FileReader) != "undefined") {
                var reader = new FileReader();
                reader.onload = function (e) {
                    var rows = e.target.result.split("\n");
                    var resultRows = [];
                    for (var i = 0; i < rows.length; i++) {
                        if(rows[i]!==""){
                            resultRows.push(rows[i]);
                        }
                    }
                    $.ajax({
                        url:'student/uploading/uploadFiles/students',
                        type:'post',
                        data:{data: resultRows},
                        success:function(res){
                            bootbox.alert("successfully uploaded.")
                        }
                    });
                }
                reader.readAsText($("#uploaded_file")[0].files[0]);
            } else {
                alert("This browser does not support HTML5.");
            }
        } else {
            alert("Please upload a valid CSV file.");
        }
    }
    
    function dialogBoxModal(titleName){
//        alert(titleName);
            $("#name").html('<span class="green"> Confirm student </span> '+titleName.split('_')[2]+' '+titleName.split('_')[3]+'<span class="red"> ? </span>');
            dialogBox(
                    model = {
                            type: 'local',
                            data: 'dialog-message2',
                            title:'Confirm Student',
                            title_icon: 'fa-user',
                            width: 400,
                            buttons: [
                                    {
                                            'html': "<i class='ace-icon fa fa-times'></i> Cancel",
                                            'class': 'btn_cancel btn btn-xs',
                                            click: function(){
                                                    $(this).dialog('close');
                                            }
                                    },
                                            {
                                            'html': "<i class='ace-icon fa fa-check'></i> Confirm",
                                            'class': 'btn-primary btn btn-xs',
                                            click: function(){
                                                    var id = titleName.split('_')[0];
                                                    var studid = titleName.split('_')[1];
                                                    var fname = titleName.split('_')[2];
                                                    var lname = titleName.split('_')[3];
                                                    $.ajax({
                                                       url:'student/students/getApproved',
                                                       type:'post',
                                                       data:{id:id,studid: studid,firstname:fname,lastname: lname},
                                                       success:function(){
                                                           location.reload();
                                                       }
                                                    });
                                            }
                                    }
                            ]
                    }
            );
    }
    
    function remove_upload(){
        location.reload();
    }
    
</script>

<script src="assets/js/jPages.js"></script>
<script src="assets/js/jquery.slimscroll.min.js"></script>
<script src="assets/js/bootbox.min.js"></script>
<script src="assets/js/ace-elements.min.js"></script>
<!--<script src="assets/js/ace.min.js"></script>-->

<script src="assets/js/jquery.dataTables.min.js"></script>
<script src="assets/js/jquery.dataTables.bootstrap.js"></script>
