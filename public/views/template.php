<!DOCTYPE html>
<base href="<?php print base_url(); ?>" />
<html lang="en">
	<head>
		<link rel="icon" href="https://cdn4.iconfinder.com/data/icons/SHINE7/general/400/bulb.png" type="image/gif">
		<?php include_once ('includes/meta.php'); ?>
	</head>
		<body class="no-skin">
			<!-- #section:basics/navbar.layout -->
			<div id="navbar" class="navbar navbar-default navbar-fixed-top">
				<?php include_once ('includes/navbar.php'); ?>
			</div>

			<!-- /section:basics/navbar.layout -->
			<div class="main-container" id="main-container">
				<script type="text/javascript">
					try{ace.settings.check('main-container' , 'fixed')}catch(e){}
				</script>

				<!-- #section:basics/sidebar -->
				<div id="sidebar" class="sidebar responsive">
					<?php include_once ('includes/sidebar.php'); ?>
				</div>

				<!-- /section:basics/sidebar -->
				<div class="main-content">
					<!-- #section:basics/content.breadcrumbs -->
					<div class="breadcrumbs" id="breadcrumbs">
						<script type="text/javascript">
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						</script>

						<ul class="breadcrumb">
							<li><i class="ace-icon fa fa-home home-icon"></i></li>
							<?php print set_breadcrumb();  ?>
						</ul><!-- /.breadcrumb -->

						<script>
						$(document).ready(function(){
							usersession = '<?php echo $this->session->userdata('access_name');?>';
								$.ajax({
									url:'profiles/getProfileInfo',
									type:'post',
									dataType:'json',
									data:{access_name:usersession},
									success:function(data){
									    var fname = "";
									    var photo = "";
									    $.each(data, function (key, val){
									        fname+=val.firstname;
									        photo+=val.photo;
									    });
									    $("#fullname").html(fname);
									    if(!photo){
									    	photo = 'assets/avatars/default_image.png';
									    }
									    $("#nav-user-photo").attr('src',photo);
									}
								});
							});
						</script>
						<!-- /section:basics/content.searchbox -->
					</div>

					<!-- /.ace-settings-container / Advance Search -->
					<?php
					if (!empty($content)): print $this->load->view('includes/advance_search_view', $content ,TRUE); endif;
					?>
					<!-- /.ace-settings-container / Advance Search -->

					<!-- /section:basics/content.breadcrumbs -->
					<div class="page-content">
						<?php if (!empty($page_title)): ?>
							<!-- /section:settings.box -->
							<div class="page-header">
								<h1>
									<?php print $page_title; ?>
									<small><i class="ace-icon fa fa-angle-double-right"></i></small>
								</h1>
							</div><!-- /.page-header -->
						<?php endif; ?>
						<?php
							if (!empty($content)): print $content; endif;
						?>
						<div class="clearfix" style=""></div>
					</div><!-- /.page-content -->
				</div><!-- /.main-content -->
				<div class="footer">
					<div class="footer-inner">
						<?php include_once ('includes/footer.php'); ?>
					</div>
				</div>
				<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
					<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
				</a>
			</div><!-- /.main-container -->

			<!-- page specific plugin scripts -->
			<!-- ace scripts -->
			<script src="assets/js/ace.min.js"></script>
			<script src="assets/js/ace-elements.min.js"></script>

			<!-- inline scripts related to this page -->
			<link rel="stylesheet" href="assets/css/ace.onpage-help.css" />
			<link rel="stylesheet" href="docs/assets/js/themes/sunburst.css" />

			<script type="text/javascript"> ace.vars['base'] = '..'; </script>
			<script src="assets/js/ace/ace.onpage-help.js"></script>
			<script src="docs/assets/js/rainbow.js"></script>
			<script src="docs/assets/js/language/generic.js"></script>
			<script src="docs/assets/js/language/html.js"></script>
			<script src="docs/assets/js/language/css.js"></script>
			<script src="docs/assets/js/language/javascript.js"></script>
			<script src="assets/nprogress/nprogress.js"></script>
		</body>
</html>
