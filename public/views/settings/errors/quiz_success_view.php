<div class="error-container">
	<div class="well">
		<h1 class="grey lighter smaller">
			<span class="blue bigger-125">
				<i class="ace-icon fa fa-check"></i>
				Done! 
			</span>
		</h1>

		<hr />
		<h3 class="lighter smaller">
			<i class="ace-icon fa fa-certificate blue bigger-125"></i>
			<?php echo $message; ?>
		</h3>

		<div class="space"></div>

		<div>
			<h4 class="lighter smaller">Meanwhile, try one of the following:</h4>

			<ul class="list-unstyled spaced inline bigger-110 margin-15">
				<li>
					<i class="ace-icon fa fa-hand-o-right blue"></i>
					Read the faq
				</li>

				<li>
					<i class="ace-icon fa fa-hand-o-right blue"></i>
					Give us more info on how this specific error occurred!
				</li>
			</ul>
		</div>

		<hr />
		<div class="space"></div>

		<div class="center">
			<a href="teacher/online_quiz_list" class="btn btn-success">
				<i class="ace-icon fa fa-arrow-left"></i>
				Back to quizes
			</a>

			<a href="home_touchcommerce" class="btn btn-primary">
				<i class="ace-icon fa fa-home bigger-120"></i>
				Home
			</a>
		</div>
	</div>
</div>