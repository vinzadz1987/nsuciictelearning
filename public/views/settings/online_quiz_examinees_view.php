<div class="col-sm-12">
	<h3 class="row header smaller lighter green">
		<span class="col-sm-8">
			<?php echo $title ?>
		</span>
	</h3>

	<div class="row">
		<div class="col-xs-12">
			<div class="row">
				<div id="grid-name" class="grid-wrap col-xs-12 compress head-primary">
					<table id="grid-table"></table>
					<div id="grid_pager"></div>
				</div>
			</div>
		</div><!-- /.col -->
	</div><!-- /.row -->
</div>


<!-- page specific plugin scripts -->
<script src="assets/js/jqGrid/jquery.jqGrid.preset.js"></script>
<script src="assets/js/jqGrid/jquery.jqGrid.min.js"></script>
<script src="assets/js/jqGrid/i18n/grid.locale-en.js"></script>
<!-- chosen plugin -->
<script src="assets/js/chosen.jquery.min.js"></script>

<script type="text/javascript">
	jQuery(function($) {

		quiz_id = <?php echo json_encode($quiz_id); ?>;
		
		$('#grid-table').wd_jqGrid({
			url: 'settings/jqGrid_settings_ctrl/load_data',
			module: 'online_quiz_examinees',
			module_data: {quiz_id:quiz_id},
			colNames: ['ID','Employee ID','Name','Partial Score','Final Score','Date','Status'],
			colModel: [	{name:'toqr_id',index:'toqr_id', width:0, sorttype:"int",hidden:true},
						{name:'toqr_emp_id',index:'toqr_emp_id',width:60, editable:true, align:'center'},
						{name:'emp_name',index:'emp_name', width:200, editable: true, align: 'center'},
						{name:'toqr_partial_score',index:'toqr_partial_score', width:60, editable: true, align: 'center'},
						{name:'toqr_final_score',index:'toqr_final_score', width:60, editable: true, align: 'center'},
						{name:'toqr_date',index:'toqr_date', width:60, editable: true, align: 'center'},
						{name:'toqr_status',index:'toqr_status', width:60, editable: true, align: 'center'},
					],
			sortname: 'toqr_id',
			//editurl: 'developers/jqGrid/actions',
			caption: false,
			height: 400,
			altRows: true,
			ondblClickRow: function(rowid,iRow,iCol,e){
				var rowData = $(this).jqGrid('getRowData',rowid);
				if( $.stripHtml(rowData.toqr_status) == 'Pending' )
					$.redirect('settings/online_quiz_pending', {quiz_id:$.get('quiz'), result_id:rowData.toqr_id, emp_name:rowData.emp_name});
			}
		});
	});

	$('#add_question').click(function(){
		$('.ajaxModal').modalBox({
			modal_view:'add_question_modal',
			modal_data: {title:'value'},
			title: false,
			width: 400,
			button_cancel: 'Cancel',
			button_ok: 'save',
		});
	})

</script>