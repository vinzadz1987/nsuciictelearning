<link rel="stylesheet" href="assets/css/chosen.css" />
<div class="row" id='mtd-kpi-report'>
	<div class="col-xs-12">		
		<div class="row">
				<div class="col-xs-12">
						<table id="grid_table"></table>
						<div id="grid_pager"></div>
				</div>
		</div>		
	</div>
</div>
<div id="modal_error" class="modal" tabindex="-1">
	<div class="modal-dialog" style="width:375px">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">TC Reporting Tool</h4>
			</div>

			<div class="modal-body">
				<div class="row">
					<div class="col-sm-12">
						<div id="errorhtml"></div>
					</div>
				</div>
			</div>

			<div class="modal-footer">
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i>
					Close
				</button>
				<button class="btn btn-sm btn-primary btnok" style="display:none;">
					<i class="ace-icon fa fa-check-square-o"></i>
					Delete
				</button>
			</div>
		</div>
	</div>
</div><!-- PAGE CONTENT ENDS -->
<?php
	switch($this->main_model->account_title()){
		case "ChazDean":$account = 'WEN';break;
		case "Comcast Order Entry":$account = 'Comcast-OE';break;
		case "Sheer Cover":case "Meaningful Beauty":$account = 'MBSC';break;
		case "TMobileUS":$account = 'T-Mobile';break;
		case "TMO-Prepaid":$account = 'T-Mobile SIM';break;
		case "VirginUSA":$account = 'Virgin Mobile';break;
		case "eHarm":$account = 'eHarmony';break;
		default:$account = $this->main_model->account_title();break;
	}
?>
<div id="dialog-add" class="hide">									
	<div class="col-xs-7">
	<form method="post" id="formsetting" role="form" class="form-horizontal">
		<div class="form-group">
			<div class="control-group col-sm-10">
				<div class="form-group">
					<label for="form-field-1" class="col-sm-3 control-label no-padding-right"> Account </label>
					<div class="col-sm-9">
						<input type="text" name="account" class="form-control" value='<?php echo $account; ?>' readonly>
					</div>
				</div>
			</div>
			<div class="control-group col-sm-10">
				<div class="form-group">
					<label for="form-field-1" class="col-sm-3 control-label no-padding-right"> Start Date </label>
					<div class="col-sm-9">
						<div class="input-group">
							<input class="form-control date-picker" id="start_dates" name="start_date" type="text" data-date-format="yyyy-mm-dd" />
							<span class="input-group-addon">
								<i class="fa fa-calendar bigger-110"></i>
							</span>
						</div>
						
					</div>
				</div>
			</div>
			<div class="control-group col-sm-10">
				<div class="form-group">
					<label for="form-field-1" class="col-sm-3 control-label no-padding-right"> End Date </label>
					<div class="col-sm-9">
						<div class="input-group">
							<input class="form-control date-picker" id="end_dates" name="end_date" type="text" data-date-format="yyyy-mm-dd" />
							<span class="input-group-addon">
								<i class="fa fa-calendar bigger-110"></i>
							</span>
						</div>
					</div>
				</div>
			</div>
			<div class="control-group col-sm-10">
				<div class="form-group">
					<label for="form-field-1" class="col-sm-3 control-label no-padding-right"> Goal Type </label>
					<div class="col-sm-9">
						<select id="goal_types" name="goal_type[]">
							<option value=""></option>
							<?
								$goaltype_array = array("Sales","NSO (Legacy)","NSO (OE)","Sales w/ NSO","Delivered","Assisted","Billed Hours","CVN","SPH","SPHwNSO","QA","360","CSAT","Absences","WFU","Linear Utilization","Cumm. Utilization","ART","IRT","AHT","Lates");
								
								foreach($goaltype_array as $goal){
									echo '<option value="'.$goal.'">'.$goal.'</option>';
								}
							?>
						</select>
					</div>
				</div>
			</div>
			<div class="control-group col-sm-10">
				<div class="form-group">
					<label for="form-field-1" class="col-sm-3 control-label no-padding-right"> Goal </label>
					<div class="col-sm-9">
						<input type="text" name="goal_value[]" id="goal_values" class="form-control">
					</div>
				</div>
			</div>
			<div class="control-group col-sm-10">
				<div class="form-group">
					<label for="form-field-1" class="col-sm-3 control-label no-padding-right"> Passing Score </label>
					<div class="col-sm-9">
						<input type="text" name="pass_score[]" id="pass_scores" class="form-control">
					</div>
				</div>
			</div>
			<div class="control-group col-sm-10 text-right">
				<button class="btn btn-app btn-yellow btn-xs" id="addsetting">
					<i class="ace-icon fa fa-arrow-circle-o-down bigger-160"></i>
					Add
				</button>
			</div>
			<div id="addsetting_div"></div>
			
		</div>				
		<hr>
	</form>
	</div>
	<div class="col-xs-5">

		<blockquote>
			<p class="lighter line-height-125">
				<h3 class="widget-title grey lighter">
					<i class="ace-icon fa fa-leaf green"></i>
					NOTE
				</h3>
				Setting goal metrics by assigning it by date. Click add button to add new goals for the said account and date range.
			</p>											
		</blockquote>

	</div>
							
	
</div>

<!-- page specific plugin scripts -->
<script src="assets/js/jqGrid/jquery.jqGrid.min.js"></script>
<script src="assets/js/jqGrid/i18n/grid.locale-en.js"></script>
<!-- chosen plugin -->
<script src="assets/js/chosen.jquery.min.js"></script>
<style>
	.btndiv{
		padding:2px 0;
	}
</style>
<script>
var curdate = new Date();
$(document).ready(function(){
	jqGrid_control(jqGrid_data());
	$('.chosen-select').chosen({allow_single_deselect:true}); 
	$(window).on('resize.chosen', function() {
		var w = $('.chosen-select').parent().width();
		$('.chosen-select').next().css({'width':w});
	}).trigger('resize.chosen');
	dialogWidget();
});
	

	//==================================//
	// jqGrid Function Start            //
	//==================================//
	function jqGrid_data(data,ops,adv){
		model = {
			data_case:'setting_comcast', //use for switch cases
			data: data||'', // any Data
			ops: ops||'', // operation/option data
			adv: adv||'', // advance data
			table: '#grid_table',			
			pager: '#grid_pager',
			container_url: 'settings/JqGrid_settings_ctrl/jqgrid_container',
			gridlocation: 'settings/JqGrid_settings_ctrl/load_jq_data/setting_comcast', // location of controller for gathering data from database
			headers: ['ACTION','GOAL','PASS','TYPE','START DATE','END DATE','ACCOUNT'], //header for the table
			names: ['actions','goal_value','pass_score','goal_type','start_date','end_date','account'], //use for modifaction and functons. assign names for data in a row.
			index: 'true', //set true if index is equal to name. use for query. assign index for data in a row.
			widths: [], //specific width of every column
			sortname: 'id', //use to initialize sort
			autoHeight: 'true',
			//donot add other jqgrid function here. only in jqGridOptions
			jqGridOptions: { // other functions of jqGrid. if not set default will be use. === refer to main.js ===
				// boolean value must be string e.g. 'true' or 'false'
				shrinkToFit:'true',
				height: window.innerHeight - 325,
				altRows: 'true',
				sortorder:'desc',
				editurl: 'settings/jqGrid_settings_ctrl/edit_row/kpi_setting',
				//toolbarAdd: 'true',
				caption:'Goal Setting - '+<?php echo json_encode($this->main_model->account_title()); ?>			
			}/*,
			exportToFile: {'show':true,'type':'json'}	// show:false - do not show export button, type:html -  */
		};
		return model;
	}
	
	function jqGrid_extension(){
		$("#grid_table").jqGrid('setGridWidth',window.innerWidth - 270);
		$("#grid_table").navButtonAdd('#grid_pager', {
			caption: "",
			title: "Click here to add new record",
			buttonicon: "ui-icon ace-icon fa fa-plus-circle purple",
			onClickButton: function() {
				shownewRecord();
				resetForm();
			},
			position: "first"
		});
	}
	function loadComplete_global(model,data){
		setWidgetToolbarItems({
			exportToFile:
				{
					excel:[model,data,'json']
				}
		});

		if(model.table == '#grid_table'){
			setWidgetToolbarItems({
				widgetButton:
					[
						{id:'graph1', classes:'', icon:'fa-bar-chart-o', title:'', text:'Show Graph', tableName:''}
					]	
			});
		}
		NProgress.done();
		_grid_model_loaded = model;
	}
	function styleCheckbox(table) {
		$(table).find('input[name=start_date]').datepicker({format:'yyyy-mm-dd' , autoclose:true})
		$(table).find('input[name=end_date]').datepicker({format:'yyyy-mm-dd' , autoclose:true})
	}
	
	function onSelectRow_global(model,aRowids,status){
		$.post("settings/kpi_settings/getgoalinfo",{id:aRowids},function(returnval){
			$("#ids").val(returnval[0].id);
			$("#goal_types").val(returnval[0].goal_type).trigger("chosen:updated");
			$('#goal_types').prop('disabled', true).trigger("chosen:updated");
			$("#goal_values").val(returnval[0].goal_value);
			$("#pass_scores").val(returnval[0].pass_score);
			$("#start_dates").val(returnval[0].start_date);
			$("#end_dates").val(returnval[0].end_date);
			// $("#collapseOne").addClass('in');
			// $("#collapseOne").css('height','auto');
		},'json');
	};
	function style_edit_form(form) {

		//enable datepicker on "sdate" field and switches for "stock" field
		form.find('input[name=start_date]').datepicker({format:'yyyy-mm-dd' , autoclose:true})
			.end().find('input[name=end_date]').datepicker({format:'yyyy-mm-dd' , autoclose:true})
			.end().find('input[role=checkbox]')
				.addClass('ace ace-switch ace-switch-5').after('<span class="lbl"></span>');
				   //don't wrap inside a label element, the checkbox value won't be submitted (POST'ed)
				  //.addClass('ace ace-switch ace-switch-5').wrap('<label class="inline" />').after('<span class="lbl"></span>');
	
		style_form_with_ui(form);
		//update buttons classes
		style_form_update_buttons(form);	

	}
	function add_afterSubmit_global(){
		$("#grid_table").jqGrid('setGridParam',{datatype:'json'}).trigger('reloadGrid');
	};
	//==================================//
	// jqGrid Function End              //
	//==================================//

	/*=======================================================================
				Search functions
	========================================================================*/

	$('#btn_search').click(function(e){
		e.preventDefault();

		var search = ($('#grid_search').val())?$('#grid_search').val():'';
		var byDate = ($('#grid_byDate').val())?$('#grid_byDate').val():'';
		var advance = ($('#grid_advance').val())?$('#grid_advance').val():'';		
		
		gridData = {data:search,ops:byDate,adv:advance};
		searchOnGrid('settings/JqGrid_settings_ctrl','grid_table','setting_comcast',gridData);

		searchData = {
			grid: 'grid_table',
			title: 'KPI - '+<?php echo json_encode($this->main_model->account_title()); ?>,
			search: search,
			date: byDate,
			field: advance,
		}
		//main.js
		jqGrid_caption(searchData);
	});
	
	
	$(".btnreset").click(function(){
		resetForm();
	});
	function resetForm(){		
		$('#formsetting')[0].reset();
		$('#addsetting_div').html('');
	}
	
	
	$("#addsetting").click(function(e){
		e.preventDefault();
		addnewSetting();
	});
	var set_ctr = 1;
	function addnewSetting(){
		var label_class = 'for="form-field-1" class="col-sm-3 control-label no-padding-right"';
		var label_txt = ['Goal Type','Goal','Passing Score'];
		var goal_array  =["Sales","NSO (Legacy)","NSO (OE)","Sales w/ NSO","Delivered","Assisted","Billed Hours","CVN","SPH","SPHwNSO","QA","360","CSAT","Absences","WFU","Linear Utilization","Cumm. Utilization","ART","IRT","AHT","Lates"];
		var div_input = '<select id="goal_types" name="goal_type[]" ><option value=""></option>';
		$.each(goal_array,function (i,arr_val){
			div_input = div_input+'<option value="'+arr_val+'">'+arr_val+'</option>';
		});
		div_input = div_input+'</select>';
		var divcontent = '<div id="add_divcontent'+set_ctr+'">';
		for(var i = 1;i<=3;i++){
			var txt_arr = ['goal_value','pass_score'];
			if(i>1){
				div_input = '<input type="text" name="'+txt_arr[i-2]+'[]" id="'+txt_arr[i-2]+'" class="form-control">';
			}
			
			divcontent = divcontent +'<div class="control-group col-sm-10">\
								<div class="form-group">\
									<label '+label_class+'> '+label_txt[i-1]+' </label>\
									<div class="col-sm-9">\
										'+div_input+'\
										</div>\
									</div>\
								</div>';
		}
		divcontent = divcontent +'<div class="control-group col-sm-10"><div class="form-group"><button class="btn btn-danger btn-xs pull-right" onclick="remove_content('+set_ctr+')">\
									<i class="ace-icon fa fa-trash-o  bigger-110 icon-only"></i></button></div></div></div>';
		$("#addsetting_div").append(divcontent);
		set_ctr++;
	}
	function remove_content(ctr){
		$("#add_divcontent"+ctr).remove();
	}
	
	function shownewRecord(){
		$( "#dialog-add" ).removeClass('hide').dialog({
			resizable: false,
			modal: true,
			title:"<div class='widget-header widget-header-small'><h4 class='smaller'><i class='ace-icon fa fa-cog'></i> Setting Information</div>",
			width:'900',
			title_html: true,
			buttons: [
				{
					html: "<i class='ace-icon fa fa-floppy-o bigger-110'></i>&nbsp; Save",
					"class" : "btn btn-success btn-xs",
					click: function() {
						//$( this ).dialog( "close" );
						saveSetting();
						$('#grid_table').jqGrid('setGridParam',{datatype:'json'}).trigger('reloadGrid');
					}
				}
				,
				{
					html: "<i class='ace-icon fa fa-times bigger-110'></i>&nbsp; Cancel",
					"class" : "btn btn-xs",
					click: function() {
						$( this ).dialog( "close" );
					}
				}
			]
		});
	}
	function saveSetting(){
		var formdata = $('#formsetting').serializeArray();
		var ctr_empty=0;
		$('#formsetting input').each(function(){
			if($(this).val()=='') ctr_empty++;
		});
		$('#formsetting select').each(function(){
			if($(this).val()=='') ctr_empty++;
		});
		if(ctr_empty>0){ 
			alert("All fields are required!");
		}else{
			$( "#dialog-add" ).dialog( "close" );
			$.post('settings/kpi_settings/savesettings',formdata,function(){
				$('#modal_error').modal('show');
				$("#errorhtml").html('<ul class="list-unstyled spaced"><li class="green"><i class="ace-icon fa fa-check bigger-110 green"></i>Successfully saved!</li></ul>');
				
				$(".btnok").hide();
				
			},'json');
		}
		
	}
	$("#goal_types").click(function(){
		var placehold = ($(this).val()=='ART' || $(this).val()=='IRT' || $(this).val()=='AHT')?'time format 00:00:00':'';
		$('#goal_values').attr('placeholder',placehold);
		$('#pass_scores').attr('placeholder',placehold);
	});
</script>

