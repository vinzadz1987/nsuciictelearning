<link rel="stylesheet" href="assets/css/chosen.css" />
<div class="row" id='mtd-kpi-report'>
	<div class="col-xs-12">		
		<div class="row">
				<div class="col-xs-12">
					<!-- #section:elements.accordion -->
					<!-- 
					<div id="accordion" class="accordion-style1 panel-group">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
										<i class="ace-icon fa fa-angle-down bigger-110" data-icon-hide="ace-icon fa fa-angle-down" data-icon-show="ace-icon fa fa-angle-right"></i>
										&nbsp;Setting Information
									</a>
								</h4>
							</div>

							<div class="panel-collapse collapse" id="collapseOne">
								<div class="panel-body">
									
									<div class="col-xs-5">
									<form method="post" id="formsetting" role="form" class="form-horizontal">
										<input type="hidden" name="id" id="ids" class="form-control">
										<div class="form-group">
											<div class="control-group col-sm-10">
												<div class="form-group">
													<label for="form-field-1" class="col-sm-3 control-label no-padding-right"> Account </label>
													<div class="col-sm-9">
														<input type="text" name="account" class="form-control" value='<?php echo $this->main_model->account_title(); ?>' readonly>
													</div>
												</div>
											</div>
											<div class="control-group col-sm-10">
												<div class="form-group">
													<label for="form-field-1" class="col-sm-3 control-label no-padding-right"> Start Date </label>
													<div class="col-sm-9">
														<div class="input-group">
															<input class="form-control date-picker" id="start_dates" name="start_dates" type="text" data-date-format="yyyy-mm-dd" />
															<span class="input-group-addon">
																<i class="fa fa-calendar bigger-110"></i>
															</span>
														</div>
														
													</div>
												</div>
											</div>
											<div class="control-group col-sm-10">
												<div class="form-group">
													<label for="form-field-1" class="col-sm-3 control-label no-padding-right"> End Date </label>
													<div class="col-sm-9">
														<div class="input-group">
															<input class="form-control date-picker" id="end_dates" name="end_date" type="text" data-date-format="yyyy-mm-dd" />
															<span class="input-group-addon">
																<i class="fa fa-calendar bigger-110"></i>
															</span>
														</div>
													</div>
												</div>
											</div>
											<div class="control-group col-sm-10">
												<div class="form-group">
													<label for="form-field-1" class="col-sm-3 control-label no-padding-right"> Goal Type </label>
													<div class="col-sm-9">
														<select id="goal_types" name="goal_type">
															<?
																$goaltype_array = array("Sales","NSO (Legacy)","NSO (OE)","Sales w/ NSO","Delivered","Assisted","Billed Hours","CVN","SPH","SPHwNSO","QA","360","CSAT","Absences","WFU","Linear Utilization","Cumm. Utilization","ART","IRT","AHT","Lates");
																
																foreach($goaltype_array as $goal){
																	echo '<option value="'.$goal.'">'.$goal.'</option>';
																}
															?>
														</select>
													</div>
												</div>
											</div>
											<div class="control-group col-sm-10">
												<div class="form-group">
													<label for="form-field-1" class="col-sm-3 control-label no-padding-right"> Goal </label>
													<div class="col-sm-9">
														<input type="text" name="goal_value" id="goal_values" class="form-control">
													</div>
												</div>
											</div>
											<div class="control-group col-sm-10">
												<div class="form-group">
													<label for="form-field-1" class="col-sm-3 control-label no-padding-right"> Passing Score </label>
													<div class="col-sm-9">
														<input type="text" name="pass_score" id="pass_scores" class="form-control">
													</div>
												</div>
											</div>
											<div class="control-group col-sm-10 text-right">
												<button class="btn btn-app btn-yellow btn-xs" id="addsetting">
													<i class="ace-icon fa fa-arrow-circle-o-down bigger-160"></i>
													Add
												</button>
											</div>
											<div id="addsetting_div"></div>
											
										</div>				
										<hr>
									</form>
										<div class="btndiv">
											<button class="btn btn-success btnadd">
												<i class="ace-icon fa fa-file align-top bigger-125"></i>
												Save
											</button>				
											<button class="btn btn-primary btnupdate">
												<i class="ace-icon fa fa-pencil-square-o align-top bigger-125"></i>
												Update
											</button>
											
											<button class="btn btn-danger btndelete">
												<i class="ace-icon fa fa-trash-o align-top bigger-125"></i>
												Delete
											</button>
											
											<button type="reset" class="btn btnreset">
												<i class="ace-icon fa fa-undo bigger-110"></i>
												Reset
											</button>
										</div>
									</div>
									<div class="col-xs-4">

										<blockquote>
											<p class="lighter line-height-125">
												<h3 class="widget-title grey lighter">
													<i class="ace-icon fa fa-leaf green"></i>
													NOTE
												</h3>
												Setting goal metrics by assigning it by date. Click add button to add new goals for the said account and date range.
											</p>											
										</blockquote>

									</div>
								</div>
							</div>
						</div>
					</div>
					-->
					
						<table id="grid_table"></table>
						<div id="grid_pager"></div>
				</div>
		</div>		
	</div>
</div>
<div id="modal_error" class="modal" tabindex="-1">
	<div class="modal-dialog" style="width:375px">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">TC Reporting Tool</h4>
			</div>

			<div class="modal-body">
				<div class="row">
					<div class="col-sm-12">
						<div id="errorhtml"></div>
					</div>
				</div>
			</div>

			<div class="modal-footer">
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i>
					Close
				</button>
				<button class="btn btn-sm btn-primary btnok" style="display:none;">
					<i class="ace-icon fa fa-check-square-o"></i>
					Delete
				</button>
			</div>
		</div>
	</div>
</div><!-- PAGE CONTENT ENDS -->

<div id="dialog-add" class="hide">									
	<div class="col-xs-7">
	<form method="post" id="formsetting" role="form" class="form-horizontal">
		<input type="hidden" name="id" id="ids" class="form-control">
		<div class="form-group">
			<div class="control-group col-sm-10">
				<div class="form-group">
					<label for="form-field-1" class="col-sm-3 control-label no-padding-right"> Account </label>
					<div class="col-sm-9">
						<input type="text" name="account" class="form-control" value='<?php echo $this->main_model->account_title(); ?>' readonly>
					</div>
				</div>
			</div>
			<div class="control-group col-sm-10">
				<div class="form-group">
					<label for="form-field-1" class="col-sm-3 control-label no-padding-right"> Start Date </label>
					<div class="col-sm-9">
						<div class="input-group">
							<input class="form-control date-picker" id="start_dates" name="start_dates" type="text" data-date-format="yyyy-mm-dd" />
							<span class="input-group-addon">
								<i class="fa fa-calendar bigger-110"></i>
							</span>
						</div>
						
					</div>
				</div>
			</div>
			<div class="control-group col-sm-10">
				<div class="form-group">
					<label for="form-field-1" class="col-sm-3 control-label no-padding-right"> End Date </label>
					<div class="col-sm-9">
						<div class="input-group">
							<input class="form-control date-picker" id="end_dates" name="end_date" type="text" data-date-format="yyyy-mm-dd" />
							<span class="input-group-addon">
								<i class="fa fa-calendar bigger-110"></i>
							</span>
						</div>
					</div>
				</div>
			</div>
			<div class="control-group col-sm-10">
				<div class="form-group">
					<label for="form-field-1" class="col-sm-3 control-label no-padding-right"> Goal Type </label>
					<div class="col-sm-9">
						<select id="goal_types" name="goal_type">
							<?
								$goaltype_array = array("Sales","NSO (Legacy)","NSO (OE)","Sales w/ NSO","Delivered","Assisted","Billed Hours","CVN","SPH","SPHwNSO","QA","360","CSAT","Absences","WFU","Linear Utilization","Cumm. Utilization","ART","IRT","AHT","Lates");
								
								foreach($goaltype_array as $goal){
									echo '<option value="'.$goal.'">'.$goal.'</option>';
								}
							?>
						</select>
					</div>
				</div>
			</div>
			<div class="control-group col-sm-10">
				<div class="form-group">
					<label for="form-field-1" class="col-sm-3 control-label no-padding-right"> Goal </label>
					<div class="col-sm-9">
						<input type="text" name="goal_value" id="goal_values" class="form-control">
					</div>
				</div>
			</div>
			<div class="control-group col-sm-10">
				<div class="form-group">
					<label for="form-field-1" class="col-sm-3 control-label no-padding-right"> Passing Score </label>
					<div class="col-sm-9">
						<input type="text" name="pass_score" id="pass_scores" class="form-control">
					</div>
				</div>
			</div>
			<div class="control-group col-sm-10 text-right">
				<button class="btn btn-app btn-yellow btn-xs" id="addsetting">
					<i class="ace-icon fa fa-arrow-circle-o-down bigger-160"></i>
					Add
				</button>
			</div>
			<div id="addsetting_div"></div>
			
		</div>				
		<hr>
	</form>
	</div>
	<div class="col-xs-5">

		<blockquote>
			<p class="lighter line-height-125">
				<h3 class="widget-title grey lighter">
					<i class="ace-icon fa fa-leaf green"></i>
					NOTE
				</h3>
				Setting goal metrics by assigning it by date. Click add button to add new goals for the said account and date range.
			</p>											
		</blockquote>

	</div>
							
	
</div>

<!-- page specific plugin scripts -->
<script src="assets/js/jqGrid/jquery.jqGrid.min.js"></script>
<script src="assets/js/jqGrid/i18n/grid.locale-en.js"></script>
<!-- chosen plugin -->
<script src="assets/js/chosen.jquery.min.js"></script>
<style>
	.btndiv{
		padding:2px 0;
	}
</style>
<script>
var curdate = new Date();
$(document).ready(function(){
	jqGrid_control(jqGrid_data());
	$('.chosen-select').chosen({allow_single_deselect:true}); 
	$(window).on('resize.chosen', function() {
		var w = $('.chosen-select').parent().width();
		$('.chosen-select').next().css({'width':w});
	}).trigger('resize.chosen');
	
});
	function shownewRecord(){
		$( "#dialog-add" ).removeClass('hide').dialog({
			resizable: false,
			modal: true,
			width:'900',
			title_html: true,
			buttons: [
				{
					html: "<i class='ace-icon fa fa-floppy-o bigger-110'></i>&nbsp; Save",
					"class" : "btn btn-success btn-xs",
					click: function() {
						$( this ).dialog( "close" );
					}
				}
				,
				{
					html: "<i class='ace-icon fa fa-times bigger-110'></i>&nbsp; Cancel",
					"class" : "btn btn-xs",
					click: function() {
						$( this ).dialog( "close" );
					}
				}
			]
		});
	}

	//==================================//
	// jqGrid Function Start            //
	//==================================//
	function jqGrid_data(data,ops,adv){
		model = {
			data_case:'setting_comcast', //use for switch cases
			data: data||'', // any Data
			ops: ops||'', // operation/option data
			adv: adv||'', // advance data
			table: '#grid_table',			
			pager: '#grid_pager',
			container_url: 'settings/JqGrid_settings_ctrl/jqgrid_container',
			gridlocation: 'settings/JqGrid_settings_ctrl/load_jq_data/setting_comcast', // location of controller for gathering data from database
			headers: ['ACTION','GOAL','PASS','TYPE','START DATE','END DATE','ACCOUNT'], //header for the table
			names: ['actions','goal_value','pass_score','goal_type','start_date','end_date','account'], //use for modifaction and functons. assign names for data in a row.
			index: 'true', //set true if index is equal to name. use for query. assign index for data in a row.
			widths: [], //specific width of every column
			sortname: 'id', //use to initialize sort
			autoHeight: 'true',
			//donot add other jqgrid function here. only in jqGridOptions
			jqGridOptions: { // other functions of jqGrid. if not set default will be use. === refer to main.js ===
				// boolean value must be string e.g. 'true' or 'false'
				shrinkToFit:'true',
				height: window.innerHeight - 325,
				altRows: 'true',
				sortorder:'desc',
				editurl: 'settings/jqGrid_settings_ctrl/edit_row/kpi_setting',
				//toolbarAdd: 'true',
				caption:'Goal Setting - '+<?php echo json_encode($this->main_model->account_title()); ?>			
			}/*,
			exportToFile: {'show':true,'type':'json'}	// show:false - do not show export button, type:html -  */
		};
		return model;
	}
	
	function jqGrid_extension(){
		$("#grid_table").jqGrid('setGridWidth',window.innerWidth - 270);
		$("#grid_table").navButtonAdd('#grid_pager', {
			caption: "",
			title: "Click here to add new record",
			buttonicon: "ui-icon ace-icon fa fa-plus-circle purple",
			onClickButton: function() {
				shownewRecord();
				resetForm();
			},
			position: "first"
		});
	}
	function loadComplete_global(model,data){
		setWidgetToolbarItems({
			exportToFile:
				{
					excel:[model,data,'json']
				}
		});

		if(model.table == '#grid_table'){
			setWidgetToolbarItems({
				widgetButton:
					[
						{id:'graph1', classes:'', icon:'fa-bar-chart-o', title:'', text:'Show Graph', tableName:''}
					]	
			});
		}
		NProgress.done();
		_grid_model_loaded = model;
	}
	function styleCheckbox(table) {
		$(table).find('input[name=start_date]').datepicker({format:'yyyy-mm-dd' , autoclose:true})
		$(table).find('input[name=end_date]').datepicker({format:'yyyy-mm-dd' , autoclose:true})
	}
	
	function onSelectRow_global(model,aRowids,status){
		$.post("settings/kpi_settings/getgoalinfo",{id:aRowids},function(returnval){
			$("#ids").val(returnval[0].id);
			$("#goal_types").val(returnval[0].goal_type).trigger("chosen:updated");
			$('#goal_types').prop('disabled', true).trigger("chosen:updated");
			$("#goal_values").val(returnval[0].goal_value);
			$("#pass_scores").val(returnval[0].pass_score);
			$("#start_dates").val(returnval[0].start_date);
			$("#end_dates").val(returnval[0].end_date);
			// $("#collapseOne").addClass('in');
			// $("#collapseOne").css('height','auto');
		},'json');
	};
	function style_edit_form(form) {

		//enable datepicker on "sdate" field and switches for "stock" field
		form.find('input[name=start_date]').datepicker({format:'yyyy-mm-dd' , autoclose:true})
			.end().find('input[name=end_date]').datepicker({format:'yyyy-mm-dd' , autoclose:true})
			.end().find('input[role=checkbox]')
				.addClass('ace ace-switch ace-switch-5').after('<span class="lbl"></span>');
				   //don't wrap inside a label element, the checkbox value won't be submitted (POST'ed)
				  //.addClass('ace ace-switch ace-switch-5').wrap('<label class="inline" />').after('<span class="lbl"></span>');
	
		style_form_with_ui(form);
		//update buttons classes
		style_form_update_buttons(form);	

	}
	function add_afterSubmit_global(){
		$("#grid_table").jqGrid('setGridParam',{datatype:'json'}).trigger('reloadGrid');
	};
	//==================================//
	// jqGrid Function End              //
	//==================================//

	/*=======================================================================
				Search functions
	========================================================================*/

	$('#btn_search').click(function(e){
		e.preventDefault();

		var search = ($('#grid_search').val())?$('#grid_search').val():'';
		var byDate = ($('#grid_byDate').val())?$('#grid_byDate').val():'';
		var advance = ($('#grid_advance').val())?$('#grid_advance').val():'';		
		
		gridData = {data:search,ops:byDate,adv:advance};
		searchOnGrid('settings/JqGrid_settings_ctrl','grid_table','setting_comcast',gridData);

		searchData = {
			grid: 'grid_table',
			title: 'KPI - '+<?php echo json_encode($this->main_model->account_title()); ?>,
			search: search,
			date: byDate,
			field: advance,
		}
		//main.js
		jqGrid_caption(searchData);
	});
	
	$(".btnadd").click(function(){
		btnactions('add');		
	});
	$(".btnupdate").click(function(){
		btnactions('edit');
	});
	$(".btndelete").click(function(){
		var errormsg = "";
		var litxt = '<li class="text-success">';
		errormsg = ($("#ids").val()=='')?errormsg+'<li class="text-warning bigger-110 red"><i class="ace-icon fa fa-exclamation-triangle"></i>You must first click on the list below!</li>':errormsg;
		if(errormsg==""){	
			$.post("settings/kpi_settings/getgoalinfo",{id:$("#ids").val()},function(returnval){
				$('#modal_error').modal('show');
				$(".btnok").show();
				 $("#errorhtml").html('<h2>Goal Setting Information</h2><ul class="list-unstyled spaced">\
				 '+litxt+'<b>Account :</b> '+returnval[0].account+'</li>\
				 '+litxt+'<b>Goal Type :</b> '+returnval[0].goal_type+'</li>\
				 '+litxt+'<b>Goal Value :</b> '+returnval[0].goal_value+'</li>\
				 '+litxt+'<b>Pass Score :</b> '+returnval[0].pass_score+'</li>\
				'+litxt+'<b>Start Date :</b> '+returnval[0].start_date+'</li>\
				'+litxt+'<b>End Date :</b> '+returnval[0].end_date+'</li></ul>');			
			},'json')
			
		}else{
			$('#modal_error').modal('show');
			$("#errorhtml").html('<ul class="list-unstyled spaced">'+errormsg+'</ul>');
		}
	});
	$(".btnok").click(function(){
		btnactions('delete');
	});
	$(".btnreset").click(function(){
		resetForm();
	});
	function resetForm(){
		$("#ids").val('');
		$("#goal_types").val(' ').trigger("chosen:updated");
		$('#goal_types').prop('disabled', false).trigger("chosen:updated");
		$('#addsetting_div').html('');
	}
	function btnactions(btn){
		var errormsg = "";
		var errortxt = '<li class="text-warning bigger-110 red"><i class="ace-icon fa fa-exclamation-triangle"></i>';
		switch(btn){
			case "add":
				errormsg = ($("#goal_types").val()=='')?errormsg+errortxt+'Please choose a goal type!</li>':errormsg;
				errormsg = ($("#goal_values").val()=='')?errormsg+errortxt+'Goal value is empty!<li>':errormsg;
				errormsg = ($("#pass_scores").val()=='')?errormsg+errortxt+'Pass score is empty!<li>':errormsg;
				errormsg = ($("#start_dates").val()=='')?errormsg+errortxt+'Start Date is empty!<li>':errormsg;
				errormsg = ($("#end_dates").val()=='')?errormsg+errortxt+'End Date is empty!<li>':errormsg;
				var datasubmit = $("#formsetting").serialize();
				var url = "addgoalsetting";
				var successmsg = 'Saved!';
			break;
			case "edit":
				errormsg = ($("#ids").val()=='')?errormsg+errortxt+'You must first click on the list below!</li>':errormsg;
				var datasubmit = $("#formsetting").serialize();
				var url = "editgoalsetting";
				var successmsg = 'Updated!';
			break;
			case "delete":
				errormsg = ($("#ids").val()=='')?errormsg+errortxt+'You must first click on the list below!</li>':errormsg;
				var datasubmit = {id:$("#ids").val()};
				var url = "removegoalsetting";
				var successmsg = 'Deleted!';
			break;
		}
		
		if(errormsg==""){		
			$.post("settings/kpi_settings/"+url,datasubmit,function(){	
				$('#modal_error').modal('show');
				if(btn=='delete')$(".btnok").show(); else $(".btnok").hide();
				$("#errorhtml").html('<ul class="list-unstyled spaced"><li class="green"><i class="ace-icon fa fa-check bigger-110 green"></i>Successfully '+successmsg+'!</li></ul>');
				$('#formsetting')[0].reset();
				$("#goal_types").val(' ').trigger("chosen:updated");
				$('#grid_table').jqGrid('setGridParam',{datatype:'json'}).trigger('reloadGrid');
				$(".btnok").hide();
			},'json');
		}else{
			$('#modal_error').modal('show');
			$("#errorhtml").html('<ul class="list-unstyled spaced">'+errormsg+'</ul>');
		}
		
	}
	var set_ctr = 1;
	$("#addsetting").click(function(e){
		e.preventDefault();
		var label_class = 'for="form-field-1" class="col-sm-3 control-label no-padding-right"';
		var label_txt = ['Goal Type','Goal','Passing Score'];
		var goal_array  =["Sales","NSO (Legacy)","NSO (OE)","Sales w/ NSO","Delivered","Assisted","Billed Hours","CVN","SPH","SPHwNSO","QA","360","CSAT","Absences","WFU","Linear Utilization","Cumm. Utilization","ART","IRT","AHT","Lates"];
		var div_input = '<select id="goal_types1" name="goal_type1" data-placeholder="Choose type of goal..." >';
		$.each(goal_array,function (i,arr_val){
			div_input = div_input+'<option value="'+arr_val+'">'+arr_val+'</option>';
		});
		div_input = div_input+'</select>';
		var divcontent = '<div id="add_divcontent'+set_ctr+'">';
		for(var i = 1;i<=3;i++){
			var txt_arr = ['goal_value[]','pass_score[]'];
			if(i>1){
				div_input = '';
				div_input = div_input +'<input type="text" name="'+txt_arr[i-2]+'" id="'+txt_arr[i-2]+'" class="form-control">';
			}
			
			divcontent = divcontent +'<div class="control-group col-sm-10">\
								<div class="form-group">\
									<label '+label_class+'> '+label_txt[i-1]+' </label>\
									<div class="col-sm-9">\
										'+div_input+'\
										</div>\
									</div>\
								</div>';
		}
		divcontent = divcontent +'<div class="control-group col-sm-10"><div class="form-group"><button class="btn btn-danger btn-xs pull-right" onclick="remove_content('+set_ctr+')">\
									<i class="ace-icon fa fa-trash-o  bigger-110 icon-only"></i></button></div></div></div>';
		$("#addsetting_div").append(divcontent);
		set_ctr++;
	});
	
	function remove_content(ctr){
		$("#add_divcontent"+ctr).remove();
	}
	
</script>

