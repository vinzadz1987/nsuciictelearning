<div class="row">
	<h2 class="center green"><?php echo $quiz_name; ?> ( Checking for Essay )</h2>
	<div class="hr hr-dotted hr-16"></div>
	<h3 class="col-xs-12"><?php echo $emp_name; ?></h3>
	<div class="col-xs-12">
		<form id="essay_form" method="POST" action="settings/online_quiz_pending/calculate_final" role="form">
			<input type="hidden" name="result_id" value="<?php echo $result_id ?>">
		<?php echo $essays; ?>
	</div>
	<div class="col-xs-12 margin-top-xl">
		<div class="clearfix form-actions">
			<div class="col-md-12">
			<button id="quiz_submit" class="btn btn-info pull-right" type="submit">
				<i class="ace-icon fa fa-check bigger-110"></i>
				Submit
				</button>
			</div>
		</div>
		</from>
	</div>
</div>


<script type="text/javascript">
	jQuery(function($) {
		
		$('#quiz_submit').click(function(e){
			e.preventDefault();
			error = false;
			inputs = $('#essay_form').find('.input_essay');
			$.each( inputs, function(){
				var input = $(this);
					
				if( input.val() > input.data('score') || ! input.val() ){
					input.next('span').addClass('text-danger');
					error = true;
				}else{
					input.next('span').removeClass('text-danger');
				}
			});
			if( error ){
				bootbox.alert( 'Please meet the requirement for all fields.' );
			}else{
				$('#essay_form').submit();
			}
		});

		$('input').keypress(function(e){
			if (e.keyCode == 13) {
				e.preventDefault();
			}
		})
	
	});


</script>