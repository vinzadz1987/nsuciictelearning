<link rel="stylesheet" href="assets/css/chosen.css" />
<div class="row" id='mtd-kpi-report'>
	<div class="col-xs-12">		
		<div class="row">
				<div class="col-xs-5">
					<!-- #section:elements.accordion -->
						<table id="grid_table"></table>
						<div id="grid_pager"></div>
				</div>
		</div>		
	</div>
</div>
<!-- page specific plugin scripts -->
<script src="assets/js/jqGrid/jquery.jqGrid.min.js"></script>
<script src="assets/js/jqGrid/i18n/grid.locale-en.js"></script>
<!-- chosen plugin -->
<script src="assets/js/chosen.jquery.min.js"></script>
<style>
	.btndiv{
		padding:2px 0;
	}
</style>
<script>
var curdate = new Date();
$(document).ready(function(){
	jqGrid_control(jqGrid_data());
	$('.chosen-select').chosen({allow_single_deselect:true});
	
});

	//==================================//
	// jqGrid Function Start            //
	//==================================//
	function jqGrid_data(data,ops,adv){
		model = {
			data_case:'point_system', //use for switch cases
			data: data||'', // any Data
			ops: ops||'', // operation/option data
			adv: adv||'', // advance data
			table: '#grid_table',			
			// pager: '#grid_pager',
			container_url: 'settings/JqGrid_settings_ctrl/jqgrid_container',
			gridlocation: 'settings/JqGrid_settings_ctrl/load_jq_data/point_system', // location of controller for gathering data from database
			headers: [' ','ID #','RT LOGIN','LASTNAME','FIRSTNAME','SUPERVISOR','ABS <span class="btn_abs_plus fa fa-plus-square" onclick="showabsdetails();"></span>','PRESENT','ABS %','LATE','UT','OT/WOD','NCNS','PARTIAL POINTS','REDEMP POINTS','OVERALL POINTS','SANCTION'], //header for the table
			names: ['actions','`ID_#`','RT_Login','Last_Name','First_Name','Supervisor','absent','presnt','absenteeism','late','ut','ot','ncns','partial_points','redemp_points','overall_points','sanction'], //use for modifaction and functons. assign names for data in a row.
			index: ['actions','uid_num','RT_Login','Last_Name','First_Name','Supervisor','absent','presnt','absenteeism','late','ut','ot','ncns','partial_points','redemp_points','overall_points','sanction'], //set true if index is equal to name. use for query. assign index for data in a row.			
			widths: [70,70,100,100,100,180], //specific width of every column
			sortname: 'ID_#', //use to initialize sort
			autoHeight: 'true',
			//donot add other jqgrid function here. only in jqGridOptions
			jqGridOptions: { // other functions of jqGrid. if not set default will be use. === refer to main.js ===
				// boolean value must be string e.g. 'true' or 'false'
				shrinkToFit:'true',
				height: window.innerHeight - 325,
				altRows: 'true',
				rowNum:-1,
				editurl: 'settings/jqGrid_settings_ctrl/edit_row/point_system',
				caption:<?php echo json_encode($this->main_model->account_title()); ?>+' - Point System '+ (curdate.getMonth() + 1) +'/'+ curdate.getDate() +'/'+ curdate.getFullYear()
			}/*,
			exportToFile: {'show':true,'type':'json'}	// show:false - do not show export button, type:html -  */
		};
		return model;
	}
	
	function jqGrid_extension(){
		$("#grid_table").jqGrid('setGridWidth',window.innerWidth - 270);
		setLabelColor('grid_table','actions,`ID_#`,RT_Login,Last_Name,First_Name,Supervisor','bg-warning');//main.js
		setLabelColor('grid_table','presnt,absenteeism','bg-success');//main.js
	}
	function loadComplete_global(model,data){
		setWidgetToolbarItems({
			exportToFile:
				{
					excel:[model,data,'json']
				}
		});

		if(model.table == '#grid_table'){
			setWidgetToolbarItems({
				widgetButton:
					[
						{id:'graph1', classes:'', icon:'fa-bar-chart-o', title:'', text:'Show Graph', tableName:''}
					]	
			});
		}
		NProgress.done();
		_grid_model_loaded = model;
	}
	function updateActionIcons(table) {
		
		var replacement = 
		{
			'ui-icon-trash' : '',
			'ui-icon-disk' : 'ace-icon fa fa-check green',
			'ui-icon-cancel' : 'ace-icon fa fa-times red'
		};
		$(table).find('.ui-pg-div span.ui-icon').each(function(){
			var icon = $(this);
			var $class = $.trim(icon.attr('class').replace('ui-icon', ''));
			if($class in replacement) icon.attr('class', 'ui-icon '+replacement[$class]);
		})
		$(".ui-inline-del").hide();
	}
	function add_afterSubmit_global(){
		$("#grid_table").jqGrid('setGridParam',{datatype:'json'}).trigger('reloadGrid');
		$(".ui-inline-del").hide();
	};
	//==================================//
	// jqGrid Function End              //
	//==================================//

	/*=======================================================================
				Search functions
	========================================================================*/

	$('#btn_search').click(function(e){
		e.preventDefault();

		var search = ($('#grid_search').val())?$('#grid_search').val():'';
		var byDate = ($('#grid_byDate').val())?$('#grid_byDate').val():'';
		var advance = ($('#grid_advance').val())?$('#grid_advance').val():'';		
		
		gridData = {data:search,ops:byDate,adv:advance};
		searchOnGrid('settings/JqGrid_settings_ctrl','grid_table','point_system',gridData);

		searchData = {
			grid: 'grid_table',
			title: 'KPI - '+<?php echo json_encode($this->main_model->account_title()); ?>,
			search: search,
			date: byDate,
			field: advance,
		}
		//main.js
		jqGrid_caption(searchData);
	});
	var showhide=0;
	function showabsdetails(){
		if((showhide%2)==0){
			var showhidecol = 'showCol';
			$(".btn_abs_plus").removeClass('fa-plus-square');
			$(".btn_abs_plus").addClass('fa-minus-square');
		}else{
			var showhidecol = 'hideCol';
			$(".btn_abs_plus").addClass('fa-plus-square');
			$(".btn_abs_plus").removeClass('fa-minus-square');
		}
		
		$("#grid_table").jqGrid(showhidecol,"presnt");
		$("#grid_table").jqGrid(showhidecol,"absenteeism");
		showhide++;
	}
</script>

