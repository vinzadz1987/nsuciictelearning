<link rel="stylesheet" href="assets/css/chosen.css" />
<div class="row" id='mtd-kpi-report'>
	<div class="col-xs-12">		
		<div class="row">
				<div class="col-xs-5">
					<!-- #section:elements.accordion -->
					<div id="accordion" class="accordion-style1 panel-group">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
										<i class="ace-icon fa fa-angle-down bigger-110" data-icon-hide="ace-icon fa fa-angle-down" data-icon-show="ace-icon fa fa-angle-right"></i>
										&nbsp;Setting Information
									</a>
								</h4>
							</div>

							<div class="panel-collapse collapse in" id="collapseOne" style="height:325px;">
								<div class="panel-body">
									<form method="post" id="formsetting" role="form" class="form-horizontal">
										<input type="hidden" name="id" id="ids" class="form-control">
										<div class="form-group">											
											<div class="control-group col-sm-10">
												<div class="form-group">
													<label for="form-field-1" class="col-sm-3 control-label no-padding-right"> Color </label>
													<div class="col-sm-9">
														<select class="chosen-select" id="goal_colors" name="goal_color" data-placeholder="Choose color..." style="padding:20px !important">
															<option value="">  </option>
															<option value="default" class='label-default'> default</option>
															<option value="primary" class='label-primary'> primary</option>
															<option value="info" class='label-info'> info</option>
															<option value="success" class='label-success'> success</option>
															<option value="danger" class='label-danger'> danger</option>
															<option value="warning" class='label-warning'> warning</option>
															<option value="inverse" class='label-inverse'> inverse</option>
															<option value="pink" class='label-pink'> pink</option>
															<option value="purple" class='label-purple'> purple</option>
															<option value="yellow" class='label-yellow'> yellow</option>
															<option value="light" class='label-light'> light</option>
														</select>
													</div>
												</div>
											</div>
											<div class="control-group col-sm-10">
												<div class="form-group">
													<label for="form-field-1" class="col-sm-3 control-label no-padding-right"> Goal Type </label>
													<div class="col-sm-9">
														<select class="chosen-select" id="goal_types" name="goal_type" data-placeholder="Choose type of goal..." style="padding:20px !important">
															<option value="">  </option>
															<option value="Goal">Goal</option>
															<option value="Pass">Pass</option>
															<option value="Fail">Fail</option>
														</select>
													</div>
												</div>
											</div>
											
											
											<button type="reset" class="btn btnreset">
												<i class="ace-icon fa fa-undo bigger-110"></i>
												Reset
											</button>
										</div>				
										<hr>
									</form>
										<div class="btndiv">
											<button class="btn btn-success btnadd">
												<i class="ace-icon fa fa-file align-top bigger-125"></i>
												Add New
											</button>				
											<button class="btn btn-primary btnupdate">
												<i class="ace-icon fa fa-pencil-square-o align-top bigger-125"></i>
												Update
											</button>
											<button class="btn btn-danger btndelete">
												<i class="ace-icon fa fa-trash-o align-top bigger-125"></i>
												Delete
											</button>
										</div>
								</div>
							</div>
						</div>
					</div>
					
						<table id="grid_table"></table>
						<div id="grid_pager"></div>
				</div>
		</div>		
	</div>
</div>
<div id="modal_error" class="modal" tabindex="-1">
	<div class="modal-dialog" style="width:375px">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">TC Reporting Tool</h4>
			</div>

			<div class="modal-body">
				<div class="row">
					<div class="col-sm-12">
						<div id="errorhtml"></div>
					</div>
				</div>
			</div>

			<div class="modal-footer">
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i>
					Close
				</button>
				<button class="btn btn-sm btn-primary btnok" style="display:none;">
					<i class="ace-icon fa fa-check-square-o"></i>
					Delete
				</button>
			</div>
		</div>
	</div>
</div><!-- PAGE CONTENT ENDS -->
<!-- page specific plugin scripts -->
<script src="assets/js/jqGrid/jquery.jqGrid.min.js"></script>
<script src="assets/js/jqGrid/i18n/grid.locale-en.js"></script>
<!-- chosen plugin -->
<script src="assets/js/chosen.jquery.min.js"></script>
<style>
	.btndiv{
		padding:2px 0;
	}
</style>
<script>
var curdate = new Date();
$(document).ready(function(){
	jqGrid_control(jqGrid_data());
	$('.chosen-select').chosen({allow_single_deselect:true}); 
	$(window).on('resize.chosen', function() {
		var w = $('.chosen-select').parent().width();
		$('.chosen-select').next().css({'width':w});
	}).trigger('resize.chosen');
	
});

	//==================================//
	// jqGrid Function Start            //
	//==================================//
	function jqGrid_data(data,ops,adv){
		model = {
			data_case:'setting_color_comcast', //use for switch cases
			data: data||'', // any Data
			ops: ops||'', // operation/option data
			adv: adv||'', // advance data
			table: '#grid_table',			
			pager: '#grid_pager',
			container_url: 'settings/JqGrid_settings_ctrl/jqgrid_container',
			gridlocation: 'settings/JqGrid_settings_ctrl/load_jq_data/setting_color_comcast', // location of controller for gathering data from database
			headers: ['TYPE','COLOR'], //header for the table
			names: ['goal_type','goal_color'], //use for modifaction and functons. assign names for data in a row.
			index: 'true', //set true if index is equal to name. use for query. assign index for data in a row.
			widths: [], //specific width of every column
			sortname: 'id', //use to initialize sort
			autoHeight: 'true',
			//donot add other jqgrid function here. only in jqGridOptions
			jqGridOptions: { // other functions of jqGrid. if not set default will be use. === refer to main.js ===
				// boolean value must be string e.g. 'true' or 'false'
				shrinkToFit:'true',
				height: window.innerHeight - 325,
				altRows: 'true',
				sortorder:'desc',
				caption:'Goal Setting - '+<?php echo json_encode($this->main_model->account_title()); ?>			
			}/*,
			exportToFile: {'show':true,'type':'json'}	// show:false - do not show export button, type:html -  */
		};
		return model;
	}
	
	function jqGrid_extension(){
		$("#grid_table").jqGrid('setGridWidth',window.innerWidth - 270);		
			
	}
	function loadComplete_global(model,data){
		setWidgetToolbarItems({
			exportToFile:
				{
					excel:[model,data,'json']
				}
		});

		if(model.table == '#grid_table'){
			setWidgetToolbarItems({
				widgetButton:
					[
						{id:'graph1', classes:'', icon:'fa-bar-chart-o', title:'', text:'Show Graph', tableName:''}
					]	
			});
		}
		NProgress.done();
		_grid_model_loaded = model;
	}
	
	function onSelectRow_global(model,aRowids,status){
		$.post("settings/kpi_settings_color/getcolorinfo",{id:aRowids},function(returnval){
			$("#ids").val(returnval[0].id);
			$("#goal_types").val(returnval[0].goal_type).trigger("chosen:updated");
			$('#goal_types').prop('disabled', true).trigger("chosen:updated");
			$("#goal_colors").val(returnval[0].goal_color).trigger("chosen:updated");
			$("#collapseOne").addClass('in');
			$("#collapseOne").css('height','325px');
		},'json');
	};
	//==================================//
	// jqGrid Function End              //
	//==================================//

	/*=======================================================================
				Search functions
	========================================================================*/

	$('#btn_search').click(function(e){
		e.preventDefault();

		var search = ($('#grid_search').val())?$('#grid_search').val():'';
		var byDate = ($('#grid_byDate').val())?$('#grid_byDate').val():'';
		var advance = ($('#grid_advance').val())?$('#grid_advance').val():'';		
		
		gridData = {data:search,ops:byDate,adv:advance};
		searchOnGrid('settings/JqGrid_settings_ctrl','grid_table','setting_color_comcast',gridData);

		searchData = {
			grid: 'grid_table',
			title: 'KPI - '+<?php echo json_encode($this->main_model->account_title()); ?>,
			search: search,
			date: byDate,
			field: advance,
		}
		//main.js
		jqGrid_caption(searchData);
	});
	
	$(".btnadd").click(function(){
		btnactions('add');		
	});
	$(".btnupdate").click(function(){
		btnactions('edit');
	});
	$(".btndelete").click(function(){
		var errormsg = "";
		var litxt = '<li class="text-success">';
		errormsg = ($("#ids").val()=='')?errormsg+'<li class="text-warning bigger-110 red"><i class="ace-icon fa fa-exclamation-triangle"></i>You must first click on the list below!</li>':errormsg;
		if(errormsg==""){	
			$.post("settings/kpi_settings_color/getcolorinfo",{id:$("#ids").val()},function(returnval){
				$('#modal_error').modal('show');
				$(".btnok").show();
				 $("#errorhtml").html('<ul class="list-unstyled spaced">\
				 '+litxt+'<b>Goal Type :</b> '+returnval[0].goal_type+'</li>\
				 '+litxt+'<b>Goal Color :</b> <span class="badge label-'+returnval[0].goal_color+'">'+returnval[0].goal_color+'</span></li></ul>');			
			},'json')
			
		}else{
			$('#modal_error').modal('show');
			$("#errorhtml").html('<ul class="list-unstyled spaced">'+errormsg+'</ul>');
		}
		$('#grid_table').jqGrid('setGridParam',{datatype:'json'}).trigger('reloadGrid');
	});
	$(".btnok").click(function(){
		btnactions('delete');
	});
	$(".btnreset").click(function(){
		$("#ids").val('');
		$("#goal_types").val(' ').trigger("chosen:updated");
		$('#goal_types').prop('disabled', false).trigger("chosen:updated");
	});
	function btnactions(btn){
		var errormsg = "";
		var errortxt = '<li class="text-warning bigger-110 red"><i class="ace-icon fa fa-exclamation-triangle"></i>';
		switch(btn){
			case "add":
				errormsg = ($("#goal_types").val()=='')?errormsg+errortxt+'Please choose a goal type!</li>':errormsg;
				errormsg = ($("#goal_colors").val()=='')?errormsg+errortxt+'Goal color is empty!<li>':errormsg;
				var datasubmit = $("#formsetting").serialize();
				var url = "addcolorsetting";
				var successmsg = 'Saved!';
			break;
			case "edit":
				errormsg = ($("#ids").val()=='')?errormsg+errortxt+'You must first click on the list below!</li>':errormsg;
				var datasubmit = $("#formsetting").serialize();
				var url = "editcolorsetting";
				var successmsg = 'Updated!';
			break;
			case "delete":
				errormsg = ($("#ids").val()=='')?errormsg+errortxt+'You must first click on the list below!</li>':errormsg;
				var datasubmit = {id:$("#ids").val()};
				var url = "removecolorsetting";
				var successmsg = 'Deleted!';
			break;
		}
		
		if(errormsg==""){		
			$.post("settings/kpi_settings_color/"+url,datasubmit,function(){	
				$('#modal_error').modal('show');
				if(btn=='delete')$(".btnok").show(); else $(".btnok").hide();
				$("#errorhtml").html('<ul class="list-unstyled spaced"><li class="green"><i class="ace-icon fa fa-check bigger-110 green"></i>Successfully '+successmsg+'!</li></ul>');
				$('#formsetting')[0].reset();
				$("#goal_types").val(' ').trigger("chosen:updated");
				$("#goal_colors").val(' ').trigger("chosen:updated");
				$('#grid_table').jqGrid('setGridParam',{datatype:'json'}).trigger('reloadGrid');
				$(".btnok").hide();
			},'json');
		}else{
			$('#modal_error').modal('show');
			$("#errorhtml").html('<ul class="list-unstyled spaced">'+errormsg+'</ul>');
		}
		$('#grid_table').jqGrid('setGridParam',{datatype:'json'}).trigger('reloadGrid');
		
	}
	
</script>

