<div class="row">
	<div class="col-xs-12">		
		<div class="row">
				<div class="col-xs-5">
					<!-- #section:elements.accordion -->					
					<table id="grid_table"></table>
					<div id="grid_pager"></div>
					<br>				
					<button class="btn btn-purple btnpreview">
						Scorecard Preview
						<i class="ace-icon fa fa-desktop  align-top bigger-125 icon-on-right"></i>
					</button>
				</div>
				
		</div>
		
			
	</div>
</div>
<div id="dialog-message" class="hide">
	<div class="col-xs-12">
				<div class="widget-box transparent">
					<div class="widget-header widget-header-small">
						<button class="btn btn-primary dropbtn">
							<i class="ace-icon fa fa-users align-top bigger-125"></i>
							Accounts
						</button>
						<div class="dropdown dropdown-preview">
							<ul class="dropdown-menu dropmenu">
								<li><a href="#" class="menuAccount">Default</a></li>
								<li class="divider"></li>
								<li><a href="#" class="menuAccount">Comcast (Legacy)</a></li>
								<li><a href="#" class="menuAccount">Comcast-OE</a></li>
								<li><a href="#" class="menuAccount">Sylmark</a></li>
								<li><a href="#" class="menuAccount">WEN</a></li>
								<li><a href="#" class="menuAccount">Entertainment Cruises</a></li>
								<li><a href="#" class="menuAccount">Meaningful Beauty</a></li>
								<li><a href="#" class="menuAccount">Sheer Cover</a></li>
								<li><a href="#" class="menuAccount">LifeLock</a></li>
								<li><a href="#" class="menuAccount">T-Mobile SIM</a></li>
								<li><a href="#" class="menuAccount">T-Mobile</a></li>
								<li><a href="#" class="menuAccount">Virgin Mobile</a></li>
								
								
							</ul>
						</div>
					</div>
					<div class="widget-body" style="height:400px;overflow-y:scoll;">
						<div class="widget-main">
							
							
							<table class="table table-striped table-bordered table-hover" id="previewTable">
								<thead>
									<tr>
										<th class="center bg-warning">Perfect Score</th>
										<th class="center">Metric</th>
										<th class="center">Data Acquired</th>
										<th class="center bg-success">Weight</th>
									</tr>
								</thead>

								<tbody>
									
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
</div><!-- #dialog-message -->

<div id="dialog-add" class="hide">									
	<div class="col-xs-7">
	<form method="post" id="formsetting" role="form" class="form-horizontal">
		<div class="form-group">
			<div class="control-group col-sm-10">
				<div class="form-group">
					<label for="form-field-1" class="col-sm-3 control-label no-padding-right"> Account </label>
					<div class="col-sm-9">
						<select id="account" name="account">
							<option value="Default">Default</option>
							<?
								$account_array= array("Comcast (Legacy)"=>"Comcast (Legacy)","Comcast-OE"=>"Comcast-OE","Sylmark"=>"Sylmark","WEN"=>"WEN","Entertainment Cruises"=>"Entertainment Cruises","MBSC"=>"Meaningful Beauty","MBSC"=>"Sheer Cover","LifeLock"=>"LifeLock","T-Mobile SIM"=>"T-Mobile SIM","T-Mobile"=>"T-Mobile","Virgin Mobile"=>"Virgin Mobile","Net 10"=>"Net 10","eHarmony"=>"eHarmony");
								
								foreach($account_array as $key=>$val){
									echo '<option value="'.$key.'">'.$val.'</option>';
								}
							?>
						</select>
					</div>
				</div>
			</div>
			<div class="control-group col-sm-10">
				<div class="form-group">
					<label for="form-field-1" class="col-sm-3 control-label no-padding-right"> Metric Name </label>
					<div class="col-sm-9">
						<select id="metric_name" name="metric_name[]">
							<option value=""></option>
							<?
								$goaltype_array = array("Sales","NSO (Legacy)","NSO (OE)","Sales w/ NSO","Delivered","Assisted","Billed Hours","CVN","SPH","SPHwNSO","QA","360","CSAT","Absences","WFU","Linear Utilization","Cumm. Utilization","ART","IRT","AHT","Lates");
								
								foreach($goaltype_array as $goal){
									echo '<option value="'.$goal.'">'.$goal.'</option>';
								}
							?>
						</select>
					</div>
				</div>
			</div>			
			<div class="control-group col-sm-10">
				<div class="form-group">
					<label for="form-field-1" class="col-sm-3 control-label no-padding-right"> Perfect Score </label>
					<div class="col-sm-9">
						<input type="text" name="perfect_score[]" id="perfect_score" class="form-control">
					</div>
				</div>
			</div>			
			<div class="control-group col-sm-10">
				<div class="form-group">
					<label for="form-field-1" class="col-sm-3 control-label no-padding-right"> Target</label>
					<div class="col-sm-9">
						<input type="text" name="target[]" id="target" class="form-control">
					</div>
				</div>
			</div>			
			<div class="control-group col-sm-10">
				<div class="form-group">
					<label for="form-field-1" class="col-sm-3 control-label no-padding-right"> Order </label>
					<div class="col-sm-9">
						<input type="text" name="sort_id[]" id="sort_id" class="form-control">
					</div>
				</div>
			</div>
			<div class="control-group col-sm-10">
				<div class="form-group">
					<label for="form-field-1" class="col-sm-3 control-label no-padding-right"> Status </label>
					<div class="col-sm-9">
						<select id="stats" name="stats[]">
							<option value="Active">Active</option>
							<option value="Active">Inactive</option>							
						</select>
					</div>
				</div>
			</div>		
			<div class="control-group col-sm-10 text-right">
				<button class="btn btn-app btn-yellow btn-xs" id="addsetting">
					<i class="ace-icon fa fa-arrow-circle-o-down bigger-160"></i>
					Add
				</button>
			</div>
			<div id="addsetting_div"></div>
			
		</div>				
		<hr>
	</form>
	</div>
	<div class="col-xs-5">

		<blockquote>
			<p class="lighter line-height-125">
				<h3 class="widget-title grey lighter">
					<i class="ace-icon fa fa-leaf green"></i>
					NOTE
				</h3>
				Setting scorecard metrics by assigning it by account. Click add button to add new metric for the said account.
			</p>											
		</blockquote>

	</div>
							
	
</div>
<div id="modal_error" class="modal" tabindex="-1">
	<div class="modal-dialog" style="width:375px">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="blue bigger">TC Reporting Tool</h4>
			</div>

			<div class="modal-body">
				<div class="row">
					<div class="col-sm-12">
						<div id="errorhtml"></div>
					</div>
				</div>
			</div>

			<div class="modal-footer">
				<button class="btn btn-sm" data-dismiss="modal">
					<i class="ace-icon fa fa-times"></i>
					Close
				</button>
				<button class="btn btn-sm btn-primary btnok" style="display:none;">
					<i class="ace-icon fa fa-check-square-o"></i>
					Delete
				</button>
			</div>
		</div>
	</div>
</div>
<!-- page specific plugin scripts -->
<script src="assets/js/jqGrid/jquery.jqGrid.min.js"></script>
<script src="assets/js/jqGrid/i18n/grid.locale-en.js"></script>
<!-- chosen plugin -->
<script src="assets/js/chosen.jquery.min.js"></script>
<style>
	.btndiv{
		padding:2px 0;
	}
	.table tbody tr:hover td,.table tbody tr:hover th {
		  background-color: transparent;
	}
	.datadetails{
		background-color: #6fb3e0 !important;
		font-weight:bold;
	}
	.datawarning{
		background-color: #fcf8e3 !important;
	}
	
</style>
<script>
var curdate = new Date();
$(document).ready(function(){
	jqGrid_control(jqGrid_data());
	dialogWidget();
});
$( ".btnpreview" ).on('click', function(e) {
		e.preventDefault();
		showpreviewdata("Default");
		var dialog = $( "#dialog-message" ).removeClass('hide').dialog({
			modal: true,
			title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='ace-icon fa fa-cog'></i> Scorecard Preview</h4></div>",
			title_html: true,
			width:800,
			buttons: [
				{
					text: "Close",
					"class" : "btn btn-primary btn-xs",
					click: function() {
						$( this ).dialog( "close" ); 
					} 
				}
			]
		});
		
		 $(".dropbtn").click(function(e){
			e.preventDefault();
			$(".dropmenu").show();
		 });
		 $(".menuAccount").click(function(e){
			e.preventDefault();
			var thistext = $(this).text();
			var acc = (thistext == "Meaningful Beauty" || thistext == "Sheer Cover")?'MBSC':thistext;
			showpreviewdata(acc);
			$(".dropmenu").hide();
			$(".dropbtn").html('<i class="ace-icon fa fa-users align-top bigger-125"></i>'+thistext);
		 });
		/**
		dialog.data( "uiDialog" )._title = function(title) {
			title.html( this.options.title );
		};
		**/
	});
	function showpreviewdata(account){
		$.post("settings/metric_settings/showmetricpreview",{account:account},function(resultdata){
			var countobj = Object.keys(resultdata).length;
			$("#previewTable > tbody").html('');
			if(countobj>0){
				$.each(resultdata,function(i,data){					
					$("#previewTable > tbody").append('\
					<tr><td class="center datawarning" rowspan="3">'+data.perfect_score+' %</td>\
						<td class="center datadetails" colspan="3">'+data.metric_name+'</td>\
					</tr><tr><td>Actual</td><td id="actualid_sph">0</td><td id="weight_sph" class="center dataweight" rowspan="2">0 %</td>\
					</tr><tr><td>Target</td><td id="targetid_sph">'+data.target+'</td></tr>');					
				});
			}else{
				$("#previewTable > tbody").append('<tr><td class="center datawarning" colspan="4">This account doesn\'t have any metric setting set, so <b>Default Metric Setting</b> will be applied!</td></tr>');
			}
		},'json');
	}
	//==================================//
	// jqGrid Function Start            //
	//==================================//
	function jqGrid_data(data,ops,adv){
		model = {
			data_case:'metric_settings', //use for switch cases
			data: data||'', // any Data
			ops: ops||'', // operation/option data
			adv: adv||'', // advance data
			table: '#grid_table',			
			pager: '#grid_pager',
			container_url: 'settings/JqGrid_settings_ctrl/jqgrid_container',
			gridlocation: 'settings/JqGrid_settings_ctrl/load_jq_data/metric_settings', // location of controller for gathering data from database
			headers: ['ACTION','ACCOUNT','METRIC NAME','PERFECT SCORE','TARGET','ORDER','STATUS'], //header for the table
			names: ['actions','account','metric_name','perfect_score','target','sort_id','stats'], //use for modifaction and functons. assign names for data in a row.
			index: 'true', //set true if index is equal to name. use for query. assign index for data in a row.
			widths: [], //specific width of every column
			sortname: 'id', //use to initialize sort
			autoHeight: 'true',
			//donot add other jqgrid function here. only in jqGridOptions
			jqGridOptions: { // other functions of jqGrid. if not set default will be use. === refer to main.js ===
				// boolean value must be string e.g. 'true' or 'false'
				shrinkToFit:'true',
				height: window.innerHeight - 325,
				altRows: 'true',
				sortorder:'desc',
				editurl: 'settings/jqGrid_settings_ctrl/edit_row/metric_setting',
				// toolbarAdd: 'true',
				caption:'Metric Setting'
			}/*,
			exportToFile: {'show':true,'type':'json'}	// show:false - do not show export button, type:html -  */
		};
		return model;
	}
	
	function jqGrid_extension(){
		$("#grid_table").jqGrid('setGridWidth',window.innerWidth - 270);
		$("#grid_table").navButtonAdd('#grid_pager', {
			caption: "",
			title: "Click here to add new record",
			buttonicon: "ui-icon ace-icon fa fa-plus-circle purple",
			onClickButton: function() {
				shownewRecord();
				resetForm();
			},
			position: "first"
		});	
	}
	function loadComplete_global(model,data){
		setWidgetToolbarItems({
			exportToFile:
				{
					excel:[model,data,'json']
				}
		});

		if(model.table == '#grid_table'){
			setWidgetToolbarItems({
				widgetButton:
					[
						{id:'graph1', classes:'', icon:'fa-bar-chart-o', title:'', text:'Show Graph', tableName:''}
					]	
			});
		}
		NProgress.done();
		_grid_model_loaded = model;
	}
	function add_afterSubmit_global(){
		$("#grid_table").jqGrid('setGridParam',{datatype:'json'}).trigger('reloadGrid');
	};
	function styleCheckbox(table) {
		$(table).find('input[name=startdate_weight]').datepicker({format:'yyyy-mm-dd' , autoclose:true})
				.end().find('input[name=enddate_weight]').datepicker({format:'yyyy-mm-dd' , autoclose:true});
	}
	function style_edit_form(form) {
		//enable datepicker on "sdate" field and switches for "stock" field
		form.find('input[name=startdate_weight]').datepicker({format:'yyyy-mm-dd' , autoclose:true})
			.end().find('input[name=enddate_weight]').datepicker({format:'yyyy-mm-dd' , autoclose:true});
	
		style_form_with_ui(form);
		//update buttons classes
		style_form_update_buttons(form);	

	}
	function add_afterSubmit_global(){
		$("#grid_table").jqGrid('setGridParam',{datatype:'json'}).trigger('reloadGrid');
	};
	//==================================//
	// jqGrid Function End              //
	//==================================//

	/*=======================================================================
				Search functions
	========================================================================*/

	$('#btn_search').click(function(e){
		e.preventDefault();

		var search = ($('#grid_search').val())?$('#grid_search').val():'';
		var byDate = ($('#grid_byDate').val())?$('#grid_byDate').val():'';
		var advance = ($('#grid_advance').val())?$('#grid_advance').val():'';		
		
		gridData = {data:search,ops:byDate,adv:advance};
		searchOnGrid('settings/JqGrid_settings_ctrl','grid_table','metric_settings',gridData);

		searchData = {
			grid: 'grid_table',
			title: 'KPI - '+<?php echo json_encode($this->main_model->account_title()); ?>,
			search: search,
			date: byDate,
			field: advance,
		}
		//main.js
		jqGrid_caption(searchData);
	});
	function resetForm(){		
		$('#formsetting')[0].reset();
		$('#addsetting_div').html('');
	}
	
	$("#addsetting").click(function(e){
		e.preventDefault();
		addnewSetting();
	});
	var set_ctr = 1;
	function addnewSetting(){
		var label_class = 'for="form-field-1" class="col-sm-3 control-label no-padding-right"';
		var label_txt = ['Metric Name','Perfect Score','Target','Order','Status'];		
	
		var metric_array  =["Sales","NSO (Legacy)","NSO (OE)","Sales w/ NSO","Delivered","Assisted","Billed Hours","CVN","SPH","SPHwNSO","QA","360","CSAT","Absences","WFU","Linear Utilization","Cumm. Utilization","ART","IRT","AHT","Lates"];
		var div_input = '<select id="metric_name" name="metric_name[]" ><option value=""></option>';
		$.each(metric_array,function (i,arr_val){
			div_input = div_input+'<option value="'+arr_val+'">'+arr_val+'</option>';
		});
		div_input = div_input+'</select>';
		var divcontent = '<div id="add_divcontent'+set_ctr+'">';
		for(var i = 1;i<=5;i++){
			var txt_arr = ['perfect_score','target','sort_id'];
			if(i>1){
				if(i==5){
					var div_input = '<select id="stats" name="stats[]" ><option value="Active">Active</option><option value="Inactive">Inactive</option></select>';					
				}else{
					div_input = '<input type="text" name="'+txt_arr[i-2]+'[]" id="'+txt_arr[i-2]+'" class="form-control">';
				}
			}
			
			divcontent = divcontent +'<div class="control-group col-sm-10">\
								<div class="form-group">\
									<label '+label_class+'> '+label_txt[i-1]+' </label>\
									<div class="col-sm-9">\
										'+div_input+'\
										</div>\
									</div>\
								</div>';
		}
		divcontent = divcontent +'<div class="control-group col-sm-10"><div class="form-group"><button class="btn btn-danger btn-xs pull-right" onclick="remove_content('+set_ctr+')">\
									<i class="ace-icon fa fa-trash-o  bigger-110 icon-only"></i></button></div></div></div>';
		$("#addsetting_div").append(divcontent);
		set_ctr++;
	}
	function remove_content(ctr){
		$("#add_divcontent"+ctr).remove();
	}
	
	function shownewRecord(){
		$( "#dialog-add" ).removeClass('hide').dialog({
			resizable: false,
			modal: true,
			title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='ace-icon fa fa-cog'></i> Scorecard Preview</h4></div>",
			width:'900',
			title_html: true,
			buttons: [
				{
					html: "<i class='ace-icon fa fa-floppy-o bigger-110'></i>&nbsp; Save",
					"class" : "btn btn-success btn-xs",
					click: function() {
						//$( this ).dialog( "close" );
						saveSetting();
						$('#grid_table').jqGrid('setGridParam',{datatype:'json'}).trigger('reloadGrid');
					}
				}
				,
				{
					html: "<i class='ace-icon fa fa-times bigger-110'></i>&nbsp; Cancel",
					"class" : "btn btn-xs",
					click: function() {
						$( this ).dialog( "close" );
					}
				}
			]
		});
	}
	function saveSetting(){
		var formdata = $('#formsetting').serializeArray();
		var ctr_empty=0;
		$('#formsetting input').each(function(){
			if($(this).val()=='') ctr_empty++;
		});
		$('#formsetting select').each(function(){
			if($(this).val()=='') ctr_empty++;
		});
		if(ctr_empty>0){ 
			alert("All fields are required!");
		}else{
			$( "#dialog-add" ).dialog( "close" );
			$.post('settings/metric_settings/savesettings',formdata,function(){
				$('#modal_error').modal('show');
				$("#errorhtml").html('<ul class="list-unstyled spaced"><li class="green"><i class="ace-icon fa fa-check bigger-110 green"></i>Successfully saved!</li></ul>');
				
				$(".btnok").hide();
				
			},'json');
		}
		
	}
</script>

