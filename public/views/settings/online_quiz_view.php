<div class="col-sm-12">
	<h3 class="row header smaller lighter green">
		<span class="col-sm-8">
			Online Test
			<!--<span class="comment-count">2</span>-->
		</span>
		<span class="col-sm-4">
			<button id="add_question" class="btn btn-sm btn-success pull-right">
				<i class="ace-icon fa fa-plus bigger-110"></i>
				Add New
			</button>
		</span>
	</h3>

	<div class="row">
            <div class="col-xs-12">
                <div class="alert alert-block alert-info">
                        <button type="button" class="close" data-dismiss="alert">
                                <i class="ace-icon fa fa-times"></i>
                        </button>

                        <i class="ace-icon fa fa-comment green"></i>
                        To add Question for every quiz just
                        <strong class="green">
                                Double click 
                        </strong>the row.
                </div>
            </div>
		<div class="col-xs-12">
			<div class="row">
				<div id="grid-name" class="grid-wrap col-xs-12 compress head-primary">
					<table id="grid-table"></table>
					<div id="grid_pager"></div>
				</div>
			</div>
		</div><!-- /.col -->
	</div><!-- /.row -->
</div>


<!-- page specific plugin scripts -->
<script src="assets/js/jqGrid/jquery.jqGrid.preset.js"></script>
<script src="assets/js/grid.locale-en.js"></script>
<script src="assets/js/jquery.jqGrid.min.js"></script>
<!-- chosen plugin -->
<script src="assets/js/chosen.jquery.min.js"></script>

<script type="text/javascript">
	jQuery(function($) {
		$('#grid-table').wd_jqGrid({
			url: 'settings/jqGrid_settings_ctrl/load_data',
			module: 'online_quiz',
			pager: '#grid_pager',
			colNames: ['ID','Quiz Name','Learning Materials','Teacher','Taker','Not Taker', 'Date','Time','Actions'], 
			colModel: [	{name:'toq_id',index:'toq_id', width:0, sorttype:"int",hidden:true},
						{name:'toq_name',index:'toq_name',width:100, editable:true, cellattr: function() { return 'style="white-space: normal;"' }},
						{name:'training_materials',index:'training_materials', width:180, editable: true, align: 'center' },
						{name:'author',index:'author', width:70, editable: true, align: 'center'},
						{name:'taker',index:'taker', width:50, editable: true, align: 'center'},
                        {name:'not_taker',index:'not_taker', width:50, editable: true, align: 'center'},
						{name:'updated_date',index:'updated_date', width:50, editable: true, align: 'center'},
                        {name:'test_time',index:'test_time', width:50, editable: true, align: 'center'},
						{name:'actions',index:'actions', width:110, fixed:true, sortable:false, resize:false, align: 'center',
							formatter:'actions', 
							formatoptions:{ 
								keys:true,
								//delbutton: false,//disable delete button
								delOptions:{recreateForm: true, beforeShowForm:beforeDeleteCallback},
								editbutton:false
							}
						},
					],
			sortname: 'toq_id',
			editurl: 'settings/online_quiz/delete_quiz',
			caption: false,
			height: 400,
			altRows: true,
			ondblClickRow: function(rowid,iRow,iCol,e){
				var rowData = $(this).jqGrid('getRowData',rowid);
				$.redirect('teacher/online_quiz_questions', {quiz_id:rowData.toq_id, title:rowData.toq_name});
			},
			loadComplete: function(data){
				var grid = $(this);

				// add custom button in action field
				
				setTimeout(function(){
		            styleCheckbox(grid);
		            
		            updateActionIcons(grid);
		            updatePagerIcons(grid);
		            enableTooltips(grid);
		          }, 0);

				$(grid).create_action_icon({
					icon_type: 'ui-icon ui-icon-share blue',
					options: {
						title: 'View Quiz',
						click: function(e) {
							var rowid = $(e.target).closest("tr.jqgrow").attr("id");
							var rowData = grid.jqGrid('getRowData',rowid);
							$.redirect('teacher/online_quiz_sheet', {quiz_id:rowData.toq_id, title:rowData.toq_name});
						}
					}
				});

				$(grid).create_action_icon({
					icon_type: 'ace-icon fa fa-plus green margin-top-s bigger-130', // bigger or size always on the last.
					options: {
						title: 'Add materials',
						click: function(e) {
							var rowid = $(e.target).closest("tr.jqgrow").attr("id");
							var rowData = grid.jqGrid('getRowData',rowid);
							//console.log(rowData.toq_id);
							$('.ajaxModal').modalBox({
								modal_view:'add_materials_modal',
								title: false,
								width: 400,
								modal_data:rowData.toq_id,
								button_cancel: 'Cancel',
								button_ok: 'save'
							});
						}
					}
				});
				
                $(grid).create_action_icon({
					icon_type: 'ace-icon glyphicon glyphicon-time orange margin-top-s bigger-130', // bigger or size always on the last.
					options: {
						title: 'Add Time',
						click: function(e) {
							var rowid = $(e.target).closest("tr.jqgrow").attr("id");
							var rowData = grid.jqGrid('getRowData',rowid);
							$('.ajaxModal').modalBox({
								modal_view:'add_time_modal',
								title: false,
								width: 400,
								modal_data:rowData.toq_id,
								button_cancel: 'Cancel',
								button_ok: 'save'
							});
						}
					}
				});
			}
		});
	});


	$('#add_question').click(function(){
		$('.ajaxModal').modalBox({
			modal_view:'add_quiz_modal',
			title: false,
			width: 400,
			button_cancel: 'Cancel',
			button_ok: 'save',
		});
	});

	function showDowloads(){
		alert('Show Downloads');
	}

	function show_examinees(id,quiz_name){
		$('.ajaxModal').modalBox({
			modal_view:'online_quiz_examinees_modal',
			modal_data: {quiz_id:id},
			title: "<div class='widget-header'><h4 class='smaller'><i class='ace-icon fa fa-check green'></i> <span class='text-success'>"+quiz_name+"</span> ( Already taken this exam ) </h4></div>",
			width: 800,
			button_cancel: 'Cancel',
			button_ok: 'save',
		});
	}

	function show_not_examinees(id,quiz_name){
		$('.ajaxModal').modalBox({
			modal_view:'online_quiz_not_examinees_modal',
			modal_data: {quiz_id:id},
			title: "<div class='widget-header'><h4 class='smaller'><i class='ace-icon fa fa-exclamation-triangle red'></i> <span class='text-danger'>"+quiz_name+"</span> ( Not yet taken this exam ) </h4></div>",
			width: 800,
			button_cancel: 'Cancel',
			button_ok: 'save',
		});
	}

</script>