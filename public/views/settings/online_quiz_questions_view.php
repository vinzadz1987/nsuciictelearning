<div class="col-sm-12">
	<h3 class="row header smaller lighter green">
		<span class="col-sm-8">
			<span id="quiz_title"><?php echo $title ?></span>
			<i id="btn_edit_title" class="ace-icon cursor fa fa-pencil-square-o"></i>
		</span>
		<span class="col-sm-4">			
			<?php if($quiz_id){ ?>
			<div class="pull-right">
                        <table>
                            <tr>
				<?php if( $status != 'Published' ){ ?>
                                <td>
                                    <button id="publish_quiz" class="btn btn-sm btn-purple margin-right-xxl">
					<i class="ace-icon fa fa-pencil-square-o bigger-110"></i>
					Publish
                                    </button>
                                </td>
                                <td>
                                    <button id="add_question" class="btn btn-sm btn-success">
                                            <i class="ace-icon fa fa-plus bigger-110"></i>
                                            Add New
                                    </button>
                                </td>
                                <td> &nbsp; </td>
				<?php } ?>
                                <td>
                                    <button id="view_quiz" class="btn btn-sm btn-primary">
                                            <i class="ace-icon fa fa-share bigger-110"></i>
                                            View quiz
                                    </button>
                                </td>
                            </tr>
                        </table>
			</div>
			<?php } ?>
			<span class="text-muted pull-right margin-right-xl margin-top-s smaller-70 <?php echo ($status=='Published') ? '':'hide' ?>">status: <span class="u-line dashed text-primary">Published</span></span>
		</span>
	</h3>

	<div class="row">
		<div class="col-xs-12">
			<div class="row">
				<div id="grid-name" class="grid-wrap col-xs-12 compress head-success">
					<table id="grid-table"></table>
					<div id="grid_pager"></div>
				</div>
			</div>
		</div><!-- /.col -->
	</div><!-- /.row -->
</div>


<!-- page specific plugin scripts -->
<!--<script src="assets/js/jqGrid/jquery.jqGrid.preset.js"></script>
<script src="assets/js/jqGrid/jquery.jqGrid.min.js"></script>
<script src="assets/js/jqGrid/i18n/grid.locale-en.js"></script>-->
<script src="assets/js/jqGrid/jquery.jqGrid.preset.js"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/jqgrid/4.6.0/js/jquery.jqGrid.src.js"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqgrid/4.6.0/js/i18n/grid.locale-en.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqgrid/4.6.0/js/jquery.jqGrid.min.js"></script>
<!-- chosen plugin -->
<script src="assets/js/chosen.jquery.min.js"></script>

<script type="text/javascript">
	jQuery(function($) {

		quiz_id = <?php echo json_encode($quiz_id); ?>;
		quiz_status = <?php echo json_encode($status); ?>;
		action = ( quiz_status == 'Published' ) ? true : false; // Hide acction if quiz is published.
		$('#grid-table').wd_jqGrid({
			url: 'settings/jqGrid_settings_ctrl/load_data',
			module: 'online_quiz_questions',
			module_data: {quiz_id:quiz_id},
			pager: '#grid_pager',
			colNames: ['ID','Question','Type','Choises','Answer','Actions'],
			colModel: [	{name:'toqq_id',index:'toqq_id', width:0, sorttype:"int",hidden:true},
						{name:'toqq_question',index:'toqq_question',width:150, editable:true, cellattr: function() { return 'style="white-space: normal;"' }},
						{name:'toqq_type',index:'toqq_type', width:60, editable: true,align: 'center'},
						{name:'toqq_choices',index:'toqq_choices', width:120, editable: true, cellattr: function() { return 'style="white-space: normal;"' }},
						{name:'toqq_answer',index:'toqq_answer', width:70, editable: true,cellattr: function() { return 'style="white-space: normal;"' }}, 
						{name:'actions',index:'actions', width:80, fixed:true, hidden: action, sortable:false, resize:false, align: 'center',
							formatter:'actions', 
							formatoptions:{ 
								keys:true,
								//delbutton: false,//disable delete button
								delOptions:{recreateForm: true, beforeShowForm:beforeDeleteCallback},
								editbutton:false
							}
						},
					],
			sortname: 'toqq_id',
			editurl: 'agent/online_quiz_questions/delete_quiz_item',
			caption: false,
			height: 450,
			altRows: true,
			loadComplete: function(data){
				var grid = $(this);

				// add custom button in action field
				
				setTimeout(function(){
		            styleCheckbox(grid);
		            
		            updateActionIcons(grid);
		            updatePagerIcons(grid);
		            enableTooltips(grid);
		          }, 0);

				var iCol = $.getColumnIndexByName(grid,'actions');
				console.log(grid.find(">tbody>tr.jqgrow>td:nth-child(" + (iCol + 1) + ")"));
				grid.find(">tbody>tr.jqgrow>td:nth-child(" + (iCol + 1) + ")")
				  .each(function() {
				      $("<div>", {
				          title: "View Quiz",
				          mouseover: function() {
				              $(this).addClass('ui-state-hover');
				          },
				          mouseout: function() {
				              $(this).removeClass('ui-state-hover');
				          },
				          click: function(e) {
							var rowid = $(e.target).closest("tr.jqgrow").attr("id");
							var rowData = grid.jqGrid('getRowData',rowid);
							//$.redirect('agent/online_quiz_sheet', {quiz_id:rowData.toq_id, title:rowData.toq_name});
							$('.ajaxModal').modalBox({
								modal_view:'update_question_modal',
								modal_data: {row_data:rowData},
								title: false,
								width: 400,
								button_cancel: 'Cancel',
								button_ok: 'Update',
								button_ok_color: 'btn-success'
							});
				          }
				      }
				    ).css({"margin-left": "5px", float: "left", cursor: "pointer"})
				     .addClass("ui-pg-div ui-inline-custom")
				     .append('<span class="ui-icon ui-icon-pencil"></span>')
				     .appendTo($(this).children("div"));
				});
			}
		});
	});

	$('#add_question').click(function(){
		$('.ajaxModal').modalBox({
			modal_view:'add_question_modal',
			modal_data: {quiz_id:quiz_id},
			title: false,
			width: 400,
			button_cancel: 'Cancel',
			button_ok: 'Save',
		});
	});

	$('#btn_edit_title').click(function(){
		editTitle(this);
	});

	$('#view_quiz').click(function(){
		title = $('#quiz_title').html();
		$.redirect('teacher/online_quiz_sheet', {quiz_id:quiz_id, title:title});
	});

	$('#publish_quiz').click(function(){
		button = $(this);
		title = $('#quiz_title').html();
		bootbox.confirm('Are you sure you want to publish this quiz? When quiz is publish update will be disable.',function(result){
			if( result ){
				$.redirect('teacher/online_quiz_questions/publish_quiz', {quiz_id:quiz_id, title:title});
			}
		});
	})

	function editTitle(title){
		box = $(title).closest('span');
		old_data = $('#quiz_title').html();
		res_html = '\
			<input id="title_value" type="text" value="'+old_data+'" class="col-xs-6">\
			<i id="btn_save_title" class="ace-icon cursor margin-left-xm margin-top-xm fa fa-check"></i>\
		';
		box.html(res_html);
		$('#btn_save_title').click(function(){
			title = $('#title_value').val();
			if( title ){
				$.post('teacher/online_quiz_questions/edit_title',{title:title,quiz_id:quiz_id},function(res){
					if( res == 'success' ){
						res_edit_html = '\
							<span id="quiz_title">'+title+'</span>\
							<i id="btn_edit_title" class="ace-icon cursor fa fa-pencil-square-o"></i>\
						';
						box.html(res_edit_html);
						$('#btn_edit_title').click(function(){
							editTitle(this);
						});
					}
				});
			}else{
				bootbox.alert('Field is empty.');
			}
		})
	}

</script>