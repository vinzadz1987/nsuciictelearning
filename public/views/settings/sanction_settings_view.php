<link rel="stylesheet" href="assets/css/chosen.css" />
<div class="row" id='mtd-kpi-report'>
	<div class="col-xs-12">		
		<div class="row">
				<div class="col-xs-5">
					<!-- #section:elements.accordion -->
						<table id="grid_table"></table>
						<div id="grid_pager"></div>
				</div>
		</div>		
	</div>
</div>
<!-- page specific plugin scripts -->
<script src="assets/js/jqGrid/jquery.jqGrid.min.js"></script>
<script src="assets/js/jqGrid/i18n/grid.locale-en.js"></script>
<!-- chosen plugin -->
<script src="assets/js/chosen.jquery.min.js"></script>
<style>
	.btndiv{
		padding:2px 0;
	}
</style>
<script>
var curdate = new Date();
$(document).ready(function(){
	jqGrid_control(jqGrid_data());
	$('.chosen-select').chosen({allow_single_deselect:true});
	
});

	//==================================//
	// jqGrid Function Start            //
	//==================================//
	function jqGrid_data(data,ops,adv){
		model = {
			data_case:'sanction_settings', //use for switch cases
			data: data||'', // any Data
			ops: ops||'', // operation/option data
			adv: adv||'', // advance data
			table: '#grid_table',			
			pager: '#grid_pager',
			container_url: 'settings/JqGrid_settings_ctrl/jqgrid_container',
			gridlocation: 'settings/JqGrid_settings_ctrl/load_jq_data/sanction_settings', // location of controller for gathering data from database
			headers: ['ACTION','SANCTION NAME','FROM','TO'], //header for the table
			names: ['actions','sanction_name','sanction_from','sanction_to'], //use for modifaction and functons. assign names for data in a row.
			index: 'true', //set true if index is equal to name. use for query. assign index for data in a row.
			widths: [], //specific width of every column
			sortname: 'id', //use to initialize sort
			autoHeight: 'true',
			//donot add other jqgrid function here. only in jqGridOptions
			jqGridOptions: { // other functions of jqGrid. if not set default will be use. === refer to main.js ===
				// boolean value must be string e.g. 'true' or 'false'
				shrinkToFit:'true',
				height: window.innerHeight - 325,
				altRows: 'true',
				sortorder:'desc',
				editurl: 'settings/jqGrid_settings_ctrl/edit_row/sanction_setting',
				toolbarAdd: 'true',
				caption:'Sanction Setting'
			}/*,
			exportToFile: {'show':true,'type':'json'}	// show:false - do not show export button, type:html -  */
		};
		return model;
	}
	
	function jqGrid_extension(){
		$("#grid_table").jqGrid('setGridWidth',window.innerWidth - 270);
			
	}
	function loadComplete_global(model,data){
		setWidgetToolbarItems({
			exportToFile:
				{
					excel:[model,data,'json']
				}
		});

		if(model.table == '#grid_table'){
			setWidgetToolbarItems({
				widgetButton:
					[
						{id:'graph1', classes:'', icon:'fa-bar-chart-o', title:'', text:'Show Graph', tableName:''}
					]	
			});
		}
		NProgress.done();
		_grid_model_loaded = model;
	}
	function add_afterSubmit_global(){
		$("#grid_table").jqGrid('setGridParam',{datatype:'json'}).trigger('reloadGrid');
	};
	//==================================//
	// jqGrid Function End              //
	//==================================//

	/*=======================================================================
				Search functions
	========================================================================*/

	$('#btn_search').click(function(e){
		e.preventDefault();

		var search = ($('#grid_search').val())?$('#grid_search').val():'';
		var byDate = ($('#grid_byDate').val())?$('#grid_byDate').val():'';
		var advance = ($('#grid_advance').val())?$('#grid_advance').val():'';		
		
		gridData = {data:search,ops:byDate,adv:advance};
		searchOnGrid('settings/JqGrid_settings_ctrl','grid_table','sanction_settings',gridData);

		searchData = {
			grid: 'grid_table',
			title: 'KPI - '+<?php echo json_encode($this->main_model->account_title()); ?>,
			search: search,
			date: byDate,
			field: advance,
		}
		//main.js
		jqGrid_caption(searchData);
	});
	
	
</script>

