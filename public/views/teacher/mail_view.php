<div class="col-sm-12">
    <div class="row">
            <div class="col-xs-12">
                    <div class="widget-box transparent">
                        <h3 class="row header smaller lighter blue">
                                <span class="col-sm-8">
                                        Message
                                </span>
                                <span class="col-sm-4">
                                        <button id="send_message" class="btn btn-sm btn-primary pull-right">
                                                <i class="ace-icon fa fa-plus bigger-110"></i>
                                                New Message
                                        </button>
                                </span>
                        </h3>
                        <br>

                        <div class="widget-body">
                                <div class="widget-body-inner" style="display: block;">
                                        <style>
                                            .chose-image{
                                                width: 30px;
                                                height: 30px;
                                            }
                                            .chose-image-list{
                                                width: 30px;
                                                height: 30px;
                                            }
                                        </style>
                                        
                                        
                                        <div class="col-sm-12">
                                            
                                                <div class="tabbable tabs-left">
                                                        <ul class="nav nav-tabs" id="myTab3">
                                                                <li class="active">
                                                                    <a data-toggle="tab" href="#inbox" onclick="inboxmessages(1)">
                                                                                <i class="pink ace-icon fa fa-envelope bigger-110"></i>
                                                                                Inbox
                                                                        </a>
                                                                </li>

                                                                <li>
                                                                    <a data-toggle="tab" href="#sent" onclick="sentmessages()">
                                                                                <i class="blue ace-icon fa fa-envelope-o bigger-110"></i>
                                                                                Sent
                                                                        </a>
                                                                </li>
                                                        </ul>

                                                        <div class="tab-content">
                                                                <div id="inbox" class="tab-pane in active">
<!--                                                                    <span class="col-xs-6 pull-right">
                                                                        <span class="pull-right inline">
                                                                                <span class="btn-toolbar inline middle no-margin">
                                                                                        <span id="accordion-style" data-toggle="buttons" class="btn-group no-margin">
                                                                                                <label class="btn btn-xs btn-yellow active">
                                                                                                        <input value="1" type="radio">
                                                                                                        Teacher
                                                                                                </label>

                                                                                                <label class="btn btn-xs btn-yellow ">
                                                                                                        <input value="2" type="radio">
                                                                                                        Student
                                                                                                </label>
                                                                                        </span>
                                                                                </span>
                                                                        </span>
                                                                    </span>
                                                                    <br><br>-->
                                                                    <div id="inbox_messages1"></div>
                                                                    <div id="inbox_messages2"></div>
                                                                </div>

                                                                <div id="sent" class="tab-pane">
<!--                                                                    <span class="col-xs-6 pull-right">
                                                                        <span class="pull-right inline">
                                                                                <span class="btn-toolbar inline middle no-margin">
                                                                                        <span id="accordion-style" data-toggle="buttons" class="btn-group no-margin">
                                                                                                <label class="btn btn-xs btn-yellow active">
                                                                                                        <input value="1" type="radio">
                                                                                                        Teacher
                                                                                                </label>

                                                                                                <label class="btn btn-xs btn-yellow ">
                                                                                                        <input value="2" type="radio">
                                                                                                        Student
                                                                                                </label>
                                                                                        </span>
                                                                                </span>
                                                                        </span>
                                                                    </span>
                                                                    <br><br>-->
                                                                    <div id="sent_messages"></div>
                                                                </div>
                                                        </div>
                                                </div>

                                                 
                                        </div> 
                                        
                                        
                                        <div class="col-sm-12">
                                                <div class="widget-box transparent" id="recent-box">

                                                        <div class="widget-body">
                                                                <div class="widget-main padding-4">
                                                                        <div class="tab-content padding-8">
                                                                            

                                                                                <div id="comment-tab" class="tab-pane active">
                                                                                        <div class="comments" style="position: relative;"><div class="scroll-track"><div class="scroll-bar" style="height: 283px; top: 0px;"></div></div>
                                                                                            <div>
                                                                                                
                                                                                                
                                                                                                
                                                                                                

                                                                                                <div id="messagesContent">
                                                                                                    <!--- Messages Content Here -->    
                                                                                                </div>              


                                                                                            </div>
                                                                                            
                                                                                        </div>


                                                                                </div>
                                                                        </div>
                                                                </div><!-- /.widget-main -->
                                                        </div><!-- /.widget-body -->
                                                </div><!-- /.widget-box -->
                                        </div>
                                    
                                </div>
                        </div><!-- /.widget-body -->
                    </div>
            </div>
        
        
            
            
        
            
<style>
.slide-container {
    display: none;
}
</style>
            </div>
        <!--</div>-->
</div><!-- /.col -->
<script>
    $(document).ready(function(){
        
        $('#send_message').click(function(){
		$('.ajaxModal').modalBox({
			modal_view:'send_message_modal',
			title: false,
			width: 500,
			button_cancel: 'Cancel',
			button_ok: 'Send'
		});
	});
        
    $('.ui-dialog').css('overflow','visible');
    $('.ui-dialog-content').css('overflow','visible');
    $('.chosen-select').chosen({allow_single_deselect:true});
    //resize the chosen on window resize
    $(window).on('resize.chosen', function() {
        var w = $('.chosen-select').parent().width();
        $('.chosen-select').next().css({'width':w});
    }).trigger('resize.chosen');
    
    for(i=1;i<=2;i++){
        inboxmessages(i);
    }
    sentmessages();
         
    });
    
    function inboxmessages(i){
        // Create Base64 Object
        var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9+/=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/rn/g,"n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}

        $.ajax({
            url:'teacher/mail/getinboxmessages/'+i,
            type:'post',
            dataType:'json',
            success:function(data){
                var list = '';
                $.each(data, function (key, v){
                    if(v.insphoto == null){
                        var imagesphotos = "assets/img/default-user2.png";
                    }else{
                        imagesphotos = v.insphoto;
                    }
                    if(v.status == 0 && '<?php echo $this->session->userdata('access_name')?>' == "Teacher"){
                        status = "";
                        var reply_message = '<i class="ace-icon fa fa-comment-o"></i>';
                        var from ="<span class='badge badge-primary'>From: "+v.message_from+" "+v.firstname+" "+v.lastname+"</span>";
                    }else{
                        reply_message = "";
                        from = "";
                    }
                    list += '<div class="itemdiv commentdiv">'+
                                    '    <div class="user">'+
                                    '            <img alt="Ritas Avatar" src="'+imagesphotos+'">'+
                                    '    </div>'+
                                    '    <div class="body">'+
                                    '            <div class="name">'+
                                    '                   '+from+' <b><br> To: '+ v.useid +' '+v.message_from_name+' </b>'+
                                    '            </div>'+
                                    '            <div class="time">'+
                                    '                    '+status+' <i class="ace-icon fa fa-clock-o"></i>'+
                                    '                    <span class="red">'+v.messdate+'</span>'+
                                    '            </div>'+
                                    '            <div class="text">'+
                                    '                    <i class="ace-icon fa fa-quote-left"></i>'+
                                    '                    '+Base64.decode(v.message_content.substring(0, 200))+' '+
                                    '            </div>'+
                                    '    </div>'+
                                    '    <div class="tools">'+
                                    '            <div class="action-buttons bigger-125">'+
                                    '                    <a href="javascript:void(0)" onclick="showMessages(\''+v.message_from+'_'+v.insname+' '+ v.inslname+'\')">'+
                                    '                            '+reply_message+'  <i class="ace-icon fa fa-mail-forward blue"></i>'+
                                    '                    </a>'+
                                    '                    <a href="#">'+
                                    '                            <i class="ace-icon fa fa-trash-o red"></i>'+
                                    '                    </a>'+
                                    '            </div>'+
                                    '    </div>'+
                                '</div>';
                });
                $("#inbox_messages"+i).html(list);
            }
        });
    }
    
    function sentmessages(){
        // Create Base64 Object
        var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9+/=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/rn/g,"n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}

        $.ajax({
            url:'teacher/mail/getsentmessages',
            type:'post',
            dataType:'json',
            success:function(data){
                var list = '';
                $.each(data, function (key, v){
                    if(v.studpoto == null || v.studpoto == ""){
                        imagesphotos = "assets/img/default-user2.png";
                    }else{
                        imagesphotos = v.studpoto;
                    }
//                    if(v.useid == '<?php echo $this->session->userdata('user_id')?>' && v.status == 0){
//                        status = "";
//                        var status_primary = " padding-8 ";
//                        var reply_message = '<i class="ace-icon fa fa-comment-o"></i>';
//                        var from ="<span class='badge badge-primary'>From: "+ v.message_from+' '+v.insname+' '+ v.inslname+"</span>";
//                    }else{
//                        reply_message = "";
//                        from = "";
//                        status = "<span class='badge badge-warning'>Sent</span>";
//                    }
                    list += '<div class="itemdiv commentdiv">'+
                                    '    <div class="user">'+
                                    '            <img alt="Ritas Avatar" src="'+imagesphotos+'">'+
                                    '    </div>'+
                                    '    <div class="body">'+
                                    '            <div class="name">'+
                                    '                   <b><br> To: '+ v.useid +' '+v.message_from_name+' </b>'+
                                    '            </div>'+
                                    '            <div class="time">'+
                                    '                    '+status+' <i class="ace-icon fa fa-clock-o"></i>'+
                                    '                    <span class="red">'+v.messdate+'</span>'+
                                    '            </div>'+
                                    '            <div class="text">'+
                                    '                    <i class="ace-icon fa fa-quote-left"></i>'+
                                    '                    '+Base64.decode(v.message_content.substring(0, 200))+' '+
                                    '            </div>'+
                                    '    </div>'+
                                    '    <div class="tools">'+
                                    '            <div class="action-buttons bigger-125">'+
                                    '                    <a href="javascript:void(0)" onclick="showMessages(\''+v.message_from+'_'+v.insname+' '+ v.inslname+'\')">'+
                                    '                           <i class="ace-icon fa fa-mail-forward blue"></i>'+
                                    '                    </a>'+
                                    '                    <a href="#">'+
                                    '                            <i class="ace-icon fa fa-trash-o red"></i>'+
                                    '                    </a>'+
                                    '            </div>'+
                                    '    </div>'+
                                '</div>';
                });
                $("#sent_messages").html(list);
            }
        });
    }
    
    function showMessages(fromid){
        $('.ajaxModal').modalBox({
                modal_view:'showMessagesModal',
                title: false,
                width: 500,
                modal_data:fromid,
                button_cancel: 'Cancel'
//                button_ok: 'Send'
        });
    }
    
</script>
<script src="assets/js/jPages.js"></script>
<script src="assets/js/jquery.slimscroll.min.js"></script>
<script src="assets/js/bootbox.min.js"></script>
<script src="assets/js/ace-elements.min.js"></script>

<script src="assets/js/jquery.fileDownload.js"></script>
<script src="assets/js/chosen.jquery.min.js"></script>
<script src="assets/js/ImageSelect.jquery.js"></script>
