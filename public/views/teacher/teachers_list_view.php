<div class="col-sm-12">
    
    <div class="page-header">
            <h1>
                    Instructors
            </h1>
    </div>
    
    
    <div class="row">
            <div class="form-group">
                    <div class="col-xs-12">
                        <button id="create" type="button" class="btn btn-primary btn-sm pull-right"><i class="fa fa-plus"></i> Create Teacher</button>
                    </div>
            </div>

    <div class="col-xs-12">
            <div class="space-2"></div>
            <table id="dataTable_view" class="table table-striped table-bordered table-hover">
                <thead>
                <tr>
                    <th>Teachers ID</th>
                    <th style="width:250px;">Full Name</th>
                    <th style="width:150px;">Mobile Number</th>
                    <th>Designation</th>
                    <th>Created Date</th>
                    <th style="width:140px;">Action</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
    </div>

    <!---- UPDATE INSTRUCTOR MODAL -->
        
    <div id="updateModal" class="hide">
        <div class="space-4"></div>
            <form class="form-horizontal" role="form">
                    <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Teacher ID </label>

                            <div class="col-sm-9">
                                    <input id="teachIDe" placeholder="Edit Teachers ID" class="col-xs-12" type="text">
                            </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1-1"> First Name </label>

                            <div class="col-sm-9">
                                <input id="teachFnamee" placeholder="Edit First Name" class="col-xs-12" type="text">
                            </div>
                    </div>
                
                    
                    <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-2"> Middle Name </label>

                            <div class="col-sm-9">
                                 <input placeholder="Edit Middle Name" id="teachMnamee" class="col-xs-12" type="text">
                            </div>
                    </div>

                    <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-2"> Last Name </label>

                            <div class="col-sm-9">
                                 <input placeholder="Edit Last Name" id="teachLnamee" class="col-xs-12" type="text">
                            </div>
                    </div>
            </form>
    </div>
    
    <!---- END UPDATE INSTRUCTOR MODAL -->
    
    <!---- ADD INSTRUCTOR MODAL -->
    
    <div id="dialog-message2" class="hide">
            <div class="space-4"></div>
            <form class="form-horizontal" role="form">
                    <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Teacher ID </label>

                            <div class="col-sm-9">
                                    <input id="input1" placeholder="Teachers ID" class="col-xs-12" type="text">
                            </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1-1"> First Name </label>

                            <div class="col-sm-9">
                                <input id="input2" placeholder="First Name" class="col-xs-12" type="text">
                            </div>
                    </div>
                
                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1-1"> Middle Name </label>

                            <div class="col-sm-9">
                                <input id="input4" placeholder="Middle Name" class="col-xs-12" type="text">
                            </div>
                    </div>

                    <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="form-field-2"> Last Name </label>

                            <div class="col-sm-9">
                                    <input placeholder="Last Name" id="input3" class="col-xs-12" type="text">
                            </div>
                    </div>
            </form>
    </div>
    
    <!---- END ADD INSTRUCTOR MODAL -->
    
    </div>
</div>

<script>
    $(document).ready(function(){
        
         $('table#dataTable_view').dataTable({"scrollX": true});
         viewDataTable();
        
         $("#create").click(function(){
             dialogBoxModalAdd('Create Teacher');
         });
         
         
    });
    
    
    function viewDataTable(){
        $.ajax({
            url:'teacher/teachers/getTeachers',
            type:'post',
            dataType:'json',
            success:function(data){
                var table = $('table#dataTable_view').DataTable({
                                 bDestroy: true,
                                 bLengthChange: false,
                                 paging: false
                              });
                             if (table) table.fnClearTable();
                $.each(data, function (key, val){
                    var items = ["btn-success","btn-warning","btn-default","btn-info","btn-danger","btn-inverse"];
                    var rand = items[Math.floor(Math.random() * items.length)];
                    var photos = val.photo;
                    if(photos == ""){
                        photos = "assets/img/default-user2.png";
                    }
                    if(val.status == 0){
                        status = '<a class="btn btn-xs '+rand+'" href="javascript:void(0)" title="pending" \n\
                                            onclick="dialogBoxModalConfirm('+val.instructid+')">\n\
                                        Pending\n\
                                </a>';
                    }else{
                        status = '';
                    }
                    $('table#dataTable_view').dataTable().fnAddData( [
                        val.instructor_id,
                        '<img class="pull-left img-circle" alt="" src="'+photos+'" style="height: 25px; width: 30px">&nbsp;'+val.firstname+' '+val.middlename+' '+val.lastname,
                        ''+val.instructor_mobile_number+'',
                        ''+val.instructor_designation+'',
                        ''+val.date_added+'',
                        '<div class="hidden-xs hidden-xs action-buttons">\n\
                                '+status+'\n\
                                <a class="green" href="javascript:void(0)" title="Edit" \n\
                                            onclick="dialogBoxModalUpdate(\''+val.instructid+'_'+val.instructor_id+'_'+val.firstname+'_'+val.middlename+'_'+val.lastname+'\')">\n\
                                        <i class="ace-icon fa fa-pencil bigger-100"></i>\n\
                                </a>\n\
                                <a class="red" href="javascript:void(0)" title="Delete" onclick="removeInstructor('+val.instructid+')">\n\
                                        <i class="ace-icon fa fa-trash-o bigger-100"></i>\n\
                                </a>\n\
                        </div>'
                    ]);
                });
            }
        });
    }
    
    
    function getLessons(id){
        $.ajax({
                url:'student/subjects/getLessonsCount',
                type:'post',
                dataType:'json',
                data:{id:id},
                success:function(data){
                        var clesson = "";
                        $.each(data, function (key, val){
                                clesson += val.count_lesson;
                        });
                        $("span#lesson_count"+id).html(clesson);
                }
        });
    }
    
                
    function dialogBoxModalAdd(titleName){
            dialogBox(
                    model = {
                            type: 'local',
                            data: 'dialog-message2',
                            title: titleName,
                            title_icon: 'fa-ticket',
                            width: 300,
                            buttons: [
                                    {
                                            'html': "<i class='ace-icon fa fa-times'></i> Cancel",
                                            'class': 'btn_cancel btn btn-xs',
                                            click: function(){
                                                    $(this).dialog('close');
                                            }
                                    },
                                            {
                                            'html': "<i class='ace-icon fa fa-check'></i> Submit",
                                            'class': 'btn-primary btn btn-xs',
                                            click: function(){
                                                    var teachid = $("#input1").val();
                                                    var fname = $("#input2").val();
                                                    var lname = $("#input3").val();
                                                    var mname = $("#input4").val();
                                                    $.ajax({
                                                       url:'teacher/teachers/addTeacher',
                                                       type:'post',
                                                       data:{teacherid:teachid,firstname:fname,lastname: lname,middlename:mname},
                                                       success:function(){
                                                           alert('ADDED SUCCESSFULLY');
                                                           viewDataTable();
                                                       }
                                                    });
                                            }
                                    }
                            ]
                    }
            );
    }
    
      function dialogBoxModalUpdate(params){
            var tblinsid = params.split('_')[1];
            $("#teachIDe").val(tblinsid);
            var tfname = params.split('_')[2];
            $("#teachFnamee").val(tfname);
            var tmname = params.split('_')[3];
            $("#teachMnamee").val(tmname);
            var tlname = params.split('_')[4];
            $("#teachLnamee").val(tlname);
            dialogBox(
                    model = {
                            type: 'local',
                            data: 'updateModal',
                            title: 'Update Instructor',
                            title_icon: 'fa-ticket',
                            width: 300,
                            buttons: [
                                    {
                                            'html': "<i class='ace-icon fa fa-times'></i> Cancel",
                                            'class': 'btn_cancel btn btn-xs',
                                            click: function(){
                                                    $(this).dialog('close');
                                            }
                                    },
                                            {
                                            'html': "<i class='ace-icon fa fa-check'></i> Submit",
                                            'class': 'btn-primary btn btn-xs',
                                            click: function(){
                                                    var tblinsid = params.split('_')[0];
                                                    var tinsid = $("#teachIDe").val();    
                                                    var tfname = $("#teachFnamee").val();
                                                    var tmname = $("#teachMnamee").val();
                                                    var tlname = $("#teachLnamee").val();
                                                    $.ajax({
                                                       url:'teacher/teachers/updateTeacher',
                                                       type:'post',
                                                       data:{tblinsid:tblinsid,instructor_id:tinsid,firstname:tfname,middlename:tmname,lastname: tlname},
                                                       success:function(){
                                                           alert('UPDATED SUCCESSFULLY');
                                                           viewDataTable();
                                                       }
                                                    });
                                            }
                                    }
                            ]
                    }
            );
    }
    
    
    function dialogBoxModalConfirm(id){
			bootbox.confirm("Are you sure you want to Confirm this Instructor?", function(confirmed) {
				if(confirmed === true){
					$.ajax({
						url:'teacher/teachers/confirmInstructor',
						type:'post',
						data:{id:id},
						success:function(res){
                                                    alert(res);
                                                    viewDataTable();
						}
					});
				}
			});
		}
    
    
    function removeInstructor(id){
			bootbox.confirm("Are you sure you want to remove this Instructor?", function(confirmed) {
				if(confirmed === true){
					$.ajax({
						url:'teacher/teachers/removeTeacher',
						type:'post',
						data:{id:id},
						success:function(){
                                                    viewDataTable();
						}
					});
				}
			});
		}
    
</script>

<script src="assets/js/jPages.js"></script>
<script src="assets/js/jquery.slimscroll.min.js"></script>
<script src="assets/js/bootbox.min.js"></script>
<script src="assets/js/ace-elements.min.js"></script>

<script src="assets/js/jquery.dataTables.min.js"></script>
<script src="assets/js/jquery.dataTables.bootstrap.js"></script>
