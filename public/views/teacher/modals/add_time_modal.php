<link href="assets/js/timersetter/css/jquery.timesetter.css" rel="stylesheet">

<div class="modal-header">
    <p class="blue bigger-120">Add Time</p>
</div>
<div class="space"></div>
<form class="form-horizontal" role="form">
    <div class="form-group">
        <label class="col-sm-3 control-label no-padding-right"> Time </label>
        <div class="col-sm-9">
            <div class="div2"></div>
        </div>
    </div>
</form>
<script>
/*

On page load call the below code

*/

$(document).ready(function ()
{
    var options1 = {
        hour: {
            value: 0,
            min: 0,
            max: 24,
            step: 1,
            symbol: "hrs"
        },
        minute: {
            value: 0,
            min: 0,
            max: 60,
            step: 15,
            symbol: "mins"
        },
        direction: "increment", // increment or decrement
        inputHourTextbox: null, // hour textbox
        inputMinuteTextbox: null, // minutes textbox
        postfixText: "", // text to display after the input fields
        numberPaddingChar: '0' // number left padding character ex: 00052
    };

//    $(".div1").timesetter(options1).setHour(17);
    $(".div2").timesetter().setValuesByTotalMinutes(175);

});
</script>
<script>
    var $id = <?php echo json_encode($result); ?>;
    $(".btn_ok_mdl").click(function(){
        saveTime($("#txtHours").val(),$("#txtMinutes").val());
    });

    function saveTime(hrs,mns){
        $.ajax({
            url:'teacher/teachers/addTime',
            type:'post',
            data: {hrs:hrs,min:mns,id:$id},
            success: function (bogoko) {
                $("#grid-table").jqGrid('setGridParam',{datatype:'json'}).trigger('reloadGrid');
            }
        })
    }
</script>