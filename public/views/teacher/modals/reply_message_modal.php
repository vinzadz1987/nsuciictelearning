<style>
    .chose-image{
        width: 20px;
        height: 20px;
    }
    .chose-image-list{
        width: 20px;
        height: 20px;
    }
</style>

<div class="modal-header">
    <p class="blue bigger-120"><i class="ace-icon fa fa-plus bigger-110"></i> New Message</p>
</div>
<div class="space"></div>
<form class="form-horizontal" role="form">
    
    <div class="form-group">
        <div class="col-sm-9">
            <select multiple="" class="chosen-select" id="students" data-placeholder="Name..."></select>
        </div>
    </div>
    <div class="form-group">
            <div class="col-sm-12">
                <textarea class="form-control" placeholder="" id="message"></textarea>
            </div>
    </div>
</form>

<script src="assets/js/ImageSelect.jquery.js"></script>
<script>
    
    getStudents();

    $('.ui-dialog').css('overflow','visible');
    $('.ui-dialog-content').css('overflow','visible');
    $('.chosen-select').chosen({allow_single_deselect:true});
    //resize the chosen on window resize
    $(window).on('resize.chosen', function() {
        var w = $('.chosen-select').parent().width();
        $('.chosen-select').next().css({'width':370});
    }).trigger('resize.chosen');
    
    $(".btn_ok_mdl").click(function(){
        var student = []; var image = [];
        $.each($("#students option:selected"), function(){
            student.push($(this).val().split('_')[0]+'_'+$(this).text());
//            image.push($(this).attr('data-img-src'));
        });
//        alert(image);
        saveMessage(student.join(", "));
//        alert(student);
    });
    
    function getStudents(){
        
    $.ajax({
        url:'teacher/mail/getStudents',
        type:'post',
        dataType:'json',
        success: function (data) {
            var option = "";
            $.each(data, function(i,v){
                 photo = v.photo;
                if(photo == ''){
                    vphoto = 'assets/img/default-user2.png';
                }else{
                    vphoto = 'assets/img/default-user2.png';
                }
                name = v.firstname +' '+v.lastname;
                option += '<option data-img-src="'+vphoto+'"  value="'+v.student_id+'_'+name+'"> '+name+ '</option>';
            });
            $('#students').html( option );
            $("#students").trigger("chosen:updated");
        }
    });
    }

    function saveMessage(student, image){
        $.ajax({
            url:'teacher/mail/sendMessage',
            type:'post',
            data: {data:student,messages:$("#message").val()}, //,image:image
            success: function () {
                 messageList();
            }
        })
    }
</script>