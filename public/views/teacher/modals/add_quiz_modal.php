<div class="modal-header">
	<button class="close" data-dismiss="modal" type="button">×</button>
	<p class="blue bigger-120">Add Quiz</p>
</div>
<div class="space"></div>
<form id="add_quiz_form" class="form-horizontal" role="form">
	<div class="form-group">
		<label class="col-sm-3 control-label no-padding-right"> Quiz title </label>
		<div class="col-sm-9">
			<input class="col-sm-11" type="text" name="quiz_name">
		</div>
	</div>
	<!----- <div class="form-group">
		<label class="col-sm-3 control-label no-padding-right"> Year/Semester </label>
		<div class="col-sm-9">
			<select name="yearSem" id="yearSem" class="chosen-select">
				<option value=""> Year / Semester </option>
				<option value="1_1">First Year - First Semester</option>
				<option value="1_2">First Year -  Second Semester</option>
				<option value="2_1">Second Year - First Semester</option>
				<option value="2_2">Second Year -  Second Semester</option>
				<option value="3_1">Third Year - First Semester</option>
				<option value="3_2">Third Year -  First Semester</option>
				<option value="4_1">Fourth Year - First Semester</option>
				<option value="4_2">Fourth Year - Second Semester</option>
			</select>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-3 control-label no-padding-right"> Subject </label>
		<div class="col-sm-9">
			<select class="col-sm-11" name="subjects" id="subjects"></select>
		</div>
	</div> ----->
</form>

<script>
	$('.ui-dialog').css('overflow','visible');
    $('.ui-dialog-content').css('overflow','visible');
    $('.chosen-select').chosen({allow_single_deselect:true});
    //resize the chosen on window resize
    $(window).on('resize.chosen', function() {
        var w = '250';
        $('.chosen-select').next().css({'width':w});
    }).trigger('resize.chosen');
	$("#yearSem").on("change", function(){
		// alert($(this).val());
		getSubjectListByYearSem($(this).val());
	});
	$('.btn_ok_mdl').click(function(){
		var quiz_name = $('[name="quiz_name"]').val();
		if( quiz_name ){
			$.post( 'teacher/online_quiz_sheet/add_quiz', {quiz_name:quiz_name},function(res){
				if( res == 'success' ){
					$.closeDialog();
					$.reloadGrid($('#grid-table'));
				}
			})
		}else{
			alert('Please input Quiz title.');
		}
	});

	function getSubjectListByYearSem(params){
		$.ajax({
			url:'',
			type:'Post',
			dataType:'JSON',
			data:{},
			success : function(data) {
				console.log(data);
			}
		})
	}
</script>