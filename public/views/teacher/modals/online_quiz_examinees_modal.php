<div class="row">
	<div id="patient_list_wrapper" class="grid-wrap col-xs-12 head-primary">
		<table id="examinees-table"></table>
		<div id="examinees_pager"></div>
	</div>
</div>


<!-- page specific plugin scripts -->
<script src="assets/js/jqGrid/jquery.jqGrid.preset.js"></script>
<script src="assets/js/jqGrid/jquery.jqGrid.min.js"></script>
<script src="assets/js/jqGrid/i18n/grid.locale-en.js"></script>
<!-- chosen plugin -->
<script src="assets/js/chosen.jquery.min.js"></script>

<script type="text/javascript">
	jQuery(function($) {
		quiz_id = <?php echo json_encode($result['quiz_id']); ?>;
		
		$('#examinees-table').wd_jqGrid({
			url: 'settings/jqGrid_settings_ctrl/load_data',
			module_data: {quiz_id:quiz_id},
			module: 'online_quiz_examinees',
			//module_data: {ops:'l_name',status:0},
			colNames: ['ID','Student ID','Name','Partial Score','Final Score','Date','Status'],
			colModel: [	{name:'toqr_id',index:'toqr_id', width:0, sorttype:"int",hidden:true},
						{name:'toqr_emp_id',index:'toqr_emp_id',width:60, editable:true, align:'center'},
						{name:'emp_name',index:'emp_name', width:200, editable: true, align: 'center'},
						{name:'toqr_partial_score',index:'toqr_partial_score', width:60, editable: true, align: 'center'},
						{name:'toqr_final_score',index:'toqr_final_score', width:60, editable: true, align: 'center'},
						{name:'toqr_date',index:'toqr_date', width:60, editable: true, align: 'center'},
						{name:'toqr_status',index:'toqr_status', width:60, editable: true, align: 'center'},
					],
			sortname: 'toqr_id',
			//editurl: 'developers/jqGrid/actions',
			caption: false,
			height: 300,
			ondblClickRow: function(rowid,iRow,iCol,e){
				var rowData = $(this).jqGrid('getRowData',rowid);
				if( $.stripHtml(rowData.toqr_status) == 'Pending' )
					$.redirect('settings/online_quiz_pending', {quiz_id:quiz_id, result_id:rowData.toqr_id, emp_name:rowData.emp_name});
			}
		});

	});

</script>