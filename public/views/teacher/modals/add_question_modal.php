<div class="modal-header">
	<button class="close" data-dismiss="modal" type="button">×</button>
	<p class="blue bigger-120">Add Question</p>
</div>
<form id="add_question_form" data-id="<?php echo $result['quiz_id'] ?>">
	<table class="custom-dialog col-xs-12">
		<tbody>
			<tr>
				<td class="align-right lighter">Question</td>
				<td><textarea name="quiz_question" class="col-xs-10 important"></textarea></td>
			</tr>
			<tr id="quiz_type">
				<td class="align-right lighter">Type</td>
				<td>
					<select id="q_type" name="quiz_type" class="input-sm important">
						<option value="">&nbsp;</option>
						<option value="True or False">True or False</option>
						<option value="Enumeration">Enumeration</option>
						<option value="Identification">Identification</option>
						<option value="Identification Plus">Identification Plus</option>
						<option value="Multiple Choice">Multiple Choice</option>
						<option value="Essay">Essay</option>
					</select>
				</td>
			</tr>			
		</tbody>
	</table>
</form>

<script>
	jQuery(function($) {
		$('.close').click(function(){
			$.closeDialog();
		})

		$('[name="quiz_type"]').change(function(){
			value = $(this).find(":selected").text();
			if( value ){
				$('.q_type').remove();
				switch(value){
					case 'True or False': 	trueOrFalse();		break;
					case 'Identification': 	identification();	break;
					case 'Identification Plus': 	identificationPlus();	break;
					case 'Enumeration': 	multipleChoice('enumeration');	break;
					case 'Multiple Choice': multipleChoice();	break;
					case 'Essay': 			essay();			break;
					default: 	$('.q_type').remove();			break;
				}
			}
		});

		$('.btn_ok_mdl').click(function(){
			var form = $('#add_question_form'),
				formData = form.serialize(),
				multiChoice = $('#choice_answer_data').find('[name="quiz_answer"]'),
				multiChoiceData = [],
				identificationPlus = $('#choice_answer_data').find('.question'),
				identificationPlusData = {},
				error = false,
				quizId = form.data('id');

			// create array of multiple choice data.
			$.each( multiChoice,function(key,data){
				multiChoiceData.push($(data).val());
			});

			// create array of indentification Plus data.
			$.each( identificationPlus,function(){
				var ipQ = $(this).find('label').html(); // get question from label
				var ipA = $(this).find('input').val(); // get answer from input
				identificationPlusData[ipQ] = ipA; // Object { ipQ = "ipA" }
			});
			important_field = form.find('.important');
			$('.notify').remove();
			$.each( important_field, function(){
				var label_box = $(this).closest('td').prev();
				if( $(this).hasClass('radio') ){
					if( ! $('[name="quiz_answer"]:checked').val() ){
						label_box.prepend('<span class="notify text-danger margin-right-s">*</span>');
						error = true;
					}
				}else if( $(this).hasClass('question') ){
					questionBox = $(this).find('input');
					if( ! questionBox.val() ){
						$(this).prepend('<span class="notify text-danger margin-right-s">*</span>');
						error = true;
					}
				}else{
					if( ! $(this).val() ){
						label_box.prepend('<span class="notify text-danger margin-right-s">*</span>');
						error = true;
					}
				}
			})

			if( ! error ){
				$.post( 'teacher/online_quiz_questions/add_question', {form_data:formData,i_plus:JSON.stringify(identificationPlusData),m_data:multiChoiceData,quiz_id:quizId},function(res){
					if( res == 'success' ){
						$.closeDialog();
						$.reloadGrid($('#grid-table'));
					}
				});
			}else{
				alert('Important fields are needed.');
			}
		})

	});

	function identificationPlus(){
		res_html = '\
			<tr id="quiz_choices" class="q_type">\
				<td class="align-right lighter"> Add question </td>\
				<td>\
					<div class="input-group col-xs-10">\
						<input id="add_quiz_choices" type="text" class="col-xs-12">\
						<span class="input-group-btn">\
							<button id="quiz_choices_btn" class="btn btn-success btn-sm" type="button">\
							<span class="ace-icon fa fa-plus icon-on-right bigger-110"></span>\
							Add\
							</button>\
						</span>\
					</div>\
				</td>\
			</tr>\
		';
		$('#quiz_type').after(res_html);
		preventEnter(); // prevent add quiz enter key
		$('#quiz_choices_btn').click(function(){
			choices = $('#add_quiz_choices').val();
			if( choices ){
				answer_data = '\
					<div class="question col-xs-12 important">\
						<label>'+choices+'</label><br>\
						<input type="text" class="input-sm col-xs-10">\
						<i class="remove_choice ace-icon cursor fa fa-times bigger-120 red margin-left-s margin-top-s"></i>\
					</div>\
				';
				answer_container = '\
					<tr id="choice_answer" class="q_type">\
						<td class="align-right lighter">Questions</td>\
						<td id="choice_answer_data">\
						</td>\
					</tr>\
				';

				if( $('#choice_answer').length ){
					$('#choice_answer_data').append(answer_data);
				}else{
					$('#quiz_choices').after(answer_container);
					$('#choice_answer_data').append(answer_data);
				}
			}

			$('#add_quiz_choices').val('');

			$('.remove_choice').click(function(){
				$(this).closest('div').remove();
			})
		});
	}

	function essay(){
		res_html = '\
			<tr class="q_type">\
				<td class="align-right lighter">Answer</td>\
				<td>\
					<input name="quiz_answer" class="input-sm col-xs-2 important" type="text">\
					<label class="lighter margin-left-xm margin-top-s text-muted smaller-90">Heighest posible score.</label>\
				</td>\
			</tr>\
		';
		$('#quiz_type').after(res_html);
		preventEnter();
	}

	function identification(){
		res_html = '\
			<tr class="q_type">\
				<td class="align-right lighter">Answer</td>\
				<td><input name="quiz_answer" class="input-sm col-xs-10 important" type="text" autocomplete="off"></td>\
			</tr>\
		';
		$('#quiz_type').after(res_html);
		preventEnter();
	}

	function trueOrFalse(){
		res_html = '\
			<tr class="q_type">\
				<td class="align-right lighter">Answer</td>\
				<td>\
					<div class="radio important">\
						<label>\
							<input class="ace" type="radio" name="quiz_answer" value="True">\
							<span class="lbl"> True</span>\
						</label>\
						<label class="margin-left-xl">\
							<input class="ace" type="radio" name="quiz_answer" value="False">\
							<span class="lbl"> False</span>\
						</label>\
					</div>\
				</td>\
			</tr>\
		';
		$('#quiz_type').after(res_html);
	}

	function multipleChoice(mChoice){
		mChoice = mChoice || 'important';
		add_text = ( mChoice == 'enumeration' )  ? 'Add Answers' : 'Add Choices';
		res_html = '\
			<tr id="quiz_choices" class="q_type">\
				<td class="align-right lighter">'+add_text+'</td>\
				<td>\
					<div class="input-group col-xs-10">\
						<input id="add_quiz_choices" type="text" class="col-xs-12">\
						<span class="input-group-btn">\
							<button id="quiz_choices_btn" class="btn btn-success btn-sm" type="button">\
							<span class="ace-icon fa fa-plus icon-on-right bigger-110"></span>\
							Add\
							</button>\
						</span>\
					</div>\
				</td>\
			</tr>\
		';
		$('#quiz_type').after(res_html);
		preventEnter(); // prevent add quiz enter key
		$('#quiz_choices_btn').click(function(){
			choices = $('#add_quiz_choices').val();
			if( choices ){
				answer_data = '\
					<div class="radio">\
						<label>\
							<input class="ace" type="radio" name="quiz_answer" value="'+choices+'">\
							<span class="lbl lighter smaller-90"> '+choices+'</span>\
						</label>\
						<i class="remove_choice ace-icon cursor fa fa-times bigger-120 red margin-left-s"></i>\
					</div>\
				';
				answer_container = '\
					<tr id="choice_answer" class="q_type">\
						<td class="align-right lighter">Answer</td>\
						<td id="choice_answer_data" class="radio '+mChoice+'">\
						</td>\
					</tr>\
				';

				if( $('#choice_answer').length ){
					$('#choice_answer_data').append(answer_data);
				}else{
					$('#quiz_choices').after(answer_container);
					$('#choice_answer_data').append(answer_data);
				}
			}

			$('#add_quiz_choices').val('');

			$('.remove_choice').click(function(){
				$(this).closest('div').remove();
			})
		});
	}

	function preventEnter(){
		$('input').keypress(function(e){
			if (e.keyCode == 13) {
				e.preventDefault();
			}
		});
	}
</script>