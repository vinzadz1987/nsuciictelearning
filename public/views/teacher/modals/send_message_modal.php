<style>
    .chose-image{
        width: 20px;
        height: 20px;
    }
    .chose-image-list{
        width: 20px;
        height: 20px;
    }
</style>

<div class="modal-header">
    <p class="blue bigger-120"><i class="ace-icon fa fa-plus bigger-110"></i> New Message</p>
</div>
<div class="space"></div>

<div class="tabbable no-border">
            <ul class="nav nav-tabs">
                    <li class="active">
                            <a data-toggle="tab" href="#student">
                                    <i class="pink ace-icon fa fa-user green bigger-110"></i>
                                    Student
                            </a>
                    </li>

                    <li>
                            <a data-toggle="tab" href="#teacher">
                                    <i class="blue ace-icon fa fa-user blue bigger-110"></i>
                                    Teacher
                            </a>
                    </li>

            </ul>

            <div class="tab-content">
                    <div id="student" class="tab-pane in active">
                        <form class="form-horizontal" role="form">
    
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <select multiple="" class="chosen-select" id="students" data-placeholder="Student Name..."></select>
                                </div>
                            </div>
                            <div class="form-group">
                                    <div class="col-sm-12">
                                        <textarea class="form-control" placeholder="" id="message"></textarea>
                                    </div>
                            </div>
                        </form>
                    </div>

                    <div id="teacher" class="tab-pane">
                        <form class="form-horizontal" role="form">
    
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <select multiple="" class="chosen-select" id="teachers" data-placeholder="Teacher Name..."></select>
                                </div>
                            </div>
                            <div class="form-group">
                                    <div class="col-sm-12">
                                        <textarea class="form-control" placeholder="" id="message"></textarea>
                                    </div>
                            </div>
                        </form>
                    </div>
            </div>
    </div>
<script src="assets/js/ImageSelect.jquery.js"></script>
<script>

    for(ii=1;ii<=2;ii++){
        getSelect(ii);
    }

    $('.ui-dialog').css('overflow','visible');
    $('.ui-dialog-content').css('overflow','visible');
    $('.chosen-select').chosen({allow_single_deselect:true});
    
    $(window).on('resize.chosen', function() {
        var w = $('.chosen-select').parent().width();
        $('.chosen-select').next().css({'width':445});
    }).trigger('resize.chosen');
    
    $(".btn_ok_mdl").click(function(){
        var student = [];
        $.each($("#students option:selected"), function(){
            student.push($(this).val().split('_')[0]+'_'+$(this).text());
        });
        saveMessage(student.join(", "));
    });
    
    function getSelect(ii){
            $.ajax({
                url:'teacher/mail/getselect/'+ii,
                type:'post',
                dataType:'json',
                success: function (data) {
                    var option = "";
                    $.each(data, function(i,v){
                        photo = v.photo;
                        if(photo == ''){
                            vphoto = 'assets/img/default-user2.png';
                        }else{
                            vphoto = 'assets/img/default-user2.png';
                        }
                        if(ii == 1){
                          var val = v.student_id;
                        }
                        if(ii == 2){
                            val = v.instructor_id;
                        }
                        name = v.firstname +' '+v.lastname;
                        option += '<option data-img-src="'+vphoto+'"  value="'+val+'_'+name+'"> '+name+ '</option>';
                    });
                    if(ii == 1){
                        var selection = "students";
                    }
                    if(ii == 2){
                        selection = "teachers";
                    }
                    $('#'+selection).html( option );
                    $("#"+selection).trigger("chosen:updated");
                }
            });
    }
    
    function saveMessage(student){
        var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9+/=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/rn/g,"n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}
        var encodedStringMessage = Base64.encode($("#message").val());
        $.ajax({
            url:'teacher/mail/sendMessage',
            type:'post',
            data: {data:student,messages:encodedStringMessage},
            success: function () {
                 messageList();
            }
        });
    }
    
</script>