<style>
    .chose-image{
        width: 20px;
        height: 20px;
    }
    .chose-image-list{
        width: 20px;
        height: 20px;
    }
</style>

<div class="modal-header">
    <p class="blue bigger-120">Invite Students for Conference</p>
</div>
<div class="space"></div>
<form class="form-horizontal" role="form">
    <div class="form-group">
            <div class="col-sm-12">
                <input id="confe_title" placeholder="Conference Title" class="form-control" type="text">
            </div>
    </div>
    <div class="form-group">
        <div class="col-sm-9">
            <select multiple="" class="chosen-select" id="students" data-placeholder="Choose a Students..."></select>
        </div>
    </div>
</form>

<script src="assets/js/ImageSelect.jquery.js"></script>
<script>
    
    $.ajax({
        url:'teacher/mail/getStudents',
        type:'post',
        dataType:'json',
        success: function (data) {
            var option = "";
            $.each(data, function(i,v){
                 photo = v.photo;
                if(photo == ''){
                    vphoto = 'assets/img/default-user2.png';
                }else{
                    vphoto = photo;
                }
                name = v.firstname +' '+v.lastname;
                option += '<option data-img-src="'+vphoto+'"  value="'+v.student_id+'_'+name+'"> '+name+ '</option>';
            });
            $('#students').html( option );
            $("#students").trigger("chosen:updated");
        }
    });

    $('.ui-dialog').css('overflow','visible');
    $('.ui-dialog-content').css('overflow','visible');
    $('.chosen-select').chosen({allow_single_deselect:true});
    //resize the chosen on window resize
    $(window).on('resize.chosen', function() {
        var w = $('.chosen-select').parent().width();
        $('.chosen-select').next().css({'width':370});
    }).trigger('resize.chosen');
    $(".btn_ok_mdl").click(function(){
        var student = []; var image = [];
        $.each($("#students option:selected"), function(){
            student.push($(this).val());
        });
        saveInvite(student.join(", "), image.join("~"));


    });

    function saveInvite(student){
        if($("#confe_title").val() == "" || student == "") {
            alert("Required Fieds is empty.");
            return false;
        }
        $.ajax({
            url:'teacher/conference/saveInvite',
            type:'post',
            data: {data:student,title:$("#confe_title").val()},
            success: function () {
                 conference();
                 alert("Conference set successfully.");
                 location.reload();
            }
        })
    }
</script>