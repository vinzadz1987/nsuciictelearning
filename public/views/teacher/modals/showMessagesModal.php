<div class="modal-header">
    <p class="blue bigger-120"><?php $rstring = str_replace('_',' ',json_encode($result)); echo str_replace('"', "", $rstring); ?></p>
</div>
   
									<div class="widget-box">
										<div class="widget-header">
											<h4 class="widget-title lighter smaller">
												<i class="ace-icon fa fa-comment blue"></i>
												Conversation
											</h4>
										</div>

										<div class="widget-body">
											<div class="widget-main no-padding">
												<!-- #section:pages/dashboard.conversations -->
												<div class="dialogs">
													<div class="itemdiv dialogdiv">
														<div class="user">
															<img alt="Alexa's Avatar" src="../assets/avatars/avatar1.png" />
														</div>

														<div class="body">
															<div class="time">
																<i class="ace-icon fa fa-clock-o"></i>
																<span class="green">4 sec</span>
															</div>

															<div class="name">
																<a href="#">Alexa</a>
															</div>
															<div class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque commodo massa sed ipsum porttitor facilisis.</div>

															<div class="tools">
																<a href="#" class="btn btn-minier btn-info">
																	<i class="icon-only ace-icon fa fa-share"></i>
																</a>
															</div>
														</div>
													</div>
                                                                                                    
                                                                                                        <div class="itemdiv dialogdiv">
														<div class="user">
															<img alt="Alexa's Avatar" src="../assets/avatars/avatar1.png" />
														</div>

														<div class="body">
															<div class="time">
																<i class="ace-icon fa fa-clock-o"></i>
																<span class="green">4 sec</span>
															</div>

															<div class="name">
																<a href="#">Alexa</a>
															</div>
															<div class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque commodo massa sed ipsum porttitor facilisis.</div>

															<div class="tools">
																<a href="#" class="btn btn-minier btn-info">
																	<i class="icon-only ace-icon fa fa-share"></i>
																</a>
															</div>
														</div>
													</div>

													<div class="itemdiv dialogdiv">
														<div class="user">
															<img alt="John's Avatar" src="../assets/avatars/avatar.png" />
														</div>

														<div class="body">
															<div class="time">
																<i class="ace-icon fa fa-clock-o"></i>
																<span class="blue">38 sec</span>
															</div>

															<div class="name">
																<a href="#">John</a>
															</div>
															<div class="text">Raw denim you probably haven&#39;t heard of them jean shorts Austin.</div>

															<div class="tools">
																<a href="#" class="btn btn-minier btn-info">
																	<i class="icon-only ace-icon fa fa-share"></i>
																</a>
															</div>
														</div>
													</div>

													<div class="itemdiv dialogdiv">
														<div class="user">
															<img alt="Bob's Avatar" src="../assets/avatars/user.jpg" />
														</div>

														<div class="body">
															<div class="time">
																<i class="ace-icon fa fa-clock-o"></i>
																<span class="orange">2 min</span>
															</div>

															<div class="name">
																<a href="#">Bob</a>
																<span class="label label-info arrowed arrowed-in-right">admin</span>
															</div>
															<div class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque commodo massa sed ipsum porttitor facilisis.</div>

															<div class="tools">
																<a href="#" class="btn btn-minier btn-info">
																	<i class="icon-only ace-icon fa fa-share"></i>
																</a>
															</div>
														</div>
													</div>

													<div class="itemdiv dialogdiv">
														<div class="user">
															<img alt="Jim's Avatar" src="../assets/avatars/avatar4.png" />
														</div>

														<div class="body">
															<div class="time">
																<i class="ace-icon fa fa-clock-o"></i>
																<span class="grey">3 min</span>
															</div>

															<div class="name">
																<a href="#">Jim</a>
															</div>
															<div class="text">Raw denim you probably haven&#39;t heard of them jean shorts Austin.</div>

															<div class="tools">
																<a href="#" class="btn btn-minier btn-info">
																	<i class="icon-only ace-icon fa fa-share"></i>
																</a>
															</div>
														</div>
													</div>

													<div class="itemdiv dialogdiv">
														<div class="user">
															<img alt="Alexa's Avatar" src="../assets/avatars/avatar1.png" />
														</div>

														<div class="body">
															<div class="time">
																<i class="ace-icon fa fa-clock-o"></i>
																<span class="green">4 min</span>
															</div>

															<div class="name">
																<a href="#">Alexa</a>
															</div>
															<div class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</div>

															<div class="tools">
																<a href="#" class="btn btn-minier btn-info">
																	<i class="icon-only ace-icon fa fa-share"></i>
																</a>
															</div>
														</div>
													</div>
												</div>

												<!-- /section:pages/dashboard.conversations -->
												<form>
													<div class="form-actions">
														<div class="input-group">
															<input placeholder="Type your message here ..." type="text" class="form-control" name="message" />
															<span class="input-group-btn">
																<button class="btn btn-sm btn-info no-radius" type="button">
																	<i class="ace-icon fa fa-share"></i>
																	Send
																</button>
															</span>
														</div>
													</div>
												</form>
											</div><!-- /.widget-main -->
										</div><!-- /.widget-body -->
									</div><!-- /.widget-box -->


<script>
    var $id = <?php echo json_encode($result); ?>;
    //console.log(id);
    
    $('.dialogs,.comments').ace_scroll({
                size: 300
    });
    
    $.ajax({
        url:'teacher/training_materials/getTrainingMaterialSelect',
        type:'post',
        dataType:'json',
        success: function (data) {
            var option = "";
            $.each(data, function(i,v){
                //v.uploaded_filename.replace(/_/g,' ').split('.')[0]
//                alert(v.uploaded_filename.replace(/_/g,' '))
                option += '<option value="'+ v.uploaded_filename.replace(/_/g,' ').split('.')[0] +'">'+ v.uploaded_filename.replace(/_/g,' ').split('.')[0] + '</option>';
            });
            $('#trainingmaterials').html( option );
            $("#trainingmaterials").trigger("chosen:updated");
        }
    });
    $('.ui-dialog').css('overflow','visible');
    $('.ui-dialog-content').css('overflow','visible');
    $('.chosen-select').chosen({allow_single_deselect:true});
    //resize the chosen on window resize
    $(window).on('resize.chosen', function() {
        var w = $('.chosen-select').parent().width();
        $('.chosen-select').next().css({'width':w});
    }).trigger('resize.chosen');
    $(".btn_ok_mdl").click(function(){
        var training = [];
        $.each($("#trainingmaterials option:selected"), function(){
            training.push($(this).val());
            
        });
//        alert(training);
        savedtrainingmaterials(training.join(", "),$id)


    });

    function savedtrainingmaterials(training,id){
        $.ajax({
            url:'teacher/training_materials/savedTrainingmaterials',
            type:'post',
            data: {data:training,id:id},
            success: function (bogoko) {
                $("#grid-table").jqGrid('setGridParam',{datatype:'json'}).trigger('reloadGrid');
            }
        })
    }
</script>