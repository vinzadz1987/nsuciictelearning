<div class="modal-header">
    <p class="blue bigger-120">Add Study Materials</p>
</div>
<div class="space"></div>
<form class="form-horizontal" role="form">
    <div class="form-group">
        <label class="col-sm-3 control-label no-padding-right"> Name </label>
        <div class="col-sm-9">
            <select multiple="" class="chosen-select" id="trainingmaterials" data-placeholder="Choose a training materials..."></select>
        </div>
    </div>
</form>
<script>
    var $id = <?php echo json_encode($result); ?>;
    $.ajax({
        url:'teacher/training_materials/getTrainingMaterialSelect',
        type:'post',
        dataType:'json',
        success: function (data) {
            var option = "";
            $.each(data, function(i,v){
                option += '<option value="'+ v.uploaded_filename.replace(/_/g,' ').split('.')[0] +'">'+ v.uploaded_filename.replace(/_/g,' ').split('.')[0] + '</option>';
            });
            $('#trainingmaterials').html( option );
            $("#trainingmaterials").trigger("chosen:updated");
        }
    });
    $('.ui-dialog').css('overflow','visible');
    $('.ui-dialog-content').css('overflow','visible');
    $('.chosen-select').chosen({allow_single_deselect:true});
    //resize the chosen on window resize
    $(window).on('resize.chosen', function() {
        var w = $('.chosen-select').parent().width();
        $('.chosen-select').next().css({'width':w});
    }).trigger('resize.chosen');
    $(".btn_ok_mdl").click(function(){
        var training = [];
        $.each($("#trainingmaterials option:selected"), function(){
            training.push($(this).val());
            
        });
        savedtrainingmaterials(training.join(", "),$id)
    });

    function savedtrainingmaterials(training,id){
        $.ajax({
            url:'teacher/training_materials/savedTrainingmaterials',
            type:'post',
            data: {data:training,id:id},
            success: function (bogoko) {
                $("#grid-table").jqGrid('setGridParam',{datatype:'json'}).trigger('reloadGrid');
            }
        })
    }
</script>