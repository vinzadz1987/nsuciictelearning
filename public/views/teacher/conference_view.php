<style>
    .slide-container {
        display: none;
    }
    ._2pi8{
        width: 200px;
    }
</style>
<div class="col-sm-12">
    <div class="row">
            <div class="col-xs-12">
                    <div class="widget-box transparent">
                        <div class="widget-header widget-header-flat">
                                <h4 class="widget-title lighter">
                                        <i class="ace-icon fa fa-bookmark blue"></i>
                                        <span> Conference </span>
                                </h4>
                                <div class="widget-toolbar">
                                        <a href="#" data-action="collapse">
                                                <i class="ace-icon fa fa-chevron-up"></i>
                                        </a>
                                </div>
                        </div>
                        
                        <div class="space-6"></div>
                        
                        <?php $accessname =  $this->session->userdata('access_name'); ?>
                        
                        <?php if($accessname == "Students") { }else{ ?>
                        
                        <div class="row">
                            <div class="col-xs-10 pull-right">                    
                                <div class="form-group">
                                    <button id="create_lesson" type="button" class="btn btn-primary btn-sm pull-right"><i class="fa fa-users"></i> Invite Students </button>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                        <div class="space-6"></div>
                        
                        <div id="accordion" class="accordion-style1 panel-group">
                            <!--- CONFERENCE LIST -->
                        </div>
                        
                        
                         <div id="dialog-message2" class="hide">
                             <div id="insideModal">
                                 <iframe src="https://appear.in/nsu_ciict_student" width=900" height="600" frameborder="0"></iframe>
                             </div>
                        </div>
                        
                        <div class="space-6"></div>
                        <div class="space-6"></div>
                    </div>
            </div>
        </div>
</div><!-- /.col -->
<script src="assets/js/chosen.jquery.min.js"></script>
<script>
	$(document).ready(function(){
		conference();
		$("#create_lesson").click(function(){
			invite_student();
		});
	});
    
    
    function invite_student(){
		$('.ajaxModal').modalBox({
                        modal_view:'invite_student_modal',
                        title: false,
                        width: 400,
                        modal_data:"rowData.toq_id",
                        button_cancel: 'Cancel',
                        button_ok: 'save'
                });
               
	}
    
    function conference(){
        $.ajax({
            url:'teacher/conference/conferenceList',
            type:'post',
            dataType:'json',
            success:function(data){
                var list = '';
                $.each(data, function (key, val){
                        buttonconference = '<span style="padding: 10px 10px 10px 10px; float: right;"><button id="create_lesson" type="button" class="btn btn-primary btn-sm pull-right" onclick="startConference()"><i class="fa fa-film"></i> Start Conference </button></span>';
                            list += '<div class="panel panel-default conferenceList">\n\
                                                <div class="panel-heading">\n\
                                                        <h4 class="panel-title">\n\
                                                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse'+val.conference_id_generated+'">\n\
                                                                    <i class="bigger-110 ace-icon fa fa-angle-right" data-icon-hide="ace-icon fa fa-angle-down" data-icon-show="ace-icon fa fa-angle-right"></i>\n\
                                                                         <span class="badge badge-info">'+showJoiner(val.conference_member, val.conference_status, val.conference_date, val.conference_id_generated, val.participants_name)[1]+'</span>\n\
                                                                        <span>\n\
                                                                        '+val.conference_title+'\n\
                                                                <?php if ($this->session->userdata('access_name') == 'Teacher') { ?>\n\
                                                                    <i class="fa fa-times red pull-right" data-id="'+val.conference_id_generated+'" onclick="deleteConference(this)"></i></span>\n\
                                                                <?php } ?>\n\
                                                                </a>\n\
                                                        </h4>\n\
                                                </div>\n\
                                                <div class="panel-collapse collapse" id="collapse'+val.conference_id_generated+'" style="height: 0px;">\n\
                                                        '+buttonconference+'\n\
                                                        <div class="panel-body">\n\
                                                               '+showJoiner(val.conference_member, val.conference_status, val.conference_date, val.conference_id_generated, val.participants_name, val.photo)[0]+' \n\
                                                        </div>\n\
                                                </div>\n\
                                        </div>';
                        });
                $("#accordion").html(list);
            }
        });
    }
    
    function deleteConference(p){
    	alert('Are you sure you want to delete me.');
    	// console.log($(p).data('id'));
    }

    function showJoiner(member, status, date, confeid, parti_name,photo){
        ipart = member.split(',').length;
        var participants = "";
        var countParticipants = -1;
        var partiPhoto = '';
        for( i = 0; i <= ipart; i++ ){
            countParticipants++;
            if(typeof member.split(',')[i] === "undefined"){
                imember = "";
            }else{
                parti = member.split(',')[i];
                name = parti.split('_')[1];
                pname = parti_name.split(',')[i];
                 if(typeof pname === "undefined"){
                    imemberpname = "";
                }else{
                    imemberpname = pname;
                }
                id = parti.split('_')[0];
                if(status == "0"){
                   var stat = 'Pending';
                }
                partiPhoto = (photo ==  null) ? '<img src="assets/img/default-user2.png">' : '<img src="'+photo+'">';
                imember = '<div class="itemdiv memberdiv" style="width:211px;">\n\
                                <div class="user">\n\
                                        '+partiPhoto+'\n\
                                </div>\n\
                                <div class="body">\n\
                                        <div class="name">\n\
                                                <span class="blue bold">'+imemberpname+'</span>\n\
                                        </div>\n\
                                        <div class="name">\n\
                                                <span>'+id+'</span>\n\
                                        </div>\n\
                                        <div class="time">\n\
                                                <i class="ace-icon fa fa-clock-o"></i>\n\
                                                <span class="green">'+date+'</span>\n\
                                        </div>\n\
                                        <div>\n\
                                                <span class="label label-warning label-sm" id=stat'+confeid+'>'+stat+'</span>\n\
                                                <div class="inline position-relative">\n\
                                                        <button class="btn btn-minier btn-yellow btn-no-border">\n\
                                                                <i class="ace-icon fa fa-times icon-only bigger-120"></i>\n\
                                                        </button>\n\
                                                </div>\n\
                                        </div>\n\
                                </div>\n\
                        </div>';
            }
            participants += imember;
        }
        return [participants, countParticipants];
    }
    
    function inviteStatus(id, confeid){
         $.ajax({
            url:'teacher/conference/studentConfeStat',
            type:'post',
            dataType:'json',
            data:{studid:id,confeID:confeid},
            success:function(data){
                 var list = '';
                $.each(data, function (key, val){
                    list+=val.status;
                });
                $("#stat"+confeid).html(list);
            }
         });
    }
    
    function dialogBoxModal(titleName){
                dialog_message = 'dialog-message2';
                dialog_width = '300';
                header_name = 'Invite Students In Conference';
                $("#upload_id").val(titleName.split('_')[0]);
                getCourse();
            dialogBox(
                    model = {
                            type: 'local',
                            data: dialog_message,
                            title: header_name,
                            title_icon: 'fa-ticket',
                            width: dialog_width,
                            height: 300,
                            buttons: [
                                    {
                                            'html': "<i class='ace-icon fa fa-times'></i> Cancel",
                                            'class': 'btn_cancel btn btn-xs btn-danger',
                                            click: function(){
                                                    $(this).dialog('close');
                                            }
                                    }
                                    ,
                                    {
                                            'html': "<i class='ace-icon fa fa-check'></i> Submit",
                                            'class': 'btn-primary btn btn-xs',
                                            click: function(){
                                                    $.ajax({
                                                       url:'teacher/conference/conferenceList',
                                                       type:'post',
                                                       data:{title:$("#form-field-1-1").val(),studIDS:name},
                                                       success:function(){
                                                             lessonsList(subid);
                                                             $("#lesson_name_input").val("");
                                                             $("#description_textarea").val("");
                                                       }
                                                    });
                                            }
                                    }
                            ]
                    }
            );
    }
    
    function startConference(){
                dialog_message = 'dialog-message2';
                dialog_width = '950';
                header_name = 'Conference';
            dialogBox(
                    model = {
                            type: 'local',
                            data: dialog_message,
                            title: header_name,
                            title_icon: 'fa-ticket',
                            width: dialog_width,
                            height: 750,
                            buttons: [
                                    {
                                            'html': "<i class='ace-icon fa fa-times'></i> Cancel",
                                            'class': 'btn_cancel btn btn-xs btn-danger',
                                            click: function(){
                                                    $(this).dialog('close');
                                            }
                                    }
                                    ,
                                    {
                                            'html': "<i class='ace-icon fa fa-check'></i> Submit",
                                            'class': 'btn-primary btn btn-xs',
                                            click: function(){
                                            }
                                    }
                            ]
                    }
            );
    }
    
    function getCourse(){
                            $.ajax({
                               url:'login/getCourse',
                               type:'post',
                               dataType:'json',
                               success: function(data){
                                   var option = '<option value="">Course</option>';
                                   $.each(data, function(key, val){
                                       option += '<option value="'+val.course_id+'"> '+val.course_name+'</option>';
                                    });
                                   $("#course").html(option);
                               }
                            });
                    }
    
    
</script>
<script src="assets/js/jPages.js"></script>
<script src="assets/js/jquery.slimscroll.min.js"></script>
<script src="assets/js/bootbox.min.js"></script>
<script src="assets/js/ace-elements.min.js"></script>

<script src="assets/js/jquery.dataTables.min.js"></script>
<script src="assets/js/jquery.dataTables.bootstrap.js"></script>

<script src="assets/js/jquery.fileDownload.js"></script>
