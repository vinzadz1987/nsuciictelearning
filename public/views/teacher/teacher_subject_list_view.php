<div class="col-sm-12">
    <div class="row">
            <!---- PROSPECTUS TAB ----->
            <div id="prospectusTab">
                    <div id="widget-container-col-1" class="col-xs-12 col-sm-12 widget-container-col ui-sortable">
                        <div class="widget-box widget-color-blue ui-sortable-handle" id="widget-box-2">
                                <div class="widget-header">
                                        <h5 class="widget-title bigger lighter">
                                                <i class="ace-icon fa  fa-user"></i>
                                                FIRST YEAR
                                        </h5>
                                </div>
                                <div class="widget-body">
                                        <div class="widget-main no-padding">
                                                <div class="col-sm-12">
                                                <div class="widget-box transparent">
                                                        <div class="widget-header widget-header-flat">
                                                                <h4 class="widget-title lighter blue">
                                                                        <i class="ace-icon glyphicon glyphicon-file"></i>
                                                                        First Semester
                                                                </h4>

                                                                <div class="widget-toolbar">
                                                                        <a href="#" data-action="collapse">
                                                                                <i class="ace-icon fa fa-chevron-up"></i>
                                                                        </a>
                                                                </div>
                                                        </div>

                                                        <div class="widget-body">
                                                                <div class="widget-main no-padding">
                                                                        <table class="table table-bordered table-striped" id="tbl_fyfs"></table>
                                                                </div><!-- /.widget-main -->
                                                        </div><!-- /.widget-body -->
                                                </div><!-- /.widget-box -->
                                            </div>

                                            <div class="col-sm-12">
                                                    <div class="widget-box transparent">
                                                            <div class="widget-header widget-header-flat">
                                                                    <h4 class="widget-title lighter blue">
                                                                            <i class="ace-icon glyphicon glyphicon-file"></i>
                                                                            Second Semester
                                                                    </h4>

                                                                    <div class="widget-toolbar">
                                                                            <a href="#" data-action="collapse">
                                                                                    <i class="ace-icon fa fa-chevron-up"></i>
                                                                            </a>
                                                                    </div>
                                                            </div>

                                                            <div class="widget-body">
                                                                    <div class="widget-main no-padding">
                                                                            <table class="table table-bordered table-striped" id="tbl_fyss"></table>
                                                                    </div><!-- /.widget-main -->
                                                            </div><!-- /.widget-body -->
                                                    </div><!-- /.widget-box -->
                                            </div>
                                    </div>
                            </div>
                        </div>
                    </div>

                    <div id="widget-container-col-1" class="col-xs-12 col-sm-12 widget-container-col ui-sortable">
                        <div class="widget-box widget-color-blue ui-sortable-handle" id="widget-box-2">
                                <div class="widget-header">
                                        <h5 class="widget-title bigger lighter">
                                                <i class="ace-icon fa  fa-user"></i>
                                                SECOND YEAR
                                        </h5>
                                </div>
                                <div class="widget-body">
                                        <div class="widget-main no-padding">
                                                <div class="col-sm-12">
                                                        <div class="widget-box transparent">
                                                                <div class="widget-header widget-header-flat">
                                                                        <h4 class="widget-title lighter green">
                                                                                <i class="ace-icon glyphicon glyphicon-file"></i>
                                                                                First Semester
                                                                        </h4>

                                                                        <div class="widget-toolbar">
                                                                                <a href="#" data-action="collapse">
                                                                                        <i class="ace-icon fa fa-chevron-up"></i>
                                                                                </a>
                                                                        </div>
                                                                </div>

                                                                <div class="widget-body">
                                                                        <div class="widget-main no-padding">
                                                                                <table class="table table-bordered table-striped" id="tbl_syfs"></table>
                                                                        </div><!-- /.widget-main -->
                                                                </div><!-- /.widget-body -->
                                                        </div><!-- /.widget-box -->
                                                </div>
                                                <div class="col-sm-12">
                                                        <div class="widget-box transparent">
                                                                <div class="widget-header widget-header-flat">
                                                                        <h4 class="widget-title lighter green">
                                                                                <i class="ace-icon glyphicon glyphicon-file"></i>
                                                                                Second Semester
                                                                        </h4>

                                                                        <div class="widget-toolbar">
                                                                                <a href="#" data-action="collapse">
                                                                                        <i class="ace-icon fa fa-chevron-up"></i>
                                                                                </a>
                                                                        </div>
                                                                </div>

                                                                <div class="widget-body">
                                                                        <div class="widget-main no-padding">
                                                                                <table class="table table-bordered table-striped" id="tbl_syss"></table>
                                                                        </div><!-- /.widget-main -->
                                                                </div><!-- /.widget-body -->
                                                        </div><!-- /.widget-box -->
                                                </div>
                                        </div>
                            </div>
                        </div>
                    </div>


                    <div id="widget-container-col-1" class="col-xs-12 col-sm-12 widget-container-col ui-sortable">
                        <div class="widget-box widget-color-blue ui-sortable-handle" id="widget-box-2">
                                <div class="widget-header">
                                        <h5 class="widget-title bigger lighter">
                                                <i class="ace-icon fa  fa-user"></i>
                                                THIRD YEAR
                                        </h5>
                                </div>
                                <div class="widget-body">
                                        <div class="widget-main no-padding">

                                            <div class="col-sm-12">
                                                    <div class="widget-box transparent">
                                                            <div class="widget-header widget-header-flat">
                                                                    <h4 class="widget-title lighter purple">
                                                                            <i class="ace-icon glyphicon glyphicon-file"></i>
                                                                            First Semester
                                                                    </h4>

                                                                    <div class="widget-toolbar">
                                                                            <a href="#" data-action="collapse">
                                                                                    <i class="ace-icon fa fa-chevron-up"></i>
                                                                            </a>
                                                                    </div>
                                                            </div>

                                                            <div class="widget-body">
                                                                    <div class="widget-main no-padding">
                                                                            <table class="table table-bordered table-striped" id="tbl_tyfs"></table>
                                                                    </div><!-- /.widget-main -->
                                                            </div><!-- /.widget-body -->
                                                    </div><!-- /.widget-box -->
                                            </div>

                                            <div class="col-sm-12">
                                                    <div class="widget-box transparent">
                                                            <div class="widget-header widget-header-flat">
                                                                    <h4 class="widget-title lighter purple">
                                                                            <i class="ace-icon glyphicon glyphicon-file"></i>
                                                                            Second Semester
                                                                    </h4>

                                                                    <div class="widget-toolbar">
                                                                            <a href="#" data-action="collapse">
                                                                                    <i class="ace-icon fa fa-chevron-up"></i>
                                                                            </a>
                                                                    </div>
                                                            </div>

                                                            <div class="widget-body">
                                                                    <div class="widget-main no-padding">
                                                                            <table class="table table-bordered table-striped" id="tbl_tyss"></table>
                                                                    </div><!-- /.widget-main -->
                                                            </div><!-- /.widget-body -->
                                                    </div><!-- /.widget-box -->
                                            </div>

                                        </div>
                            </div>
                        </div>
                    </div>


                    <div id="widget-container-col-1" class="col-xs-12 col-sm-12 widget-container-col ui-sortable">
                        <div class="widget-box widget-color-blue ui-sortable-handle" id="widget-box-2">
                                <div class="widget-header">
                                        <h5 class="widget-title bigger lighter">
                                                <i class="ace-icon fa  fa-user"></i>
                                                FOURTH YEAR
                                        </h5>
                                </div>
                                <div class="widget-body">
                                        <div class="widget-main no-padding">

                                            <div class="col-sm-12">
                                                    <div class="widget-box transparent">
                                                            <div class="widget-header widget-header-flat">
                                                                    <h4 class="widget-title lighter red">
                                                                            <i class="ace-icon glyphicon glyphicon-file"></i>
                                                                            First Semester
                                                                    </h4>

                                                                    <div class="widget-toolbar">
                                                                            <a href="#" data-action="collapse">
                                                                                    <i class="ace-icon fa fa-chevron-up"></i>
                                                                            </a>
                                                                    </div>
                                                            </div>

                                                            <div class="widget-body">
                                                                    <div class="widget-main no-padding">
                                                                            <table class="table table-bordered table-striped" id="tbl_fryfs"></table>
                                                                    </div><!-- /.widget-main -->
                                                            </div><!-- /.widget-body -->
                                                    </div><!-- /.widget-box -->
                                            </div>

                                            <div class="col-sm-12">
                                                    <div class="widget-box transparent">
                                                            <div class="widget-header widget-header-flat">
                                                                    <h4 class="widget-title lighter red">
                                                                            <i class="ace-icon glyphicon glyphicon-file"></i>
                                                                            Second Semester
                                                                    </h4>

                                                                    <div class="widget-toolbar">
                                                                            <a href="#" data-action="collapse">
                                                                                    <i class="ace-icon fa fa-chevron-up"></i>
                                                                            </a>
                                                                    </div>
                                                            </div>

                                                            <div class="widget-body">
                                                                    <div class="widget-main no-padding">
                                                                            <table class="table table-bordered table-striped" id="tbl_fryss"></table>
                                                                    </div><!-- /.widget-main -->
                                                            </div><!-- /.widget-body -->
                                                    </div><!-- /.widget-box -->
                                            </div>

                                        </div>
                            </div>
                        </div>
                    </div>
                
            </div>
            
            <!------ END OF PROSPECTUS TAB ---->
            
            <!------ LESSON LIST VIEW --------->
            
            
            
            <div id="lessonTab" style="display: none">
                <div class="col-xs-12">
                        <div class="widget-box transparent">
                            <div class="widget-header widget-header-flat">
                                    <h4 class="widget-title lighter">
                                            <i class="ace-icon fa fa-bookmark blue"></i>
                                            <span id="lesson_name"><span id="subjectCode"></span> <small id="subjectDesc"></small> </span>
                                    </h4>
                                    <div class="widget-toolbar">
                                            <a href="#" data-action="collapse">
                                                    <i class="ace-icon fa fa-chevron-up"></i>
                                            </a>
                                    </div>
                            </div>
                            <div class="space-6"></div>
                            <div class="row">
                                <div class="col-xs-12">                    
                                    <div class="form-group">
                                        <button id="create_lesson" type="button" class="btn btn-primary btn-sm pull-right"><i class="fa fa-plus"></i> Create Lesson </button>
                                    </div>
                                </div>
                            </div>
                            <div class="space-6"></div>
                            <div class="widget-body">
                                    <div class="widget-body-inner" style="display: block;">
                                                <div class="widget-main no-padding">
                                                     <div id="faq-tab-1" class="tab-pane fade in active">
                                                            <div id="faq-list-1" class="panel-group accordion-style1 accordion-style2"></div>
                                                    </div>
                                                </div>
                                        <!-- /.widget-main -->
                                    </div>
                            </div><!-- /.widget-body -->
                        </div>
                </div>
            </div>
            
            <!----- END OF LESSON LIST ----->


        
            <!----- STUDENT LIST MODAL --->
                <div id="dialog-message" class="hide">
                    <div id="insideModal">
                        <table id="dataTable_view" class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Student ID</th>
                                <th>Grade</th>
                                <th>Full Name</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            <!---- END OF STUDENT LIST MODAL --->
            
            <!---- CREATE LESSON MODAL --->
                <div id="addLessonModal" class="hide">
                        <div class="space-4"></div>
                        <form class="form-horizontal" role="form">
                                <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Lesson name </label>

                                        <div class="col-sm-9">
                                            <span id="subjectID4Create" class="hide"></span>
                                            <input id="lesson_name_input" placeholder="Lesson name" class="col-xs-12" type="text">
                                        </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1-1"> Description </label>
                                    
                                        <div class="col-sm-9">
                                                <input id="description_textarea" placeholder="Description" class="col-xs-12" type="text">
                                        </div>
                                </div>
                        </form>
                </div>
            <!---- END CREATE LESSON MODAL --->
            
            <!---- UPLOAD MODAL --->
            <div id="uploadModal" class="hide">
                <!--<div id="insideModal">-->
                    <div class="col-sm-12">
                            <div class="widget-box transparent" id="recent-box">
                                    <div class="widget-header">
                                            <h4 class="widget-title lighter smaller">
                                                    <i class="ace-icon fa fa-rss orange"></i>LEARNING MATERIALS
                                            </h4>

                                            <div class="widget-toolbar no-border">
                                                    <ul class="nav nav-tabs" id="recent-tab">
                                                            <li class="active">
                                                                    <a data-toggle="tab" href="#docs-tab" aria-expanded="true">Files</a>
                                                            </li>

                                                            <!--<li class="">
                                                                    <a data-toggle="tab" href="#images-tab" aria-expanded="false">Images</a>
                                                            </li>-->

                                                            <li class="">
                                                                    <a data-toggle="tab" href="#videos-tab" aria-expanded="false">Video</a>
                                                            </li>
                                                            
                                                            <li class="">
                                                                    <a data-toggle="tab" href="#text-tab" aria-expanded="false">Notes</a>
                                                            </li>
                                                    </ul>
                                            </div>
                                    </div>

                                    <div class="widget-body">
                                            <div class="widget-main padding-4">
                                                    <div class="tab-content padding-8">
                                                            <div id="docs-tab" class="tab-pane active">
                                                                <div class="col-sm-12">
                                                                        <div class="widget-box transparent" id="recent-box">
                                                                                <div id="upload-tab" class="tab-pane">
                                                                                        <h4 class="smaller lighter green">
                                                                                                <i class="ace-icon fa fa-upload"></i>
                                                                                                Upload materials
                                                                                        </h4>

                                                                                        <div class="panel-body">
                                                                                                <!--action="uploading/uploadPhysicalFiles"-->
                                                                                                <form enctype="multipart/form-data"  method="post" id="uploadForm">
                                                                                                        <input type="file" id="uploaded_file" name="files" />
                                                                                                        <input type="hidden" name="upload_name" id="upload_id"/>
                                                                                                        <input type="hidden" name="params" id="paramid" value="<?php echo $params; ?>"/>
                                                                                                        <input id="uploadExcel" type="submit" class="btn btn-success pull-right" value="Upload"/>
                                                                                                </form>
                                                                                                <div class="form-group">
                                                                                                    <span class="red">Only accept .doc,.csv,.docx and .pdf files. </span>
                                                                                                </div>
                                                                                        </div>

                                                                                </div>
                                                                        </div>
                                                                </div>
                                                            </div>

                                                            <div id="images-tab" class="tab-pane">
                                                                <div class="col-sm-12">
                                                                    <input id="form-field-1-1" placeholder="Video Embed" class="form-control" type="text">
                                                                </div>
                                                            </div><!-- /.#member-tab -->

                                                            <div id="videos-tab" class="tab-pane">
                                                                <div class="col-sm-12">
                                                                    <div class="alert alert-block alert-success">
                                                                            <button type="button" class="close" data-dismiss="alert">
                                                                                    <i class="ace-icon fa fa-times"></i>
                                                                            </button>

                                                                            <i class="ace-icon fa fa-check green"></i>
                                                                            You can get embed video by through  <strong class="green">Shared</strong>
                                                                            and then Embed, copy and paste the code below and put
                                                                            in the embed text area. Thank you.
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <input id="video_title" placeholder="Video Title" class="form-control" type="text">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <textarea id="video_description" placeholder="Video Description" class="form-control"></textarea>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <textarea id="video_embed" placeholder="Embed Video Here" class="form-control"></textarea>
                                                                    </div>
                                                                    <input id="addVideo" type="submit" class="btn btn-primary pull-right" value="Add Video"/>
                                                                </div>
                                                            </div>
                                                            
                                                            <div id="text-tab" class="tab-pane">
                                                                <div class="col-sm-12">
                                                                    <div class="form-group">
                                                                        <input id="title_notes" placeholder="Notes Title" class="form-control" type="text">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <textarea id="title_description" placeholder="Notes Description" class="form-control"></textarea>
                                                                    </div>
                                                                    <input id="addNotes" type="submit" class="btn btn-success pull-right" value="Create Notes"/>
                                                                </div>
                                                            </div>
                                                    </div>
                                            </div><!-- /.widget-main -->
                                    </div><!-- /.widget-body -->
                            </div><!-- /.widget-box -->
                    </div>
                <!--</div>-->
            </div>
            
    </div>
</div>

<script>
    $(document).ready(function(){
        
         $("#create_subject").click(function(){
             dialogBoxModalAdd('Create Subjects');
         });
         
         $("#create_lesson").click(function(){
             dialogBoxModalLesson('Create Lesson');
         });
         
         $("#addNotes").click(function(){
             addNotes();
         });
         
         $("#addVideo").click(function(){
             addVideo();
         });
         
         yoursubjects();
         
         $('#uploaded_file').ace_file_input({
                style:'well',
                btn_choose:'Drop files here or click to choose',
                btn_change:null,
                no_icon:'ace-icon fa fa-cloud-upload',
                droppable:true,
                thumbnail:'small'
        }).on('change', function(){
                var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.csv|.doc|.docx|.pdf)$/;
                (regex.test($("#uploaded_file").val().toLowerCase())) ? '':alert("you can only upload csv, doc and pdf.");
                $('.remove').attr('href','javascript:void(0)');
                $('.fa-times').attr('onclick','remove_upload()');
        });
        
        $( '#uploadForm' )
            .submit( function( e ) {
              $.ajax( {
                url: 'uploading/uploadPhysicalFiles',
                type: 'POST',
                data: new FormData( $("#uploadForm")[0] ),
                processData: false,
                contentType: false
              } );
                var subid = $("#subjectID4Create").text(); 
                lessonsList(subid);
                alert("FILE UPLOAD SUCCESSFULLY");
              e.preventDefault();
            } );
         
    });
    
    function addVideo(){
        var video_title = $("#video_title").val();
        var video_desc = $("#video_description").val();
        var video_embed = $("#video_embed").val();
        var subid = $("#upload_id").val();
        $.ajax({
            url:'teacher/teacher_subjects/addVideo',
            type:'post',
            data:{subjectid:subid,video_title:video_title,video_desc: video_desc, video_embed: video_embed},
            success:function(){
                alert("VIDEO ADDED");
                var id = $("#subjectID4Create").text(); 
                lessonsList(id);
            }
         });
    }
    
    function addNotes(){
        var notes_title = $("#title_notes").val();
        var notes_desc = $("#title_description").val();
        var subid = $("#upload_id").val();
        $.ajax({
            url:'teacher/teacher_subjects/addNotes',
            type:'post',
            data:{subjectid:subid,notes_title:notes_title,notes_desc: notes_desc},
            success:function(){
                alert("NOTE ADDED");
                var id = $("#subjectID4Create").text(); 
                lessonsList(id);
            }
         });
    }
    
    function yoursubjects(){
        $.ajax({
                url:'teacher/teacher_subjects/getSubjects',
                type:'post',
                dataType:'json',
                success:function(data){
                        var thead = '<thead class="thin-border-bottom success">\n\
                                                        <tr>\n\
                                                                <th>SUBJ. NO.</th>\n\
                                                                <th>DESCRIPTIVE TITLE</th>\n\
                                                                <th>PRE-REQUISITE</th>\n\
                                                                <th>LEC</th>\n\
                                                                <th>LAB</th>\n\
                                                                <th>UNITS</th>\n\
                                                                <th></th>\n\
                                                        </tr>\n\
                                                </thead>';

                                                var trcontent1 = thead +'<tbody>'; 
                                                var trcontent2 = thead +'<tbody>';
                                                var trcontent3 = thead +'<tbody>';
                                                var trcontent4 = thead +'<tbody>'; 
                                                var trcontent5 = thead +'<tbody>'; 
                                                var trcontent6 = thead +'<tbody>';
                                                var trcontent7 = thead +'<tbody>'; 
                                                var trcontent8 = thead +'<tbody>';
                                                
                        $.each(data, function (key, val){
                                var snw = 'style="width:200px"'; 
                                var dtw = 'style="width:500px"';
                                ins_name = val.firstname+' '+val.lastname;
                                log_ins = '<?php echo $this->session->userdata('user_id') ?>';
                                if(val.year == 1 && val.sem == 1){
                                        hasactions = actions(log_ins, val.prospectid,val.instructor,ins_name,val.count_students, val.subject_code, val.description, val.photo);

                                        trcontent1 += insideTable(val,hasactions,snw,dtw,val.count_status,val.count_students);
                                }else if(val.year == 1 && val.sem == 2){
                                        hasactions = actions(log_ins, val.prospectid,val.instructor,ins_name,val.count_students, val.subject_code, val.description, val.photo);

                                        trcontent2 += insideTable(val,hasactions,snw,dtw,val.count_status,val.count_students);
                                }else if(val.year == 2 && val.sem == 1){
                                        hasactions = actions(log_ins, val.prospectid,val.instructor,ins_name,val.count_students, val.subject_code, val.description, val.photo);

                                        trcontent3 += insideTable(val,hasactions,snw,dtw,val.count_status,val.count_students);
                                }else if(val.year == 2 && val.sem == 2){

                                        hasactions = actions(log_ins, val.prospectid,val.instructor,ins_name,val.count_students, val.subject_code, val.description, val.photo);

                                        trcontent4 += insideTable(val,hasactions,snw,dtw,val.count_status,val.count_students);
                                }else if(val.year == 3 && val.sem == 1){

                                        hasactions = actions(log_ins, val.prospectid,val.instructor,ins_name,val.count_students, val.subject_code, val.description, val.photo);

                                        trcontent5 += insideTable(val,hasactions,snw,dtw,val.count_status,val.count_students);
                                }else if(val.year == 3 && val.sem == 2){

                                        hasactions = actions(log_ins, val.prospectid,val.instructor,ins_name,val.count_students, val.subject_code, val.description, val.photo);

                                        trcontent6 += insideTable(val,hasactions,snw,dtw,val.count_status,val.count_students);
                                }else if(val.year == 4 && val.sem == 1){

                                        hasactions = actions(log_ins, val.prospectid,val.instructor,ins_name,val.count_students, val.subject_code, val.description, val.photo);

                                        trcontent7 += insideTable(val,hasactions,snw,dtw,val.count_status,val.count_students);
                                }else if(val.year == 4 && val.sem == 2){

                                        hasactions = actions(log_ins, val.prospectid,val.instructor,ins_name,val.count_students, val.subject_code, val.description, val.photo);

                                        trcontent8 += insideTable(val,hasactions,snw,dtw,val.count_status,val.count_students);
                                }
                        });
                        
                        $('table#tbl_fyfs').html(trcontent1 +'</tbody>'); 
                        $('table#tbl_fyss').html(trcontent2 +'</tbody>');
                        $('table#tbl_syfs').html(trcontent3 +'</tbody>'); 
                        $('table#tbl_syss').html(trcontent4 +'</tbody>');
                        $('table#tbl_tyfs').html(trcontent5 +'</tbody>');
                        $('table#tbl_tyss').html(trcontent6 +'</tbody>');
                        $('table#tbl_fryfs').html(trcontent7 +'</tbody>');
                        $('table#tbl_fryss').html(trcontent8 +'</tbody>');
                            
                }
        });
    }
    
    function insideTable(val,hasactions,snw,dtw,status,studcount){
        return '<tr id="'+val.prospectid+'">\n\
                    <td '+snw+' class="">'+val.subject_code+'</td>\n\
                    <td '+dtw+' class="">'+val.description+''+pending_stud(status,studcount,val.instructor)+'</td>\n\
                    <td class="">\n\
                            <b class="green"></b>\n\
                    </td>\n\
                    <td class="">\n\
                            <b class="green"></b>\n\
                    </td>\n\
                    <td class="">\n\
                            <b class="green"></b>\n\
                    </td>\n\
                    <td class="">'+val.units+'</td>\n\
                    <td class="" '+snw+'>\n\
                            '+hasactions+'\n\
                    </td>\n\
                </tr>';
                        
    }
    
    function pending_stud(param,studcount,instructor){
            if('<?php echo $this->session->userdata('user_id')?>' == instructor){
                if(param == 0 || studcount == 0){
                    return '';
                }
                if(param > 0 ){
                    return '<span class="pull-right blue smaller-90">\n\
                                <small>'+param+' pending</small>\n\
                            </span>';
                }
            }else{
                return '';
            }
        
    }
    
    function actions(log_ins,subid,insid,ins_name,count_stud, subcode, description, photo){
        var items = ["label-success","label-danger","label-info","label-inverse"," label-pink","label-yellow","label-purple"];
        var rand = items[Math.floor(Math.random() * items.length)];
        if(log_ins == insid){
            return'<div class="action-buttons">\n\
                        <a href="javascript:void(0)" class="green">\n\
                                <span class="label label-lg '+rand+'" onclick="dialogBoxModal(\''+subid+'_'+insid+'_'+subcode+'_'+status+'\')"> '+count_stud+' <i class="fa fa-user"></i> </span>\n\
                        </a>\n\
                        <a href="javascript:void(0)" class="green">\n\
                                <span class="label label-lg '+rand+'" onclick="showLessons(\''+subid+','+subcode+','+description+'\')"> <i class="fa fa-files-o"></i> Lessons </span>\n\
                        </a>\n\
                     </div>';
        }else{
            if(photo == ''){
                photo = 'assets/img/default-user2.png';
            }
            return'<div class="action-buttons">\n\
                            <img class="pull-left img-circle" alt="" src="'+photo+'" style="height: 20px; width: 20px; text-align: center">&nbsp;'+ins_name+' \n\
                    </div>';           
            
        }
    }
    
    function subjectenrolled(insid){
        var yoursub;
            if('<?php echo $this->session->userdata('user_id')?>' == insid){
                yoursub += "text-sm";
            }else{
                yoursub += "";
            }
        return identsubs = yoursub.replace(/undefined/g,"");
    }
    
    function showLessons(params){
        $("#prospectusTab").fadeOut();
        $("#lessonTab").fadeIn();
        $("#subjectCode").html(params.split(',')[1]);
        $("#subjectDesc").html(params.split(',')[2]);
        $("#subjectID4Create").html(params.split(',')[0]);
        subjectID = params.split(',')[0];
        lessonsList(subjectID);
    }
    
    function lessonsList(id){
        $.ajax({
            url:'student/subjects/getLessons',
            type:'post',
            dataType:'json',
            data:{id:id},
            success:function(data){
                var list = '';
                $.each(data, function (key, val){
                    
                    var items = ["label-success",
                                 "label-light","label-yellow","label-primary white"];
                    var rand = items[Math.floor(Math.random() * items.length)];
                    list += '<div class="panel panel-default">\n\
                            <div class="panel-heading" onclick="'+showDownloads(val.lesson_id)+'">\n\
                                    <a href="#faq-1-'+val.lesson_id+'" data-parent="#faq-list-'+val.lesson_id+'" data-toggle="collapse" class="accordion-toggle '+rand+' collapsed">\n\
                                            <i class="ace-icon fa fa-chevron-left pull-right" data-icon-hide="ace-icon fa fa-chevron-down" data-icon-show="ace-icon fa fa-chevron-left"></i>\n\
                                            <i class="ace-icon white fa fa-cloud-upload pull-right bigger-150" data-icon-hide="ace-icon fa fa-cloud-upload" data-icon-show="ace-icon fa fa-cloud-upload" title="Upload materials" onclick="dialogUploadModal(\''+val.lesson_id+'_'+val.lesson_name+'\')"></i>\n\
                                            &nbsp;<span></span> <span class="bold"><i class="ace-icon fa fa-files-o bigger-130"></i> '+val.lesson_name.toUpperCase()+'</span><small> '+val.lesson_description+' </small>\n\
                                    </a>\n\
                            </div>\n\
                            <div class="panel-collapse collapse" id="faq-1-'+val.lesson_id+'">\n\
                                    <div class="panel-body">\n\
                                            <div id="downloadlist'+val.lesson_id+'">\n\
                                            </div>\n\
                                    </div>\n\
                            </div>\n\
                    </div>';
                });
                
                $("#faq-list-1").html(list);
            }
        });
    }
    
    function showDownloads(id){
        $.ajax({
            url:'student/subjects/getDownloads',
            type:'post',
            dataType:'json',
            data:{download_id:id}, 
            success:function(data){
                upload = '<div class="col-xs-12 col-sm-12 widget-container-col ui-sortable">\n\
                                <div class="widget-header widget-header-flat">\n\
                                        <h5 class="widget-title lighter">\n\
                                                <i class="ace-icon fa fa-star orange"></i>\n\
                                                Documents \n\
                                        </h5>\n\
                                        <div class="widget-toolbar">\n\
                                        </div>\n\
                                </div><div class="space-4"></div><p>';
                $.each(data, function (key, val){
                    var items = ["btn-success","btn-danger","btn-info","btn-inverse"," btn-pink","btn-yellow","btn-purple"];
                    var rand = items[Math.floor(Math.random() * items.length)];
                    var filename = val.uploaded_filename.split('.')[1];
                    if(filename == 'docx' || filename == 'doc'){
                        var icon = 'word_icon';
                        btn_color = rand;
                    }else if(filename == 'xlsx'){ 
                         icon = 'excel-icon';
                         btn_color = rand;
                    }else if(filename == 'pdf'){
                         icon = 'pdf_icon';
                         btn_color = rand;
                    }else{
                        icon = 'powerpoint-icon';
                        btn_color = rand;
                    }
                    upload +=  '\n\
                                  <span id=fileShow'+val.uploaded_id+' title='+val.uploaded_path+' class="btn btn-app '+btn_color+' btn-sm no-radius" style="padding-right: 10px; width:202px;" onclick=downloadFile('+val.uploaded_id+')>\n\
                                          <img src="assets/img/'+icon+'.png" style="width:30px; height: 30px; border: 0px; border-radius: 0px"/>\n\
                                          <small title='+val.uploaded_filename.split('.')[0]+'>'+val.uploaded_filename.split('.')[0].substring(0,9)+'..</small>\n\
                                  </span>';
                });
                    //////////////// NOTES //////////////////////
                    upload += '</p></div><div class="space-20"></div><div class="col-xs-12 col-sm-12 widget-container-col ui-sortable"><div class="widget-header widget-header-flat">\n\
                                        <h5 class="widget-title ">\n\
                                                <i class="ace-icon fa fa-file-archive-o orange"></i>\n\
                                                Notes \n\
                                        </h5>\n\
                                        <div class="widget-toolbar">\n\
                                        </div>\n\
                                </div><div class="space-4"></div><p id="notes'+id+'">';
                                                    
                    upload +=getnotes(id)+'</p></div>'; 
                    
                    //////////////// END NOTES //////////////////////
                    
                    //////////////// VIDEOS //////////////////////
                
                    upload += '<br><div class="col-xs-12 col-sm-12 widget-container-col ui-sortable"><div class="widget-header widget-header-flat">\n\
                                        <h5 class="widget-title lighter">\n\
                                                <i class="ace-icon fa fa-film orange"></i>\n\
                                                Video \n\
                                        </h5>\n\
                                        <div class="widget-toolbar">\n\
                                        </div>\n\
                                </div><div class="space-4"></div><p id="videos'+id+'">';
                                                    
                    upload +=getVideos(id)+'</p></div>'; 
                    
                    //////////////// END NOTES //////////////////////
                
                $("#downloadlist"+id).html(upload);
            }
        });

    }
    
    function getnotes(id){
        $.ajax({
            url:'teacher/teacher_subjects/getNotes',
            type:'post',
            dataType:'json',
            data:{id:id},
            success:function(data){
                var list = '';
                $.each(data, function (key, val){
                    //var items = ["widget-color-dark","widget-color-pink","widget-color-orange","widget-color-blue"," widget-color-dark"];
                    //var rand = items[Math.floor(Math.random() * items.length)];
                    list += '<div class="col-xs-12 col-sm-6 widget-container-col  ui-sortable">\n\
                                    <div class="widget-box ">\n\
                                            <div class="widget-header">\n\
                                                    <h5 class="widget-title smaller">'+val.notes_title+'</h5>\n\
                                                    <div class="widget-toolbar">\n\
                                                       <a href=""><i class="ace-icon fa fa-times"></i></a>\n\
                                                    </div>\n\
                                            </div>\n\
                                            <div class="widget-body">\n\
                                                    <div class="widget-main padding-6">\n\
                                                            <div class="alert alert-info"> '+val.notes_description +' </div>\n\
                                                    </div>\n\
                                            </div>\n\
                                    </div>\n\
                            </div>';
                });
                $("#downloadlist"+id).find('#notes'+id).html(list);  
            }
        });
         
    }
    
    function getVideos(id){
        $.ajax({
            url:'teacher/teacher_subjects/getVideos',
            type:'post',
            dataType:'json',
            data:{id:id},
            success:function(data){
                var list = '';
                $.each(data, function (key, val){
                    var items = ["widget-color-dark","widget-color-pink","widget-color-orange","widget-color-blue"," widget-color-dark"];
                    var rand = items[Math.floor(Math.random() * items.length)];
                    list += '<div class="col-xs-12 col-sm-12 widget-container-col  ui-sortable">\n\
                                    <div class="widget-box '+rand+'">\n\
                                            <div class="widget-header">\n\
                                                    <h5 class="widget-title smaller">'+val.video_name+' <small> '+val.video_description+' </small></h5>\n\
                                                    <div class="widget-toolbar">\n\
                                                       <a href=""><i class="ace-icon fa fa-times"></i></a>\n\
                                                    </div>\n\
                                            </div>\n\
                                            <div class="widget-body">\n\
                                                    <div class="widget-main padding-6">\n\
                                                          <center>   '+val.video_embed +' </center> \n\
                                                    </div>\n\
                                            </div>\n\
                                    </div>\n\
                            </div>';
                });
                $("#downloadlist"+id).find('#videos'+id).html(list);  
            }
        });
    }
   
    function dialogBoxModal(val){
            $.ajax({
                url:'teacher/teacher_subjects/getEnrolledStudents',
                type:'post',
                dataType:'json',
                data:{subject_id:val.split('_')[0]},
                success:function(data){
                     var table = $('table#dataTable_view').DataTable({
                        bDestroy: true,
                        bLengthChange: false,
                        paging: false
                     });
                    if (table) table.fnClearTable();
                    $.each(data, function (key, value){
                        var photo = value.studphoto;
                        if(photo == ""){
                            vphoto = "assets/img/default-user2.png";
                        }else{
                            vphoto = photo;
                        }
                        if(value.stats == 1){
                            igrades = '<input type="text" value="'+value.grades+'" id="i_'+value.enrolled_id+'" class="hidden col-xs-8"><a id="icE_'+value.enrolled_id+'"  href="javascript:void(0)" onclick="showiEdit(\''+value.enrolled_id+'_'+value.grades+'\')"><i class="fa fa-pencil-square-o red pull-right"></i></a><a id="icA_'+value.enrolled_id+'"  href="javascript:void(0)" class="hidden" onclick="iGrades(\''+value.enrolled_id+'_'+value.grades+'\')"><i class="fa fa-check blue pull-right"></i></a>'; //
                        }else{
                            igrades = ''
                        }
                        $('table#dataTable_view').dataTable().fnAddData( [
                            value.student_id,
                            '<span id="grd_'+value.enrolled_id+'">'+value.grades +'</span> '+igrades,
                            '<img class="pull-left img-circle" alt="" src="'+vphoto+'" style="width: 20px; height: 20px;">\n\
                                &nbsp;'+value.firstname +' '+value.lastname +' \n\
                                <span class="pull-right">\n\
                                    '+confirm_students(value.stats,value.student_id,value.subject_id,value.enrolled_id,val.split('_')[2])+'\n\
                                </span>'
                        ]);
                    });
                }
            });
            openModal(val.split('_')[2]);
    }
    
    function showiEdit(param){
       $("#i_"+param.split('_')[0]).removeClass('hidden');
       $("#icE_"+param.split('_')[0]).addClass('hidden');
       $("#icA_"+param.split('_')[0]).removeClass('hidden');
       $("#grd_"+param.split('_')[0]).addClass('hidden');
    }
    
    function iGrades(param){
         $.ajax({
            url:'teacher/teacher_subjects/addGrades',
            type:'post',
            data:{subjectid:param.split('_')[0],grades:$("#i_"+param.split('_')[0]).val()},
            success:function(){
                alert("GREET! GRADE ADDED.");
            }
         });
    }
    
    function dialogBoxModalLesson(){
            dialog_message = 'addLessonModal';
            dialog_width = '400';
            header_name = $("#subjectCode").text();
            dialogBox(
                    model = {
                            type: 'local',
                            data: dialog_message,
                            title: header_name,
                            title_icon: 'fa-ticket',
                            width: dialog_width,
                            buttons: [
                                    {
                                            'html': "<i class='ace-icon fa fa-times'></i> Cancel",
                                            'class': 'btn_cancel btn btn-xs btn-danger',
                                            click: function(){
                                                    $(this).dialog('close');
                                            }
                                    }
                                    ,
                                    {
                                            'html': "<i class='ace-icon fa fa-check'></i> Submit",
                                            'class': 'btn-primary btn btn-xs',
                                            click: function(){
                                                var name = $("#lesson_name_input").val();
                                                var descript = $("#description_textarea").val();
                                                if(name == "" || descript == ""){
                                                    alert('REQUIRED FIELD/S IS EMPTY.');
                                                }else{
                                                    var subid = $("#subjectID4Create").text(); 
                                                    $.ajax({
                                                       url:'student/subjects/addLessons',
                                                       type:'post',
                                                       data:{subjectid:subid,name:name,description: descript},
                                                       success:function(){
                                                             lessonsList(subid);
                                                             $("#lesson_name_input").val("");
                                                             $("#description_textarea").val("");
                                                       }
                                                    });
                                                }
                                            }
                                    }
                            ]
                    }
            );
    }
    
     function dialogUploadModal(titleName){
            var dialog_message = 'uploadModal'; 
            var dialog_width = '500';
            var header_name = titleName.split('_')[1];
            $("#upload_id").val(titleName.split('_')[0]);
            dialogBox(
                    model = {
                            type: 'local',
                            data: dialog_message,
                            title: header_name,
                            title_icon: 'fa-ticket',
                            width: dialog_width,
                            buttons: [
                                    {
                                            'html': "<i class='ace-icon fa fa-times'></i> Cancel",
                                            'class': 'btn_cancel btn btn-xs btn-danger',
                                            click: function(){
                                                    $(this).dialog('close');
                                            }
                                    }
                            ]
                    }
            );
        
        
    }
    
     function downloadFile(id){
            $.fileDownload($("#fileShow"+id).attr('title'),{});
    }
    
    function confirm_students(ident, studid, subid, enrollid, subject){
        if(ident == 1){
            return '';
        }else{
            return '<button type="button" class="btn btn-xs btn-info" id="confirm'+enrollid+'" onclick="approveStudent(\''+studid+'_'+subid+'_'+enrollid+'_'+subject+'\')">confirm</button>';
        }
    }
 
  
  
    function approveStudent(param){
            var r = confirm("Approve Student?");
            if (r == true) {
                $.ajax({
                        url:'teacher/teacher_subjects/approveStudent',
                        type:'post',
                        data:{param:param.split('_')[0]+'_'+param.split('_')[1]},
                        success:function(){
                            dialogBoxModal(param.split('_')[1]);
                            openModal(param.split('_')[3]);
                            yoursubjects();
                        }
                });
            } else {
                exit();
            }
    }
    
    function openModal(subject){
        dialogBox(
                    model = {
                            type: 'local',
                            data: 'dialog-message',
                            title: 'Student Enrolled in '+subject,
                            title_icon: 'fa-user',
                            width: 700,
                            buttons: [
                                    {
                                            'html': "<i class='ace-icon fa fa-times white'></i> Cancel",
                                            'class': 'btn_cancel btn btn-xs btn-danger',
                                            click: function(){
                                                    $(this).dialog('close');
                                            }
                                    }
                            ]
                    }
            );
    }
    
    
</script>

<script src="assets/js/jPages.js"></script>
<script src="assets/js/jquery.slimscroll.min.js"></script>
<script src="assets/js/bootbox.min.js"></script>
<script src="assets/js/ace-elements.min.js"></script>

<script src="assets/js/jquery.dataTables.min.js"></script>
<script src="assets/js/jquery.dataTables.bootstrap.js"></script>

<script src="assets/js/jquery.fileDownload.js"></script>
