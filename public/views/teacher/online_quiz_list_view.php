<div class="col-sm-12">
	<h3 class="row header smaller lighter green">
		<span class="col-sm-8">
			Online Test
		</span>
	</h3>

	<div class="row">
		<div class="col-xs-12">
			<div class="row">
				<div id="grid-name" class="grid-wrap col-xs-12 compress head-primary">
					<table id="grid-table"></table>
					<div id="grid_pager"></div>
				</div>
			</div>
		</div><!-- /.col -->
	</div><!-- /.row -->
</div>

<script src="assets/js/jqGrid/jquery.jqGrid.preset.js"></script>
<script src="assets/js/grid.locale-en.js"></script>
<script src="assets/js/jquery.jqGrid.min.js"></script>
<!-- chosen plugin -->
<script src="assets/js/chosen.jquery.min.js"></script>
<script type="text/javascript">
	jQuery(function($) {
		$('#grid-table').wd_jqGrid({
			url: 'settings/jqGrid_settings_ctrl/load_data',
			module: 'online_quiz',
			module_data: {agent_quiz:true},
			pager: '#grid_pager',
			colNames: ['ID','Quiz Name','Learning Materials','Score','Teacher','Date'],
			colModel: [	
				{name:'toq_id',index:'toq_id', width:0, sorttype:"int",hidden:true},
				{name:'toq_name',index:'toq_name',width:100, editable:true, cellattr: function() { return 'style="white-space: normal;"' }},
				{name:'training_materials',index:'training_materials', width:200, editable: true, align: 'center'},
				{name:'score',index:'score', width:90, editable: true, align: 'center'},
				{name:'author',index:'author', width:90, editable: true, align: 'center'},
				{name:'updated_date',index:'updated_date', width:50, editable: true, align: 'center'}
			],
			sortname: 'toq_id',
			caption: false,
			height: 400,
			altRows: true,
			ondblClickRow: function(rowid,iRow,iCol,e){
				var rowData = $(this).jqGrid('getRowData',rowid);
				$.redirect('teacher/online_quiz_list', {quiz_id:rowData.toq_id, title:rowData.toq_name});
			},
			loadComplete: function(data){
				var grid = $(this);
				// add custom button in action field
				setTimeout(function(){
					styleCheckbox(grid);
					updateActionIcons(grid);
					updatePagerIcons(grid);
					enableTooltips(grid);
				}, 0);
			}
		});
	});
	function showDowloads(){
		alert('Show Dowloawable files.');
	} 
</script>