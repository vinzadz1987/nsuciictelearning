<div class="row">
	<h2 class="center green"><?php echo $title; ?></h2>
	<div class="hr hr-dotted hr-16"></div>
        
        
        <h1><span id="timerText">--:--</span> Remaining</h1>

        
        
	<div class="col-xs-12">
		<form id="form_quiz" method="POST" action="teacher/online_quiz_sheet/calculate_partial" role="form">
			<input type="hidden" name="quiz_id" value="<?php echo $quiz_id ?>">
		<?php echo $quizes; ?>
	</div>
	<div class="col-xs-12">
		<div class="clearfix form-actions">
			<div class="col-md-12">
			<button id="quiz_submit" class="btn btn-info pull-right" type="submit">
				<i class="ace-icon fa fa-check bigger-110"></i>
				Submit
				</button>
			</div>
		</div>
		</from>
	</div>
</div>

<script src="assets/js/bootstrap-tag.min.js"></script>

<!-- inline scripts related to this page -->
<script type="text/javascript">
        
        $(function () {
            
             $.ajax({
                    url:'teacher/teachers/getQuizTime',
                    type:'post',
                    dataType:'json',
                    data:{id:"<?php echo $quiz_id ?>"},
                    success:function(data){
                            var res = "";
                            $.each(data, function (key, val){
                                    res += val.test_time;
                            });
                            quizTime(res.split(':')[0].match(/\d+/),res.split(':')[1].match(/\d+/));
                    }
            });
            
            
        });
        
        function quizTime(hrs,mns){
            var mins = hrs; // Min test time
            var secs = mns; // Seconds (In addition to min) test time
            var timerDisplay = $('#timerText');

            //Globals: 
            var timeExpired = false;

            // Test time in seconds
            var totalTime = secs + (mins * 60);

            var countDown = function (callback) {
                var interval;
                interval = setInterval(function () {
                    if (secs === 0) {
                        if (mins === 0) {
                            timerDisplay.text('0:00');
                            clearInterval(interval);
                            callback();
                            return;
                        } else {
                            mins--;
                            secs = 60;
                        }
                    }
                    var minute_text;
                    if (mins > 0) {
                        minute_text = mins;
                    } else {
                        minute_text = '0';
                    }
                    var second_text = secs < 10 ? ('0' + secs) : secs;

                    timerDisplay.text(minute_text + ':' + second_text);
                    secs--;
                }, 1000, timeUp);
            };

            // When time elapses: submit form
            var timeUp = function () {
                alert("Time's Up!");
                timeExpired = true;
                document.getElementById('form_quiz').submit();
            };

            // Start the clock
            countDown(timeUp);

            $('#form_quiz').submit(function () {
                if (!timeExpired) {
                    alert("Times up!");
                } else {
                    // Continue submit
                    return true;
                }
            });
        }
    
	jQuery(function($) {
		
		var tag_input = $('.enumeration');
		try{
			tag_input.tag({placeholder:tag_input.attr('placeholder')})
			tag_input.parent().addClass('col-xs-3');
		}
		catch(e) {
			//display a textarea for old IE, because it doesn't support this plugin or another one I tried!
			tag_input.after('<textarea name="'+tag_input.attr('name')+'" rows="3">'+tag_input.val()+'</textarea>').remove();
			//$('#form-field-tags').autosize({append: "\n"});
		}

	});

	$('#quiz_submit').click(function(e){
		e.preventDefault();

		bootbox.confirm("Are you sure?", function(result) {
			if(result) {
				$('#form_quiz').submit();
			}
		});
		
	});

</script>