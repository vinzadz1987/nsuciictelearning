<div class="col-sm-12">
    <div class="row">
            <div class="col-xs-12">
                    <div class="space-2"></div>
                    <table id="dataTable_view" class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>STUDENT ID</th>
                            <th>FIRSTNAME</th>
                            <th>LASTNAME</th>
                            <th>SUBJECT ENROLLED</th>
                            <th>STATUS</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
            </div>
        
            <div id="dialog-message" class="hide">
                    <div id="insideModal"></div>
            </div>
            </div>
            </div>
</div><!-- /.col -->

<script>
    $(document).ready(function(){
        
         $('table#dataTable_view').dataTable({"scrollX": true});
         viewDataTable();
        
         $("#create_subject").click(function(){
             dialogBoxModalAdd('Create Subjects');
         });
         
         
    });
    
    
    function viewDataTable(){
        $.ajax({
            url:'teacher/mystudents/getStudents',
            type:'post',
            dataType:'json',
            success:function(data){
                $.each(data, function (key, val){
                    $('table#dataTable_view').dataTable().fnAddData( [
                        val.student_id,
                        val.firstname,
                        val.lastname,
                        val.subject_code+'<small> '+val.description+'</small>',
                        ''+enrolledStatus(val)+''  
                    ]);
                });
            }
        });
    }
    
    function enrolledStatus(val){
        var status = '';
        if(val.aprstats == 0){
            status = '<div class="action-buttons">\n\
                        <a href="javascript:void(0)" class="green">\n\
                                <span class="label label-lg label-danger arrowed-out" onclick="dialogBoxModal(\''+val.student_id+'_'+val.firstname+'_'+val.lastname+'_'+val.subject_code+'_'+val.studenrolled+'_'+val.subject_id+'_'+val.enrolled_id+'\')">Confirm</span>\n\
                        </a>\n\
                     </div>';
        }else{
            
            status = '<div class="action-buttons">\n\
                        <a href="javascript:void(0)" class="green">\n\
                                <span class="label label-lg label-success arrowed-in-right arrowed-in">Approved</span>\n\
                        </a>\n\
                     </div>';
       }
       return status;
   }
   
    function dialogBoxModal(params){
            $("#insideModal").html('Confirm Student '+params.split('_')[1]+' '+params.split('_')[2]+'?');
            dialogBox(
                    model = {
                            type: 'local',
                            data: 'dialog-message',
                            title: 'Subject Enrolled '+params.split('_')[3],
                            title_icon: 'fa-book',
                            width: 500,
                            buttons: [
                                    {
                                            'html': "<i class='ace-icon fa fa-times'></i> Cancel",
                                            'class': 'btn_cancel btn btn-xs btn-danger',
                                            click: function(){
                                                    $(this).dialog('close');
                                            }
                                    },
                                            {
                                            'html': "<i class='ace-icon fa fa-check'></i> Confirm",
                                            'class': 'btn_cancel btn btn-xs btn-primary',
                                            click: function(){
                                                    var studid = params.split('_')[0];
                                                    var subid = params.split('_')[5];;
                                                    var enrolled_student_id = params.split('_')[4];
                                                    var subject_enrolled_id = params.split('_')[6];
                                                    $.ajax({
                                                       url:'teacher/mystudents/enrolledStudent',
                                                       type:'post',
                                                       data:{studentid:studid,subjectid:subid,enrollstudent: enrolled_student_id,enrollid:subject_enrolled_id},
                                                       success:function(){
                                                           location.reload();
                                                       }
                                                    });
                                            }
                                    }
                            ]
                    }
            );
    }
 
</script>

<script src="assets/js/jPages.js"></script>
<script src="assets/js/jquery.slimscroll.min.js"></script>
<script src="assets/js/bootbox.min.js"></script>
<script src="assets/js/ace-elements.min.js"></script>

<script src="assets/js/jquery.dataTables.min.js"></script>
<script src="assets/js/jquery.dataTables.bootstrap.js"></script>
