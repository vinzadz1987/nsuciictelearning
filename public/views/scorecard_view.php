<div class="row profilerow">
	<div class="col-xs-12">
		<!-- PAGE CONTENT BEGINS -->
		<div>
			<div id="user-profile-1" class="user-profile row">
				<div class="col-xs-12 col-sm-3 center">
					<div>
						<!-- #section:pages/profile.picture -->
						<span class="profile-picture">
							<img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="assets/avatars/profile-pic.jpg" />
						</span>
						<!-- /section:pages/profile.picture -->
						<div class="space-4"></div>
						<div class="width-80 label label-info label-xlg arrowed-in arrowed-in-right">
							<div class="inline position-relative">
								<a href="" class="user-title-label dropdown-toggle" data-toggle="dropdown">
									<i class="ace-icon fa fa-circle light-green"></i>
									&nbsp;
									<span id="mystatus" class="white">Online</span>
								</a>
							</div>
						</div>
					</div>
					<div class="space-6"></div>
				</div>
				<div class="col-xs-12 col-sm-9">
					<div class="space-12"></div>
					<!-- #section:pages/profile.info -->
					<div class="profile-user-info profile-user-info-striped">
						<div class="profile-info-row">
							<div class="profile-info-name"> ID Number </div>
							<div class="profile-info-value">
								<div class="input-group searchg">
									<input type="text" placeholder="Type your id" id="idnumber" class="form-control search-query">
									<span class="input-group-btn">
										<button class="btn btn-purple btn-sm btnsearch" type="button">
											Search
											<i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
										</button>
									</span>
								</div>
							</div>
						</div>
						<div class="profile-info-row">
							<div class="profile-info-name"> Login </div>
							<div class="profile-info-value"><span  id="username"> n/a </span></div>
						</div>
						<div class="profile-info-row">
							<div class="profile-info-name"> Full Name </div>
							<div class="profile-info-value"><span id="lastname"> n/a </span><span id="firstname"> n/a </span> </div>
						</div>
						<div class="profile-info-row">
							<div class="profile-info-name"> Organization / Account </div>
							<div class="profile-info-value"><span id="account"> n/a </span></div>
						</div>
						<div class="profile-info-row">
							<div class="profile-info-name"> Hire Date </div>
							<div class="profile-info-value"><span id="hiredate"> n/a </span></div>
						</div>
						<div class="profile-info-row">
							<div class="profile-info-name"> Production Date </div>
							<div class="profile-info-value"><span id="prod_date"> n/a </span></div>
						</div>
						<div class="profile-info-row">
							<div class="profile-info-name"> Supervisor </div>
							<div class="profile-info-value"><span id="supervisor"> n/a </span></div>
						</div>
						<div class="profile-info-row">
							<div class="profile-info-name"> Operations Manager </div>
							<div class="profile-info-value"><span id="manager"> n/a </span></div>
						</div>
					</div>
					<!-- /section:pages/profile.info -->
				</div>
			</div>
		</div>
		<!-- PAGE CONTENT ENDS -->
	</div><!-- /.col -->
</div><!-- /.row -->
<!--div class="space-20"></div-->
<div class="row">
	<div class="col-xs-12">
		<!-- PAGE CONTENT BEGINS -->
			<div class="col-xs-12">
				<div class="widget-box transparent">
					<div class="widget-header widget-header-small" style="height:40px;">
						<h4 class="widget-title lighter smaller h4title" style="float:left;">
							<i class="ace-icon fa fa-rss orange"></i>Meeting Date 
						</h4>
						<!--span class="editable orange spanmdate" id="meeting_date"><?=date('Y-m-d')?></span-->
						<div style="float:left;margin-left:10px;">
						<div class="input-group searchg">
								<input type="text" placeholder="Type date range" value="<?=date('m/01/Y').' - '.date('m/t/Y')?>" id="score_date" class="form-control date_range-picker">
								<span class="input-group-btn">
									<button class="btn btn-purple btn-sm btnsearchdate" type="button">
										Search
										<i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
									</button>
								</span>
							</div>
						</div>
					</div>
					<div class="widget-body">
						<div class="widget-main">
							
							<div class="col-sm-2 infobox-container">
								<div class="infobox infobox-red">
									<div class="infobox-icon">
										<i class="ace-icon fa fa-indent"></i>
									</div>
									<div class="infobox-data">
										<span class="infobox-data-number">0</span>
										<div class="infobox-content">Absences</div>
									</div>
								</div>
							</div>
							<div class="col-sm-2 infobox-container">
								<div class="infobox infobox-pink">
									<div class="infobox-icon">
										<i class="ace-icon fa fa-indent"></i>
									</div>
									<div class="infobox-data">
										<span class="infobox-data-number">0</span>
										<div class="infobox-content">Tardiness</div>
									</div>
								</div>
							</div>
							<div class="col-sm-2 infobox-container">
								<div class="infobox infobox-orange">
									<div class="infobox-icon">
										<i class="ace-icon fa fa-outdent"></i>
									</div>
									<div class="infobox-data">
										<span class="infobox-data-number">0</span>
										<div class="infobox-content">Vacation Leaves</div>
									</div>
								</div>
							</div>
																		
							<div class="infobox infobox-green infobox-small infobox-dark pull-right infoboxcont_cat">
								<div class="infobox-icon">
									<i class="ace-icon fa fa-thumbs-o-up cat_icon"></i>
								</div>

								<div class="infobox-data">
									<div class="infobox-content cat_desc">PASSED</div>
								</div>
							</div>	
							<div class="infobox infobox-blue infobox-small infobox-dark pull-right">
								<div class="infobox-icon">
									<i class="ace-icon fa fa-bar-chart-o"></i>
								</div>

								<div class="infobox-data">
									<div class="infobox-content">Score</div>
									<div class="infobox-content totalscore">0 %</div>
								</div>
							</div>											
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-12">
				<div class="widget-box transparent">
					<div class="widget-header widget-header-small">
					</div>
					<div class="widget-body">
						<div class="widget-main">
							
							<table class="table table-striped table-bordered table-hover" id="scoreTable">
								<thead>
									<tr>
										<th class="center bg-warning">Perfect Score</th>
										<th class="center">Metric</th>
										<th class="center">Data Acquired</th>
										<th class="center bg-success">Weight</th>
									</tr>
								</thead>
								<tbody>									
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		<!-- PAGE CONTENT ENDS -->
	</div><!-- /.col -->
</div><!-- /.row -->		
<script src="assets/js/jquery-ui.custom.min.js"></script>

<!--[if lte IE 8]>
  <script src="assets/js/excanvas.min.js"></script>
<![endif]-->
<script src="assets/js/jquery-ui.custom.min.js"></script>
<script src="assets/js/jquery.ui.touch-punch.min.js"></script>
<script src="assets/js/jquery.gritter.min.js"></script>
<script src="assets/js/bootbox.min.js"></script>
<script src="assets/js/jquery.easypiechart.min.js"></script>
<script src="assets/js/date-time/bootstrap-datepicker.min.js"></script>
<script src="assets/js/jquery.hotkeys.min.js"></script>
<script src="assets/js/bootstrap-wysiwyg.min.js"></script>
<script src="assets/js/select2.min.js"></script>
<script src="assets/js/fuelux/fuelux.spinner.min.js"></script>
<script src="assets/js/x-editable/bootstrap-editable.min.js"></script>
<script src="assets/js/x-editable/ace-editable.min.js"></script>
<script src="assets/js/jquery.maskedinput.min.js"></script>
<style>
	.profile-info-name{width:165px;}
	.table tbody tr:hover td,.table tbody tr:hover th {
		  background-color: transparent;
	}
	.datadetails{
		background-color: #6fb3e0 !important;
		font-weight:bold;
	}
	.datawarning{
		background-color: #fcf8e3 !important;
	}
	.dataweight{
		background-color: #dff0d8 !important;
		font-weight:bold;
		font-size:15px;
	}
	.search-query{width:150px !important;}
	.searchg{display:block !important;}
	.profilerow{height:315px !important;}
</style>
<!-- inline scripts related to this page -->
<script type="text/javascript">
	showpreviewdata();
	$(".btnsearch").click(function(){
		$.post("scorecards/get_agent_info",{idnumber:$("#idnumber").val()},function(returnval){
			if(returnval!=0){
				$("#username").text(returnval[0].RT_Login);
				$("#lastname").text(returnval[0].Last_Name);
				$("#firstname").text(returnval[0].First_Name);
				$("#account").text(returnval[0].Account);
				$("#hiredate").text(returnval[0].Hire_Date);
				$("#prod_date").text(returnval[0].Production_Date);
				$("#supervisor").text(returnval[0].Supervisor);
				$("#manager").text(returnval[0].Shift_Manager);
				showscorecard($("#idnumber").val(),$("#score_date").val(),returnval[0].Account,returnval[0].RT_Login);
			}else{
				alert('No Agent Found!');
			}
		},'json');
	});
	
	$(".btnsearchdate").click(function(){
		$.post("scorecards/get_agent_info",{idnumber:$("#idnumber").val()},function(returnval){
			if(returnval!=0){
				$("#username").text(returnval[0].RT_Login);
				$("#lastname").text(returnval[0].Last_Name);
				$("#firstname").text(returnval[0].First_Name);
				$("#account").text(returnval[0].Account);
				$("#hiredate").text(returnval[0].Hire_Date);
				$("#prod_date").text(returnval[0].Production_Date);
				$("#supervisor").text(returnval[0].Supervisor);
				$("#manager").text(returnval[0].Shift_Manager);
				showscorecard($("#idnumber").val(),$("#score_date").val(),returnval[0].Account,returnval[0].RT_Login);
			}else{
				alert('No Agent Found!');
			}
		},'json');
	});
	function showpreviewdata(){
		$.post("settings/metric_settings/showmetricpreview",{account:'Default'},function(resultdata){
			var countobj = Object.keys(resultdata).length;
			$("#scoreTable > tbody").html('<tr><td class="center datawarning" colspan="4">This is the <b>Default Scorecard Setting</b>!</td></tr>');
			if(countobj>0){
				$.each(resultdata,function(i,data){
					
					$("#scoreTable > tbody").append('\
					<tr><td class="center datawarning" rowspan="3">'+data.perfect_score+' %</td>\
						<td class="center datadetails" colspan="3">'+data.metric_name+'</td>\
					</tr><tr><td>Actual</td><td id="actualid_sph">0</td><td id="weight_sph" class="center dataweight" rowspan="2">0 %</td>\
					</tr><tr><td>Target</td><td id="targetid_sph">'+data.target+'</td></tr>');					
				});
			}
		},'json');
	}
	function showscorecard(idnumber,scoredate,account,rtlogin){
		$.post("scorecards/showmetric",{account:account},function(metricdata){
			var countobj = Object.keys(metricdata).length;
			$("#scoreTable > tbody").html('');
			if(countobj>0){
				$.post("scorecards/get_scorecard",{idnumber:idnumber,scoredate:scoredate,account:account,rtlogin:rtlogin},function(returnval){
					if(returnval!=0){
						var actual = '';
						var weigth = '';
						$.each(metricdata,function(i,data){	
							switch(data.metric_name){
								case "SPH":
									actual = returnval[0].sph;
									weigth = returnval[0].weight_sph;
								break;
								case "360":
									actual = returnval[0].a360;
									weigth = returnval[0].weight_360;
								break;
								case "QA":
									actual = returnval[0].qa;
									weigth = returnval[0].weight_qa;
								break;
								case "Absences":
									actual = returnval[0].abs;
									weigth = returnval[0].weight_abs;
								break;
								case "Lates":
									actual = 0;
									weigth = returnval[0].weight_lates;
								break;
								case "WFU":
									actual = returnval[0].wfu;
									weigth = returnval[0].weight_wfu;
								break;
							}
							$("#scoreTable > tbody").append('\
							<tr><td class="center datawarning" rowspan="3">'+data.perfect_score+' %</td>\
								<td class="center datadetails" colspan="3">'+data.metric_name+'</td>\
							</tr><tr><td>Actual</td><td >'+actual+'</td>\
							<td  class="center dataweight" rowspan="2">'+weigth+'</td>\
							</tr><tr><td>Target</td><td >'+data.target+'</td></tr>');					
						});
						
						$(".totalscore").text(returnval[0].totalscore+' %');
						if(returnval[0].totalscore < 75){
							$(".infoboxcont_cat").removeClass("infobox-green");
							$(".infoboxcont_cat").addClass("infobox-red");
							$(".cat_icon").removeClass("fa-thumbs-o-up");
							$(".cat_icon").addClass("fa-thumbs-o-down");
							$(".cat_desc").text('FAILED');
						}else{
							$(".infoboxcont_cat").addClass("infobox-green");
							$(".infoboxcont_cat").removeClass("infobox-red");
							$(".cat_icon").addClass("fa-thumbs-o-up");
							$(".cat_icon").removeClass("fa-thumbs-o-down");
							$(".cat_desc").text('PASSED');
						}
					}
				},'json');
				
				
			}
		},'json');
	}
	jQuery(function($) {
		
		//editables on first profile page
		$.fn.editable.defaults.mode = 'inline';
		$.fn.editableform.loading = "<div class='editableform-loading'><i class='ace-icon fa fa-spinner fa-spin fa-2x light-blue'></i></div>";
		$.fn.editableform.buttons = '<button type="submit" class="btn btn-info editable-submit"><i class="ace-icon fa fa-check"></i></button>'+
									'<button type="button" class="btn editable-cancel"><i class="ace-icon fa fa-times"></i></button>';  
		
		// *** editable avatar *** //
		try {//ie8 throws some harmless exceptions, so let's catch'em
	
			//first let's add a fake appendChild method for Image element for browsers that have a problem with this
			//because editable plugin calls appendChild, and it causes errors on IE at unpredicted points
			try {
				document.createElement('IMG').appendChild(document.createElement('B'));
			} catch(e) {
				Image.prototype.appendChild = function(el){}
			}
	
			var last_gritter
			$('#avatar').editable({
				type: 'image',
				name: 'avatar',
				value: null,
				image: {
					//specify ace file input plugin's options here
					btn_choose: 'Change Avatar',
					droppable: true,
					maxSize: 110000,//~100Kb
	
					//and a few extra ones here
					name: 'avatar',//put the field name here as well, will be used inside the custom plugin
					on_error : function(error_type) {//on_error function will be called when the selected file has a problem
						if(last_gritter) $.gritter.remove(last_gritter);
						if(error_type == 1) {//file format error
							last_gritter = $.gritter.add({
								title: 'File is not an image!',
								text: 'Please choose a jpg|gif|png image!',
								class_name: 'gritter-error gritter-center'
							});
						} else if(error_type == 2) {//file size rror
							last_gritter = $.gritter.add({
								title: 'File too big!',
								text: 'Image size should not exceed 100Kb!',
								class_name: 'gritter-error gritter-center'
							});
						}
						else {//other error
						}
					},
					on_success : function() {
						$.gritter.removeAll();
					}
				},
				url: function(params) {
					// ***UPDATE AVATAR HERE*** //
					//for a working upload example you can replace the contents of this function with 
					//examples/profile-avatar-update.js
	
					var deferred = new $.Deferred
	
					var value = $('#avatar').next().find('input[type=hidden]:eq(0)').val();
					if(!value || value.length == 0) {
						deferred.resolve();
						return deferred.promise();
					}
	
	
					//dummy upload
					setTimeout(function(){
						if("FileReader" in window) {
							//for browsers that have a thumbnail of selected image
							var thumb = $('#avatar').next().find('img').data('thumb');
							if(thumb) $('#avatar').get(0).src = thumb;
						}
						
						deferred.resolve({'status':'OK'});
	
						if(last_gritter) $.gritter.remove(last_gritter);
						last_gritter = $.gritter.add({
							title: 'Avatar Updated!',
							text: 'Uploading to server can be easily implemented. A working example is included with the template.',
							class_name: 'gritter-info gritter-center'
						});
						
					 } , parseInt(Math.random() * 800 + 800))
	
					return deferred.promise();
					
					// ***END OF UPDATE AVATAR HERE*** //
				},
				
				success: function(response, newValue) {
				}
			})
		}catch(e) {}
		
		
	
		//another option is using modals
		$('#avatar2').on('click', function(){
			var modal = 
			'<div class="modal fade">\
			  <div class="modal-dialog">\
			   <div class="modal-content">\
				<div class="modal-header">\
					<button type="button" class="close" data-dismiss="modal">&times;</button>\
					<h4 class="blue">Change Avatar</h4>\
				</div>\
				\
				<form class="no-margin">\
				 <div class="modal-body">\
					<div class="space-4"></div>\
					<div style="width:75%;margin-left:12%;"><input type="file" name="file-input" /></div>\
				 </div>\
				\
				 <div class="modal-footer center">\
					<button type="submit" class="btn btn-sm btn-success"><i class="ace-icon fa fa-check"></i> Submit</button>\
					<button type="button" class="btn btn-sm" data-dismiss="modal"><i class="ace-icon fa fa-times"></i> Cancel</button>\
				 </div>\
				</form>\
			  </div>\
			 </div>\
			</div>';
			
			
			var modal = $(modal);
			modal.modal("show").on("hidden", function(){
				modal.remove();
			});
	
			var working = false;
	
			var form = modal.find('form:eq(0)');
			var file = form.find('input[type=file]').eq(0);
			file.ace_file_input({
				style:'well',
				btn_choose:'Click to choose new avatar',
				btn_change:null,
				no_icon:'ace-icon fa fa-picture-o',
				thumbnail:'small',
				before_remove: function() {
					//don't remove/reset files while being uploaded
					return !working;
				},
				allowExt: ['jpg', 'jpeg', 'png', 'gif'],
				allowMime: ['image/jpg', 'image/jpeg', 'image/png', 'image/gif']
			});
	
			form.on('submit', function(){
				if(!file.data('ace_input_files')) return false;
				
				file.ace_file_input('disable');
				form.find('button').attr('disabled', 'disabled');
				form.find('.modal-body').append("<div class='center'><i class='ace-icon fa fa-spinner fa-spin bigger-150 orange'></i></div>");
				
				var deferred = new $.Deferred;
				working = true;
				deferred.done(function() {
					form.find('button').removeAttr('disabled');
					form.find('input[type=file]').ace_file_input('enable');
					form.find('.modal-body > :last-child').remove();
					
					modal.modal("hide");
	
					var thumb = file.next().find('img').data('thumb');
					if(thumb) $('#avatar2').get(0).src = thumb;
	
					working = false;
				});
				
				
				setTimeout(function(){
					deferred.resolve();
				} , parseInt(Math.random() * 800 + 800));
	
				return false;
			});
					
		});
	
		
	});
	function getscorecard(idnumber,scoredate,account,rtlogin){
		$.post("scorecards/get_scorecard",{idnumber:idnumber,scoredate:scoredate,account:account,rtlogin:rtlogin},function(returnval){
			if(returnval!=0){
				$("#actualid_sph").text(returnval[0].sph);
				$("#actualid_360").text(returnval[0].a360);
				$("#actualid_qa").text(returnval[0].qa);
				$("#actualid_wfu").text(returnval[0].wfu);
				$("#actualid_abs").text(returnval[0].abs);
				$("#actualid_late").text('0');
				$("#weight_sph").text(returnval[0].weight_sph);
				$("#weight_360").text(returnval[0].weight_360);
				$("#weight_qa").text(returnval[0].weight_qa);
				$("#weight_wfu").text(returnval[0].weight_wfu);
				$("#weight_abs").text(returnval[0].weight_abs);
				$("#weight_lates").text(returnval[0].weight_lates);
				$(".totalscore").text(returnval[0].totalscore);
				
			}
		},'json');
	}
</script>
       