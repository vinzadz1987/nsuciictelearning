<div id="chartContainer" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

<script src="assets/Highcharts/js/highcharts.js"></script>
<script src="assets/Highcharts/js/modules/exporting.js"></script>

<script type="text/javascript">
	$(function () {
        highChart({chart:'chartContainer',data_case:'chartSample'});
    });  

</script>