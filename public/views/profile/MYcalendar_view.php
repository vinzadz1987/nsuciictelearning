					<div class="row">
						<div class="col-xs-12">
							<!-- PAGE CONTENT BEGINS -->
							<div class="row">
								<div class="col-sm-9">
									<div class="space"></div>

									<!-- #section:plugins/data-time.calendar -->
									<div id="calendar"></div>
									<!-- /section:plugins/data-time.calendar -->
								</div>

								<div class="col-sm-3">
									<div class="widget-box transparent">
										<div class="widget-header">
											<h4>Draggable events</h4>
										</div>

										<div class="widget-body">
											<div class="widget-main no-padding">
												<div id="external-events">
													<div class="external-event label-grey" data-class="label-grey" data-caledar-time='08:00 AM - 05:00 PM'>
														<i class="ace-icon fa fa-arrows"></i>
														<span class=''><b>My Event A</b> : <i>08:00 AM - 05:00 PM</i></span>
													</div>

													<div class="external-event label-success" data-class="label-success">
														<i class="ace-icon fa fa-arrows"></i>
														My Event B
													</div>

													<div class="external-event label-danger" data-class="label-danger">
														<i class="ace-icon fa fa-arrows"></i>
														My Event C
													</div>

													<div class="external-event label-purple" data-class="label-purple">
														<i class="ace-icon fa fa-arrows"></i>
														My Event D
													</div>

													<div class="external-event label-yellow" data-class="label-yellow">
														<i class="ace-icon fa fa-arrows"></i>
														My Event E
													</div>

													<div class="external-event label-pink" data-class="label-pink">
														<i class="ace-icon fa fa-arrows"></i>
														My Event F
													</div>

													<div class="external-event label-info" data-class="label-info">
														<i class="ace-icon fa fa-arrows"></i>
														My Event G
													</div>

													<label>
														<input type="checkbox" class="ace ace-checkbox" id="drop-remove" />
														<span class="lbl"> Remove after drop</span>
													</label>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<!-- PAGE CONTENT ENDS -->
						</div><!-- /.col -->
					</div><!-- /.row -->


		<!-- page specific plugin scripts -->
		<script src="assets/js/jquery-ui.custom.min.js"></script>
		<script src="assets/js/jquery.ui.touch-punch.min.js"></script>
		<script src="assets/js/fullcalendar.min.js"></script>
		<script src="assets/js/bootbox.min.js"></script>

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
		$(document).ready(function(){

	/* initialize the external events
	-----------------------------------------------------------------*/
	
	$('#external-events div.external-event').each(function() {

		// create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
		// it doesn't need to have a start or end
		var eventObject = {
			title: $.trim($(this).text()) // use the element's text as the event title
		};

		// store the Event Object in the DOM element so we can get to it later
		$(this).data('eventObject', eventObject);

		// make the event draggable using jQuery UI
		$(this).draggable({
			zIndex: 999,
			revert: true,      // will cause the event to go back to its
			revertDuration: 0  //  original position after the drag
		});
		
	});




	/* initialize the calendar
	-----------------------------------------------------------------*/

	var date = new Date();
	var d = date.getDate();
	var m = date.getMonth();
	var y = date.getFullYear();

	
	var calendar = $('#calendar').fullCalendar({
		//isRTL: true,
		 buttonText: {
			prev: '<i class="ace-icon fa fa-chevron-left"></i>',
			next: '<i class="ace-icon fa fa-chevron-right"></i>'
		},
	/* DEFAULT EVENTS
		header: {
			left: 'prev,next today',
			center: 'title',
			right: 'month,agendaWeek,agendaDay'
		},
		events: [
		  {
			title: 'All Day Event',
			start: new Date(y, m, 1),
			className: 'label-important'
		  },
		  {
			title: 'Long Event',
			start: new Date(y, m, d-5),
			end: new Date(y, m, d-2),
			className: 'label-success'
		  },
		  {
			title: 'Some Event',
			start: new Date(y, m, d-3, 16, 0),
			allDay: false
		  }
		]
		,*/
		editable: true,
		droppable: true, // this allows things to be dropped onto the calendar !!!
		drop: function(date, allDay) { // this function is called when something is dropped
		
			// retrieve the dropped element's stored Event Object
			var originalEventObject = $(this).data('eventObject');
			var $extraEventClass = $(this).attr('data-class');
			
			// we need to copy it, so that multiple events don't have a reference to the same object
			var copiedEventObject = $.extend({}, originalEventObject);
			
			// assign it the date that was reported
			copiedEventObject.start = date;
			copiedEventObject.allDay = allDay;
			if($extraEventClass) copiedEventObject['className'] = [$extraEventClass];
			
			// render the event on the calendar
			// the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
			$('#calendar').fullCalendar('renderEvent', copiedEventObject, true);
			
			// is the "remove after drop" checkbox checked?
			if ($('#drop-remove').is(':checked')) {
				// if so, remove the element from the "Draggable Events" list
				$(this).remove();
			}
			
			//------------- MIRACLE HAPPENS HERE :) --------------
			
			// save event on drop;			
			//saveEvent(date, '', event_startTime, event_endTime, originalEventObject.title);
		}
		,
		selectable: true,
		selectHelper: true,
		select: function(start, end, allDay) {
			
			bootbox.prompt("New Event Title:", function(title) { 
				if (title !== null) {
					calendar.fullCalendar('renderEvent',
						{
							title: title,
							start: start,
							end: end,
							allDay: allDay
						},
						true // make the event "stick"
					);
				}
			});		
			calendar.fullCalendar('unselect');
		},
		eventClick: function(calEvent, jsEvent, view) {
			
			$.each(calEvent, function(key, val){
			alert(key+' : '+val);
			});
			calEvent.allDay = false; 
			calendar.fullCalendar('updateEvent', calEvent);
			
			eventTitle = calEvent.title;
			eventStartDate = calEvent.start != null ? $.fullCalendar.formatDate(calEvent.start,'MMM dd, yyyy') : "";
			eventEndDate = calEvent.end != null ? $.fullCalendar.formatDate(calEvent.end,'MMM dd, yyyy') : "";
			//display a modal
			var modal = 
			'<div class="modal fade">\
			  <div class="modal-dialog">\
			   <div class="modal-content">\
				 <div class="modal-body">\
					 <h4 class="header green lighter bigger bold">\
						<i class="ace-icon fa fa-calendar orange"></i>\
						Create New Event\
					</h4>\
					<div class="col-xs-13">\
						<button type="button" class="close" data-dismiss="modal" style="margin-top:-10px;">&times;</button><br>\
						<form class="form-horizontal" role="form">\
							<div class="form-group">\
								<label for="form-field-1" class="col-sm-3 control-label no-padding-right">Name</label>\
								<div class="col-sm-8">\
									<input type="text" class="form-control" placeholder="" id="create_event_name" name="create_event_name" value="'+eventTitle+'">\
								</div>\
							</div>\
							<div class="form-group">\
								<label for="form-field-1" class="col-sm-3 control-label no-padding-right">Details</label>\
								<div class="col-sm-8">\
									<input type="text" class="form-control" id="create_event_details" name="create_event_details">\
								</div>\
							</div>\
							<div class="form-group">\
								<label for="form-field-1" class="col-sm-3 control-label no-padding-right">Where</label>\
								<div class="col-sm-8">\
									<input type="text" class="form-control" id="event_where" name="event_where">\
								</div>\
							</div>\
							<div class="form-group">\
								<label for="form-field-1" class="col-sm-3 control-label no-padding-right">When</label>\
								<div class="input-daterange input-group col-sm-8" style="margin-left: 12px;float:left;">\
									<input type="text" id="event_start_date" name="event_start_date" class="form-control" placeholder="Start date" value="'+eventStartDate+'">\
									<span class="input-group-addon">\
										<i class="fa fa-long-arrow-right"></i>\
									</span>\
									<input type="text" id="event_end_date" name="event_end_date" class="form-control" value="'+eventEndDate+'" placeholder="Empty if one-day event">\
								</div>\
							</div>\
							<div class="form-group">\
								<label for="form-field-1" class="col-sm-3 control-label no-padding-right">Time</label>\
								<div class="input-daterange input-group col-sm-8" style="margin-left: 12px;float:left;">\
									<input type="text" id="event_start_time" name="event_start_time" class="form-control" placeholder="Start">\
									<span class="input-group-addon">\
										<i class="fa fa-long-arrow-right"></i>\
									</span>\
									<input type="text" id="event_end_time" name="event_end_time" class="form-control" placeholder="End">\
								</div>\
							</div>\
							<div class="form-group">\
								<label for="form-field-1" class="col-sm-3 control-label no-padding-right">Event Type</label>\
								<div class="col-sm-8">\
								<select data-placeholder="Choose a Type..." id="event_type" name="event_type" class="">\
									<option value="personal">Personal</option>\
									<option value="company">Company</option>\
								</select>\
								</div>\
							</div>\
						</form>\
					</div>\
				 </div>\
				 <div class="modal-footer">\
					<button type="submit" class="btn btn-sm btn-success" data-action="save"><i class="ace-icon fa fa-check"></i> Save</button>\
					<button type="button" class="btn btn-sm btn-danger" data-action="delete"><i class="ace-icon fa fa-trash-o"></i> Delete Event</button>\
					<button type="button" class="btn btn-sm" data-dismiss="modal"><i class="ace-icon fa fa-times"></i> Cancel</button>\
				 </div>\
			  </div>\
			 </div>\
			</div>';
			
			var modal = $(modal).appendTo('body');
			// modal.find('form').on('submit', function(ev){
				// ev.preventDefault();

				// calEvent.title = $(this).find("input[type=text]").val();
				// calendar.fullCalendar('updateEvent', calEvent);
				// modal.modal("hide");
			// });
			
			modal.find('button[data-action=delete]').on('click', function() {
				calendar.fullCalendar('removeEvents' , function(ev){
					return (ev._id == calEvent._id);
				})
				modal.modal("hide");
			});
			modal.find('button[data-action=save]').on('click', function() {				
				calEvent.title = $("input#create_event_name").val();
				calendar.fullCalendar('updateEvent', calEvent);
				modal.modal("hide");
			});
			modal.modal('show').on('hidden', function(){
				modal.remove();
			});


			console.log(calEvent.id);
			console.log(jsEvent);
			console.log(view);

			// change the border color just for fun
			//$(this).css('border-color', 'red');

		}
		
	});
})
/*====================	FUNCTIONS =======================*/
function saveEvent(event_name,event_details,event_where,event_startDate, event_endDate, event_startTime, event_endTime, event_type){
	$.ajax({
		url: 'profiles/save_event',
		type: 'POST',
		data:{ '_event_name': event_name,'_event_details': event_details,'_event_where': event_where,'_event_startDate': event_startDate, '_event_endDate': event_endDate, '_event_startTime': event_startTime, '_event_endTime': event_endTime, '_event_type':event_type },
		statusCode: {
			500: function(){
				alert('Something went wrong!');
			},
			404: function(){
				alert('Something went wrong!');
			}
		}			
	})
	.done(function(result,textStatus, jqHXR){ 
		if(jqHXR.status == 200){ 				
			if(result != ''){
				calendar.fullCalendar('renderEvent',
					{
						title: 'title',
						start: 'start',
						end: 'end',
						allDay: true
					},
					true // make the event "stick"
				);
				calendar.fullCalendar('unselect');				
				modal.modal("hide");
			
				
			
			}else if(result == ''){
				alert('Something went wrong');
			}else
				alert(result);					
		}				
	});
}

</script>