<div class="col-xs-12">

                    <div class="">
                            <div id="user-profile-1" class="user-profile row">
                                    <div class="col-xs-12 col-sm-3 center">
                                            <div style="padding-top: 35px">
                                                    <span class="profile-picture">
                                                            <img id="avatar" class="editable img-responsive editable-click editable-empty" alt="Alex's Avatar" src="assets/img/default-user2.png" style="height: 200px"></img>
                                                    </span>

                                                    <div class="space-4"></div>

                                                    <div class="width-80 label label-info label-xlg arrowed-in arrowed-in-right">
                                                            <div class="inline position-relative">
                                                                    <a href="#" class="user-title-label dropdown-toggle" data-toggle="dropdown">
                                                                            <i class="ace-icon fa fa-circle light-green"></i>
                                                                            &nbsp;
                                                                            <span class="white" id="statName">Name</span>
                                                                    </a>
                                                            </div>
                                                    </div>
                                            </div>

                                            <div class="space-6"></div>

                                            <div class="profile-contact-info">
                                            </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-9">

                                            <div class="space-12"></div>

                                            <h4 class="widget-title blue smaller pull-left">
                                                    <i class="ace-icon fa fa-user orange"></i>
                                                    Personal Information
                                            </h4>

                                            <div class="widget-toolbar action-buttons">
                                                    <a href="#" data-action="reload">
                                                            <i class="ace-icon fa fa-refresh blue"></i>
                                                    </a>
                                                     &nbsp;
                                                     <a href="javascript:void(0)" class="pink" onclick="openModal()">
                                                            <i class="ace-icon fa fa-edit"></i>
                                                    </a>
                                            </div>

                                            <div class="profile-user-info profile-user-info-striped">
                                                    <div class="profile-info-row">
                                                            <div class="profile-info-name"> Username </div>

                                                            <div class="profile-info-value">
                                                                    <span class="editable editable-click" id="username">User ID</span>
                                                            </div>
                                                    </div>

                                                    <div class="profile-info-row">
                                                            <div class="profile-info-name"> Full Name </div>

                                                            <div class="profile-info-value">
                                                                    <span class="editable editable-click" id="fullname">Full Name</span>
                                                                    <input type="hidden" name="username" />
                                                            </div>
                                                    </div>

                                                    <div class="profile-info-row">
                                                            <div class="profile-info-name"> Mobile </div>

                                                            <div class="profile-info-value">
                                                                    <span class="editable editable-click" id="mobile">Mobile</span>
                                                            </div>
                                                    </div>

                                                    <div class="profile-info-row">
                                                            <div class="profile-info-name"> Joined </div>

                                                            <div class="profile-info-value">
                                                                    <span class="editable editable-click" id="signup">2010/06/20</span>
                                                            </div>
                                                    </div>


                                                    <div class="profile-info-row" id="designation_profile">
                                                            <div class="profile-info-name"> Designation </div>

                                                            <div class="profile-info-value">
                                                                    <span class="editable editable-click" id="about"> Designation </span>
                                                            </div>
                                                    </div>

                                                    <div class="profile-info-row" id="course_profile">
                                                            <div class="profile-info-name"> Course </div>

                                                            <div class="profile-info-value">
                                                                    <span class="editable editable-click" id="course"> Course </span>
                                                            </div>
                                                    </div>

                                                    <div class="profile-info-row" id="year_profile">
                                                            <div class="profile-info-name"> Year </div>

                                                            <div class="profile-info-value">
                                                                    <span class="editable editable-click" id="year"> Year </span>
                                                            </div>
                                                    </div>

                                                    <div class="profile-info-row" id="semester_profile">
                                                            <div class="profile-info-name"> Semester </div>

                                                            <div class="profile-info-value">
                                                                    <span class="editable editable-click" id="semester"> Semester </span>
                                                            </div>
                                                    </div>
                                            </div>

                                            <div class="space-20"></div>
<!--
                                            <div class="widget-box transparent">
                                                    <div class="widget-header widget-header-small">
                                                            <h4 class="widget-title blue smaller">
                                                                    <i class="ace-icon fa fa-rss orange"></i>
                                                                    Recent Activities
                                                            </h4>

                                                            <div class="widget-toolbar action-buttons">
                                                                    <a href="#" data-action="reload">
                                                                            <i class="ace-icon fa fa-refresh blue"></i>
                                                                    </a>
&nbsp;
                                                                    <a href="#" class="pink">
                                                                            <i class="ace-icon fa fa-trash-o"></i>
                                                                    </a>
                                                            </div>
                                                    </div>

                                                    <div class="widget-body">
                                                            <div class="widget-main padding-8">
                                                                    <div id="profile-feed-1" class="profile-feed ace-scroll" style="position: relative;"><div class="scroll-track scroll-active" style="display: block; height: 200px;"><div class="scroll-bar" style="height: 62px; top: 0px;"></div></div><div class="scroll-content" style="max-height: 200px;">
                                                                            

                                                                            <div class="profile-activity clearfix">
                                                                                    <div>
                                                                                            <img class="pull-left" alt="David Palms's avatar" src="assets/images/avatars/avatar4.png">
                                                                                            <a class="user" href="#"> David Palms </a>

                                                                                            left a comment on Alex's wall.
                                                                                            <div class="time">
                                                                                                    <i class="ace-icon fa fa-clock-o bigger-110"></i>
                                                                                                    8 hours ago
                                                                                            </div>
                                                                                    </div>

                                                                                    <div class="tools action-buttons">
                                                                                            <a href="#" class="blue">
                                                                                                    <i class="ace-icon fa fa-pencil bigger-125"></i>
                                                                                            </a>

                                                                                            <a href="#" class="red">
                                                                                                    <i class="ace-icon fa fa-times bigger-125"></i>
                                                                                            </a>
                                                                                    </div>
                                                                            </div>

                                                                            <div class="profile-activity clearfix">
                                                                                    <div>
                                                                                            <i class="pull-left thumbicon fa fa-pencil-square-o btn-pink no-hover"></i>
                                                                                            <a class="user" href="#"> Alex Doe </a>
                                                                                            published a new blog post.
                                                                                            <a href="#">Read now</a>

                                                                                            <div class="time">
                                                                                                    <i class="ace-icon fa fa-clock-o bigger-110"></i>
                                                                                                    11 hours ago
                                                                                            </div>
                                                                                    </div>

                                                                                    <div class="tools action-buttons">
                                                                                            <a href="#" class="blue">
                                                                                                    <i class="ace-icon fa fa-pencil bigger-125"></i>
                                                                                            </a>

                                                                                            <a href="#" class="red">
                                                                                                    <i class="ace-icon fa fa-times bigger-125"></i>
                                                                                            </a>
                                                                                    </div>
                                                                            </div>

                                                                            

                                                                            <div class="profile-activity clearfix">
                                                                                    <div>
                                                                                            <i class="pull-left thumbicon fa fa-key btn-info no-hover"></i>
                                                                                            <a class="user" href="#"> Alex Doe </a>

                                                                                            logged in.
                                                                                            <div class="time">
                                                                                                    <i class="ace-icon fa fa-clock-o bigger-110"></i>
                                                                                                    16 hours ago
                                                                                            </div>
                                                                                    </div>

                                                                                    <div class="tools action-buttons">
                                                                                            <a href="#" class="blue">
                                                                                                    <i class="ace-icon fa fa-pencil bigger-125"></i>
                                                                                            </a>

                                                                                            <a href="#" class="red">
                                                                                                    <i class="ace-icon fa fa-times bigger-125"></i>
                                                                                            </a>
                                                                                    </div>
                                                                            </div>
                                                                    </div></div>
                                                            </div>
                                                    </div>
                                            </div>-->

<!--                                            <div class="hr hr2 hr-double"></div>

                                            <div class="space-6"></div>

                                            <div class="center">
                                                    <button type="button" class="btn btn-sm btn-primary btn-white btn-round">
                                                            <i class="ace-icon fa fa-rss bigger-150 middle orange2"></i>
                                                            <span class="bigger-110">View more activities</span>

                                                            <i class="icon-on-right ace-icon fa fa-arrow-right"></i>
                                                    </button>
                                            </div>-->
                                    </div>
                            </div>
                    </div>
                    
                    <div id="dialog-message" class="hide">
                        <div class="alert alert-warning no-margin alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert">
                                            <i class="ace-icon fa fa-times"></i>
                                    </button>

                                    <i class="ace-icon fa fa-mercury bigger-120 blue"></i>
                                   Some fields are not editable and need to consult to Admin.
                        </div>
                        <br>
                        <div class="profile-user-info profile-user-info-striped">
                            
                            <div class="profile-info-row">
                                    <div class="profile-info-name"> Username </div>

                                    <div class="profile-info-value">
                                            <span class="editable editable-click" id="edit_username">User ID</span>
                                    </div>
                            </div>

                            <div class="profile-info-row">
                                    <div class="profile-info-name"> First Name </div>

                                    <div class="profile-info-value">
                                            <!--<span class="editable editable-click" id="edit_fullname">Full Name</span>-->
                                            <input type="text" id="edit_firstname" name="edit_firstname">
                                            <!--<input type="hidden" name="edit_firstname" />-->
                                    </div>
                            </div>
                            
                            <div class="profile-info-row">
                                    <div class="profile-info-name"> Last Name </div>

                                    <div class="profile-info-value">
                                            <!--<span class="editable editable-click" id="edit_fullname">Full Name</span>-->
                                            <input type="text" id="edit_lastname" name="edit_lastname">
                                            <input type="hidden" name="username" />
                                    </div>
                            </div>

                            <div class="profile-info-row">
                                    <div class="profile-info-name"> Mobile </div>

                                    <div class="profile-info-value">
                                            <!--<span class="editable editable-click" id="edit_mobile">Mobile</span>-->
                                            <input type="text" id="edit_mobile">
                                    </div>
                            </div>

                            <div class="profile-info-row">
                                    <div class="profile-info-name"> Joined </div>

                                    <div class="profile-info-value">
                                            <span class="editable editable-click" id="edit_signup">2010/06/20</span>
                                    </div>
                            </div>


                            <div class="profile-info-row" id="designation_profile">
                                    <div class="profile-info-name"> Designation </div>

                                    <div class="profile-info-value">
                                            <span class="editable editable-click" id="edit_about"> Designation </span>
                                    </div>
                            </div>

                            <div class="profile-info-row" id="edit_course_profile">
                                    <div class="profile-info-name"> Course </div>

                                    <div class="profile-info-value">
                                            <span class="editable editable-click" id="edit_course"> Course </span>
                                    </div>
                            </div>

                            <div class="profile-info-row" id="edit_year_profile">
                                    <div class="profile-info-name"> Year </div>

                                    <div class="profile-info-value">
                                            <span class="editable editable-click" id="edit_year"> Year </span>
                                    </div>
                            </div>

                            <div class="profile-info-row" id="edit_semester_profile">
                                    <div class="profile-info-name"> Semester </div>

                                    <div class="profile-info-value">
                                            <span class="editable editable-click" id="edit_semester"> Semester </span>
                                    </div>
                            </div>
                    </div>
                    </div>
                    
                    <!-- PAGE CONTENT ENDS -->
            </div>					



</div><!-- /.row -->
		<script src="assets/js/jquery-ui.custom.min.js"></script>

		<!--[if lte IE 8]>
		  <script src="assets/js/excanvas.min.js"></script>
		<![endif]-->
		<script src="assets/js/jquery-ui.custom.min.js"></script>
		<script src="assets/js/jquery.ui.touch-punch.min.js"></script>
		<script src="assets/js/jquery.gritter.min.js"></script>
		<script src="assets/js/bootbox.min.js"></script>
		<script src="assets/js/jquery.easypiechart.min.js"></script>
		<script src="assets/js/date-time/bootstrap-datepicker.min.js"></script>
		<script src="assets/js/jquery.hotkeys.min.js"></script>
		<script src="assets/js/bootstrap-wysiwyg.min.js"></script>
		<script src="assets/js/select2.min.js"></script>
		<script src="assets/js/fuelux/fuelux.spinner.min.js"></script>
		<script src="assets/js/x-editable/bootstrap-editable.min.js"></script>
		<script src="assets/js/x-editable/ace-editable.min.js"></script>
		<script src="assets/js/jquery.maskedinput.min.js"></script>

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {
				profile();
				// *** editable avatar *** //
				try {//ie8 throws some harmless exceptions, so let's catch'em
			
					//first let's add a fake appendChild method for Image element for browsers that have a problem with this
					//because editable plugin calls appendChild, and it causes errors on IE at unpredicted points
					try {
						document.createElement('IMG').appendChild(document.createElement('B'));
					} catch(e) {
						Image.prototype.appendChild = function(el){}
					}
			
					var last_gritter
					$('#avatar').editable({
						type: 'image',
						name: 'avatar',
						value: null,
						image: {
							//specify ace file input plugin's options here
							btn_choose: 'Change Avatar',
							droppable: true,
							maxSize: 110000,//~100Kb
			
							//and a few extra ones here
							name: 'avatar',//put the field name here as well, will be used inside the custom plugin
							on_error : function(error_type) {//on_error function will be called when the selected file has a problem
								if(last_gritter) $.gritter.remove(last_gritter);
								if(error_type == 1) {//file format error
									last_gritter = $.gritter.add({
										title: 'File is not an image!',
										text: 'Please choose a jpg|gif|png image!',
										class_name: 'gritter-error gritter-center'
									});
								} else if(error_type == 2) {//file size rror
									last_gritter = $.gritter.add({
										title: 'File too big!',
										text: 'Image size should not exceed 100Kb!',
										class_name: 'gritter-error gritter-center'
									});
								}
								else {//other error
								}
							},
							on_success : function() {
								$.gritter.removeAll();
							}
						},
					    url: function(params) {
							// ***UPDATE AVATAR HERE*** //
							//for a working upload example you can replace the contents of this function with 
							//examples/profile-avatar-update.js
			
							var deferred = new $.Deferred
			
							var value = $('#avatar').next().find('input[type=hidden]:eq(0)').val();
							if(!value || value.length == 0) {
								deferred.resolve();
								return deferred.promise();
							}
			
			
							//dummy upload
							setTimeout(function(){
								if("FileReader" in window) {
									//for browsers that have a thumbnail of selected image
									var thumb = $('#avatar').next().find('img').data('thumb');
									if(thumb) $('#avatar').get(0).src = thumb;
//                                                                        alert(thumb);
                                                                        saveProfileImage(thumb);
                                                                        $("#nav-user-photo").get(0).src = thumb;
								}
								
								deferred.resolve({'status':'OK'});
			
								if(last_gritter) $.gritter.remove(last_gritter);
								last_gritter = $.gritter.add({
									title: 'Profile Updated!',
									text: 'Successfully Updated Profile Picture.',
									class_name: 'gritter-info gritter-center'
								});
								
							 } , parseInt(Math.random() * 800 + 800))
			
							return deferred.promise();
							
							// ***END OF UPDATE AVATAR HERE*** //
						},
						
						success: function(response, newValue) {
						}
					})
				}catch(e) {}
				
				
			
			
				///////////////////////////////////////////
				$('#user-profile-3')
				.find('input[type=file]').ace_file_input({
					style:'well',
					btn_choose:'Change avatar',
					btn_change:null,
					no_icon:'ace-icon fa fa-picture-o',
					thumbnail:'large',
					droppable:true,
					
					allowExt: ['jpg', 'jpeg', 'png', 'gif'],
					allowMime: ['image/jpg', 'image/jpeg', 'image/png', 'image/gif']
				})
				.end().find('button[type=reset]').on(ace.click_event, function(){
					$('#user-profile-3 input[type=file]').ace_file_input('reset_input');
				})
				.end().find('.date-picker').datepicker().next().on(ace.click_event, function(){
					$(this).prev().focus();
				})
				$('.input-mask-phone').mask('(999) 999-9999');
			
			
			
				////////////////////
				//change profile
				$('[data-toggle="buttons"] .btn').on('click', function(e){
					var target = $(this).find('input[type=radio]');
					var which = parseInt(target.val());
					$('.user-profile').parent().addClass('hide');
					$('#user-profile-'+which).parent().removeClass('hide');
				});
			});
                        
                         function openModal(){
                            dialogBox(
                                        model = {
                                                type: 'local',
                                                data: 'dialog-message',
                                                title: 'Update Information',
                                                title_icon: 'fa-user',
                                                width: 600,
                                                buttons: [
                                                        {
                                                                'html': "<i class='ace-icon fa fa-times white'></i> Cancel",
                                                                'class': 'btn_cancel btn btn-xs btn-danger',
                                                                click: function(){
                                                                        $(this).dialog('close');
                                                                }
                                                        },
                                                        {
                                                                'html': "<i class='ace-icon fa fa-check'></i> Submit",
                                                                'class': 'btn-primary btn btn-xs',
                                                                click: function(){
                                                                    lname = $("input[name=edit_lastname]").val();
                                                                    fname = $("input[name=edit_firstname]").val();
                                                                    mobile = $("#edit_mobile").val();
                                                                    if(lname == "" || fnma == ""){
                                                                        alert('REQUIRED FIELD/S IS EMPTY.');
                                                                    }else{
                                                                        var profid = $("#username").val(); 
                                                                        $.ajax({
                                                                           url:'profiles/updateProfile',
                                                                           type:'post',
                                                                           data:{profileid:profid,lname:name,fname:fname},
                                                                           success:function(){
                                                                                 profile();
                                                                           }
                                                                        });
                                                                    }
                                                                }
                                                        }
                                                ]
                                        }
                                );
                        }
                        
                        function saveProfileImage(thumbs){
                            $.ajax({
                               url:'profiles/saveProfileImage',
                               type:'Post',
                               data:{imgsrc:thumbs},
                               success:function(){
                                   profile();
                               }
                            });
                        }
                        
                        function upperCase(str){
//                            var str = "hello world";
                            str = str.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                                return letter.toUpperCase();
                            });
                            return str; //Displays "Hello World"
                        }
                        
                        function profile(){
                            usersession = '<?php echo $this->session->userdata('access_name');?>';
                            $.ajax({
                                url:'profiles/getProfileInfo',
                                type:'post',
                                dataType:'json',
                                data:{access_name:usersession},
                                success:function(data){
                                    var id = ""; var fname = ""; var gender = "";
                                    var mobile = ""; var date_added = ""; var designation = "";
                                    var photo = ""; var course=""; var year=""; var semes="";
                                    var frstname = ""; var lstname = "";
                                    $.each(data, function (key, val){
                                        if(usersession == 'Students'){
                                            id+=val.student_id;  
                                            fname+=val.firstname+' '+val.middlename+' '+val.lastname;
                                            mobile+=val.contact_number;
                                            frstname += val.firstname;
                                            lstname += val.lastname;
                                        }else{
                                            id+=val.instructor_id;
                                            fname+=val.firstname+' '+val.middlename+' '+val.lastname;
                                            mobile+=val.instructor_mobile_number;
                                            frstname += val.firstname;
                                            lstname += val.lastname;
                                        }
                                        gender+=val.gender;
                                        date_added+=val.date_added;
                                        designation+=val.instructor_designation;
                                        photo+=val.photo;
                                        course+= val.course_name;
                                        year+=val.curr_year;
                                        semes+=val.semester;
                                    });
                                    $("#username").html(id);
                                    $("input[name=username]").attr('value',fname);
                                    $("#fullname").html(fname);
                                    $("#statName").html('<small>'+fname+'</small>');
                                    $("#semester").html(semes);
                                    $("#year").html(year);
                                    $("#gender").html(gender);
                                    $("#mobile").html(mobile);
                                    $("#signup").html(date_added);
                                    $("#course").html(course);
                                    
                                    //edit
                                    $("#edit_username").html(id);
                                    $("input[name=edit_lastname]").attr('value',lstname);
                                    $("input[name=edit_firstname]").attr('value',frstname);
                                    $("#edit_mobile").val(mobile);
                                    $("#edit_signup").html(date_added);
                                    
                                    if(usersession == "Students"){
                                        $("#designation_profile").addClass('hidden');
                                    }else{
                                        $("#course_profile").addClass('hidden');
                                        $("#semester_profile").addClass('hidden');
                                        $("#year_profile").addClass('hidden');
                                        
                                        $("#edit_course_profile").addClass('hidden');
                                        $("#edit_semester_profile").addClass('hidden');
                                        $("#edit_year_profile").addClass('hidden');
                                    }
                                    $("#about").html(designation);
                                    $("#edit_about").html(designation);
                                    $("#avatar").attr('src',photo);
                                }
                            });
                        }
                        
		</script>
<!--                <script src="http://172.28.2.120:8000/socket.io/socket.io.js" type="text/javascript"></script>
		<script>
			try
			{

			    var socket = io.connect('http://172.28.2.120:8000/notify');

				    socket.on("connect", function() {
				        console.log("connected");
				        // Let the server know who is connected...
				        //$('#serverstat').find('span').css('color','green').html('Online');
				        $('#mystatus').html('Online');
				        socket.emit("user", { Id: 101 });
				    });
				    socket.on('update', function (data) {
				    	// Do something cool like update badges/status/etc...
				    	$("#mystatus").html('Updated');
				        console.log('updated');
				    });
				    socket.on('disconnect', function (data) {
				    	// Do something cool like update badges/status/etc...
				    	$("#mystatus").html('Offline');
				        console.log('disconnected');
				    });
				} catch (e) { console.log(e); 
			}
		</script>-->