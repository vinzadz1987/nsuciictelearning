<div class="row">
	<div class="space-8"></div>
	<form class="form-horizontal" id="top_form" role="form">
	    <div class="col-xs-12">
	    	<div class="form-group">
	    		<input id="field_name" name="field_name" type="hidden" value="Sales">
	    		<input id="data_case" name="data_case" type="hidden" value="<?php echo $data['data_case']; ?>">
		    	<div class="col-xs-4 no-padding-right">
		    		<select class="form-control" name="top">
		    			<option value="DESC"> Highest </option>
		    			<option value="ASC"> Lowest </option>
		    		</select>
		    	</div>
		    	<div class="col-xs-2 no-padding-right">
		    		<input id="limit" name="limit" class="col-xs-12" type="text" value="10">
		    	</div>
		    	<div class="col-sm-6">
		    		<select id="field_list" class="form-control" name="field">
		    		</select>
		    	</div>
		    </div>
	    </div>
	    <div class="col-xs-12">
			<div class="form-group">
				<label class="col-sm-4 control-label no-padding-right" for="date"> Date </label>
				<div class="col-sm-8">
					<input type="text" id="date" name="date" class="date_range-picker col-xs-12" />
				</div>
			</div>
	    </div>
	    <div class="col-xs-12 error_field">
	    	
	    </div>
	</form>
</div>

<script>
	
	$(document).ready(function(){
		field_dropDown();
	});

	$('.btn_submit').on('click', function(e){
		if( $('#limit').val() && $('#date').val() ){
			get_top($('#top_form').serialize());
		}else{
			$('.error_field').html(''+
				'<div class="alert alert-danger">'+
	    		'<strong>'+
	    			'<i class="ace-icon fa fa-exclamation-triangle red"></i>'+
	    			' Oh snap! '+
	    		'</strong>'+
	    		' Fill blank fields.'+
	    	'</div>');
		}

	});

	function get_top(data){
		closeDialog();
		showDialog(
			model = {
				location: 'generic/modal_ctrl/modal',
				data_case: 'modals/top_list_modal',
				data: data,
				title: 'Top List',
				title_icon: 'fa-users ',
				width: 1000,
				height: 600,
			}

		)
	}

	function dialogClose(event,ui,thisData,close){
		$(thisData).remove();
	}

	$('.date_range-picker').daterangepicker({
		'applyClass' : 'btn-sm btn-success',
		'cancelClass' : 'btn-sm btn-default',
		locale: {
			applyLabel: 'Apply',
			cancelLabel: 'Cancel',
		},
		format: 'YYYY-MM-DD'
	})

	function field_dropDown(){
		var grid_headers = [];
		var grid_fields = [];
		// _grid_model_loaded FROM view loadComplete of jqGrid function
		$.each( _grid_model_loaded.colModel, function( key, value ) {
			switch(value.name){
				case 'actions': case '`ID_#`': case 'ID_#': case 'Last_Name': case 'RT_Login': case 'First_Name':
				case 'Supervisor': case '`Tenure_in_(YY/MM/DD)`': case 'Shift': case '`Tenure_(Months)`':
				case 'RT_#': case 'Tenure': case 'RT Login': case 'ID #': case 'Last Name': case 'First Name':
					break;
				default:
					grid_fields.push(value.name);
					grid_headers.push(_grid_model_loaded.colNames[key]);
					break;
			}
		});
		dropDown_local('field_list',grid_headers,grid_fields);
		dropdown_function();
	}

	function dropdown_function(){
		$('#field_list').change(function(e){
			$('#field_name').val($('#field_list option:selected').text());
		})
	}

</script>