<div class="row">
	<div class="col-xs-12">
		<div class="col-sm-6">
			<div class="btn-group">
				<button data-toggle="dropdown" class="btn btn-sm btn-success dropdown-toggle">
					Fileds
					<i class="ace-icon fa fa-angle-down icon-on-right"></i>
				</button>

				<ul id='field_list' class=" dropdown-menu dropdown-success"></ul>
			</div><!-- /.btn-group -->
		</div>
	</div>
	
	<div class="col-xs-12">
		<div class="space-16"></div>
		<form id="formData" class="form-horizontal" role="form">
			<div id="form_field"></div>
			
			<?php if($data['date'] != 'hidden'){ ?>
			<div class="form-group">
				<label class="col-sm-4 control-label no-padding-right" for="date_range"> Date </label>

				<div class="col-sm-6">
					<input class="form-control date_range-picker input_field" type="text" name="date" />
				</div>
			</div>
			<?php } ?>
		</form>
	</div>
</div>
<script src="assets/js/bootstrap-tag.min.js"></script>
<script src="assets/js/chosen.jquery.min.js"></script>
<script>
	var res_date = <?php echo json_encode($data['date']) ?>;
	$(document).ready(function(){
		// grid_name, grid_headers, grid_fields -> from VIEW
		field_dropDown();
	});

	

	$('.btn_submit').click(function(e){
		var case_res = _grid_model_loaded.gridlocation.split('/');
		data_case = case_res[case_res.length-1];
		var formData = $('#formData').serializeArray();
			
		date = '';
		// $.each(formData,function(i,v){
			// if(v['name']=='date')
				// date = v['value'];
		// })
		//console.log(formData);
		date = (res_date == 'hidden') ? sDate : date;
		var _grid_name = _grid_model_loaded.table.replace('#','');
		var allData = {};
		var allData = {
			data: formData,
			ops: date,
			adv:'advanceSearch'
		}
		var location = _grid_model_berfore.container_url.replace('/jqgrid_container','');
		searchOnGrid(location,_grid_name,data_case,allData);	// in main.js
		closeDialog();
		reloadJqGrid(_grid_name);
	})

	$('.date-picker').datepicker({
		autoclose: true,
		todayHighlight: true
	})
	//to translate the daterange picker, please copy the "examples/daterange-fr.js" contents here before initialization
	$('.date_range-picker').daterangepicker({
		'applyClass' : 'btn-sm btn-success',
		'cancelClass' : 'btn-sm btn-default',
		locale: {
			applyLabel: 'Apply',
			cancelLabel: 'Cancel',
		}
	})

	function field_dropDown(){
		var grid_headers = [];
		var grid_fields = [];

		$.each( _grid_model_loaded.colModel, function( key, value ) {
			if(value.hidden != true){
				if(value.name != 'actions'){
					grid_fields.push(value.name);
					grid_headers.push(_grid_model_loaded.colNames[key]);
				}
			}
		});
		dropDown_local('field_list',grid_headers,grid_fields,'','list','');
	}

	function dropDown_local_function(){
		$('.dropdown-menu li a').click(function(e){
			e.preventDefault();
			$(this).addClass('hidden');
			var field_label = $(this).html();
			var field_data = $(this).attr('href');
			add_fields(field_label,field_data);
		})
	};
	function add_fields(label,data){
		data_cleaned = cleanData(data);

		var field = '';
		var tool = '';
		//console.log(data_cleaned);
		switch(data_cleaned){
			case 'Supervisor':
			field = '<div class="col-sm-8"><select multiple="" name="'+data+'" data="'+data+'" class="form-control chosen-select">'+<?php echo json_encode($data['supervisor_dropDown']); ?>+'</select></div>';
				break;
			case 'ID_': case 'RT_Login': case 'Last_Name': case 'First_Name': case 'Tenure_in_': case 'RT_Login':
			field = '<div class="col-sm-8"><input name="'+data+'" data="'+data+'" type="text" class="input_field col-xs-12 input_tags" /></div>';
				break;
			case 'Shift':
			field = '<div class="col-sm-8">'+
						'<select name="'+data+'" data="'+data+'" class="form-control">'+
							'<option></option>'+
							'<option value="Morning">Morning</option>'+
							'<option value="Afternoon">Afternoon</option>'+
						'</select>'+
					'</div>';
				break;

			default:
			field = '<div class="col-sm-5"><input name="'+data+'" data="'+data+'" type="text" class="input_field col-xs-12" /></div>';
			tool = '<div class="col-sm-3"><select id="'+data_cleaned+'_tool" name="'+data_cleaned+'_tool" class="form-control"><option></option></select></div>';
				break;
		}

		$('#form_field').append(''+
			'<div class="form-group '+data_cleaned+'">'+
				'<label class="col-sm-3 control-label no-padding-right" > '+label+' </label>'+
					tool+field+
				'<div class="col-sm-1 no-padding">'+
					'<button data="'+data_cleaned+'" data_field="'+data+'" data_label="'+label+'" class="btn_removeField btn btn-danger btn-sm pull-left">'+
						'<i class="ace-icon fa fa-times bigger-110 icon-only"></i>'+
					'</button>'+
				'</div>'+
			'</div>'
		);

		selectOption('search_tool','#'+data_cleaned+'_tool','','','5,6');
		//call input_tags function @ main.js
		input_tags();
		activate_chosen();
		$('.tags').addClass('width_max');
		//console.log(tag_class);
		$('.input_field').keypress(function(e){
			if ( e.which == 13 ) {
				e.preventDefault();
			}
		})

		$('.btn_removeField').click(function(e){
			$('.'+$(this).attr('data')).remove();
			$('[href="'+$(this).attr('data_field')+'"]').removeClass('hidden');
		})
	}

	$('.input_field').keypress(function(e){
		if ( e.which == 13 ) {
			e.preventDefault();
		}
	})

	function cleanData(str){
		str = str.replace('`','');
		var n = str.indexOf('#');
		str = str.substr(0, (n != -1 ) ? n : str.length );
		var n = str.indexOf('(');
		str = str.substr(0, (n != -1 ) ? n : str.length );
		return str;
	}

</script>