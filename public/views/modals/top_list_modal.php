<div class="row">
	<div class="col-xs-12">
		<div class="table_content">
			<table id="top_up_table"></table>
			<div id="top_up_pager"></div>
		</div>
		<!-- PAGE CONTENT ENDS -->
	</div><!-- /.col -->
</div><!-- /.row -->

<?php
	$top_val = $data['list_data']['top'];
	$limit = $data['list_data']['limit'];
	$field = $data['list_data']['field'];
	$field_name = $data['list_data']['field_name'];
	$date_val = $data['list_data']['date'];
	$data_case = $data['list_data']['data_case'];
	$date = explode(' - ', $date_val);
	if($date[0] == $date[1]){
		$date_res = ' From '.date('F d, Y', strtotime($date[0]));
	}else $date_res = ' From '.date('F d, Y', strtotime($date[0])).' To '.date('F d, Y', strtotime($date[1]));
	$caption = $top.$limit.' in "'.$field.'"'.$date_res;
?>

<script>
	var field = <?php echo json_encode($field) ?>;
	var limit = <?php echo json_encode($limit) ?>;
	var date_val = <?php echo json_encode($date_val) ?>;
	var top_val = <?php echo json_encode($top_val) ?>;
	$(document).ready(function(){
		jqGrid_control(jqGrid_data(field,date_val,limit,top_val));
	});
	//==================================//
	// jqGrid Function Start            //
	//==================================//
	function jqGrid_data(data,ops,adv,adv2){
		model = {
			data_case:'top_up', //use for switch cases
			data: data||'', // any Data
			ops: ops||'', // operation/option data
			adv: adv||'', // advance data
			adv2: adv2||'', // advance data
			table: '#top_up_table',
			content_name: 'table_content',
			//pager: '#top_up_pager',
			container_url: 'reports/jqGrid_reports_ctrl/jqgrid_container',
			gridlocation: 'reports/jqGrid_reports_ctrl/load_jq_data/top_up', // location of controller for gathering data from database
			headers: ['ID #','RT Login','Last Name','First Name','Supervisor','Tenure','Shift',''+<?php echo json_encode($field_name); ?>+''], //header for the table
			names: ['`ID_#`','RT_Login','Last_Name','First_Name','Supervisor','`Tenure_in_(YY/MM/DD)`','Shift','output_data'], //use for modifaction and functons. assign names for data in a row.
			index: 'true', //set true if index is equal to name. use for query. assign index for data in a row.
			widths: [80,80,180,180,180,155,80,80], //specific width of every column
			sortname: 'Last_Name', //use to initialize sort
			autoHeight: 'true',
			//donot add other jqgrid function here. only in jqGridOptions
			jqGridOptions: { // other functions of jqGrid. if not set default will be use. === refer to main.js ===
				// boolean value must be string e.g. 'true' or 'false'
				//shrinkToFit:'true',
				altRows: 'true',
				scroll:1,
				caption: <?php echo json_encode($caption); ?>,
				/*footerrow: 'true',
				userDataOnFooter: 'true'*/
			}
		};
		return model;
	}

	function loadComplete_global(model,data){}
	function jqGrid_extension(table,model){}

</script>