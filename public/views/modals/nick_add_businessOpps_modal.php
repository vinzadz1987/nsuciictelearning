<div class="row">
	<div class="col-xs-12">
		<div class="space-8"></div>
		<form class="form-horizontal" role="form">
			<div class="form-group">
				<div class="col-sm-7">
					<select id="fields" name="fields" class="form-control">
						<option value="owner">Owner</option>
						<option value="status">Status</option>
						<option value="client">Client Name</option>
						<option value="lob">LOB</option>
						<option value="service">Service</option>
						<option value="growth">Growth / New</option>
						<option value="location">Location</option>
					</select>
				</div>
				<div class="col-sm-5">
					<input id="data_value" name="data_value" type="text" placeholder="value..." class="col-xs-12">
				</div>
			</div>
		</form>

	</div>
	<div class="col-xs-12 error_field">
</div>

<script>
	$('.btn_submit').on('click', function(e){
		e.preventDefault();
		if( val_data = $('#data_value').val() )
			addSettings(val_data,$('#fields').val());
		else error_msg();
	})

	$('#data_value').on('keypress', function(e){
		if ( e.which == 13 ) {
			e.preventDefault();
			if( val_data = $(this).val() )
				addSettings(val_data,$('#fields').val());
			else error_msg();
		}
	})

	function addSettings(data,field){
		$.ajax({
			url: 'vendor_manager/business_opportunities/add_settings',
			type: 'POST',
			data: {data:data,field:field},
			dataType: 'json',
			success: function(res){
				switch(res){
					case true:
						$('.error_field').html(''+
							'<div class="alert alert-success">'+
				    		'<strong>'+
				    			'<i class="ace-icon fa fa-check green"></i>'+
				    			' Well done! '+
				    		'</strong>'+
				    		' Successfully added.'+
				    	'</div>');
						break;
					case false:
						error_msg(' Try again');
						break;
					default:
						error_msg(' '+res);
					break;
				}
			}
		})
	}

	function error_msg(data){
		data = data || ' Fill blank fields.';
		$('.error_field').html(''+
			'<div class="alert alert-danger">'+
    		'<strong>'+
    			'<i class="ace-icon fa fa-exclamation-triangle red"></i>'+
    			' Warning! '+
    		'</strong>'+
    		data+
    	'</div>');
	}
</script>