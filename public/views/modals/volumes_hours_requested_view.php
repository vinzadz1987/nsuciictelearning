<?php //print_r($data['getid_data'])?>
<div class="row" id='mtd-kpi-report'>
	<div class="col-xs-12">		
		<div class="row">
			<div class="col-xs-12">
				<div id="recent-box" class="widget-box transparent">
					<!-- #section:elements.tab -->
					<div class="gridcont">
						<table id="hours_request_table"></table>
						<!--div id="hours_request_pager"></div-->
					</div>
				</div>
			</div>
		</div>		
	</div>
</div>
<style>
	.firstCol{
		width:140px;
		background-color: #d4ebf9;
	}
	.sum_det{
		margin-bottom:10px;
	}
</style>
<script>
var subcat_id = <?php echo json_encode($data['getid_data']['subcat_id']); ?>;

$(document).ready(function(){
	jqGrid_control(jqGrid_data(subcat_id));
	//alert(<?php echo json_encode($data['getid_data']['subcat_id']); ?>);
});
var curdate = new Date();
//alert(subcat_id);


	//==================================//
	// jqGrid Function Start            //
	//==================================//
	function jqGrid_data(data,ops,adv){
		var caption_name='';
		switch(subcat_id){
			case '10':
				caption_name = 'Hours Requested';
			break;
			case '11':
				caption_name = 'Hours Delivered';
			break;
			case '12':
				caption_name = 'Chats Handled';
			break;
			case '13':
				caption_name = 'Abandonment (Missed Opps)';
			break;
			case '16':
				caption_name = 'Actual FTE';
			break;
			
		}
		
		//mHeight = window.innerHeight - 550;
		model = {
			data_case:'volumes_hours_requested', //use for switch cases
			data: data||'', // any Data
			ops: ops||'', // operation/option data
			adv: adv||'', // advance data
			table: '#hours_request_table',			
			pager: '#hours_request_pager',
			container_url: 'vendor_manager/jqGrid_vendor_manager_ctrl/jqgrid_container',
			gridlocation: 'vendor_manager/jqGrid_vendor_manager_ctrl/load_jq_data/volumes_hours_requested', //location of controller for gathering data from database
			headers: ['id','name',' ',' ','YTD','JAN','FEB','MAR','APR','MAY','JUN','JUL','AUG','SEP','OCT','NOV','DEC'], //header for the table
			names: ['subcat_id','catname','actions','anothersub_name','YTD','JANUARY','FEBRUARY','MARCH','APRIL','MAY','JUNE','JULY','AUGUST','SEPTEMBER','OCTOBER','NOVEMBER','DECEMBER'], //use for modifaction and functons. assign names for data in a row.
			index: 'true', //set true if index is equal to name. use for query. assign index for data in a row.
			widths: [0,80,80,250], //specific width of every column
			sortname: 'anothersub_id', //use to initialize sort
			//donot add other jqgrid function here. only in jqGridOptions
			jqGridOptions: { // other functions of jqGrid. if not set default will be use. === refer to main.js ===
				// boolean value must be string e.g. 'true' or 'false'
				shrinkToFit:'true',
				height: window.innerHeight - 385,
				altRows: 'true',
				scroll:0,
				grouping: 'true',
				groupingView : { 
					groupField : ['catname'],
					groupColumnShow : [false],
					groupText :['<b>{0}</b>'],
					groupOrder: ['asc'],
					groupSummary : [true]
				}, 
				caption: caption_name,
				editurl: 'vendor_manager/jqGrid_vendor_manager_ctrl/edit_row_hours_requested',
				toolbarEdit: 'false',
				toolbarAdd: 'false',
				pgbuttons: 'false',
				viewrecords: 'false',
				rowList: [], 
				pgtext: 'null', 
				toolbarDel: 'false',
			}
		};
		return model;
	}
	
		function jqGrid_extension(){
		setLabelColor('grid_table','ID_#','bg-danger');//main.js
		//$("#grid_table").jqGrid('setFrozenColumns');		
	}
	
	
	
function loadComplete_global(model,data){
		setWidgetToolbarItems({
			exportToFile:
				{
					excel:[model,data,'json']
				}, 
			widgetButton:
				[
					{id:'graph1', classes:'', icon:'fa-bar-chart-o', text:'Show Graph'}
				]		
		});
		NProgress.done();
		_grid_model_loaded = model;
		
		// Add this function when using jqgrid inline save
		inlineSaveReload(model.table);
		
	}
	function updateActionIcons(table) {
	
		var replacement = 
		{
			'ui-icon-trash' : '',
			'ui-icon-disk' : 'ace-icon fa fa-check red',
			'ui-icon-cancel' : 'ace-icon fa fa-times red'
		};
		$(table).find('.ui-pg-div span.ui-icon').each(function(){
			var icon = $(this);
			var $class = $.trim(icon.attr('class').replace('ui-icon', ''));
			if($class in replacement) icon.attr('class', 'ui-icon '+replacement[$class]);
		})
		
		$('.ui-inline-del').hide();
		
	}
	//==================================//
	// jqGrid Function End              //
	//==================================//

	/*=======================================================================
				Search functions
	========================================================================*/

	$('#btn_search').click(function(e){
		e.preventDefault();

		var search = ($('#grid_search').val())?$('#grid_search').val():'';
		var byDate = ($('#grid_byDate').val())?$('#grid_byDate').val():'';
		var advance = ($('#grid_advance').val())?$('#grid_advance').val():'';		
		
		gridData = {data:search,ops:byDate,adv:advance};
		searchOnGrid('vendor_manager/jqGrid_vendor_manager_ctrl','grid_table','monthly_cs_review',gridData);
		
		searchData = {
			grid: 'grid_table',
			title: 'Summary',
			search: search,
			date: byDate,
			field: advance,
		}
		//main.js
		jqGrid_caption(searchData);
	})
</script>