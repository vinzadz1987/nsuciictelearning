<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class JqGrid_settings_model extends CI_Model {

	function getAllData($start,$limit,$sidx,$sord,$where,$module,$module_data){
		if($limit != NULL)
	    	$this->db->limit($limit,$start);
	    if($where != NULL)
	        $this->db->where($where,NULL,FALSE);
	    $this->db->order_by($sidx,$sord);

		switch ($module) {
			case 'online_quiz_questions':				
				$this->db->select('toqq_id,toqq_question,toqq_type,toqq_choices,toqq_answer');
				$this->db->where('online_quiz_id',$module_data['quiz_id']);
				$this->db->where_not_in('toqq_status',1);
				$sql = $this->db->get('tbl_online_quiz_questions');
            break;
			case 'online_quiz':					
				$this->db->select('
				toq_id,
				toq_name,
				toq_id taker,
				toq_id not_taker,
				updated_date,
				toq_quiz_status,
				toq_status,
				training_materials,
				test_time,
				test_time as subject
				');
				$this->db->select('CONCAT_WS(", ", te.lastname,te.firstname) author',FALSE);
				$this->db->join('tbl_instructor te','te.instructor_id = toq.created_by','left');
				if( isset($module_data['agent_quiz']) )
				$this->db->where('toq_quiz_status','Published');
				$this->db->where_not_in('toq_status',1);
				$sql = $this->db->get('tbl_online_quiz toq');
			break;
			case 'online_quiz_examinees':		
				$this->db->select('toqr_quiz_id,toqr_id,toqr_emp_id,toqr_partial_score,toqr_final_score,toqr_date,toqr_status');
				$this->db->select('CONCAT_WS(", ", te.lastname,te.firstname) emp_name',FALSE);
				$this->db->join('tbl_students te','te.student_id = toqr.toqr_emp_id','left');
				$this->db->where('toqr_quiz_id',$module_data['quiz_id']);
				$sql = $this->db->get('tbl_online_quiz_results toqr');
            break;
			case 'online_quiz_not_examinees':
				$this->db->select('te.student_id emp_id');
				$this->db->select('CONCAT_WS(", ", te.lastname,te.firstname) emp_name',FALSE);
				$sql = $this->db->get('tbl_students te');
            break;
			case 'examinee_list':
				$this->db->select('toqr_id,toqr_emp_id,toqr_date,toqr_status');
				$this->db->select('CONCAT_WS(", ", te.lastname,te.firstname) emp_name',FALSE);
				$this->db->join('tbl_ te','te.employee_id = toqr.toqr_emp_id','left');
				$this->db->where('toqr_quiz_id',$module_data['quiz_id']);
				$sql = $this->db->get('tbl_online_quiz_results toqr');
            break;
		}
		

		
		$query = $sql;
	    return $query->result();

	}

}