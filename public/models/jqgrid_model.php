<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class JqGrid_model extends CI_Model {

	function __construct() {
		parent::__construct();
		$this->load->model('one_model','one');
		
		$this->firstday = date('Y-m-01');
		$this->lastday = date('Y-m-t');
		$this->month = $this->firstday . ' - ' .$this->lastday;
	}
       

	function getAllData($start,$limit,$sidx,$sord,$where,$kind,$data,$ops,$adv){  
		$this->month = ($ops) ? $ops : $this->month;
		
		switch ($kind){
                    
                                                    case 'training_materials':					
                                                                            $this->db->select('uploaded_id, uploaded_filename, uploaded_date');
                                                                            $sql = $this->db->get('tbl_uploaded');
                                                                            $searchlike = 'uploaded_filename';
                                                    break;

                                                    case 'agent_list':					
                                                                            $this->db->select('att_uid, first_name, last_name,rt_id, location,position, supervisor, qa, complete_name, tenurity');
                                                                            $sql = $this->db->get('tbl_agent_roster');
                                                                            $searchlike = 'last_name';
                                                    break;
                                                
                                                    case 'stack_rank':					
                                                                            $this->db->select('stack_id,first_name, last_name,supervisor, location,acvn, sph, nps1, nps2, qa,uploaded_date, acvn_rank');
                                                                            //$this->db->order_by('first_name','asc');
                                                                            $sql = $this->db->get('tbl_stack_rank');
                                                                            $searchlike = 'last_name';
                                                    break;
            
                                    }

           
			if($adv!="")				
				$this->db->like("{$adv}",$data,'after');
			else $this->db->where($this->main_model->searchLike($data,$searchlike));
			
		
		if($limit != NULL)
	    	$this->db->limit($limit,$start);
                                    if($where != NULL)
                                        $this->db->where($where,NULL,FALSE);
                                    $this->db->order_by($sidx,$sord);
		
		$query = $sql;
                                    return $query->result();

	}
	
	function showpoint($id,$date){
		$this->db->_reset_select();
		$this->db->select('sum(absent) absent,sum(late) late,sum(undertime) undertime,sum(overtime) overtime,sum(ncns) ncns,sum(partial_points) partial_points,sum(redemp_points) redemp_points,sum(overall_points) overall_points,sanction');
		$this->db->where('empid',$id);
		$this->db->where($this->main_model->datelike('point_date',$date));
		$res_sql = $this->db->get('tbl_attendance_point_upload');
    	return $res_sql->result_array();
	}
	//=========================================//
	// jqGrid Function                         //
	// Edit, Add and Delete                    //
	//=========================================//
	function editRow($cases,$data){
	
		foreach($data as $key=>$val){
			if($key!="oper" && $val!="_empty")				
				$postdata[$key]=$val;
		}
		switch($cases){
			case "kpi_setting":
				$id = 'id';
				$table = 'tbl_goal_settings';
				if($data['oper']=='add')
					$postdata['account'] = $this->main_model->tbl_account('account_name');
			break;
			case "sanction_setting":
				$id = 'id';
				$table = 'tbl_attendance_sanction';
			break;
			case "point_system":
				$id='empid';
				$table = 'tbl_attendance_point_upload';
				$this->db->where('point_date',$postdata['point_date']);
				$verify = $this->verifypointid($postdata['id'],$postdata['point_date']);
				$data['oper'] = ($verify==0)?'add':$data['oper'];
				$postdata['empid'] = $postdata['id'];
				unset($postdata['id']);				
			break;
			case "weight_setting":
				$id = 'id';
				$table = 'tbl_scorecard_weight';
			break;
			case "metric_setting":
				$id = 'id';
				$table = 'tbl_scorecard_metric';
			break;
		}
		// print_r($postdata);exit;
		$arrayval_blank = array_search('',$postdata);
		
		switch ($data['oper']) {
			case "edit":
				$this->db->where($id,$postdata[$id]);
				$this->db->update($table, $postdata); 
				$res = true;
			break;
			case "del":
				$this->db->delete($table, array($id => $postdata[$id])); 
				$res = true;
			break;
			case "add":
				$this->db->insert($table, $postdata);
				$res = true;
			break;
			default:
				$res = true;
			break;
		}
		return $res;
	}
	
                  function getAgentCount(){
                       echo "allcount";
                  }
        
	function savegoalsetting($postdata){
		$this->db->insert('tbl_goal_settings', $postdata);
		return true;
	}

	function updategoalsetting($postdata){
		$this->db->where('id',	$postdata['id']);
		$this->db->update('tbl_goal_settings', $postdata); 
		return true;
	}
	function deletegoalsetting($postdata){
		$this->db->delete('tbl_goal_settings', array('id' => $postdata['id'])); 
		return true;
	}
	function getgoalinfo(){
		$this->db->_reset_select();
		$this->db->select('*');
		$this->db->where('id',$this->input->post('id'));
		$res_sql = $this->db->get('tbl_goal_settings');
		
		echo json_encode($res_sql->result_array()); 
	}
	function savecolorsetting(){
		$this->db->insert('tbl_setting_goal_color', $this->input->post()); 
		return true;
	}

	function updatecolorsetting(){
		$this->db->where('id',	$this->input->post('id'));
		$this->db->update('tbl_setting_goal_color', $this->input->post()); 
		return true;
	}
	function deletecolorsetting(){
		$this->db->delete('tbl_setting_goal_color', array('id' => $this->input->post('id'))); 
		return true;
	}
	function getcolorinfo(){
		$this->db->_reset_select();
		$this->db->select('*');
		$this->db->where('id',$this->input->post('id'));
		$res_sql = $this->db->get('tbl_setting_goal_color');
		
		echo json_encode($res_sql->result_array()); 
	}
	function getlegendinfo(){
		$this->db->_reset_select();
		$this->db->select('*');
		$this->db->where('id',$this->input->post('id'));
		$res_sql = $this->db->get('tbl_attendance_legend');
		echo json_encode($res_sql->result_array()); 
	}
	function static_goal_type(){
		$gtype_ar = array("SPHwNSO:SPH w/ NSO","QA:QA","360:360","WFU:WFU","SPH:SPH");
		return implode(';', $gtype_ar);
	}
	function savelegendsetting($postdata){
		$this->db->insert('tbl_attendance_legend', $postdata);
		return true;
	}
	function updatelegendsetting($postdata){
		$this->db->where('id',	$postdata['id']);
		$this->db->update('tbl_attendance_legend', $postdata); 
		return true;
	}
	function deletelegendsetting($postdata){
		$this->db->delete('tbl_attendance_legend', array('id' => $postdata['id'])); 
		return true;
	}
	function getsanction($overallpoints){
		$this->db->_reset_select();
		$this->db->select('sanction_name');
		$this->db->where('sanction_from <=',$overallpoints);
		$this->db->where('sanction_to >=',$overallpoints);
		$res_sql = $this->db->get('tbl_attendance_sanction');
		$res_row = $res_sql->row();
		
		return $res_row->sanction_name;
	}
	function verifypointid($id,$date){
		$this->db->_reset_select();
		$this->db->select('id');
		$this->db->where('empid',$id);
		$this->db->where('point_date',$date);
		$res_sql = $this->db->get('tbl_attendance_point_upload');		
		return $res_sql->num_rows();
	}
	function static_select_account(){
		$select_array = array("Default:Default","Comcast (Legacy):Comcast (Legacy)","Comcast-OE:Comcast-OE","Sylmark:Sylmark","WEN:WEN","Entertainment Cruises:Entertainment Cruises","MBSC:Meaningful Beauty","MBSC:Sheer Cover","LifeLock:LifeLock","T-Mobile SIM:T-Mobile SIM","T-Mobile:T-Mobile","Virgin Mobile:Virgin Mobile","Net 10:Net 10","eHarmony:eHarmony");
		return implode(';', $select_array);
	}
	function static_select_wtype(){
		$select_array = array("Sales:Sales","NSO (Legacy):NSO (Legacy)","NSO (OE):NSO (OE)","Sales w/ NSO:Sales w/ NSO","Delivered:Delivered","Gross:Gross","Assisted:Assisted","Billed Hours:Billed Hours","CVN:CVN","SPH:SPH","SPH w/ NSO:SPH w/ NSO","QA:QA","360:360","Conversions:Conversions","CSAT:CSAT","Absences:Absences","WFU:Workforce Utilization","Linear Utilization:Linear Utilization","Cumm. Utilization:Cumm. Utilization","ART:ART","IRT:IRT","AHT:AHT","Lates:Lates","Evals:Evals");
		return implode(';', $select_array);
	}
	function static_select_stats(){
		$select_array = array("Active:Active","Inactive:Inactive");
		return implode(';', $select_array);
	}
	function showmetricpreview($account){
		$this->db->_reset_select();
		$this->db->select('*');
		$this->db->where('account',$account);
		$this->db->where('stats','Active');
		$this->db->order_by('sort_id');
		$res_sql = $this->db->get('tbl_scorecard_metric');
		echo json_encode($res_sql->result_array()); 
	}
	function addnewsetting(){
		// print_r($this->input->post());exit;
		foreach($this->input->post() as $key=>$val){
			if($key=='goal_type' || $key=='goal_value' || $key=='pass_score'){
				
				foreach($this->input->post($key) as $val2){
					$goaldata[$key][] = $val2;
				}
			}else{
				$postdata[$key] = $val;
			}			
		}
		
		for($ctr=0;$ctr<(count($goaldata['goal_type'])); $ctr++){
			$postdata['goal_type'] = $goaldata['goal_type'][$ctr];
			$postdata['goal_value'] = $goaldata['goal_value'][$ctr];
			$postdata['pass_score'] = $goaldata['pass_score'][$ctr];
			
			$duplicate = $this->db->query("select id from tbl_goal_settings where account = '{$postdata['account']}' and start_date = '{$postdata['start_date']}' and end_date = '{$postdata['end_date']}' and goal_type = '{$postdata['goal_type']}'");

			if ($duplicate->num_rows() > 0){
				 $row = $duplicate->row(); 
				$this->db->where('id',$row->id);
				$this->db->update('tbl_goal_settings', $postdata); 
			}else{
				$this->db->insert('tbl_goal_settings', $postdata);
			}
		}
		
	}
	function addnew_scoremetric(){
		
		foreach($this->input->post() as $key=>$val){
			if($key!='account'){
				foreach($this->input->post($key) as $val2){
					$metricdata[$key][] = $val2;
				}
			}else{
				$postdata[$key] = $val;
			}
						
		}
		$postdata['date_added'] = date("Y-m-d H:i:s");
		// print_r($postdata);exit;
		for($ctr=0;$ctr<(count($metricdata['metric_name'])); $ctr++){
			$postdata['metric_name'] = $metricdata['metric_name'][$ctr];
			$postdata['perfect_score'] = $metricdata['perfect_score'][$ctr];
			$postdata['target'] = $metricdata['target'][$ctr];
			$postdata['sort_id'] = $metricdata['sort_id'][$ctr];
			$postdata['stats'] = $metricdata['stats'][$ctr];
			
			$duplicate = $this->db->query("select id from tbl_scorecard_metric where account = '{$postdata['account']}' and metric_name = '{$postdata['metric_name']}'");

			if ($duplicate->num_rows() > 0){
				 $row = $duplicate->row(); 
				$this->db->where('id',$row->id);
				$this->db->update('tbl_scorecard_metric', $postdata); 
			}else{
				$this->db->insert('tbl_scorecard_metric', $postdata);
			}
		}
		
	}
	function addnew_weightsetting(){
		
		foreach($this->input->post() as $key=>$val){
			if($key!='account' && $key!='startdate_weight' && $key!='enddate_weight'){
				foreach($this->input->post($key) as $val2){
					$weightdata[$key][] = $val2;
				}
			}else{
				$postdata[$key] = $val;
			}
		}
		
		// print_r($postdata);exit;
		for($ctr=0;$ctr<(count($weightdata['weight_type'])); $ctr++){
			$postdata['weight_type'] = $weightdata['weight_type'][$ctr];
			$postdata['start_score'] = $weightdata['start_score'][$ctr];
			$postdata['end_score'] = $weightdata['end_score'][$ctr];
			$postdata['weight_score'] = $weightdata['weight_score'][$ctr];
			
			$duplicate = $this->db->query("select id from tbl_scorecard_weight where account = '{$postdata['account']}'  and startdate_weight = '{$postdata['startdate_weight']}' and enddate_weight = '{$postdata['enddate_weight']}' and weight_type = '{$postdata['weight_type']}'");

			if ($duplicate->num_rows() > 0){
				 $row = $duplicate->row(); 
				$this->db->where('id',$row->id);
				$this->db->update('tbl_scorecard_weight', $postdata); 
			}else{
				$this->db->insert('tbl_scorecard_weight', $postdata);
			}
		}
		
	}
	
}
