<?php

class Main_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
        $this->schedule_hour = '8:00:00';
        $this->today = date('Y-m-d');
        $this->firstday = date('Y-m-01');
        $this->lastday = date('Y-m-t');
        $this->month = $this->firstday . ' - ' .$this->lastday;
    }

/*===============================================================================================================
                                    Query Function
================================================================================================================*/  


    //added by klaus
    //ajax Form only
    //For database Format
    function serializeToArray($ser){ 
        if($ser){
            $data = explode('&', $ser);
            $data_array = array();
            foreach ($data as $key => $value) {
                 $res = explode('=', $value);
                if(strpos($res[0],'%5B%5D') !== false) {
                    $holder = str_replace("%5B%5D","",$res[0]);
                    if(@$data_array[$holder]){
                        @$data_array[$holder] .= '|'.urldecode($res[1]);
                    }else{
                        @$data_array[$holder] .= urldecode($res[1]);
                    }
                }else{
                    @$data_array[$res[0]] = urldecode($res[1]);
                }
            }
            return $data_array;
        }else return false;
    }
    //added by klaus
    //normalize data of FORM Submit with array values to string value
    //For database Format
    function normalizeFormArray($data){
        if($data){
            $data_array = array();
            foreach ($data as $key => $val) {
                if(is_array($val)){
                    foreach ($val as $res => $value) {
                        if($res!=0){
                            @$data_array[$key] .= '|'.$value;
                        }else @$data_array[$key] .= $value;
                    }
                }else $data_array[$key] = $val;
            }
            return $data_array;
        }else return false;
    }

    function get_table($select,$table,$where='',$result='',$join=''){
        $query = $this->db->query("SELECT $select FROM $table $join $where");
        return $this->queryResult($query,$result,$select);
    }

    function queryResult($query,$result,$select){
        switch($result){
            case 'single':
                $res = explode(' as ', $select);
                if(count($res)>1){
                    return ($query->result())?$query->row()->$res[1]:false;
                }else{
                    return ($query->result())?$query->row()->$select:false;
                }
            break;
            case 'row_num':
                return $query->num_rows();
            break;
            case 'array':
                return $query->result_array();
            break;
            case 'object':
                return $query->result();
            break;
            case 'row_array':
            default:
                return $query->row_array();
            break;
        }
    }

    function insertWhere($table,$data,$where){
        //$where data = if exist in DB will not insert
        $where = explode(',', $where);
        $res_where = array();
        foreach ($where as $key => $val) {
            $res_where[] = $val.' = "'.$data[$val].'"';
        }
        $res = 'WHERE '.implode(' AND ', $res_where);
        //QUESTIONABLY = developer_id
        if(!$this->get_table('developer_id',$table,$res,'row_num')){
            if($this->db->insert($table,$data)){
                echo json_encode(true);
            }else echo json_encode(false);
        }
    }

    function dropDown_local(){
        $data = $this->input->post('data');
        $value = $this->input->post('value');
        $selected = $this->input->post('selected');
        $first_data = $this->input->post('first_data');
        $type = ($this->input->post('type')) ? $this->input->post('type') : 'option';
        if($type == 'option'){
            if($first_data||$first_data=='null'){
                $res = '<option value=""'.$first_data.'</option>';
            }else $res = '';
        }else if($type == 'grid'){
            $res = array();
        }else $res = '';

        foreach ($data as $key => $val) {
            switch ($type) {
                case 'list':
                    if($selected==$value[$key]){
                        $res .= '<li class="active"><a href="'.$value[$key].'">'.$val.'</a></li>';
                    }else $res .= '<li><a href="'.$value[$key].'">'.$val.'</a></li>';
                    break;

                case 'list_data':
                    if($selected==$value[$key]){
                        $res .= '<li class="active" data="'.$value[$key].'">'.$val.'</li>';
                    }else $res .= '<li data="'.$value[$key].'">'.$val.'</li>';
                    break;

                case 'grid':
                    $res[] = $value[$key].':'.$val;
                    break;
                
                case 'option':
                default:
                    if($selected==$value[$key]){
                        $res .= '<option value="'.$value[$key].'" selected="selected" >'.$val.'</option>';
                    }else $res .= '<option value="'.$value[$key].'" >'.$val.'</option>';
                    break;
            }
        }

        if($type=='grid')
            $res = implode(';', $res);
        return $res;
    }

    function dropDown($ops,$selected=NULL,$type,$val=NULL){
        $res = '';
        $type = ($type) ? $type : 'option';
        $data = array();
        $data['selected'] = $selected;
        $data['type'] = $type;
        switch ($ops) {
            case 'search_tool':
                if($val){
                    $where = 'WHERE tool_id NOT IN('.$val.')';
                }else $where = '';
                $res_array = array(
                    'select'=> 'tool_value,tool_name',
                    'table'=> 'req_search_tool',
                    'where'=> $where
                    );
                break;
            case 'developer_dropdown':
                $res_array = array(
                    'select'=> 'dropdown_id,dropdown_name',
                    'table'=> 'req_developer_dropdown',
                    'where'=> 'WHERE data_status = 1',//optional
                    );
                break;
            case 'employees':
                $res_array = array(
                    'select'=> 'employee_id,lastname,firstname',
                    'table'=> 'tbl_employees',
                    'where'=> 'WHERE status = 0'
                    );
                break;
			case 'nick_req_owner':
				$res_array = array(
                    'select'=> 'owner_name,owner_name',
                    'table'=> 'nick_req_owner',
                    'where'=> 'WHERE status = 0'
                    );
			break;
			case 'nick_req_status':
				$res_array = array(
                    'select'=> 'status_name,status_name',
                    'table'=> 'nick_req_status',
                    'where'=> 'WHERE status = 0'
                    );
			break;
			case 'tbl_accounts':
				$res_array = array(
                    'select'=> 'account_name,account_name',
                    'table'=> 'tbl_accounts',
                    'where'=> 'WHERE account_id != 0'
                    );
			break;
			case 'nick_req_service':
				$res_array = array(
                    'select'=> 'service_name,service_name',
                    'table'=> 'nick_req_service',
                    'where'=> 'WHERE service_id != 0'
                    );
			break;
            case 'nick_req_lob':
				$res_array = array(
                    'select'=> 'lob_name,lob_name',
                    'table'=> 'nick_req_lob',
                    'where'=> 'WHERE status = 0'
                    );
			break;
            case 'nick_req_growth_new':
                $res_array = array(
                    'select'=> 'growthnew_name,growthnew_name',
                    'table'=> 'nick_req_growth_new',
                    'where'=> 'WHERE req_growthnewid != 0'
                    );
            break;
			case 'nick_req_site':
				$res_array = array(
                    'select'=> 'site_name,site_name',
                    'table'=> 'nick_req_site'
                    );
			break;
			case 'nick_tbl_category':
				$res_array = array(
                    'select'=> 'cat_id,cat_name',
                    'table'=> 'nick_tbl_category'
                    );
			break;
			case 'nick_tbl_sub_cat':
				$res_array = array(
                    'select'=> 'subcat_id,subcat_name',
                    'table'=> 'nick_tbl_sub_cat'
                    );
			break;
			
            default:
                # code...
                break;
        }
        
        return $res .= self::make_dropDown(array_merge($data,$res_array));
    }

    function make_dropDown($data){
        $query = self::get_table($data['select'],$data['table'],(isset($data['where']))?$data['where']:'','array',(isset($data['join']))?$data['join']:'');
        $first_val = explode(',', $data['select'])[0];
        $second_val = explode(',', $data['select'])[1];
        if($data['type']=='option'){
            $res = '<option value="">'.(isset($data['first_data'])?$data['first_data']:'&nbsp;').'</option>';
        }else if($data['type']=='grid'){
            $res = array();
        }else $res = '';
        foreach ($query as $key => $val) {
            $f_val = $val[$first_val];
            unset($val[$first_val]);
            $res_dd_data = array();
            foreach ($val as $k => $v) {
               $res_dd_data[] = $v;
            }
            if($first_val!=$second_val){
                $dd_data = implode(isset($data['separator'])?$data['separator']:', ', $res_dd_data);
            }else $dd_data = $f_val;
            switch ((isset($data['type'])?$data['type']:'')) {
                case 'list':
                    if($f_val==$data['selected']){
                        $res .= '<li class="active"><a href="'.$f_val.'">'.$dd_data.'</a></li>';
                    }else $res .= '<li data="'.$f_val.'"><a href="'.$f_val.'">'.$dd_data.'</a></li>';
                    break;
                case 'list_data':
                    if($f_val==$data['selected']){
                        $res .= '<li class="active" data="'.$f_val.'">'.$dd_data.'</li>';
                    }else $res .= '<li data="'.$f_val.'">'.$dd_data.'</li>';
                    break;
                case 'grid':
                        $res[] = $f_val.':'.$dd_data;
                    break;
                
                case 'option':
                default:
                    if($f_val==$data['selected']){
                        $res .= '<option selected="selected" value="'.$f_val.'">'.$dd_data.'</option>';
                    }else $res .= '<option value="'.$f_val.'">'.$dd_data.'</option>';
                    break;
            }
            
        }
        if($data['type']=='grid')
            $res = implode(';', $res);
        return $res;
    }

    function tbl_account($data)
    {
        switch ($this->session->userdata('business_id')) {
            default:
            case 1: 
            case 2:
                $account_name = 'Comcast (Legacy)';
                $table_util = 'rdata_comcast_comcast_standardreports_operep_utilization';
                $table_convsum = 'rdata_comcast_comcast_standardreports_progrep_convsum';
                $table_resp = 'rdata_comcast_comcast_standardreports_operep_resp';
                //$others = 'rdata_comcast_comcast_clienttranscript_clienttranscript';
				$others = 'rdata_comcast_comcast_clienttranscript_orderdetail';
				$table_qa = 'rdata_qa_internal_comcast';
            break;
            case 3:
                $account_name = 'Comcast-OE';
                $table_util = 'rdata_comcast_comcastoe_standardreports_operep_utilization';
                $table_convsum = 'rdata_comcast_comcastoe_standardreports_progrep_convsum';
                $table_resp = 'rdata_comcast_comcastoe_standardreports_operep_resp';
                $others = 'rdata_comcast_comcast_clienttranscript_clienttranscript';
				 $table_qa = 'rdata_qa_internal_comcastoe';
            break;
            case 5: 
                $account_name = 'WEN';
                $table_util = 'rdata_chazdean_chazdean_standardreports_operep_utilization';
                $table_convsum = 'rdata_chazdean_chazdean_standardreports_progrep_convsum';
                $table_resp = 'rdata_chazdean_chazdean_standardreports_operep_resp';     
				$table_qa = 'rdata_qa_internal_chazdean';				
            break;
            case 6:
                $account_name = 'Entertainment Cruises';
                $table_util = 'rdata_entcruises_entcruises_standardreports_operep_utilization';
                $table_convsum = 'rdata_entcruises_entcruises_standardreports_progrep_convsum';
                $table_resp = 'rdata_entcruises_entcruises_standardreports_operep_resp';                
				 $table_qa = 'rdata_qa_internal_entcruises';
            break;
            case 7:
                $account_name = 'LifeLock';
                $table_util = 'rdata_lifelock_lifelock_standardreports_operep_utilization';
                $table_convsum = 'rdata_lifelock_lifelock_standardreports_progrep_convsum';
                $table_resp = 'rdata_lifelock_lifelock_standardreports_operep_resp'; 
				 $table_qa = 'rdata_qa_internal_lifelock';
            break;
            case 8:
                $account_name = 'MBSC'; 
                $table_util = 'rdata_meanbeauty_meanbeauty_standardreports_operep_utilization';
                $table_convsum = 'rdata_meanbeauty_meanbeauty_standardreports_progrep_convsum';
                $table_resp = 'rdata_meanbeauty_meanbeauty_standardreports_operep_resp';  
				 $table_qa = 'rdata_qa_internal_meanbeauty';
            break;
            case 9:
                $account_name = 'MBSC'; 
                $table_util = 'rdata_sheercover_sheercover_standardreports_operep_utilization';
                $table_convsum = 'rdata_sheercover_sheercover_standardreports_progrep_convsum';
                $table_resp = 'rdata_sheercover_sheercover_standardreports_operep_resp';  
				 $table_qa = 'rdata_qa_internal_sheercover';
            break;
            case 10:
                $account_name = 'Sylmark'; 
                $table_util = 'rdata_sylmark_sylmark_standardreports_operep_utilization';
                $table_convsum = 'rdata_sylmark_sylmark_standardreports_progrep_convsum';
                $table_resp = 'rdata_sylmark_sylmark_standardreports_operep_resp';  
				//$table_qa = 'rdata_qa_internal_tmobileus';				
            break;
            case 11:
            case 12:
                $account_name = 'T-Mobile SIM'; 
                $table_util = 'rdata_tmobileus_tmoprepaid_standardreports_operep_utilization';
                $table_convsum = 'rdata_tmobileus_tmoprepaid_standardreports_progrep_convsum';
                $table_resp = 'rdata_tmobileus_tmoprepaid_standardreports_operep_resp';
				 $table_qa = 'rdata_qa_internal_tmobilesim';
            break;
            case 13:
                $account_name = 'T-Mobile'; 
                $table_util = 'rdata_tmobileus_tmobileus_standardreports_operep_utilization';
                $table_convsum = 'rdata_tmobileus_tmobileus_standardreports_progrep_convsum';
                $table_resp = 'rdata_tmobileus_tmobileus_standardreports_operep_resp';
                $table_qa = 'rdata_qa_internal_tmobileus';
            break;
            case 14:
                $account_name = 'Virgin Mobile'; 
                $table_util = 'rdata_virginusa_virginusa_standardreports_operep_utilization';
                $table_convsum = 'rdata_virginusa_virginusa_standardreports_progrep_convsum';
                $table_resp = 'rdata_virginusa_virginusa_standardreports_operep_resp';
                $others = 'rdata_virginusa_virginusa_clienttranscript_orderdetail';
                $table_qa = 'rdata_qa_internal_virginusa';
            break;
            case 15:
                $account_name = 'Straight Talk'; 
                $table_util = 'rdata_stalk_stalk_standardreports_operep_utilization';
                $table_convsum = 'rdata_stalk_stalk_standardreports_progrep_convsum';
                $table_resp = 'rdata_stalk_stalk_standardreports_operep_resp';
                $table_qa = 'rdata_qa_internal_virginusa';
            break;
            case 16:
                $account_name = 'net10'; 
                $table_util = 'rdata_virginusa_virginusa_standardreports_operep_utilization';
                $table_convsum = 'rdata_virginusa_virginusa_standardreports_progrep_convsum';
                $table_resp = 'rdata_virginusa_virginusa_standardreports_operep_resp';
                $others = 'rdata_virginusa_virginusa_clienttranscript_orderdetail';
                $table_qa = 'rdata_qa_internal_virginusa';
            break;
            case 17:
                $account_name = 'eHarmony'; 
                $table_util = 'rdata_eharm_eharm_standardreports_operep_utilization';
                $table_convsum = 'rdata_eharm_eharm_standardreports_progrep_convsum';
                $table_resp = 'rdata_eharm_eharm_standardreports_operep_resp';
                $table_qa = 'rdata_qa_internal_virginusa';
            break;
            
        }

        switch ($data) {
            case 'table_util': return $table_util; break;
            case 'table_convsum': return $table_convsum; break;
            case 'table_resp': return $table_resp; break;
            case 'others': return $others; break;
            case 'account_name': return $account_name; break;
            case 'table_qa': return $table_qa; break;
        }
    }

/*===============================================================================================================
                                    Dates and Hours Fnction
================================================================================================================*/  

    function dateLike($field,$date,$format='Y-m-d',$datetype = null){
        
        $res = '';
        if($date){
            $date = ($date=='today')?date($format):$date;
            $res_date = explode(' - ', $date);
            
            if(count($res_date)==2){
                $check_date = explode(' ', $res_date[0]);
                $first = (count($check_date)>1)?'':'00:00:00';
                $second = (count($check_date)>1)?'':'23:59:59';
                $res = $field.' BETWEEN "'.date($format, strtotime($res_date[0])).' '.$first.'" AND "'.date($format, strtotime($res_date[1])).' '.$second.'"';
            }else if(count($res_date)==1){
				if($datetype=='varchar')
					$res = $field.' like "'.date($format, strtotime($res_date[0])).'%"';
				else
					$res = $field.' = "'.date($format, strtotime($res_date[0])).'"';
            }
        }
        
        return $res;
    }

    function hoursToSec($str_time){
        sscanf($str_time, "%d:%d:%d", $hours, $minutes, $seconds);
        return isset($seconds) ? $hours * 3600 + $minutes * 60 + $seconds : $hours * 60 + $minutes;
    }

    function secToHours($str_time){
        $hours = sprintf('%02d', floor($str_time / 3600));
        $minutes = sprintf('%02d', floor(($str_time / 60) % 60));
        $seconds = sprintf('%02d', $str_time % 60);

        return "$hours:$minutes:$seconds";
    }

    function dateInterval_range($date,$separator = ' - '){
        $res = 0;
        if($date){
            $res_date = explode($separator, $date);
            $datetime1 = date_create($res_date[0]);
            $datetime2 = date_create($res_date[1]);
            $interval = date_diff($datetime1, $datetime2);
            $res = $interval->format('%a');
        }
        return $res;
    }

    function dateInterval($first,$last,$result='%a'){
        $res = 0;
        $first = date_create($first);
        $last = date_create($last);
        $interval = date_diff($first,$last);
        $res = $interval->format($result);
        return $res;
    }

    function check_dateType($date,$separator = ' - '){
        $res_date = explode($separator, $date);
        return count($res_date);
    }

    function check_dateIfToday($date,$separator = ' - '){
        if($date){
            if($this->check_dateType($date)>1){
                if($this->dateInterval_range($date)==0){
                    $res_date = explode($separator, $date);
                    if(date('Y-m-d', strtotime($res_date[1])) == date('Y-m-d'))
                        return true;
                        return false;
                }else return false;
            }else{
                if(date('Y-m-d', strtotime($date)) == date('Y-m-d'))
                    return true;
                    return false;
            }
        }else return false;
    }

    function get_week_range($ops='',$date='',$format=''){
        $date = ($date) ? $date : date('Y-m-d');
        $format = ($format) ? $format : 'Y-m-d';
        $first = date($format,strtotime('this week',strtotime($date)));
        $last = ($ops == 'now') ? date($format,strtotime($date)) : date($format,strtotime($first . ' +6 days'));
        $res = array(
            $first,
            $last
            );
        return $res;
    }

    function get_daysInweek($ops='',$date='',$format=''){
        $date = ($date) ? $date : date('Y-m-d');
        $format = ($format) ? $format : 'Y-m-d';
        $first = date('Y-m-d',strtotime('this week',strtotime($date)));
        $last = ($ops == 'now') ? date('Y-m-d',strtotime($date)) : date('Y-m-d',strtotime($first . ' +6 days'));
        while ( $this->dateInterval($first,$last) != 0) {
            $res[] = date($format, strtotime($first));
            $first = date('Y-m-d', strtotime($first . ' +1 day '));
        }
        if($this->dateInterval($first,$last)==0)
            $res[] = date($format, strtotime($last));
        return $res;
    }

   function WeekNames(){
        $timestamp = strtotime('sunday');
        $days = array();
        for ($i = 0; $i < 7; $i++) {
            $days[] = strftime('%A', $timestamp);
            $timestamp = strtotime('+1 day', $timestamp);
        }
        return $days;
    }

    function get_daysInMonth($date='',$format=''){
        $date = ($date) ? $date : date('Y-m-d');
        $format = ($format) ? $format : 'Y-m-d';
        $res = array();
        $first = date('Y-m-01', strtotime($date));
        $last = date('Y-m-t', strtotime($date));
        while ( $this->dateInterval($first,$last) != 0) {
            $res[] = date($format, strtotime($first));
            $first = date('Y-m-d', strtotime($first . ' +1 day '));
        }
        if($this->dateInterval($first,$last)==0)
            $res[] = date($format, strtotime($last));
        return $res;
    }

    function get_Month_range($ops='',$date='',$format=''){
        $date = ($date) ? $date : date('Y-m-d');
        $format = ($format) ? $format : 'Y-m-d';
        $ops = ($ops == 'now') ? 'Y-m-d' : 'Y-m-t';
        $res = array(
            date($format,strtotime(date('Y-m-01', strtotime($date)))),
            date($format,strtotime(date($ops, strtotime($date))))    
            );

        return $res;
    }

    function get_firstQuarter_range($year='',$format=''){
        $year = ($year) ? $year.'-01-01' : date('Y-01-01');
        $format = ($format) ? $format : 'Y-m-d';
        $res = array(
            date($format,strtotime(date('Y-01-01', strtotime($year)))),
            date($format,strtotime(date('Y-03-t', strtotime($year))))    
            );
        return $res;
    }

    function get_secondQuarter_range($year='',$format=''){
        $year = ($year) ? $year.'-01-01' : date('Y-01-01');
        $format = ($format) ? $format : 'Y-m-d';
        $res = array(
            date($format,strtotime(date('Y-04-01', strtotime($year)))),
            date($format,strtotime(date('Y-06-t', strtotime($year))))    
            );
        return $res;
    }

    function get_thirdQuarter_range($year='',$format=''){
        $year = ($year) ? $year.'-01-01' : date('Y-01-01');
        $format = ($format) ? $format : 'Y-m-d';
        $res = array(
            date($format,strtotime(date('Y-07-01', strtotime($year)))),
            date($format,strtotime(date('Y-09-t', strtotime($year))))    
            );
        return $res;
    }

    function get_fourthQuarter_range($year='',$format=''){
        $year = ($year) ? $year.'-01-01' : date('Y-01-01');
        $format = ($format) ? $format : 'Y-m-d';
        $res = array(
            date($format,strtotime(date('Y-10-01', strtotime($year)))),
            date($format,strtotime(date('Y-12-t', strtotime($year))))    
            );
        return $res;
    }

    function get_daysInQuarter($ops='',$year='',$format=''){
        $format = ($format) ? $format : 'Y-m-d';
        switch ($ops) {
            case 'first': $quarter =  $this->get_firstQuarter_range($year,'Y-m-d'); break;
            case 'second': $quarter =  $this->get_secondQuarter_range($year,'Y-m-d'); break;
            case 'third': $quarter =  $this->get_thirdQuarter_range($year,'Y-m-d'); break;
            case 'fourth': $quarter =  $this->get_fourthQuarter_range($year,'Y-m-d'); break;
        }
        $first = $quarter[0];
        $last = $quarter[1];


       while ( $this->dateInterval($first,$last) != 0) {
            $res[] = date($format, strtotime($first));
            $first = date('Y-m-d', strtotime($first . ' +1 day '));
        }
        if($this->dateInterval($first,$last)==0)
            $res[] = date($format, strtotime($last));
        return $res;
    }

    function get_yearRange($year='',$format=''){
        $year = ($year) ? $year.'-01-01' : date('Y-01-01');
        $format = ($format) ? $format : 'Y-m-d';

        $res = array(
            date($format,strtotime(date('Y-01-01', strtotime($year)))),
            date($format,strtotime(date('Y-12-t', strtotime($year))))    
            );
        return $res;
    }

/*===============================================================================================================
                                    Common Functions
================================================================================================================*/  
    
    //   Display Search Results Caption
    //   e.g Result of "search" in "field" From 2014/09/2 To 2014/09/30
    function search_caption(){
        $title = $this->input->post('title');
        $search = $this->input->post('search');
        $date = $this->input->post('date');
        $field = $this->input->post('field');
        $format = $this->input->post('format');
        $format = ($format) ? $format : 'F d, Y';
        $result = ($this->input->post('result')) ? $this->input->post('result') : '';

        $search = ($search) ? ' of "<i>'.$search.'</i>"' : ' ';
        $field = ($field) ? ' in "'.$field.'"' : ' ' ;

        if($date){
            if($this->check_dateType($date)>1){
                if($this->check_dateIfToday($date)){
                    $date = 'of Today';
                }else{
                    $res_date = explode(' - ', $date);
                    $first = date($format, strtotime($res_date[0]));
                    $second = date($format, strtotime($res_date[1]));
                    $date = ($date) ? ' FROM '.$first.' To '.$second.'' : ' ';
                }
            }else{
                if($this->check_dateIfToday($date)){
                    $date = 'of Today';
                }else{
                    $date = 'of '.date($format, strtotime($date));
                }
            }
            
           
        }

        if($result == 'echo'){
            echo $title.'<b>: Resuslt </b>'.$search.$field.$date;
        }else return $title.'<b>: Resuslt </b>'.$search.$field.$date;
        
    }

    //   Check business ID for account
    //   and get account title
    function account_title(){
        $captionTitle = '';
        if($this->session->userdata('business_id')==1){
            $captionTitle = 'Comcast (Legacy)';
        }else if($this->session->userdata('business_id')==11){
            $captionTitle = 'TMO-Prepaid';
        }else{
            $captionTitle = $this->session->userdata('business_name');
        }
        return $captionTitle;
    }

    function get_percentage_time($first,$second,$zero='',$percent=true,$round=2){
        $percent = ($percent)?' %': '';
        $first = $this->hoursToSec($first);
        $second = $this->hoursToSec($second);
        $res = (($first!=0&&$second!=0)?($first/$second)*100:0);
        if($round)
            $res = round($res,$round).$percent;
        if($zero)
            $res = (round($res,$round))?$res:' - ';
        return $res;
    }
	
    function get_percentage($first,$second,$zero='',$percent=true,$round=2){
        $percent = ($percent)?' %': '';
        $res = (($first!=0&&$second!=0)?($first/$second)*100:0);
        if($round)
            $res = round($res,$round).$percent;
        if($zero)
            $res = (round($res,$round))?$res:' - ';
        return $res;
    }

    function get_quotient($first,$second,$zero='',$round=2){
        $res = (($first!=0&&$second!=0) ? ($first/$second) : 0);
        if($round){
            $res = round($res,$round);
        }
        if($zero){
            $res = (round($res,$round))?$res:' - ';
        }
        return $res;
    }

    function advanceSearch($data,$fields){
        unset($fields['`ID_#`']);
        unset($fields['`Tenure_in_(YY/MM/DD)`']);
        //print_r($fields);exit();
        $res_data = array();
        $var_data = 0;
        $res = false;
        foreach ($data as $key => $val) {
            $res_data[$val['name']] = $val['value'];
        }

        foreach ($res_data as $k => $v) {
           if(in_array($k, $fields)){
                if($v){
                    //set data
                    $clean_data = preg_replace("/[^0-9,.]/", "", strip_tags($fields[$k]));
                    $hours = explode(':', $clean_data);
                    if(count($hours)==3){
                        $var_data = $hours[0] + ($hours[1] / 60) + ($hours[2] / 3600);
                    }else{
                        $var_data = $clean_data;
                    }
                    //do search
                    switch ($res_data[$k.'_tool']) {
                        case '=':
                            if($v == $var_data){
                                $res = true;
                            }                                
                            break;
                        case '<>':
                            if($v != $var_data){
                                $res = true;
                            }   
                            break;
                        case '<':
                            if($v > $var_data){
                                $res = true;
                            }   
                            break;
                        case '>':
                            if($v < $var_data){
                                $res = true;
                            }   
                            break;
                    }
                }
           }else $res = true;
        }
        //print_r($test);exit();
        return $res;
    }

    function advanceSearchLike($data,$fields){
        $fields = explode(',', $fields);
        $res = array();
        $res_data = array();
        //$test = array();
        foreach ($data as $keys => $value) {
            if($res_data[$value['name']]){
                if($value['value'])
                    $res_data[$value['name']] = $res_data[$value['name']].'|'.$value['value'];
            }else{
                $res_data[$value['name']] = $value['value'];
            }
        }
        //$test = array();
        foreach ($res_data as $key => $val) {
            //$test[] = $val;
            if(in_array($key, $fields)){
                $res_array = array();
                $data_val = explode('|', $val);
                
                foreach ($data_val as $k => $v) {
                    $res_array[] = $key.' LIKE "%'.$v.'%"';
                }
                if($res_array){
                    $res[] = '('.implode(' || ', $res_array).')';
                }
            }
        }
        //print_r($res);exit();
        if($res){
            return '('.implode(' AND ', $res).')';
        }else return false;
    }

    /*function advanceSearchLike($data,$fields){
        $fields = explode(',', $fields);
        $res = array();
        foreach ($data as $key => $val) {
            if(in_array($val['name'], $fields)){
                $res_array = array();
                $data_val = explode('|', $val['value']);
                
                foreach ($data_val as $k => $v) {
                    $res_array[] = $val['name'].' LIKE "%'.$v.'%"';
                }
                if($res_array){
                    $res[] = '('.implode(' || ', $res_array).')';
                }
            }
        }
        if($res){
            return '('.implode(' AND ', $res).')';
        }else return false;
    }*/

    //use for active record search
    function searchLike($data,$fields,$ops=''){
        $fields = explode(',', $fields);
        $res = array();
        foreach ($fields as $key => $val) {
            if($ops=='left'){
                $res[] = $val.' LIKE "%'.$data.'"';
            }else if($ops=='right'){
                $res[] = $val.' LIKE "'.$data.'%"';
            }else{
                $res[] = $val.' LIKE "%'.$data.'%"';
            }
        }
        return '('.implode('||', $res).')';
    }

    function quickSearch($data,$fields){
        $flag = false;

        foreach ($fields as $key => $val) {
            if(stripos($val,$data) !== false)
                $flag = true;
        }

        return $flag;
    }

     // Check row of table if exist return boolean
    function row_exist($where,$table){
        if( is_array($where) ){
            $new_where = 'WHERE ';
            foreach ($where as $key => $value) {
                $new_where .= $key.' = "'.$value.'" ';

                $next = next($where);
                if( ($next || ($next === 0 || $next === '0')) ) :
                    $new_where .= ' AND ';
                endif;
            }
            $where = $new_where;
        }
        $query = $this->db->query("SELECT EXISTS(SELECT * FROM $table $where ) as result");
        if( $query->row_array()['result'] )
            return true;
            return false;
    }

/*===============================================================================================================
                                    Manual Function
================================================================================================================*/  

    function supervisorDropdown(){
        $this->db->select('DISTINCT(Supervisor) AS result');
        $this->db->order_by('Supervisor');
        $res_sql = $this->db->get('rdata_qfn_agentlist');

        $data = $res_sql->result_array();
        $res = '<option></option>';
        foreach ($data as $key => $val) {
            $res .= '<option value="'.$val['result'].'">'.$val['result'].'</option>';
        }

        return $res;
    }

}