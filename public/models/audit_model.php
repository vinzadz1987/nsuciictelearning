<?php

class Audit_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
    
    function add_audit($employee_id, $audit_desc){
		
		$data = array(
//			'employee_id'=> $employee_id,
                    'employee_id'=> $employee_id,
			'audit_desc' => $audit_desc,
		    'audit_date' => date('Y-m-d H:i:s'),
			'ip_address' => $this->input->ip_address()
		);

		$this->db->insert('tbl_audit_trail', $data);
    }
}