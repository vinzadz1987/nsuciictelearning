<?php

class Uploading_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    function uploadFilesExtract($row, $ident)
    {
        switch($ident){
            case 1:
                foreach ($row as $r ) {
                    $chatid = explode(',', $r)[0];
                    $agent_id = explode(',', $r)[1];
                    $submitdate = explode(',', $r)[2];
                    $recommend_rank_nps1 = explode(',', $r)[3];
                    $np1 = explode('-',$recommend_rank_nps1)[0];
                    $recommend_rank_nps2 = explode(',', $r)[4];
                    $np2 = explode('-',$recommend_rank_nps2)[0];
                    $table = "tbl_nps";
                    $row = "chatid,agentid,submit_date,recommend_rank_nps1,recommend_rank_nps2";
                    $value = "'" . $chatid . "','" . $agent_id . "','" . $submitdate . "','" . $np1 . "','" . $np2 . "'";
                    $this->db->query("INSERT INTO $table ($row) VALUES ($value)");
                }
                break;
            case 2:
                foreach ($row as $r ) {
                    $idnumber = explode(',', $r)[0];
                    $qaid = explode(',', $r)[1];
                    $agent_id = explode(',', $r)[2];
                    $lastname = explode(',', $r)[3];
                    $firstname = explode(',', $r)[4];
                    $supervisor = explode(',', $r)[5];
                    $manager = explode(',', $r)[6];
                    $location = explode(',', $r)[7];
                    $table = "tbl_employees";
                    $row = "employee_id,qa_id,agent_id, access_id, password,firstname,lastname,supervisor,manager,location, status";
//                    $row = "employee_id,qa_id,agent_id,firstname,lastname,supervisor,manager,location";
                    $value = "'" . $idnumber . "','" . $qaid . "','" . $agent_id . "','1','*A4B6157319038724E3560894F7F932C8886EBFCF','" . $firstname . "','" . $lastname . "','" . $supervisor . "','" .$manager. "','" . $location . "','3'";
                    $this->db->query("INSERT INTO $table ($row) VALUES ($value)");
                }
                break;
        }
    }
    function getEmployeeData(){
        $query = $this->db->query("select * from tbl_employees");
        $this->result_encode($query);
    }
    function getFileNameUpload($fileName,$uploadedPath,$lesson_id){
        $fileName_replace = str_replace(' ','_',$fileName);
        $this->db->query("insert into tbl_uploaded(uploaded_filename,uploaded_path,lesson_id) values ('".$fileName_replace."','".$uploadedPath."','".$lesson_id."')");
    }
    function result_encode($q){
        $arrayindex = array();
        foreach($q->result_array() as $r){
            $arrayindex[] = $r;
        }
        echo json_encode($arrayindex);
    }
}