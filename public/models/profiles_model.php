<?php

class Profiles_model extends CI_Model {

    private $access_menus;
    
    function __construct()
    {
        parent::__construct();
        $this->load->helper('string');
    }
		
	public function save_event_Qry($event_plugin_id,$event_name,$event_details,$event_where,$event_startDate,$event_endDate,$event_startTime,$event_endTime,$current_user,$event_type){
		if($this->checkIfEventExists($event_plugin_id) == 'no'){
			$qry = "INSERT INTO tbl_events (event_plugin_id,event_title,event_details,event_where,event_start_date,event_end_date,event_start_time,event_end_time,event_created_by,event_type)
				VALUES('$event_plugin_id','$event_name','$event_details','$event_where','$event_startDate','$event_endDate','$event_startTime','$event_endTime','$current_user','$event_type')";
		}else{
			$qry = "UPDATE tbl_events SET  event_title='$event_name',event_details='$event_details',event_where='$event_where',event_start_date='$event_startDate',event_end_date='$event_endDate',event_start_time='$event_startTime',event_end_time='$event_endTime',event_type='$event_type' WHERE event_plugin_id LIKE '$event_plugin_id'";
		}
		
		$query = $this->db->query($qry);
		if($query){
			return 'success';
		}else
			return 'error';
	}
	public function get_event_more_info_Qry($event_plugin_id){
		$qry = "SELECT event_plugin_id,event_title,event_details,event_where,event_start_date,event_end_date,event_start_time,event_end_time,event_type FROM tbl_events WHERE event_plugin_id='$event_plugin_id' AND event_is_active = '1'";
		$query = $this->db->query($qry);
		if($query->num_rows > 0){
			return $query->result();
		}else
			return 'error';	
	}
	public function checkIfEventExists($event_id){
		$qry = "SELECT event_plugin_id FROM tbl_events WHERE event_plugin_id LIKE '$event_id'";
		$query = $this->db->query($qry);
		if($query->num_rows  > 0){
			return 'yes';
		}else
			return 'no';
	}
	public function load_all_active_events_Qry(){
		$qry = "SELECT event_plugin_id FROM tbl_events WHERE event_plugin_id LIKE '$event_id'";
	}
        
                 function options($segment){
                    $q = $this->db->query("SELECT uid, $segment FROM tbl_agent_roster group by $segment");
                    $this->result_encode($q);
                }
                
                function options2(){
                    $q = $this->db->query("SELECT * FROM tbl_location");
                    $this->result_encode($q);
                }
                
                 function options_position(){
                    $q = $this->db->query("SELECT position_id,position_name FROM tbl_position");
                    $this->result_encode($q);
                }
                
                function options_supervisor(){
                    $q = $this->db->query("SELECT supervisor_id, first_name, last_name FROM tbl_supervisor ");
                    $this->result_encode($q);
                }
                
                 function options_qa($qa){
                    $q = $this->db->query("SELECT qa_id, qa_name FROM tbl_qa ");
                    $this->result_encode($q);
                }
                
                function update_info($id,$uid,$col){
                    $this->db->query("update tbl_agent_roster set ".$col."='$uid' where uid='$id'");
                }
                
                function update_agent($uid,$att_uid,$first_name,$last_name,$location,$position,$supervisor,$qa,$rt,$cn,$tn){
                   $q = $this->db->query("update tbl_agent_roster set att_uid='$att_uid',first_name='$first_name',last_name='$last_name', position='$position', location='$location', supervisor='$supervisor', qa='$qa', rt_id=$rt, complete_name = $cn, tenurity = $tn where uid='$uid'");
                    if($q){
                        echo 'successfully updated.';
                    }
                }
                
                function insert_agent($att_uid,$first_name,$last_name,$location,$position,$supervisor,$qa,$rt,$cn,$tn){
                   $q = $this->db->query("insert into tbl_agent_roster (att_uid,first_name,last_name,rt_id, supervisor, qa,complete_name,tenurity,location, position) values('$att_uid','$first_name','$last_name','$rt','$supervisor','$qa','$cn','$tn','$location','$position')");
                    if($q){
                        echo 'successfully created.';
                    }
                }
                
                function get_profile_info(){
                    $query = $this->db->query("SELECT * FROM tbl_instructor WHERE instructor_id = '".$this->session->userdata('user_id')."' ");                                      
                    $this->result_encode($query);
                }
                
                function get_profile_info2(){
                    $query = $this->db->query("SELECT a.*,b.* FROM tbl_students a, tbl_course b WHERE a.course=b.course_id AND student_id = '".$this->session->userdata('user_id')."' ");                                      
                    $this->result_encode($query);
                }
                
                function get_profile_update($image){
                    $query = $this->db->query("UPDATE tbl_students SET photo = '".$image."'  WHERE student_id = '".$this->session->userdata('user_id')."' ");                                      
                    $this->result_encode($query);
                }
                
                function get_profile_update2($image){
                    $query = $this->db->query("UPDATE tbl_instructor SET photo = '".$image."'  WHERE instructor_id = '".$this->session->userdata('user_id')."' ");                                      
                    $this->result_encode($query);
                }
                        
                function result_encode($q){

                    $arrayindex = array();
                        foreach($q->result_array() as $r){
                          $arrayindex[] = $r;
                        }

                    echo json_encode($arrayindex);

                  }
        
        
}