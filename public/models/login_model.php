<?php

class Login_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
		    $this->load->model('audit_model','audit');
    }
    
    public function verifylogin($userid, $password)
    {
        $userpassword = md5($password);
        $sql = 'SELECT user_id, firstname, lastname, middlename, password, last_login, access_name, default_controller, a.access_id, b.menu_sets FROM tbl_login_access a LEFT JOIN tbl_access b ON a.access_id = b.access_id WHERE user_id = ? AND password = "'.$userpassword.'"';
        $query = $this->db->query($sql, array($userid, $password));
        $res = $this->db->query($sql, array($userid, $password))->row();
        
        if ($query->num_rows() > 0):
        
                $logged_data = array(
                   'user_id'            => $res->user_id,
                   'firstname'          => $res->firstname,
                   'lastname'           => $res->lastname,
                   'middlename'         => $res->middlename,
                   'last_login'         => date('Y-m-d h:i:s'),
                   'default_controller' => $res->default_controller,
                   'access_name'        => $res->access_name,
                   'menu_sets'          => $res->menu_sets,
                   'logged_in'          => 1,
                );
            $this->config->set_item('default_controller', $logged_data['default_controller']);
            $data = array(
               'last_login' => $logged_data['last_login'],
            );
            $this->db->where('user_id', $res->employee_id);
            $this->db->update('tbl_login_access', $data);

      		$this->audit->add_audit($res->user_id, 'User logged in.');

            $this->session->set_userdata($logged_data);
           //endforeach;

        $this->db->close();
        
        return $query;
        endif;

    }
    
    function check_id_exist($idnumber){
        $query = $this->db->query("SELECT * FROM tbl_students WHERE student_id = '".$idnumber."'");
            if($query->num_rows == 0) {
                // row not found, do stuff...
                       echo '<i class="ace-icon fa fa-user blue"></i> Available';
           } else {
               // do other stuff...
               echo '<i class="ace-icon fa fa-times red"></i> Student ID Already Exists.';
           }
    }
	
	
	function check_id_exist_teach($idnumber){
        $query = $this->db->query("SELECT * FROM tbl_instructor WHERE instructor_id = '".$idnumber."'");
            if($query->num_rows == 0) {
                // row not found, do stuff...
                       echo '<i class="ace-icon fa fa-user blue"></i> Available';
           } else {
               // do other stuff...
               echo '<i class="ace-icon fa fa-times red"></i> Teacher ID Already Exists.';
           }
    }
            
    function register_user($idnumber,$fname,$lname,$mname,$pw,$course,$sem,$level, $subjects, $contactnum){
            
                $data = array(
                    'student_id' => $idnumber,
                    'firstname' => $fname,
                    'lastname' => $lname,
                    'middlename' => $mname,
                    'course' => $course,
                    'password' => md5($pw),
                    'curr_year' => $level,
                    'semester' => $sem,
					'contact_number' => $contactnum
                );
                $data_access = array(
                    'user_id' => $idnumber,
                    'access_id' => '4',
                    'password' => md5($pw),
                    'firstname' => $fname,
                    'middlename' => $mname,
                    'lastname' => $lname,
                );
                $this->db->insert('tbl_login_access',$data_access);
                $this->db->insert('tbl_students',$data);
                foreach($subjects as $subs){
                    $subid = explode('_',$subs)[0]; $insid = explode('_',$subs)[1];
                    $this->db->query("INSERT INTO tbl_enrolled_subject(`student_id`,subject_id,instructor_id) VALUES ('".$idnumber."','".$subid."','".$insid."')");
                }
                echo 'NOTE: Registered Students, Subjects need to be confirmed.';
            
    }
    function register_tuser($idnumber,$fname,$lname,$mname,$pw,$designation,$number){
            
            $query = null; // empty in case
            
            $query = $this->db->get_where('tbl_instructor', array( 
                //making selection
                'instructor_id' => $idnumber
            ));
            
            $count = $query->num_rows(); // counting result from query
            
            if($count == 0){
                $data = array(
                    'instructor_id' => $idnumber,
                    'firstname' => $fname,
                    'lastname' => $lname,
                    'middlename' => $mname,
                    'instructor_mobile_number' => $number,
                    'instructor_designation' => $designation,
                    'instructor_password' => md5($pw)
                );
                $this->db->insert('tbl_instructor',$data);
				echo 'NOTE: Successfully registered and subject to approve by Administrator.';
            }else{
                echo 'INSTRUCTOR ID ALREADY EXIST.';
            }
            
    }
    
    function getsubjecschoice($sem,$ylevel,$course){
            $query = $this->db->query("select * from tbl_prospectus where year = '".$ylevel."' and sem ='".$sem."' AND course = '".$course."'");
            $this->result_encode($query);
    }
    
    function get_course(){
        $query = $this->db->query("SELECT * FROM tbl_course");
        $this->result_encode($query);
    }
    
    function result_encode($q){
            $arrayindex = array();
            foreach($q->result_array() as $r){
                    $arrayindex[] = $r;
            }
            echo json_encode($arrayindex);
	}
    
}


/* 
 * Copyright (C) 2014 09th-QDSUP
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

