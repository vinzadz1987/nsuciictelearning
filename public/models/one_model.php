<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class one_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		//$this->load->model('jqGrid_reports_model','jrm');
	}

	function initialize($date)
	{
		$this->date = $date;
		$data = array();
		switch ($this->session->userdata('business_id')) {
	        default:
	        case 1: 
	        case 2: 
	        	$data['table_util'] = 'rdata_comcast_comcast_standardreports_operep_utilization';
	        	$data['table_convsum'] = 'rdata_comcast_comcast_standardreports_progrep_convsum';
	        	$data['table_resp'] = 'rdata_comcast_comcast_standardreports_operep_resp';
	        	$data['others'] = 'rdata_comcast_comcast_clienttranscript_clienttranscript';
	        break;
	        case 3:
	        	$data['table_util'] = 'rdata_comcast_comcastoe_standardreports_operep_utilization';
	        	$data['table_convsum'] = 'rdata_comcast_comcastoe_standardreports_progrep_convsum';
	        	$data['table_resp'] = 'rdata_comcast_comcastoe_standardreports_operep_resp';
	        break;
	        case 5: 
	        	$data['table_util'] = 'rdata_chazdean_chazdean_standardreports_operep_utilization';
	        	$data['table_convsum'] = 'rdata_chazdean_chazdean_standardreports_progrep_convsum';
	        	$data['table_resp'] = 'rdata_chazdean_chazdean_standardreports_operep_resp';	        	
	        break;
	        case 6: 
	        	$data['table_util'] = 'rdata_entcruises_entcruises_standardreports_operep_utilization';
	        	$data['table_convsum'] = 'rdata_entcruises_entcruises_standardreports_progrep_convsum';
	        	$data['table_resp'] = 'rdata_entcruises_entcruises_standardreports_operep_resp';	        	
	        break;
	        case 7: 
	        	$data['table_util'] = 'rdata_lifelock_lifelock_standardreports_operep_utilization';
	        	$data['table_convsum'] = 'rdata_lifelock_lifelock_standardreports_progrep_convsum';
	        	$data['table_resp'] = 'rdata_lifelock_lifelock_standardreports_operep_resp';	        	
	        break;
	        case 8: 
	        	$data['table_util'] = 'rdata_meanbeauty_meanbeauty_standardreports_operep_utilization';
	        	$data['table_convsum'] = 'rdata_meanbeauty_meanbeauty_standardreports_progrep_convsum';
	        	$data['table_resp'] = 'rdata_meanbeauty_meanbeauty_standardreports_operep_resp';	        	
	        break;
	        case 9: 
	        	$data['table_util'] = 'rdata_sheercover_sheercover_standardreports_operep_utilization';
	        	$data['table_convsum'] = 'rdata_sheercover_sheercover_standardreports_progrep_convsum';
	        	$data['table_resp'] = 'rdata_sheercover_sheercover_standardreports_operep_resp';	        	
	        break;
	        case 10: 
	        	$data['table_util'] = 'rdata_sylmark_sylmark_standardreports_operep_utilization';
	        	$data['table_convsum'] = 'rdata_sylmark_sylmark_standardreports_progrep_convsum';
	        	$data['table_resp'] = 'rdata_sylmark_sylmark_standardreports_operep_resp';	        	
	        break;
	        case 11:
	        case 12: 
	        	$data['table_util'] = 'rdata_tmobileus_tmobileus_standardreports_operep_utilization';
	        	$data['table_convsum'] = 'rdata_tmobileus_tmoprepaid_standardreports_progrep_convsum';
	        	$data['table_resp'] = 'rdata_tmobileus_tmobileus_standardreports_operep_resp';
	        break;
	        case 13: 
	        	$data['table_util'] = 'rdata_tmobileus_tmoprepaid_standardreports_operep_utilization';
	        	$data['table_convsum'] = 'rdata_tmobileus_tmoprepaid_standardreports_progrep_convsum';
	        	$data['table_resp'] = 'rdata_tmobileus_tmoprepaid_standardreports_operep_resp';
	        break;
	        case 14: 
	        	$data['table_util'] = 'rdata_virginusa_virginusa_standardreports_operep_utilization';
	        	$data['table_convsum'] = 'rdata_virginusa_virginusa_standardreports_progrep_convsum';
	        	$data['table_resp'] = 'rdata_virginusa_virginusa_standardreports_operep_resp';
	        	$data['others'] = 'rdata_virginusa_virginusa_clienttranscript_orderdetail';
	        break;
    	}
    	return $data ;
	}
   
	function hoursToSec($str_time)
	{
		sscanf($str_time, "%d:%d:%d", $hours, $minutes, $seconds);
		return isset($seconds) ? $hours * 3600 + $minutes * 60 + $seconds : $hours * 60 + $minutes;
	}

	function getData($rt_login, $idno, $result)
	{
		$this->db->_reset_select();
		$this->db->select_sum('Units_Sold','sales');
		$this->db->select_sum('Conversions','conversions');
		$this->db->select('(SELECT SEC_TO_TIME(SUM(TIME_TO_SEC(total_fix))) FROM tbl_billed_hours WHERE uid = "'.$rt_login.'" AND '.$this->main_model->dateLike('date',$this->month).') as billed_hours', FALSE);
               
		$this->db->where('agent', $rt_login);
		$this->db->where($this->main_model->datelike('period',$this->date),NULL,FALSE);
		$sales = $this->db->get($this->table_convsum)->row_array();
		/*print $this->db->last_query();
		exit();*/
		return ($sales[$result]) ? $sales[$result] : '-';
	}

	function getconversions()
	{
		$this->db->_reset_select();
		$this->db->select_sum('Conversions','conversions');
		$this->db->where('agent', $this->idno);
		$this->db->where($this->main_model->datelike('period',$this->date),NULL,FALSE);
		$sales = $this->db->get($table_convsum)->row_array();
		return ($sales['conversions']) ? $sales['conversions'] : '-';
	}

	function getdelivered($idno,$date)
	{
		$this->db->_reset_select();
		$this->db->select_sum('Requests_Assigned','delivered');
		$this->db->where('agent', $idno);
		$this->db->where($this->main_model->datelike('period',$date),NULL,FALSE);
		$sales = $this->db->get('rdata_lifelock_lifelock_standardreports_progrep_convsum')->row_array();
		return ($sales['delivered']) ? $sales['delivered'] : '-';
	}

	function getbilledhours($idno,$date)
	{
		$this->db->_reset_select();
		$this->db->select('SEC_TO_TIME(SUM(TIME_TO_SEC(total))) as result');
		$this->db->where('uid', $idno);
		$this->db->where($this->main_model->dateLike('date',$date,'Y-m-d'),NULL,FALSE);
		$billed = $this->db->get('tbl_billed_hours')->row_array();
		return ($billed['result']) ? $billed['result'] : '-';
	}

	function getCVN($idno,$date)
	{
		$conversions = $this->getconversions($idno,$date);
		$delivered = $this->getdelivered($idno,$date);
		return number_format((($conversions / $delivered) * 100), 2).'%';
	}

	function getGross($idno, $date)
	{
		$sales = $this->getsales($idno,$date);
		$conversions = $this->getconversions($idno,$date);
		return number_format((($sales / $conversions) * 100), 2);
	}

	function getescalations($idno, $date)
	{
		return '0';
	}

	function getcompliance($idno, $date)
	{
		return '-';
	}

	function getAvailTime($idno, $date)
	{
	    $this->db->_reset_select();
	    $this->db->select('SEC_TO_TIME(SUM(TIME_TO_SEC(Available_Time))) as result');
	    $this->db->where('Agent',$idno);
	    $this->db->where($this->main_model->dateLike('period',$date,'Y-m-d'));
		$wfu = $this->db->get('rdata_lifelock_lifelock_standardreports_operep_utilization')->row_array();
		return ($wfu['result']) ? $wfu['result'] : '-';
	}

/*	function getSPH($idno, $idnu, $date)
	{

		$sales = $this->main_model->hoursToSec($this->getbilledhours($idno,$date));
		$billed = $this->main_model->hoursToSec($this->getAvailTime($idnu,$date));
		$sph = number_format(($sales / $billed), 2) * 100;
		return ($sph) ? $sph.'%' : '-';
	}*/
	
	function getInternalQA($idno, $date)
	{
		$this->db->_reset_select();
		$this->db->select('Total_Score');
		$this->db->where('empid', $idno);
		$this->db->where($this->main_model->datelike('Monitor_Date',$date),NULL,FALSE);
		$internalQA = $this->db->get('rdata_qa_internal')->row_array();
		/*print $this->db->last_query();
		exit();*/
		return ($internalQA['Total_Score']) ? $internalQA['Total_Score'] : '-';
	}

	function getWFU($idno, $idnu, $date)
	{

		$billedhours = $this->hoursToSec($this->getbilledhours($idno,$date));
		$availtime = $this->hoursToSec($this->getAvailTime($idnu,$date));
		$wfu = number_format(($availtime / $billedhours), 2) * 100;
		/*print $this->db->last_query();
		exit();*/
		return ($wfu) ? $wfu.'%' : '-';
	}

	function getCummLinear($idno, $date, $linear)
	{
		$this->db->_reset_select();
		if ($linear == TRUE) {
			$util = 'Linear_Utilization_Rate';
		} else {
			$util = 'Cumulative_Utilization_Rate';
		}

		$this->db->select_avg($util);
		$this->db->where('Agent', $idno);
		$this->db->where($this->main_model->datelike('period',$date),NULL,FALSE);
		$rate = $this->db->get('rdata_lifelock_lifelock_standardreports_operep_utilization')->row_array();
		
		return ($rate[$util]) ? number_format($rate[$util],2) : '-';
	}

	function getHandleTime($idno, $date, $handle)
	{
		$this->db->_reset_select();
		switch ($handle) {
			case 'aht':
				$util = 'SEC_TO_TIME(AVG(TIME_TO_SEC(Avg_Handle_Time))) as handletime';
			break;
			case 'irt':
				$util = 'SEC_TO_TIME(AVG(TIME_TO_SEC(Avg_Initial_Response_Time))) as handletime';
			break;
			case 'art':
				$util = 'SEC_TO_TIME(AVG(TIME_TO_SEC(Avg_Response_Time))) as handletime';
			break;
			default:
			return 'n/a';
		}

		$this->db->select($util);
		$this->db->where('Agent', $idno);
		$this->db->where($this->main_model->datelike('period',$date),NULL,FALSE);
		$rate = $this->db->get('rdata_lifelock_lifelock_standardreports_operep_resp')->row_array();

		return ($rate['handletime']) ? date("H:i:s",strtotime($rate['handletime'])) : '-';
	}

	function getExsternalQA($idno, $date)
	{
		return ($internalQA['Total_Score']) ? $internalQA['Total_Score'] : '-';
	}

}