<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class JqGrid_developer_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	function getAllData($start,$limit,$sidx,$sord,$where,$kind,$data,$ops,$adv){
		date_default_timezone_set('Asia/Manila');
		switch ($kind){
			case 'sample_functions_data':
			case 'sample_data':
				$this->db->select('tds.developer_id actions,tds.developer_id, tds.name, tds.checkbox, tds.date, tdd.dropdown_name dropdown, tds.status');
				$this->db->join('req_developer_dropdown tdd','tdd.dropdown_id=tds.dropdown_id','left');
				$this->db->where('tds.data_status',1);
				//$this->db->limit(4);
				$sql = $this->db->get('tbl_developer_sample tds');
			break;
		}
		
		if($limit != NULL)
	    	$this->db->limit($limit,$start);
	    if($where != NULL)
	        $this->db->where($where,NULL,FALSE);
	    $this->db->order_by($sidx,$sord);
		
		$query = $sql;
	    return $query->result();
	}

	//=========================================//
	// jqGrid Function                         //
	// Edit, Add and Delete                    //
	//=========================================//
	function editRow($data){
		$data_array = array(
			'name'=>$data['name'],
			'checkbox'=>$data['checkbox'],
			'date'=>$data['date'],
			'status'=>$data['status'],
			'dropdown_id'=>$data['id'],
		);

		switch ($data['oper']) {
			case 'edit':
				$res = $this->db->update('tbl_developer_sample',$data_array,array('developer_id'=>$data['id']));
				break;
			case 'add':
				$res = $this->db->insert('tbl_developer_sample',$data_array);
				break;
			case 'del':
				$res = $this->db->update('tbl_developer_sample',array('data_status'=>0),array('developer_id'=>$data['id']));
				break;
			
			default:
				$res = true;
				break;
		}
		return $res;
	}
	
}