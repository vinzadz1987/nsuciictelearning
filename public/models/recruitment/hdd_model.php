<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*ALVIN*/
class Hdd_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}
        //center
        function getCenter(){
            $query = $this->db->query("SELECT * FROM tbl_centers");
            if($query->num_rows() > 0 ){
                foreach ($query->result_array() as $r){
                    $center_name[] = $r['center_name'];
                    $center_id[] = $r['center_id'];
                }
            }
            $data['center_name'] = $center_name;
            $data['center_id'] = $center_id;
            return $data;
        }
        //position
        function getPosition(){
            $query = $this->db->query("SELECT * FROM tbl_positions");
            if($query->num_rows() > 0 ){
                foreach ($query->result_array() as $r){
                    $position_name[] = $r['position_name'];
                    $position_id[] = $r['position_id'];
                }
            }
            $data['position_name'] = $position_name;
            $data['position_id'] = $position_id;
            return $data;
        }
        //addapplicant
        function add_applicant(){
            $firstname = strtoupper($this->input->post('firstname'));
            $middlename = strtoupper($this->input->post('middlename'));
            $lastname = strtoupper($this->input->post('lastname'));
            $query = $this->db->query("SELECT * FROM tbl_employees WHERE firstname ='".$firstname."' AND middlename='".$middlename."' AND lastname='".$lastname."'");
                if($query->num_rows() === 0 ){
                    $data = array(
                    'center_ids' => $this->input->post('center'),
                    'position_ids' => $this->input->post('position'),
                    'firstname' => $firstname,
                    'middlename' => $middlename,
                    'lastname' => $lastname,
                    'status' => 1
                  );
                  $this->db->insert("tbl_employees",$data);
                    $q = $this->db->get("tbl_employees");
                    foreach($q->result_array() as $r){
                        if($r['firstname'] == $firstname AND $r['middlename'] == $middlename AND $r['lastname'] == $lastname){
                            $data = array(
                                'emp_id' => $r['employee_id'],
                                'gender' => $this->input->post('gender'),
                                'birth_date' => $this->input->post('birthday'),
                                'mobile_num' => $this->input->post('mobile'),
                                'home_num' => $this->input->post('telno'),
                                'email' => $this->input->post('email'),
                                'address' => $this->input->post('address')
                            );
                            $this->db->insert('tbl_employee_details',$data);
                        }
                    }
                }else{
                    echo '1';
                }
                
        }
	
}