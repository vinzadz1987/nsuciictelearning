<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Teacher_model extends CI_Model {
    
   
        function get_enrolled_students($subjectid){
            $query = $this->db->query("SELECT a.*,a.status stats, b.*, b.photo as studphoto FROM tbl_enrolled_subject a, tbl_students b WHERE a.student_id=b.student_id AND a.subject_id = ".$subjectid."");                                      
            $this->result_encode($query);
        }
        
        function get_subjects(){
            $query = $this->db->query("SELECT prospectid,subject_code,units,instructor,year, course, "
                                                . "sem, students_id,description, firstname, lastname,photo, "
                                                . "SUM(CASE WHEN subject_id IS NULL THEN 0 ELSE 1 END) AS count_students,"
                                                . "SUM(CASE WHEN tbl_enrolled_subject.status = '1' THEN 0 ELSE 1 END) AS count_status"
                                        . " FROM `tbl_prospectus` "
                                                . "LEFT JOIN tbl_instructor ON instructor = instructor_id "
                                                . "LEFT JOIN tbl_enrolled_subject ON prospectid = subject_id "
                                        . " GROUP BY `description`,`firstname`,`lastname`");                                      
            $this->result_encode($query);
            
	}
        
        
        function get_teachers(){
            $query = $this->db->query("SELECT * FROM tbl_instructor");
            $this->result_encode($query);
	}
        
        function get_notes($id){
            $query = $this->db->query("SELECT * FROM tbl_notes WHERE notes_subject = '".$id."'");
            $this->result_encode($query);
        }
        
        function get_videos($id){
            $query = $this->db->query("SELECT * FROM tbl_learning_videos WHERE video_subject = '".$id."'");
            $this->result_encode($query);
        }
        
        function getMailStudents(){
            $query = $this->db->query("SELECT * FROM tbl_students WHERE student_id != '".$this->session->userdata('user_id')."' ");                                      
            $this->result_encode($query);
        }
        
        function get_students(){
            $query = $this->db->query("SELECT * FROM tbl_students WHERE student_id != '".$this->session->userdata('user_id')."' ");                                      
            $this->result_encode($query);
        }
        
        function get_select($stype){
            switch ($stype){
                case 1:
                        $query = $this->db->query("SELECT * FROM tbl_students WHERE student_id != '".$this->session->userdata('user_id')."' ");                                      
                    break;
                case 2:
                        $query = $this->db->query("SELECT * FROM tbl_instructor WHERE instructor_id != '".$this->session->userdata('user_id')."' ");                                      
                    break;
            }
            $this->result_encode($query);
        }
        
        function getMailList(){
            if($this->session->userdata('access_name') == "Student"){
                $query = $this->db->query("SELECT message_from, message_from_name, message_content,tbl_messages.date_added as messdate, tbl_messages.status, useid, student_id, photo  "
                                            . "FROM tbl_messages LEFT JOIN tbl_students ON useid = student_id ");   
                $this->result_encode($query);
            }else{
                $query = $this->db->query("SELECT message_from, message_from_name, message_content,tbl_messages.date_added as messdate, tbl_messages.status, useid, student_id, photo  "
                                            . "FROM tbl_messages LEFT JOIN tbl_students ON useid = student_id ");   
                $this->result_encode($query);
            }
        }
        
        function get_inboxmessages($stype){
            switch ($stype){
                case 1:
                    $access_type = $this->session->userdata('access_name');
                    if($access_type == "Teacher"){
                        $query = $this->db->query("SELECT message_from, message_from_name, message_content,tbl_messages.date_added as messdate,"
                                                        . "tbl_messages.status, useid, firstname, lastname "
                                                . "FROM tbl_messages LEFT JOIN tbl_students ON message_from = student_id WHERE useid = '".$this->session->userdata('user_id')."'");   
                    }                                 
                    break;
            }
            $this->result_encode($query);    
        }
        
        function get_sentmessages(){
            $query = $this->db->query("SELECT message_from, message_from_name, message_content,"
                                            . "tbl_messages.date_added as messdate, "
                                            . "tbl_messages.status, useid, student_id, tbl_students.photo as studpoto,"
                                            . " tbl_instructor.firstname as insname, tbl_instructor.lastname as inslname,"
                                            . " tbl_instructor.photo as insphoto  "
                                        . "FROM tbl_messages "
                                            . "LEFT JOIN tbl_students ON useid = student_id "
                                            . "LEFT JOIN tbl_instructor ON message_from = instructor_id"
                                        . " WHERE message_from = '".$this->session->userdata('user_id')."'");   
            $this->result_encode($query);
        }
        
        function get_quiz_time($qid){
            $query = $this->db->query("SELECT * FROM tbl_online_quiz WHERE toq_id = '".$qid."' ");                                      
            $this->result_encode($query);
        }
        
         function studentconfe_stat($confestud,$confeid){
            $query = $this->db->query("
                SELECT * 
                FROM 
                    tbl_conference_participants 
                WHERE 
                conference_id = '".$confeid."' AND participants_student ='".$confestud."'");                                      
            $this->result_encode($query);
        }
        
        function conference_list(){  
            $query = $this->db->query("
                SELECT
                    GROUP_CONCAT(conference_member) as conference_member, 
                    GROUP_CONCAT(participants_name) as participants_name,
                    conference_id_generated,
                    conference_title, 
                    conference_status, 
                    conference_date
                FROM
                    tbl_conference
                GROUP BY 
                    conference_id_generated 
            ");    
            $this->result_encode($query);
        }
                
        function approve_instructor($id){
            $query = $this->db->query("SELECT * from tbl_instructor WHERE instructid = '".$id."'")->result_array();
            foreach($query as $key => $res){
                $user_id = $res['instructor_id'];
                $access_id = '3';
                $pw = $res['instructor_password'];
                $fname = $res['firstname'];
                $lname = $res['lastname'];
                $mname = $res['middlename'];
            }
            $this->db->query("INSERT INTO tbl_login_access (user_id,access_id,password,firstname,middlename,lastname) VALUES ('".$user_id."','".$access_id."','".$pw."','".$fname."','".$mname."','".$lname."')");
            $this->db->query("UPDATE tbl_instructor SET status = '1' WHERE instructid = '".$id."'");
            echo 'CONFIRM SUCCESSFULLY';
        }
        
         
        function approve_students($param){
            $subid = explode('_', $param)[1];
            $studid = explode('_', $param)[0];
            $query = $this->db->query("SELECT * from tbl_prospectus WHERE prospectid = '".$subid."'")->result_array();
            foreach($query as $key => $res){
                $add_stud = $res['students_id'].','.$studid;
            }
            $this->db->query("UPDATE tbl_prospectus SET students_id = '".$add_stud."' WHERE prospectid = '".$subid."'");
            $this->db->query("UPDATE tbl_enrolled_subject SET status = '1' WHERE student_id = '".$studid."' AND subject_id = '".$subid."'");
	}
        
        
        function enrolled_students($studid,$subid,$studenrollid,$enrollid){
            
            $data = array(
                'students_id' => $studenrollid.','.$studid
            );
            $this->db->where('prospectid',$subid);
            $this->db->update('tbl_prospectus', $data); 
            
            $data2 = array(
                'status' => 1
            );
            $this->db->where('enrolled_id',$enrollid);
            $this->db->update('tbl_enrolled_subject', $data2); 
            
        }
        
        function add_teacher($teacherid,$firstname,$lastname,$middlename){
            $this->db->query("insert into tbl_instructor(instructor_id,firstname,middlename,lastname) values ('".$teacherid."','".$firstname."','".$middlename."','".$lastname."')");
            $this->db->query("insert into tbl_login_access(user_id,access_id,password,firstname,lastname) values ('".$teacherid."','3','*A4B6157319038724E3560894F7F932C8886EBFCF','".$firstname."','".$lastname."')");
        }
        
        function add_notes($subid,$subnote,$subdesc){
            $this->db->query("INSERT INTO tbl_notes (notes_subject, notes_title, notes_description) VALUES ('".$subid."','".$subnote."','".$subdesc."')");
        }
        
        
        function add_videos($subjectsid,$vtitle,$vdescription,$vembed){
            $this->db->query("INSERT INTO tbl_learning_videos(video_name,video_description,video_embed,video_subject) values ('".$vtitle."','".$vdescription."','".$vembed."','".$subjectsid."')");
        }
        
        function add_grades($id,$grades){
            $this->db->query("UPDATE tbl_enrolled_subject SET grades = '".$grades."' WHERE  enrolled_id = '".$id."'");
        }
        
        function add_time($hrs,$mns,$id){
            $this->db->query("UPDATE tbl_online_quiz SET test_time = '".$hrs."h:".$mns."mins' WHERE toq_id = '".$id."'");
        }
        
        function add_invites($student,$title,$images){
            $studselect = explode(',',$student);
            $uniqid = uniqid();
            foreach ($studselect as $selstud) {
                $this->db->query("INSERT INTO tbl_conference(teacher_id,conference_title,conference_member,participants_name,conference_id_generated) VALUES ('".$this->session->userdata('user_id')."','".$title."','".explode('_',$selstud)[0]."','".explode('_',$selstud)[1]."','".$uniqid."')");
            }
         }
         
         function send_message($student,$message){ //, $image
            $studselect = explode(',',$student);
            foreach ($studselect as $selstud) {
                $usename1 = explode('_', $selstud)[1];
                $useid = explode('_', $selstud)[0];
                $this->db->query("INSERT INTO tbl_messages(message_from,message_from_name,message_content,  useid) VALUES ('".$this->session->userdata('user_id')."','".$usename1."','". urlencode($message)."','".$useid."')");
            }    
         }
                
        function update_teacher($tid,$tinsid,$tfname,$tmname,$tlname){
            $this->db->query("UPDATE tbl_instructor SET instructor_id = '".$tinsid."',firstname = '".$tfname."', middlename = '".$tmname."', lastname = '".$tlname."' WHERE instructid = '".$tid."'");
        }
                
        function remove_subject($id){
            $this->db->query("delete from tbl_prospectus where prospectid = '".$id."'");
	}
        
        function remove_teachers($id){
            $this->db->query("DELETE FROM tbl_instructor WHERE instructid = '".$id."'");
	}
        
	function result_encode($q){
            $arrayindex = array();
            foreach($q->result_array() as $r){
                    $arrayindex[] = $r;
            }
            echo json_encode($arrayindex);
	}

}