<?php

class Teacher_student_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    function insertRank($stack_id,$acvn_rank){
        $this->db->query("UPDATE tbl_stack_rank  SET acvn_rank=".$acvn_rank." WHERE stack_id=".$stack_id."");
    }
    function getEmployeeData(){
        $query = $this->db->query("select * from tbl_employees");
        $this->result_encode($query);
    }
    function getEveryIdCount($id){
        $query = $this->db->query("select count(read_id) as countReadAgent,date_read from tbl_read where training_id ='".$id."'");
        $this->result_encode($query);
    }
    function getTrainingList(){
        $query = $this->db->query("select * from tbl_uploaded");
        $this->result_encode($query);
    }
    function getAgentRead($id){
        $query = $this->db->query("select b.employee_id,b.firstname,b.lastname,a.date_read from tbl_read a, tbl_employees b where training_id ='".$id."' and a.agent_id = b.employee_id");
        $this->result_encode($query);
    }
    function getAgentallread(){
        $query = $this->db->query("select b.employee_id,b.firstname,b.lastname,a.date_read, c.uploaded_filename from tbl_read a, tbl_employees b, tbl_uploaded c where a.training_id = c.uploaded_id and a.agent_id = b.employee_id");
        $this->result_encode($query);
    }
    function getAgentUnread($id){
        $query = $this->db->query("select b.employee_id,b.firstname,b.lastname from tbl_employees b where b.employee_id not in (select agent_id from tbl_read where training_id = '".$id."')");
        $this->result_encode($query);
    }
    function whoseReadAgent($trainid,$empid){
        $query_exist = $this->db->query("select * from tbl_read where agent_id = '".$empid."' and training_id = '".$trainid."'");
        if($query_exist->num_rows() > 0 ){
            //$this->db->query("insert into tbl_read (agent_id,training_id) values ('".$empid."','".$trainid."')");
        }else{
            $this->db->query("insert into tbl_read (agent_id,training_id) values ('".$empid."','".$trainid."')");
        }
    }
    function get_training_materials_select(){
        $query = $this->db->query("select * from tbl_uploaded");
        $this->result_encode($query);
    }
    function savedtrainingmaterials($data, $id){
        $this->db->query("update tbl_online_quiz set training_materials = '".$data."' where toq_id = '".$id."'");
    }
    function removeTrainingList($id){
        $this->db->query("delete from tbl_uploaded where uploaded_id = '".$id."'");
    }
    function result_encode($q){
        $arrayindex = array();
        foreach($q->result_array() as $r){
            $arrayindex[] = $r;
        }
        echo json_encode($arrayindex);
    }


}