<?php

class Uploading_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    function uploadFilesExtract($row, $ident)
    {
        switch($ident){
            case 1:
                foreach ($row as $r ) {
                    $idnumber = explode(',', $r)[0];
                    $fname = explode(',', $r)[1];
                    $lname = explode(',', $r)[2];
                    $mname = explode(',', $r)[3];
                    $gender = explode(',', $r)[4];
                    $course = explode(',', $r)[5];
                    $curryear = explode(',', $r)[6];
                    $semester = explode(',', $r)[7];
                    $table = "tbl_students";
                    $row = "student_id,firstname,lastname, middlename, gender,course,password,curr_year,semester";
                    $value = "'" . $idnumber . "','" . $fname . "','" . $lname . "','" . $mname . "','" . $gender . "','" . $course . "','1234','" .$curryear. "','" . $semester . "'";
                    $this->db->query("INSERT INTO $table ($row) VALUES ($value)");
                }
                break;
        }
    }
    function getEmployeeData(){
        $query = $this->db->query("select * from tbl_employees");
        $this->result_encode($query);
    }
    function getFileNameUpload($fileName,$uploadedPath){
        $queryCount = $this->db->query("SELECT count(*) as countallagent FROM tbl_employees");
        $row = $queryCount->row();
        $rowCount = $row->countallagent;
        $fileName_replace = str_replace(' ','_',$fileName);
        $this->db->query("insert into tbl_uploaded(uploaded_filename,uploaded_path,read_unread) values ('".$fileName_replace."','".$uploadedPath."','".$rowCount."')");
    }
    function result_encode($q){
        $arrayindex = array();
        foreach($q->result_array() as $r){
            $arrayindex[] = $r;
        }
        echo json_encode($arrayindex);
    }
}