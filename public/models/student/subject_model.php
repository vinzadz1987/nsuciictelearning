<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Subject_model extends CI_Model {
    
	function get_subjects(){
		$query = $this->db->query("
			SELECT prospectid,
			subject_code,
			units,
			year,
			sem,
			students_id,
			pre_requisites,
			lecture_hrs,
			lab_hrs,
			tbl_prospectus.date_added,
			`description`,
			`firstname`,
			`lastname`,
			photo,
			course_name,
			SUM(CASE WHEN PROSPECTUS_ID IS NULL THEN 0 ELSE 1 END) AS countlessons
			FROM `tbl_prospectus`
			LEFT JOIN tbl_instructor ON instructor = instructor_id
			LEFT JOIN tbl_lessons ON `prospectid`=PROSPECTUS_ID
			LEFT JOIN tbl_course ON course = course_id
			GROUP BY `description`,`firstname`,`lastname` 
		");                                      
		$this->result_encode($query);
	}
        
        function get_instructor(){
            $query = $this->db->query("select * from tbl_instructor");
            $this->result_encode($query);
	}
        
        function get_lessons($id){
            $query = $this->db->query("select * from tbl_lessons where prospectus_id = '".$id."'");
            $this->result_encode($query);
	}
        
        function get_lessons_count($id){
            $query = $this->db->query("select count(*) as count_lesson from tbl_lessons where prospectus_id = '".$id."'");
            $this->result_encode($query);
        }
        
        function get_downloads($lesson_id){
            $query = $this->db->query("select * from tbl_uploaded where lesson_id = '".$lesson_id."'");
            $this->result_encode($query);
        }
        
        function get_course(){
            $query = $this->db->query("select * from tbl_course");
            $this->result_encode($query);
        }
                
        function add_subject($subjectid,$description,$units,$year,$sem, $instructor,$course,$pre_requisites,$lecture_hrs,$lab_hrs){
            $query = $this->db->query("insert into tbl_prospectus(subject_code,description,units,year,course,sem, instructor, pre_requisites, lecture_hrs, lab_hrs) values ('".$subjectid."','".$description."','".$units."','".$year."','".$course."','".$sem."','".$instructor."','".$pre_requisites."','".$lecture_hrs."','".$lab_hrs."')");
            if($query){
                echo "success";
            }
        }
        
        function add_lessons($subjectsid,$name,$description){
            $this->db->query("insert into tbl_lessons(lesson_name,lesson_description,prospectus_id) values ('".$name."','".$description."','".$subjectsid."')");
        }
		
	function get_subjects_update($id){
	$query = $this->db->query("
		SELECT prospectid,
			subject_code,
			units,
			year,
			sem,
			students_id,
			tbl_prospectus.date_added,
			pre_requisites,
			lecture_hrs,
			lab_hrs,
			`description`,
			instructor_id,
			`firstname`,
			`lastname`,
			photo,
			course_name,
			SUM(CASE WHEN PROSPECTUS_ID IS NULL THEN 0 ELSE 1 END) AS countlessons
		FROM tbl_prospectus
			LEFT JOIN
			tbl_instructor ON instructor = instructor_id
			LEFT JOIN
			tbl_lessons ON `prospectid`=PROSPECTUS_ID
			LEFT JOIN
			tbl_course ON course = course_id
		WHERE prospectid=".$id." 
			GROUP BY `description`,`firstname`,`lastname`
		");                                      
		$this->result_encode($query);
	}
	
	function update_subject ($data){
		$postData = $data['data'];
		$dataVar = "
			subject_code = '".$postData['subid']. "',
			description = '".$postData['desc']. "',
			units = '".$postData['units']. "',
			year = '".$postData['year']. "',
			sem = '".$postData['sem']. "',
			instructor = '".$postData['inst']."',
			course = '".$postData['course']."',
			pre_requisites = '".$postData['pre_requisites']."',
			lecture_hrs = '".$postData['lec_hrs']."',
			lab_hrs = '".$postData['lab_hrs']."'
		";
		$query = $this->db->query("
			UPDATE
				tbl_prospectus set ".$dataVar."
			WHERE 
				prospectid = ".$postData['id']."
		");
		if($query){
			echo 'success';
		}
	}            
        function remove_subject($id){
            $this->db->query("delete from tbl_prospectus where prospectid = '".$id."'");
	}
        
	function result_encode($q){
            $arrayindex = array();
            foreach($q->result_array() as $r){
                    $arrayindex[] = $r;
            }
            echo json_encode($arrayindex);
	}

}