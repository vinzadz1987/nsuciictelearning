<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class JqGrid_student_model extends CI_Model {

	function getAllData($start,$limit,$sidx,$sord,$where,$module,$module_data){
		if($limit != NULL)
	    	$this->db->limit($limit,$start);
	    if($where != NULL)
	        $this->db->where($where,NULL,FALSE);
	    $this->db->order_by($sidx,$sord);

		switch ($module) {
			case 'roster':					
				$this->db->select('uid,att_uid, first_name, last_name,rt_id, location,position, supervisor, qa, complete_name, tenurity');
				$sql = $this->db->get('tbl_agent_roster');
            break;
            case 'student_list':
//                              $year = $this->input->post('year');
				$this->db->select('*');
//                                $this->db->where('curr_year','4');
				$sql = $this->db->get('tbl_students');
            break;
            case 'sup_stack_rank':
            	$date = date('Y-m-d');
				$date_submited = date('Y-m-1').' - '.$date;
				$date_submited = $this->main_model->datelike('date_submited',$date_submited);
				$date_ext = date('_Y_m',strtotime($date));
            	$this->db->select('te.agent_id agent_id,
            					te.supervisor,
            					(( SUM(rsc.sales_clean)+SUM(rsi.sales_ism) ) / SUM(ra.assisted) * 100) acvn,
            					(( SUM(rsc.sales_clean)+SUM(rsi.sales_ism) ) / SUM(rl.total_login) * 100) sph,
            					(( SUM(rn1_top.nps_sum)-SUM(rn1_bot.nps_sum) ) / SUM(rn_total.survey) ) nps_1,
            					(( SUM(rn2_top.nps_sum)-SUM(rn2_bot.nps_sum) ) / SUM(rn_total.survey) ) nps_2,
            					te.agent_id qa');
            	$this->db->join('(SELECT agent_id,COUNT(agent_id) sales_clean FROM rdata_sales_clean WHERE '.$date_submited.' GROUP BY agent_id ORDER BY date_uploaded) rsc','rsc.agent_id = te.agent_id','LEFT');
            	$this->db->join('(SELECT agent_id,COUNT(agent_id) sales_ism FROM rdata_sales_ism WHERE '.$date_submited.' GROUP BY agent_id ORDER BY date_uploaded) rsi','rsi.agent_id = te.agent_id','LEFT');
            	$this->db->join('(SELECT agent_id,SUM(assisted) assisted FROM rdata_assisted WHERE '.$date_submited.' GROUP BY agent_id ORDER BY date_uploaded) ra','ra.agent_id = te.agent_id','LEFT');
            	$this->db->join('(SELECT agent_id,SUM(TIME_TO_SEC(login_time))/3600 total_login FROM rdata_login WHERE '.$date_submited.' GROUP BY agent_id ORDER BY date_uploaded) rl','rl.agent_id = te.agent_id','LEFT');
            	$this->db->join('(SELECT agent_id,SUM(rank_nps_1) nps_sum FROM rdata_nps WHERE rank_nps_1 IN (9,10) AND '.$date_submited.' GROUP BY agent_id ORDER BY date_uploaded) rn1_top','rn1_top.agent_id = te.agent_id','LEFT',FALSE);
            	$this->db->join('(SELECT agent_id,SUM(rank_nps_1) nps_sum FROM rdata_nps WHERE rank_nps_1 <= 6 AND '.$date_submited.' GROUP BY agent_id ORDER BY date_uploaded) rn1_bot','rn1_bot.agent_id = te.agent_id','LEFT');
            	$this->db->join('(SELECT agent_id,SUM(rank_nps_2) nps_sum FROM rdata_nps WHERE rank_nps_2 IN (9,10) AND '.$date_submited.' GROUP BY agent_id ORDER BY date_uploaded) rn2_top','rn2_top.agent_id = te.agent_id','LEFT',FALSE);
            	$this->db->join('(SELECT agent_id,SUM(rank_nps_2) nps_sum FROM rdata_nps WHERE rank_nps_2 <= 6 AND '.$date_submited.' GROUP BY agent_id ORDER BY date_uploaded) rn2_bot','rn2_bot.agent_id = te.agent_id','LEFT');
            	$this->db->join('(SELECT agent_id,COUNT(agent_id) survey FROM rdata_nps WHERE '.$date_submited.' GROUP BY agent_id ORDER BY date_uploaded) rn_total','rn_total.agent_id = te.agent_id','LEFT');

				$this->db->where('te.status',0);
				$this->db->group_by('te.supervisor');
        		$sql = $this->db->get('tbl_employees te');

        		//print_r($this->db->last_query());exit();
            break;
            case 'agent_stack_rank':
            	$date = date('Y-m-d');
				$date_submited = date('Y-m-1').' - '.$date;
				$date_submited = $this->main_model->datelike('date_submited',$date_submited);
            	$this->db->select('te.agent_id agent_id,
            					te.supervisor,
            	 				(((rsc.sales_clean + rsi.sales_ism) / ra.assisted) * 100) acvn,
            					(((rsc.sales_clean + rsi.sales_ism) / rl.total_login) * 100) sph,
            	 				( (rn1_top.nps_sum-rn1_bot.nps_sum) / rn_total.survey ) nps_1,
            					( (rn2_top.nps_sum-rn2_bot.nps_sum) / rn_total.survey ) nps_2,
            					te.agent_id qa');
            	$this->db->select('CONCAT_WS(",", te.lastname, te.firstname) as fullname',FALSE);
            	$this->db->join('(SELECT agent_id,COUNT(agent_id) sales_clean FROM rdata_sales_clean WHERE '.$date_submited.' GROUP BY agent_id ORDER BY date_uploaded) rsc','rsc.agent_id = te.agent_id','LEFT');
            	$this->db->join('(SELECT agent_id,COUNT(agent_id) sales_ism FROM rdata_sales_ism WHERE '.$date_submited.' GROUP BY agent_id ORDER BY date_uploaded) rsi','rsi.agent_id = te.agent_id','LEFT');
            	$this->db->join('(SELECT agent_id,SUM(assisted) assisted FROM rdata_assisted WHERE '.$date_submited.' GROUP BY agent_id ORDER BY date_uploaded) ra','ra.agent_id = te.agent_id','LEFT');
            	$this->db->join('(SELECT agent_id,SUM(TIME_TO_SEC(login_time))/3600 total_login FROM rdata_login WHERE '.$date_submited.' GROUP BY agent_id ORDER BY date_uploaded) rl','rl.agent_id = te.agent_id','LEFT');
            	$this->db->join('(SELECT agent_id,SUM(rank_nps_1) nps_sum FROM rdata_nps WHERE rank_nps_1 IN (9,10) AND '.$date_submited.' GROUP BY agent_id ORDER BY date_uploaded) rn1_top','rn1_top.agent_id = te.agent_id','LEFT',FALSE);
            	$this->db->join('(SELECT agent_id,SUM(rank_nps_1) nps_sum FROM rdata_nps WHERE rank_nps_1 <= 6 AND '.$date_submited.' GROUP BY agent_id ORDER BY date_uploaded) rn1_bot','rn1_bot.agent_id = te.agent_id','LEFT');
            	$this->db->join('(SELECT agent_id,SUM(rank_nps_2) nps_sum FROM rdata_nps WHERE rank_nps_2 IN (9,10) AND '.$date_submited.' GROUP BY agent_id ORDER BY date_uploaded) rn2_top','rn2_top.agent_id = te.agent_id','LEFT',FALSE);
            	$this->db->join('(SELECT agent_id,SUM(rank_nps_2) nps_sum FROM rdata_nps WHERE rank_nps_2 <= 6 AND '.$date_submited.' GROUP BY agent_id ORDER BY date_uploaded) rn2_bot','rn2_bot.agent_id = te.agent_id','LEFT');
            	$this->db->join('(SELECT agent_id,COUNT(agent_id) survey FROM rdata_nps WHERE '.$date_submited.' GROUP BY agent_id ORDER BY date_uploaded) rn_total','rn_total.agent_id = te.agent_id','LEFT');
            	$this->db->where('status',0);
				$sql = $this->db->get('tbl_employees te');
            break;
            case 'raw_data':
            	$date = date('Y-m-d');
				$date_submited = date('Y-m-1').' - '.$date;
				$date_submited = $this->main_model->datelike('date_submited',$date_submited);
            	$this->db->select('te.agent_id agent_id,
            					te.supervisor,
            	 				(rsc.sales_clean + rsi.sales_ism) sales,
            	 				ra.assisted assisted,
            	 				rl.total_login total_login
            	 				');
            	$this->db->select('CONCAT_WS(",", te.lastname, te.firstname) as fullname',FALSE);
            	$this->db->join('(SELECT agent_id,COUNT(agent_id) sales_clean FROM rdata_sales_clean WHERE '.$date_submited.' GROUP BY agent_id ORDER BY date_uploaded) rsc','rsc.agent_id = te.agent_id','LEFT');
            	$this->db->join('(SELECT agent_id,COUNT(agent_id) sales_ism FROM rdata_sales_ism WHERE '.$date_submited.' GROUP BY agent_id ORDER BY date_uploaded) rsi','rsi.agent_id = te.agent_id','LEFT');
            	$this->db->join('(SELECT agent_id,SUM(assisted) assisted FROM rdata_assisted WHERE '.$date_submited.' GROUP BY agent_id ORDER BY date_uploaded) ra','ra.agent_id = te.agent_id','LEFT');
            	$this->db->join('(SELECT agent_id,SUM(TIME_TO_SEC(login_time))/3600 total_login FROM rdata_login WHERE '.$date_submited.' GROUP BY agent_id ORDER BY date_uploaded) rl','rl.agent_id = te.agent_id','LEFT');
            	$this->db->join('(SELECT agent_id,SUM(rank_nps_1) nps_sum FROM rdata_nps WHERE rank_nps_1 IN (9,10) AND '.$date_submited.' GROUP BY agent_id ORDER BY date_uploaded) rn1_top','rn1_top.agent_id = te.agent_id','LEFT',FALSE);
            	$this->db->join('(SELECT agent_id,SUM(rank_nps_1) nps_sum FROM rdata_nps WHERE rank_nps_1 <= 6 AND '.$date_submited.' GROUP BY agent_id ORDER BY date_uploaded) rn1_bot','rn1_bot.agent_id = te.agent_id','LEFT');
            	$this->db->join('(SELECT agent_id,SUM(rank_nps_2) nps_sum FROM rdata_nps WHERE rank_nps_2 IN (9,10) AND '.$date_submited.' GROUP BY agent_id ORDER BY date_uploaded) rn2_top','rn2_top.agent_id = te.agent_id','LEFT',FALSE);
            	$this->db->join('(SELECT agent_id,SUM(rank_nps_2) nps_sum FROM rdata_nps WHERE rank_nps_2 <= 6 AND '.$date_submited.' GROUP BY agent_id ORDER BY date_uploaded) rn2_bot','rn2_bot.agent_id = te.agent_id','LEFT');
            	$this->db->join('(SELECT agent_id,COUNT(agent_id) survey FROM rdata_nps WHERE '.$date_submited.' GROUP BY agent_id ORDER BY date_uploaded) rn_total','rn_total.agent_id = te.agent_id','LEFT');
            	$this->db->where('status',0);
				$sql = $this->db->get('tbl_employees te');
            break;
			case 'nps':
				$this->db->select("*");
				$sql = $this->db->get('tbl_nps');
		}
		$query = $sql;
	    return $query->result();
	}
        
	 function deleteAllDataFromDBTable($dbtable){
		 $this->db->query("TRUNCATE $dbtable");
	  }

	  function insert_agent($att_uid,$first_name,$last_name,$location,$position,$supervisor,$qa,$cn,$tn){
		$q = $this->db->query("insert into tbl_agent_roster (att_uid,first_name,last_name, supervisor, qa, complete_name, tenurity,location, position) values('$att_uid','$first_name','$last_name','$supervisor','$qa','$cn','$tn','$location','$position')");
		 if($q){
			 echo 'successfully created.';
		 }
	 }

	function update_agent($uid,$att_uid,$first_name,$last_name,$location,$position,$supervisor,$qa,$cn,$tn){
		$q = $this->db->query("update tbl_agent_roster set att_uid='$att_uid',first_name='$first_name',last_name='$last_name', position='$position', location='$location', supervisor='$supervisor', qa='$qa', complete_name = '$cn', tenurity = '$tn' where uid='$uid'");
		if($q){
			echo 'successfully updated.';
		}
	}
//
//	 function deleteOnlyOne($id,$table,$row){
//		 $this->db->query("delete from $table where $row=$id");
//	 }
//
//	function insertRankDATA($stack_id,$rank_name, $rank){
//		$pos = strpos($stack_id, '%');
//		if($pos === false){
//			$stackid = $stack_id;
//		}else{
//			$stackid = str_replace('%','',$stack_id);
//		}
//		$this->db->query("UPDATE tbl_stack_rank SET $rank_name=".$rank." WHERE stack_id=".$stackid."");
//	}

	function getEmployeeData(){
		$query = $this->db->query("select * from tbl_employees");
		$this->result_encode($query);
	}
	function getTrainingList(){
		$query = $this->db->query("select * from tbl_uploaded");
		$this->result_encode($query);
	}
	function getAgentRead($id){
		$query = $this->db->query("select agent_read,read_unread from tbl_uploaded where uploaded_id ='".$id."'");
		$this->result_encode($query);
	}
        
        function getStudentData(){
		$query = $this->db->query("select * from tbl_students");
		$this->result_encode($query);
	}
        
        
	function result_encode($q){
		$arrayindex = array();
		foreach($q->result_array() as $r){
			$arrayindex[] = $r;
		}
		echo json_encode($arrayindex);
	}

}