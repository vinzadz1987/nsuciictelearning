	<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Student_model extends CI_Model {
		function get_approved($id,$studid,$fname,$lname){
			$data = array(
			'status' => '0' 
			);
			$this->db->where('studids',$id);
			$this->db->update('tbl_students',$data);
			$data2 = array(
				'user_id' => $studid,
				'access_id' => '4',
				'password' => '*A4B6157319038724E3560894F7F932C8886EBFCF',
				'firstname' => $fname,
				'lastname' => $lname
			);
			$this->db->insert('tbl_login_access', $data2); 
		}

	function get_subjects(){
		$query = $this->db->query("
			SELECT prospectid,
				subject_code,
				units,
				year,
				course,
				sem,
				instructor, 
				students_id,
				`description`,
				`firstname`,
				`lastname`,
				photo,
				pre_requisites,
				lecture_hrs, 
				lab_hrs,
				tbl_enrolled_subject.grades as grades, 
				SUM(CASE WHEN PROSPECTUS_ID IS NULL THEN 0 ELSE 1 END) AS countlessons,
				tbl_enrolled_subject.status
			FROM `tbl_prospectus` 
				LEFT JOIN
				tbl_instructor ON instructor = instructor_id
				LEFT JOIN 
				tbl_lessons ON `prospectid`=PROSPECTUS_ID 
				LEFT JOIN
				tbl_enrolled_subject ON `prospectid` = subject_id
			GROUP BY
				`description`,`firstname`,`lastname`
		");
		$this->result_encode($query);
	}
		
		function enrolled_subjects($subid,$insid){
			$query_exist = $this->db->query("select * from tbl_enrolled_subject where student_id = '".$this->session->userdata('user_id')."' and subject_id = '".$subid."'");
			if($query_exist->num_rows() > 0 ){
			}else{
				$data = array(
				'student_id' => $this->session->userdata('user_id'),
				'subject_id' => $subid,
				'instructor_id' => $insid
				);
				$this->db->insert('tbl_enrolled_subject', $data); 
			}
		}

		function get_lessons($id){
		$query = $this->db->query("select * from tbl_lessons where prospectus_id = '".$id."'");
		$this->result_encode($query);
		}

		function get_subject_grades($subid){
		$query = $this->db->query("select * from tbl_enrolled_subject where student_id = '".$this->session->userdata('user_id')."' AND subject_id='".$subid."'");
		$this->result_encode($query);
		}

		function get_lessons_count($id){
		$query = $this->db->query("select count(*) as count_lesson from tbl_lessons where prospectus_id = '".$id."'");
		$this->result_encode($query);
		}

		function get_downloads($lesson_id){
		$query = $this->db->query("select * from tbl_uploaded where lesson_id = '".$lesson_id."'");
		$this->result_encode($query);
		}

		function add_subject($subjectid,$description,$units,$year,$sem, $instructor){
		$this->db->query("insert into tbl_prospectus(subject_code,description,units,year,sem, instructor) values ('".$subjectid."','".$description."','".$units."','".$year."','".$sem."','".$instructor."')");
		}

		function add_lessons($subjectsid,$name,$description){
		$this->db->query("insert into tbl_lessons(lesson_name,lesson_description,prospectus_id) values ('".$name."','".$description."','".$subjectsid."')");
		}

		function remove_subject($id){
		$this->db->query("delete from tbl_prospectus where prospectid = '".$id."'");
		}

		function get_subject_total_grades($data){
			 $querySelect = "
			 	SELECT 
			 		SUM(grades) as mygrades 
			 	FROM tbl_enrolled_subject
			 	WHERE student_id = '".$data['studid']."' ";
			 $this->result_encode($this->db->query($querySelect));

		}

		function result_encode($q){
			$arrayindex = array();
			foreach($q->result_array() as $r){
				$arrayindex[] = $r;
			}
			echo json_encode($arrayindex);
		}

	}