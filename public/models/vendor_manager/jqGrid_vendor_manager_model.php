<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class JqGrid_vendor_manager_model extends CI_Model {

	function __construct() {
		parent::__construct();
		$this->load->model('one_model','one');
		
		$this->firstday = date('Y-m-01');
		$this->lastday = date('Y-m-t');
		$this->month = $this->firstday . ' - ' .$this->lastday;
	}
       

	function getAllData($start,$limit,$sidx,$sord,$where,$kind,$data,$ops,$adv){  
		$this->month = ($ops) ? $ops : $this->month;
		
		switch ($kind){
				case 'category_setting':
					$this->db->select('a.cat_id, a.cat_id catid, a.subcat_id, a.cat_id action, b.cat_name label_name, a.subcat_name category, a.subcat_goal goal, a.subcat_pass_score pass_score');
					$this->db->join('nick_tbl_category b','a.cat_id=b.cat_id','left');
					$sql = $this->db->get('nick_tbl_sub_cat a');
				break;
				case 'vm_color':					
					$this->db->select('id,goal_type,goal_color');
					$sql = $this->db->get('nick_goal_color');
					$searchlike = 'goal_type,goal_color';
                break;
                case 'power_tc':					
					$this->db->select('a.cat_id, a.subcat_id, a.cat_id action, b.cat_name, a.subcat_name financial, a.subcat_goal goal, a.cat_id JANUARY, a.cat_id FEBRUARY, a.cat_id MARCH, a.cat_id APRIL, a.cat_id MAY, a.cat_id JUNE, a.cat_id JULY, a.cat_id AUGUST, a.cat_id SEPTEMBER, a.cat_id OCTOBER, a.cat_id NOVEMBER, a.cat_id DECEMBER, a.cat_id YTD');
					$this->db->join('nick_tbl_category b','a.cat_id=b.cat_id','left');
					$sql = $this->db->get('nick_tbl_sub_cat a');
				break;
				case 'power_tc_kpi':
					$this->db->select('c.campaign_id, c.client_id, d.campaign_name, c.kpi_name, c.campaign_id action, e.goal_value goal, c.campaign_id JANUARY, c.campaign_id FEBRUARY, c.campaign_id MARCH, c.campaign_id APRIL, c.campaign_id MAY, c.campaign_id JUNE, c.campaign_id JULY, c.campaign_id AUGUST, c.campaign_id SEPTEMBER, c.campaign_id OCTOBER, c.campaign_id NOVEMBER, c.campaign_id DECEMBER, c.campaign_id YTD');
					$this->db->join('nick_tbl_goals e','c.goal_id=e.goal_id','left');
					$this->db->join('tbl_campaign d','c.campaign_id=d.campaign_id','left');
					$this->db->order_by('d.campaign_id');
					$sql = $this->db->get('nick_tbl_client_kpi c');
				break;
				case 'business_opportunities':
					$this->db->select('business_id actions, business_id ,business_name, business_status, business_client_name, business_lob, business_service, business_channel, business_growth_new, business_total_fte, business_location_site, business_1st_hire_class, business_production_date, business_year_revenue, business_annualized_revenue');
					$sql = $this->db->get('nick_business_opportunities');
				break;
				case 'monthly_cs_review':
					$this->db->select('monthly_id actions, monthly_id ,TouchCommerce, Month_Actual, Month_Forecast, Month_Variance, YTD_Actual, YTD_Forecast, YTD_Variance');
					$sql = $this->db->get('nick_monthly_cs_review');
				break;
				case 'volumes_hours_requested':
					$this->db->select('anothersub_id actions, subcat_id, anothersub_id, anothersub_name, anothersub_name volumes, anothersub_goals goals, subcat_id JANUARY, subcat_id FEBRUARY, subcat_id MARCH, subcat_id APRIL, subcat_id MAY, subcat_id JUNE, subcat_id JULY, subcat_id AUGUST, subcat_id SEPTEMBER, subcat_id OCTOBER, subcat_id NOVEMBER, subcat_id DECEMBER, subcat_id YTD');
					$this->db->where('subcat_id',$data);
					$sql = $this->db->get('nick_tbl_anothersub_cat');
				//print_r($this->db->last_query());
				//print_r($data);
				//exit;
				break;
				case 'bonus':
					$this->db->select('bonus_id,bonus_cat_id,bonus_cat_name,bonus_name b_name,bonus_name,bonus_cat_id actions,bonus_id JANUARY,bonus_id FEBRUARY,bonus_id MARCH,bonus_id APRIL,bonus_id MAY,bonus_id JUNE,bonus_id JULY,bonus_id AUGUST,bonus_id SEPTEMBER,bonus_id OCTOBER,bonus_id NOVEMBER,bonus_id DECEMBER,bonus_id half_year,bonus_id half_year2,bonus_id YTD,bonus_id Q1,bonus_id Q2,bonus_id Q3,bonus_id Q4');
					$this->db->where('data_status',1);
					$this->db->order_by('bonus_cat_id');
					$sql = $this->db->get('nick_tbl_bonus_cat');
				break;
            }
			
		
		if($limit != NULL)
	    	$this->db->limit($limit,$start);
	    if($where != NULL)
	        $this->db->where($where,NULL,FALSE);
	    //$this->db->order_by($sidx,$sord);
		
		$query = $sql;
	    return $query->result();

	}

	//=========================================//
	// jqGrid Function                         //
	// Edit, Add and Delete                    //
	//=========================================//
	function getlabelinfo(){
		$this->db->_reset_select();
		$this->db->select('*');
		 $this->db->where('cat_id',$this->input->post('id'));
		 $res_sql = $this->db->get('nick_tbl_category');
		
		// print_r($this->db->last_query());
		// exit();
		echo json_encode($res_sql->result_array()); 
	}
	function getgoalinfo(){
		$this->db->_reset_select();
		$this->db->select('*');
		 $this->db->where('a.subcat_id',$this->input->post('id'));
		 $this->db->join('nick_tbl_category b','a.cat_id=b.cat_id','left');
		 $res_sql = $this->db->get('nick_tbl_sub_cat a');
		
		// print_r($this->db->last_query());
		// exit();
		echo json_encode($res_sql->result_array()); 
	}
	function getcolorinfo(){
		$this->db->_reset_select();
		$this->db->select('*');
		$this->db->where('id',$this->input->post('id'));
		$res_sql = $this->db->get('nick_goal_color');
		
		echo json_encode($res_sql->result_array()); 
	}
	function savecolorsetting(){
		$this->db->insert('nick_goal_color', $this->input->post()); 
		return true;
	}
	function updatecolorsetting(){
		$this->db->where('id',	$this->input->post('id'));
		$this->db->update('nick_goal_color', $this->input->post()); 
		return true;
	}
	function deletecolorsetting(){
		$this->db->delete('nick_goal_color', array('id' => $this->input->post('id'))); 
		return true;
	}
	function savelabelsetting($postdata){
		
		unset($postdata['id']);
		$label_array = array();
		$subcategory_array = array();
		foreach($postdata as $key => $val){
			$res[] = $val;
		}
		$label_array['cat_name'] = $res[0];
		$this->db->insert('nick_tbl_category', $label_array);
		
		//print_r($label_array);exit;
		
		return true;
	}
	function savecategorysetting($postdata){
		$category_array = array();
		foreach($postdata as $key => $val){
			$res[] = $val;
		}
		$category_array['cat_id'] = $res[1];;
		$category_array['subcat_name'] = $res[2];
		$category_array['subcat_goal'] = $res[3];
		$category_array['subcat_pass_score'] =$res[4];
		$this->db->insert('nick_tbl_sub_cat', $category_array);
		
		//print_r($res);exit;
		
		return true;
	}
	function savesubcategorysetting($postdata){
		$subcategory_array = array();
		foreach($postdata as $key => $val){
			$res[] = $val;
		}
		$subcategory_array['subcat_id'] = $res[2];
		$subcategory_array['anothersub_name'] =$res[3];
		$subcategory_array['anothersub_goals'] = $res[4];
		$subcategory_array['anothersub_pass_score'] =$res[5];
		$this->db->insert('nick_tbl_anothersub_cat', $subcategory_array);
		
		//print_r($res);exit;
		return true;
	}
	function updategoalsetting($postdata){
		//print_r($postdata);exit;
		$data_array = $postdata;
		unset($postdata['id']);
		$this->db->where('cat_id',	$data_array['id']);
		$this->db->update('nick_tbl_category', $postdata); 
		return true;
	}
	function deletegoalsetting($postdata){
	//print_r($postdata);exit;
		$this->db->delete('nick_tbl_category', array('id' => $postdata['id'])); 
		return true;
	}
	function editRow_cat($data){
		 $data_array = $data;
		 $data_insert = array();
		 unset($data['oper']);
		 unset($data['id']);
		foreach($data as $key => $val){
				if($val){
					$data_insert['subcat_name'] = $data['category'];
					$data_insert['subcat_goal'] = $data['goal'];
					$data_insert['subcat_pass_score'] = $data['pass_score'];
					$data_insert['cat_id'] = $data['cat_id'];
				}
		}
		//print_r($data);exit;
		if($data_insert){
			$cat_id = $data['cat_id'];
			$this->db->where('cat_id', $cat_id);
			$this->db->delete('nick_tbl_sub_cat');
			$this->db->insert('nick_tbl_sub_cat',$data_insert);
		}
		switch($data_array['oper']){
			case 'del':
					$cat_id = $data['cat_id'];
					$this->db->where('cat_id', $cat_id);
					$this->db->delete('nick_tbl_sub_cat');
			break;
		}
		
	}
	function set_bonus_data(){
		$this->db->_reset_select();
		$this->db->select('bonus_id,bonus_name,date_name,bonus_data');
		$sql = $this->db->get('nick_tbl_bonus_data');

		return $sql->result_array();
	}
	
	function get_all_client_data(){
		$this->db->_reset_select();
		$this->db->select('client_id,kpi_id,kpi_date,kpi_value');
		$sql = $this->db->get('nick_tbl_kpi_data');
		
		// print_r($this->db->last_query());
		// exit();
		return $sql->result_array();
	}

	function editRowBonus($data){
		
		$data_array = $data;
		$data_insert = array();
		unset($data['oper']);
		unset($data['id']);
		unset($data['b_name']);
		foreach($data as $key => $val){
			if($val){
				$data_insert[] = array(
					'bonus_id' => $data_array['id'],
					'bonus_name' => $data_array['b_name'],
					'date_name' => $key,
					'bonus_data' => preg_replace("/[^0-9.]/", "", $val)
					);
			}
		}
		if($data_insert){
			$this->db->delete('nick_tbl_bonus_data',array('bonus_id' => $data_array['id']));;
			if($this->db->insert_batch('nick_tbl_bonus_data',$data_insert))
				return true;
				return false;
		}
	}

	function editRow($data){
		 // print_r($data);
		 // exit;
		 $data_array = $data;
		 $data_insert = array();
		 unset($data['oper']);
		 unset($data['id']);
		foreach($data as $key => $val){
			$date = date('2014-m-01', strtotime($key));
				if($val){
					$data_insert[] = array(
					'cat_value'=> preg_replace("/[^0-9.]/", "", $val),
					'cat_date'=> $date,
					'subcat_id'=> $data_array['id']
					);
				}
		}
		if($data_insert){
			$this->db->delete('nick_tbl_cat_data', array('subcat_id' => $data_array['id']));
			$this->db->insert_batch('nick_tbl_cat_data',$data_insert);
		}
	}
	function editRowkpi($data){
		// print_r($data);
		// exit;
		$data_array = $data;
		$data_insertkpi = array();
		unset($data['oper']);
		unset($data['id']);
		foreach($data as $key => $val){
			$date = date('2014-m-01', strtotime($key));
			if($val){
					$data_insertkpi[] = array(
					'kpi_value'=>preg_replace("/[^0-9.]/", "", $val),
					'kpi_date'=>$date,
					'client_id'=>$data_array['id']);
			}
		}
		//print_r($data_insertkpi);exit();
		if($data_insertkpi){
			$this->db->delete('nick_tbl_kpi_data', array('client_id' => $data_array['id']));
			$this->db->insert_batch('nick_tbl_kpi_data',$data_insertkpi);
		}
	}
	function editRowbusiness($data){
		foreach($data as $key => $val){
			$res[] = $val;
		}
		 // print_r($res);
		 // exit;
		$data_array = $data;
		unset($data['oper']);
		unset($data['id']);
		switch($data_array['oper']){
			case 'edit':
				$business_id = $data_array['id'];
				// print_r($data_array['id']);
				// exit;
				foreach($data as $key => $val){
					if($val){
						if($this->checkbusinessid($data_array['id'])){
							$this->db->where('business_id', $data_array['id']);
							$this->db->update('nick_business_opportunities', $data); 
						}
					}
				}
			break;
			case 'add':
				$this->db->insert('nick_business_opportunities', $data);
			break;
			case 'del':
					$business_id = $data_array['id'];
					$this->db->where('business_id', $business_id);
					$this->db->delete('nick_business_opportunities');
			break;
		}
	}
	function editMonthly($data){
		foreach($data as $key => $val){
			$res[] = $val;
		}
		$data_array = $data;
		unset($data['oper']);
		unset($data['id']);
		switch($data_array['oper']){
			case 'edit':
				foreach($data as $key => $val){
					if($val){
						if($this->checkmonthlyid($data_array['id'])){
							$this->db->where('monthly_id', $data_array['id']);
							$this->db->update('nick_monthly_cs_review', $data); 
						}
					}
				}
			break;
			case 'add':
				if($val){
						$this->db->insert('nick_monthly_cs_review', $data); 
					}
			break;
			case 'del':
					$monthly_id = $data_array['id'];
					$this->db->where('monthly_id', $monthly_id);
					$this->db->delete('nick_monthly_cs_review');
			break;
		}
	}
	function editFinancials($data){
		 $data_array = $data;
		 $data_insert = array();
		 unset($data['oper']);
		 unset($data['id']);
		 unset($data['subcat_id']);
		 unset($data['YTD']);
		foreach($data as $key => $val){
			$date = date('2014-m-01', strtotime($key));
				if($val){
					$data_insert[] = array('cat_value'=>preg_replace("/[^0-9.]/", "", $val),'cat_date'=>$date,'subcat_id'=>$data_array['subcat_id'],'anothersub_id'=>$data_array['id']);
				}
		}
	
		if($data_insert){
			$this->db->delete('nick_tbl_cat_data', array('anothersub_id' => $data_array['id']));
			$this->db->insert_batch('nick_tbl_cat_data',$data_insert);
		}
	}
	function checkid($id,$date,$data=''){	
		$this->db->_reset_select();
		$this->db->select('catdata_id result');
		$this->db->where('subcat_id',$id);
		$this->db->where('cat_date',$date);
		if($data)$this->db->where('cat_value',$data);
		$sql = $this->db->get('nick_tbl_cat_data');
		
		return ($sql->result()) ? $sql->row()->result : false;
	}
	function checkidkpi($id,$date,$data=''){
		$this->db->_reset_select();
		$this->db->select('kpi_id result');
		$this->db->where('client_id',$id);
		$this->db->where('kpi_date',$date);
		if($data)$this->db->where('kpi_value',$data);
		$sql = $this->db->get('nick_tbl_kpi_data');
		
		return ($sql->result()) ? $sql->row()->result : false;
	}
	function checkbusinessid($id){	
		$this->db->_reset_select();
		$this->db->select('business_id result');
		$this->db->where('business_id',$id);
		$sql = $this->db->get('nick_business_opportunities');
		
		return ($sql->result()) ? $sql->row()->result : false;
	}
	function checkmonthlyid($id){
		$this->db->_reset_select();
		$this->db->select('monthly_id result');
		$this->db->where('monthly_id',$id);
		$sql = $this->db->get('nick_monthly_cs_review');
		
		return ($sql->result()) ? $sql->row()->result : false;
	}
	function getall_month($id,$subid){
		$this->db->_reset_select();
		$this->db->select('catdata_id, subcat_id, anothersub_id, cat_value, cat_date');
		$this->db->where('subcat_id',$id);
		if($subid)$this->db->where('anothersub_id',$subid);
		$res_sql = $this->db->get('nick_tbl_cat_data');
		
		return $res_sql->result_array();
	}
	function get_month($id){
		$this->db->_reset_select();
		$this->db->select('catdata_id, subcat_id, cat_value, cat_date');
		$this->db->where('subcat_id',$id);
		$res_sql = $this->db->get('nick_tbl_cat_data');
		
		  // print_r($this->db->last_query());
		  // exit();
		return $res_sql->result_array();
	}
	function get_month_vol_uti($id){
		$this->db->_reset_select();
		$this->db->select('catdata_id, subcat_id, anothersub_id, cat_value, cat_date');
		$this->db->where('anothersub_id',$id);
		$res_sql = $this->db->get('nick_tbl_cat_data');
		
		  // print_r($this->db->last_query());
		  // exit();
		return $res_sql->result_array();
	}
	function get_monthkpi($id){
		$this->db->_reset_select();
		$this->db->select('kpi_id, client_id, kpi_value, kpi_date');
		$this->db->where('client_id',$id);
		$res_sql = $this->db->get('nick_tbl_kpi_data');
		
		 // print_r($this->db->last_query());
		 // exit();
		return $res_sql->result_array();
	}
	function checkdate($date,$data){
		$res = '';
		foreach($data as $key=>$val){
			if(date('2014-m', strtotime($date))===date('Y-m', strtotime($val['cat_date'])))
				$res = $val['cat_value'];
		}
		return $res;
	}
	function checkdatekpi($date,$data){
		$res = '';
		foreach($data as $key=>$val){
			if(date('2014-m', strtotime($date))===date('Y-m', strtotime($val['kpi_date'])))
				$res = $val['kpi_value'];
		}
		return $res;
	}

	// Add Settings

	function addSettings($data,$field){
		switch ($field) {
			case 'owner':
				$table = 'nick_req_owner';
				$field_name = 'owner_name';
				break;
			case 'status':
				$table = 'nick_req_status';
				$field_name = 'status_name';
				break;
			case 'client':
				$table = 'tbl_accounts';
				$field_name = 'account_name';
				break;
			case 'lob':
				$table = 'nick_req_lob';
				$field_name = 'lob_name';
				break;
			case 'service':
				$table = 'nick_req_service';
				$field_name = 'service_name';
				break;
			case 'growth':
				$table = 'nick_req_growth_new';
				$field_name = 'growthnew_name';
				break;
			case 'location':
				$table = 'nick_req_site';
				$field_name = 'site_name';
				break;
		}
		if( !$this->main_model->get_table( $field_name,$table,'WHERE '.$field_name.' = "'.$data.'"','row_num' ) ){
			if( $this->db->insert($table,array( $field_name => $data )) )
				return true;
				return false;
		}else return 'Already existed';
	}

}