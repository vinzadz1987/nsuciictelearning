<?php

class Menu_model extends CI_Model {

    private $access_menus;
    
    function __construct()
    {
        parent::__construct();
        $this->load->helper('string');
        $this->config->set_item('submenu',  $CI->menu_model->showMenu());
        $this->config->set_item('parent_menu', $CI->menu_model->getParent());
        $this->config->set_item('solo_menu', $CI->menu_model->getSoloMenu());
    }
    
    public function showMenu()
    {
        if ($this->session->userdata('menu_sets') == ''):
            redirect('login');
        endif;
        
        $access_menus = explode(',',$this->session->userdata('menu_sets'));
        $query = $this->db->from('tbl_sub_menus');
        $query = $this->db->where_in('sub_id', $access_menus);
        $query = $this->db->get();

        if ($query->num_rows() > 0):
            $this->showParent();
            $this->db->close();
            return $query->result();
        endif;
    }
    
    public function showParent()
    {
        $access_menus = explode(',',$this->session->userdata('menu_sets'));
        $query = $this->db->from('tbl_sub_menus');
        $query = $this->db->where_in('sub_id', $access_menus);
        $query = $this->db->group_by('parent_id');
        $query = $this->db->get();
        //show_error($this->db->last_query());
        $result = array();
        $this->db->close();
        if ($query->num_rows() > 0):
            foreach ($query->result() as $row):
                $result[] = $row->parent_id;
            endforeach;
            $this->session->set_userdata('menu_result', $result);
            return $result;
        endif;
    }
    
    public function getSoloMenu()
    {
        $access_menus = explode(',',$this->session->userdata('menu_sets'));
        $query = $this->db->from('tbl_sub_menus');
        $query = $this->db->where('parent_id =', 0);
        $query = $this->db->where_in('sub_id', $access_menus);
        $query = $this->db->get();
        //show_error($this->db->last_query());
        $result = array();
        $this->db->close();
        if ($query->num_rows() > 0):
            foreach ($query->result() as $row):
                $result[] = $row->parent_id;
            endforeach;
            $this->session->set_userdata('menu_result', $result);
            $this->db->close();
            return $query->result();
        endif;
    }
    
    public function getParent()
    {

        $query = $this->db->from('tbl_parent_menu');
        $query = $this->db->where_in('parent_id',  $this->session->userdata('menu_result'));
        $query = $this->db->get();
        //show_error($this->session->userdata('menu_result'));
        if ($query->num_rows() > 0):
        $this->db->close();
        return $query->result();
        endif;
    }
}