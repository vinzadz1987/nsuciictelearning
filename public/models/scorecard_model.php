<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Scorecard_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}
       

	function getsupervisor(){
		$this->db->_reset_select();
		$this->db->select('Supervisor');
		$this->db->group_by('Supervisor');
		$res_sql = $this->db->get('rdata_qfn_agentlist');		
    	return $res_sql->result_array();
	}
	function getmanager(){
		$this->db->_reset_select();
		$this->db->select('Shift_Manager');
		$this->db->group_by('Shift_Manager');
		$res_sql = $this->db->get('rdata_qfn_agentlist');		
    	return $res_sql->result_array();
	}
	function getagentinfo(){
		$this->db->_reset_select();
		$this->db->select('*');
		$this->db->where('`ID_#`',$this->input->post('idnumber'));
		$res_sql = $this->db->get('rdata_qfn_agentlist');
		
    	return ($res_sql->num_rows()>0)?$res_sql->result_array():0;
	}
	function getscorecard(){
		$id = $this->input->post("idnumber");
		$scoredate = $this->input->post("scoredate");
		$account = $this->input->post("account");
		$rtlogin = $this->input->post("rtlogin");
		
		$billhours = $this->get_billed_hours($id,$scoredate,'number');
		$billhourstime = $this->get_billed_hours($id,$scoredate,'time');
		$qa = $this->get_qa($id,$scoredate,$account);
		$getwfu = $this->get_wfu($rtlogin,$scoredate,$account);
		$wfu = $this->main_model->get_percentage_time($getwfu,$billhourstime);
		$absences = $this->get_absences($id,$scoredate);
		$a360 = 0;
		switch($account){
			case "Comcast (Legacy)":
				$geta360 = $this->get_360evals($id,$scoredate)[0];
				$a360 = $geta360['a_360'];
			case "Comcast-OE":
				$sales = $this->get_sales_comcast($rtlogin,$scoredate,$account);
			break;
			case "Sylmark":
			case "WEN":
			case "Entertainment Cruises":
			case "MBSC":
			case "LifeLock":
				$sales = $this->get_salesConversionDeliverd($rtlogin,$scoredate,$account);
			break;	
			case "T-Mobile SIM":
				$sales = $this->get_sales_tMobile($rtlogin,$scoredate,$account);
			break;
			case "T-Mobile":
				$sales = $this->get_sales_tMobile($rtlogin,$scoredate,$account);
			break;
			case "Virgin Mobile":
				$sales = $this->get_sales_vMobile($rtlogin,$scoredate,$account);
			break;			
		}
		$a360 =number_format(round($a360,2),2);
		$qa = number_format(round($qa,2),2);
		
		$sph = ((int)$sales >0 && $billhours>=1)?round(($sales/$billhours),2) :0;
		$weightsph = $this->get_weight($sph,"SPH",$scoredate);
		$weight360 = $this->get_weight($a360,"360",$scoredate);
		$weightqa = $this->get_weight($qa,"QA",$scoredate);
		$weightabs = $this->get_weight($absences,"Absences",$scoredate);
		$weightlates = 10;
		$weightwfu = $this->get_weight($wfu,"WFU",$scoredate);
		
		$totalscore =$weightsph+ $weight360+$weightqa+$weightabs+$weightlates+$weightwfu;
		
		$return_array = array("sph"=>$sph,"a360"=>$a360.' %',"qa"=>$qa.' %',"wfu"=>$wfu,"abs"=>$absences,
							"weight_sph"=>number_format($weightsph,2).' %',"weight_360"=>number_format($weight360,2).' %',"weight_qa"=>number_format($weightqa,2).' %',
							"weight_abs"=>number_format($weightabs,2).' %',"weight_lates"=>number_format($weightlates,2).' %',"weight_wfu"=>number_format($weightwfu,2).' %',"totalscore"=>number_format($totalscore,2));
		$arr[] = $return_array;
		return $arr;
	}
	function get_billed_hours($id,$date,$type){
		$this->db->_reset_select();
		$this->db->select('SUM(TIME_TO_SEC(total_fix)) AS result,SEC_TO_TIME2(SUM(TIME_TO_SEC(total_fix))) AS result2');
		$this->db->where('uid',$id);
		$this->db->where($this->main_model->datelike('date',$date));
		$res_sql = $this->db->get('tbl_billed_hours');
		if($type=='num')
			return ($res_sql->num_rows()>0)?$res_sql->row()->result / 3600:0;
		else
			return ($res_sql->num_rows()>0)?$res_sql->row()->result2:0;
	}
	function get_sales_comcast($id,$date,$account){
		$this->db->_reset_select();
		$this->db->select('count(Final_Owning_Agent) AS result');
		$this->db->where('Final_Owning_Agent',$id);
		$this->db->where($this->main_model->datelike('Chat_Start_DateTime',$date));
		$res_sql = $this->db->get($this->tbl_account('others',$account));
    	return $res_sql->row()->result;
	}

	function get_sales_vMobile($id,$date,$account){
		$this->db->_reset_select();
		$this->db->select('count(Product_Name) AS result');
		$this->db->where('Agent_ID',$id);
		$this->db->where($this->main_model->datelike('Conversion_date',$date));
		$res_sql = $this->db->get($this->tbl_account('others',$account));

    	return $res_sql->row()->result;
	}
	
	function get_sales_tMobile($id,$date,$account){
		$this->db->_reset_select();
		$this->db->select('sum(Conversions) AS result');
		$this->db->where('agent',$id);
		$this->db->where($this->main_model->datelike('period',$date));
		$res_sql = $this->db->get($this->tbl_account('table_convsum',$account));
    	return $res_sql->row()->result;
	}
	function get_salesConversionDeliverd($id,$date,$account){ 
		$this->db->_reset_select();
		$this->db->select('sum(Conversions) AS conversions');
		$this->db->where('agent',$id);
		$this->db->where($this->main_model->datelike('period',$date));
		$res_sql = $this->db->get($this->tbl_account('table_convsum',$account));
    	return $res_sql->row()->conversions;
	}
	function get_360evals($id,$date){
		$this->db->_reset_select();
		$this->db->select('avg(`Avg`) AS a_360, sum(NCalls) AS no_evals');
		$this->db->where('empid',$id);
		$this->db->where($this->main_model->datelike('Date',$date));
		$res_sql = $this->db->get('rdata_qa360');
		
		return $res_sql->result_array(); 
	}
	function get_qa($id,$date,$account){
		$select = ($account!='Virgin Mobile')?'Total_score':'Total_defect';
		$this->db->_reset_select();
		$this->db->select_avg($select,'result');
		$this->db->where('empid',$id);
		$this->db->where($this->main_model->datelike('monitor_date',$date));
		$res_sql = $this->db->get($this->tbl_account('table_qa',$account));
    	return $res_sql->row()->result;
	}
	function get_wfu($id,$date,$account){		
		$this->db->_reset_select();
		$this->db->select('sec_to_time2(sum(time_to_sec(Available_Time))) AS wfu');
		$this->db->where('Agent',$id);
		$this->db->where($this->main_model->datelike('period',$date));
		$res_sql = $this->db->get($this->tbl_account('table_util',$account));
		
    	return $res_sql->row()->wfu;
	}
	function get_absences($id,$date){
		$this->db->_reset_select();
		$this->db->select('count(empid) AS result');
		$this->db->where('empid',$id);
		$this->db->where($this->main_model->datelike('date',$date));
		$res_sql = $this->db->get("tbl_awol");

    	return $res_sql->row()->result;
	}
	function get_weight($score,$type,$date){
		$this->db->_reset_select();
		$this->db->select('weight_score');
		$this->db->where('weight_type',$type);
		$this->db->where('start_score <=',$score);		
		$this->db->where('end_score >=',$score);
		$this->db->where($this->main_model->datelike('startdate_weight',$date));
		$this->db->where($this->main_model->datelike('enddate_weight',$date));
		$res_sql = $this->db->get("tbl_scorecard_weight");
	
    	return ($res_sql->num_rows>0)?$res_sql->row()->weight_score:0;
	}
	
	function tbl_account($data,$account)
    {
		$businessid = $this->getbusinessid($account);
        switch ($businessid) {
            case 2:
                $table_util = 'rdata_comcast_comcast_standardreports_operep_utilization';
                $table_convsum = 'rdata_comcast_comcast_standardreports_progrep_convsum';
                $table_resp = 'rdata_comcast_comcast_standardreports_operep_resp';
                $others = 'rdata_comcast_comcast_clienttranscript_clienttranscript';
				$table_qa = 'rdata_qa_internal_comcast';
            break;
            case 3:
                $table_util = 'rdata_comcast_comcastoe_standardreports_operep_utilization';
                $table_convsum = 'rdata_comcast_comcastoe_standardreports_progrep_convsum';
                $table_resp = 'rdata_comcast_comcastoe_standardreports_operep_resp';
                $others = 'rdata_comcast_comcast_clienttranscript_clienttranscript';
				 $table_qa = 'rdata_qa_internal_comcastoe';
            break;
            case 5: 
                $table_util = 'rdata_chazdean_chazdean_standardreports_operep_utilization';
                $table_convsum = 'rdata_chazdean_chazdean_standardreports_progrep_convsum';
                $table_resp = 'rdata_chazdean_chazdean_standardreports_operep_resp';                
				 $table_qa = 'rdata_qa_internal_chazdean'; 
            break;
            case 6:
                $table_util = 'rdata_entcruises_entcruises_standardreports_operep_utilization';
                $table_convsum = 'rdata_entcruises_entcruises_standardreports_progrep_convsum';
                $table_resp = 'rdata_entcruises_entcruises_standardreports_operep_resp';                
				 $table_qa = 'rdata_qa_internal_entcruises';
            break;
            case 7:
                $table_util = 'rdata_lifelock_lifelock_standardreports_operep_utilization';
                $table_convsum = 'rdata_lifelock_lifelock_standardreports_progrep_convsum';
                $table_resp = 'rdata_lifelock_lifelock_standardreports_operep_resp'; 
				 $table_qa = 'rdata_qa_internal_lifelock';
            break;
            case 8:
                $table_util = 'rdata_meanbeauty_meanbeauty_standardreports_operep_utilization';
                $table_convsum = 'rdata_meanbeauty_meanbeauty_standardreports_progrep_convsum';
                $table_resp = 'rdata_meanbeauty_meanbeauty_standardreports_operep_resp';  
				 $table_qa = 'rdata_qa_internal_meanbeauty';
            break;
            case 10:
                $table_util = 'rdata_sylmark_sylmark_standardreports_operep_utilization';
                $table_convsum = 'rdata_sylmark_sylmark_standardreports_progrep_convsum';
                $table_resp = 'rdata_sylmark_sylmark_standardreports_operep_resp'; 
            break;
            case 12:
                $table_util = 'rdata_tmobileus_tmoprepaid_standardreports_operep_utilization';
                $table_convsum = 'rdata_tmobileus_tmoprepaid_standardreports_progrep_convsum';
                $table_resp = 'rdata_tmobileus_tmoprepaid_standardreports_operep_resp';
				 $table_qa = 'rdata_qa_internal_tmobilesim';
            break;
            case 13:
                $table_util = 'rdata_tmobileus_tmobileus_standardreports_operep_utilization';
                $table_convsum = 'rdata_tmobileus_tmobileus_standardreports_progrep_convsum';
                $table_resp = 'rdata_tmobileus_tmobileus_standardreports_operep_resp';
                $table_qa = 'rdata_qa_internal_tmobileus';
            break;
            case 14:
                $table_util = 'rdata_virginusa_virginusa_standardreports_operep_utilization';
                $table_convsum = 'rdata_virginusa_virginusa_standardreports_progrep_convsum';
                $table_resp = 'rdata_virginusa_virginusa_standardreports_operep_resp';
                $others = 'rdata_virginusa_virginusa_clienttranscript_orderdetail';
				$table_qa = 'rdata_qa_internal_virginusa';
            break;
        }

        switch ($data) {
            case 'table_util': return $table_util; break;
            case 'table_convsum': return $table_convsum; break;
            case 'table_resp': return $table_resp; break;
            case 'others': return $others; break;
            case 'account_name': return $account_name; break;
            case 'table_qa': return $table_qa; break;
        }
    }
	function getbusinessid($account){
		switch($account){
			case "Comcast (Legacy)":$businessid = 2;break;
			case "Comcast-OE":$businessid = 3;break;
			case "WEN":$businessid = 5;break;
			case "Entertainment Cruises":$businessid = 6;break;
			case "LifeLock":$businessid = 7;break;
			case "MBSC":$businessid = 8;break;
			case "Sylmark":$businessid = 10;break;
			case "T-Mobile SIM":$businessid = 12;break;
			case "T-Mobile":$businessid = 13;break;
			case "Virgin Mobile":$businessid = 14;break;			
		}
		return $businessid;
	}
	function showmetricpreview($account){
		$this->db->_reset_select();
		$this->db->select('*');
		$this->db->where('account',$account);
		$this->db->where('stats','Active');
		$this->db->order_by('sort_id');
		$res_sql = $this->db->get('tbl_scorecard_metric');
		if($res_sql->num_rows() == 0 ){
			$this->db->where('account','Default');
			$this->db->where('stats','Active');
			$this->db->order_by('sort_id');
			$res_sql = $this->db->get('tbl_scorecard_metric');
		}
		// echo $this->db->last_query();
		echo json_encode($res_sql->result_array()); 
	}
}