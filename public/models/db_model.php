<?php

class Db_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    //====================================//
	// Use only this Functions if         //
	//  Select and Table value is fixed	  //
	//  and can determine name of query	  //
	//====================================//

    function all_of($table,$result='array',$data=''){
    	$join = '';
    	$where = '';
    	switch ($table) {
    		case 'tbl_developer_sample':
    			$where = 'WHERE developer_id = '.$data.' AND data_status = 1';
    			break;
    	}
    	return $this->main_model->get_table('*',$table,$where,$result,$join);
    }

    function some_of($ops,$result='array',$data=''){
    	//====================================//
		// Alway use where condition          //
		//  ($where)?$where:'WHERE someting'  //
        //  for posible alteration of data    //
		//  Only 5 output or bellow     	  //
		//====================================//
		$res = '';
    	switch ($ops) {
    		case 'developer_dropdown-2':
    			$where = 'WHERE data_status = 1';
    			$res = $this->main_model->get_table('dropdown_id,dropdown_name','req_developer_dropdown',$where,$result);
    			break;
    	}
    	return $res;
    }    

    function single($select,$table,$data=''){
    	$join = '';
    	$where = '';
    	switch ($table) {
    		case 'req_developer_dropdown':
    			$where = 'WHERE data_status = 1';
    			break;
            case 'tbl_billed_hours':
                $where = 'WHERE uid = "'.$data.'"';
                break;
    	}
    	return $this->main_model->get_table($select,$table,$where,'single',$join);
    }

    //Complex Query or query with join
    function query_of($ops,$result='',$whereData=''){
    	$res = '';
    	switch ($ops) {
    		case 'fcl_materials':
                $where = ' AND material_id = '.$whereData;
    			$res = $this->main_model->get_table(
                    'tfm.material_id,tfm.item_name,rfu.unit_name,tfm.description,tfm.unit_cost,tfm.ref_no',
                    'tbl_fcl_materials tfm',
                    'WHERE tfm.data_status = 1'.$where,
                    $result,
                    'LEFT JOIN req_fcl_units rfu ON rfu.unit_id=tfm.unit_id'
                    );
    			break;
            case 'tbl_goal_settings':
                $res = $this->main_model->get_table(
                    'DISTINCT a.goal_type,
                     (SELECT goal_value 
                        FROM tbl_goal_settings b 
                        WHERE b.goal_type = a.goal_type 
                        AND ( start_date <= "'.$whereData.'" AND end_date >= "'.$whereData.'" )
                        AND account = "'.$this->main_model->tbl_account('account_name').'"
                        ORDER BY date_created DESC LIMIT 1) as goal_value,
                     (SELECT pass_score 
                        FROM tbl_goal_settings b 
                        WHERE b.goal_type = a.goal_type 
                        AND ( start_date <= "'.$whereData.'" AND end_date >= "'.$whereData.'" )
                        AND account = "'.$this->main_model->tbl_account('account_name').'"
                        ORDER BY date_created DESC LIMIT 1) as pass_score
                    ',
                    'tbl_goal_settings a',
                    '',
                    $result
                    );
                break;
    	}
    	return $res;
    }

    function check_query($table,$data){
        //==============================//
        // $data must be array of       //
        //  fields and its data for     //
        //  compare                     //
        //==============================//
        $test = array();
        $where = '';
        foreach ($data as $key => $val) {
            $test[] = $key.' = "'.$val.'"';
        }
        if($test)
            $where_res = 'WHERE '.implode(' AND ', $test);

        $query = $this->db->query("SELECT 1 FROM $table $where_res");
        return $query->num_rows();
    }

    function get_this($ops){
        switch ($ops) {
            case 'business_name':
                return $this->main_model->get_table($ops,'tbl_business','WHERE business_id = "'.$this->session->userdata('business_id').'"','single');
                break;
        }
    }

}