<?php

class Generic_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
    
    function getEmpList()
    {
        $sql = 'SELECT employee_id, firstname, lastname FROM tbl_employees';
        $query = $this->db->query($sql);
        //$query = $this->db->get();
         $this->db->close();
        if ($query->num_rows() > 0):
            return $query;
        else: 
            return false;
        endif;
        
    }
}

/* 
 * Copyright (C) 2014 09th-QDSUP
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

