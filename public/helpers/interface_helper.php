<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	// ============= Independent Function ============= //

function that(){
	return $CI =& get_instance();
}

function queue_style($styles){
	$link_open = '<link rel="stylesheet" href="';
	$link_close = '" />';
	$links = '';
	if(  $styles && is_array($styles) ){
		foreach ($styles as $key => $value) {
			$links .= $link_open.$value.$link_close;
		}
	}
	echo $links;
}

function queue_script($styles){
	$link_open = '<script src="';
	$link_close = '" /></script>';
	$links = '';
	if(  $styles && is_array($styles) ){
		foreach ($styles as $key => $value) {
			$links .= $link_open.$value.$link_close;
		}
	}
	echo $links;
}

function get_view($location) {  
	if (file_exists(APPPATH.'views/'.$location.EXT)){
   		that()->load->view($location);
   	}else{
   		echo 'ERROR: '.$location.' not found.';
   	}
}

function print_pre($var){
	echo '<pre>';
	print_r($var);
	echo '</pre>';
}

function sort_array($key) {
    return function ($a, $b) use ($key) {
        return strnatcmp($a[$key], $b[$key]);
    };
}

function sort_array_desc($key) {
    return function ($a, $b) use ($key) {
        return strnatcmp($b[$key], $a[$key]);
    };
}


	// ========================== Ace Dependent Function ========================== //
