<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (! function_exists('checklogin')) // by Wilson & Rey
{	 
    function checklogin(){
        
		$CI =& get_instance();
        if (!$CI->session->userdata('logged_in')){
            redirect('/login');
        }
		clear_cache();
		
    }
	
    function clear_cache(){
		$CI =& get_instance();
        $CI->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
        $CI->output->set_header("Pragma: no-cache");
    }
	
}