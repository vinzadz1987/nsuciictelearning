<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (! function_exists('display_children'))
{
    function display_children($parent, $level) {
		/* START : Get current page link */
		$page_link = explode('/',$_SERVER['REQUEST_URI']);
		foreach($page_link as $key => $val){
			if($key == 0 || $key == 1){
				unset($page_link[$key]);
			}
		}
		
		$page_link = implode('/',$page_link);
		//echo $page_link;
		/* END : Get current page link */
		
    	$CI =& get_instance();
        $menus = explode(',',$CI->session->userdata('menu_sets'));

       $myStr = '';
       $i = 0;
       foreach($menus as $m){
       		$myStr .= "'$m'";
       		if(isset($menus[$i+1])){
       			$myStr .= ",";
       		}
       		$i++;
       }

		$menus = $myStr;
        $sql = "SELECT a.sub_id, a.name, a.url_link, a.menu_icon, Deriv1.Count FROM `tbl_sub_menus` a LEFT OUTER JOIN (SELECT parent_id, COUNT(*) AS Count FROM `tbl_sub_menus` GROUP BY parent_id) Deriv1 ON a.sub_id = Deriv1.parent_id WHERE a.sub_id IN (".$menus.") AND a.parent_id='".$parent."'";
		$query = $CI->db->query($sql);

		
		if ($query->num_rows <= 0) return;
		
		if($parent == 0){
			echo "<ul class='nav nav-list'>";

		}elseif($parent > 0){
			echo "<ul class='submenu'>";
		
		}
		
	    foreach ($query->result() as $row) {
	    	//$active = ($row->url_link == $CI->uri->segment(1)) ? 'active open hsub' : '';

	    	if($parent == 0){	
				//$ul =  "<ul class='nav nav-list'>";
				$i_class = $row->menu_icon;			
			}elseif($parent > 0){	
				//$ul =  "<ul class='submenu''>";
				$i_class = 'fa-caret-right';
			}
			
			// echo $ul;
			if ($row->Count > 0) {
				if($row->url_link == $page_link && $level === 2){
					$li_open = 'hsub open';
					
				}else
					$li_open = '';
				
				echo "<li class='$li_open'><a class='dropdown-toggle' href=''><i class='menu-icon fa $i_class'></i><span class='menu-text'>". $row->name . "</span><b class='arrow fa fa-angle-down'></b></a>";
				display_children($row->sub_id, $level + 1);
				echo "</li>";
			} elseif ($row->Count==0) {
				if($row->url_link == $page_link && $level === 2){
					$li_open = 'hsub open';
				}else
					$li_open = '';
				echo "<li class='$li_open'><a href='" . $row->url_link . "'><i class='menu-icon fa $i_class'></i><span class='menu-text'>" .  $row->name ."</span></a></li>";
			} else;

	    }
		 echo "</ul>";
    }
    
}

if (! function_exists('show_campaign'))
{
    function show_campaign() {
		$CI =& get_instance();
		$sql = "SELECT campaign_id, campaign_name FROM tbl_campaign";
		$query = $CI->db->query($sql);

		return $query->result();
	}
}

if (! function_exists('show_business'))
{
    function show_business($campaign_id) {
	$CI =& get_instance();
	$sql = "SELECT business_id, business_name FROM tbl_business WHERE campaign_id ='$campaign_id'";
	$query = $CI->db->query($sql);
	return $query->result();
	}
    
}
if (! function_exists('default_campaign'))
{
    function default_campaign($param) {
		$CI =& get_instance();
		$sql = "SELECT campaign_id, campaign_name FROM tbl_campaign";
		$query = $CI->db->query($sql);

		$i = 0;
		$default = array();
		foreach ($query->result() as $r) {
			if($i == 0) {
				$default = array($r->campaign_id,$r->campaign_name);
			}
			$i++;
		}
		if($param == 'id'){
			return $default[0];
		}else if($param == 'name'){
			return $default[1];
		}	
		
	}
}
if (! function_exists('default_business'))
{
    function default_business($param,$campaign_id) {
		$CI =& get_instance();
		$sql = "SELECT business_id, business_name FROM tbl_business WHERE campaign_id ='$campaign_id'";
		$query = $CI->db->query($sql);

		$i = 0;
		$default = array();
		foreach ($query->result() as $r) {
			if($i == 0) {
				$default = array($r->business_id,$r->business_name);
			}
			$i++;
		}
		if($param == 'id'){
			return $default[0];
		}else if($param == 'name'){
			return $default[1];
		}	
		
	}
}