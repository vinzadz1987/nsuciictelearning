$(document).on('click','#addformsubmit',function(){	
	var account 			= $('#account').val(); 
	var agent_id 			= $( "#agent_id option:selected" ).val()
	var coaching_date 		= $('#coaching_date').val(); 
	var sup_id 				= $( "#sup_id option:selected" ).val()
	var coaching_type 		= $( "#coaching_type option:selected" ).val()
	var csat_score 			= $('#csat_score').val(); 
	var ext_score 			= $('#ext_score').val(); 
	var overall_summary 	= $('#overall_summary').val(); 
	var chat_id 			= $( "#chat_id option:selected" ).val()
	var area_of_success 	= $('#area_of_success').val(); 
	var area_of_development = $('#area_of_development').val(); 
	var agent_action_plan 	= $('#agent_action_plan').val(); 
	//alert(account);
		if(account=="" || agent_id=="" || coaching_date=="" || csat_score=="" || ext_score==""){
			alert('Please input all fields');
		}else{
			$.post( "qa/coaching_forms/coachingforms_save", { 'account':account, agent_id: agent_id, coaching_date: coaching_date, sup_id: sup_id, coaching_type:coaching_type, csat_score:csat_score, ext_score:ext_score, overall_summary:overall_summary, chat_id:chat_id, area_of_success:area_of_success, area_of_development:area_of_development, agent_action_plan:agent_action_plan}, function( data ) {
				if(data == "success"){
					alert('User has been added');
					document.location.reload();
				}
			}, "html");
			
		}
	});