/**
 * Created by aado on 9/16/2015.
 */
    jQuery(function($) {
        document.getElementById("uploadBtn").onchange = function () {
            document.getElementById("uploadFile").value = this.value;
        };
        $('#uploadExcel').click(function(){
            uploadDataExcel();
        });

        $('#grid-table').wd_jqGrid({
            url: 'agent/jqGrid_agent_ctrl/load_data',
            module: 'nps',
            colNames: ['','CHAT ID','AGENT ID','SUBMIT DATE','RECOMMEND RANK NPS 1','RECOMMEND RANK NPS 1'],
            colModel: [	{name:'nps_id',index:'nps_id', width:0, sorttype:"int",hidden:true},
                {name:'chatid',index:'chatid',width:60, align:'center'},
                {name:'agentid',index:'agentid', width:100, align: 'center'},
                {name:'submit_date',index:'submit_date', width:80, align: 'center'},
                {name:'recommend_rank_nps1',index:'recommend_rank_nps1', width:50, align: 'center'},
                {name:'recommend_rank_nps2',index:'recommend_rank_nps2', width:50, align: 'center'}
            ],
            sortname: 'nps_id',
            caption: false,
            height: 400,
            altRows: true,
            ondblClickRow: function(rowid,iRow,iCol,e){
                var rowData = $(this).jqGrid('getRowData',rowid);
                //console.log(rowData.uid);
            },
            loadComplete: function(data){
                var grid = $(this);
                setTimeout(function(){
                    styleCheckbox(grid);
                    updateActionIcons(grid);
                    updatePagerIcons(grid);
                    enableTooltips(grid);
                }, 0);
                var iCol = $.getColumnIndexByName(grid,'actions');
                grid.find(">tbody>tr.jqgrow>td:nth-child(" + (iCol + 1) + ")");
            }
        });
    });

function uploadDataExcel(){
    var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.csv|.txt)$/;
    if (regex.test($("#uploadBtn").val().toLowerCase())) {
        if (typeof (FileReader) != "undefined") {
            var reader = new FileReader();
            reader.onload = function (e) {
                var rows = e.target.result.split("\n");
                var resultRows = [];
                for (var i = 0; i < rows.length; i++) {
                    if(rows[i]!==""){
                        resultRows.push(rows[i]);
                    }
                }
                $.ajax({
                    url:'uploading/uploadFiles/nps',
                    type:'post',
                    data:{data: resultRows},
                    success:function(res){
                        $("#grid-table").jqGrid('setGridParam',{datatype:'json'}).trigger('reloadGrid');
                    }
                });
            }
            reader.readAsText($("#uploadBtn")[0].files[0]);
        } else {
            alert("This browser does not support HTML5.");
        }
    } else {
        alert("Please upload a valid CSV file.");
    }
}
