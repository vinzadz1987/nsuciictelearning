/* jqGrid header wrap */
var _grid_model_loaded = '';
var _grid_model_data = '';
var _grid_model_berfore = '';

$(document).ready(function(){
/*=========================================================================================================
												EVENT LISTENERS 											
==========================================================================================================*/

//====================================== START: rememberTab by Rey ======================================
	baseUrl = "";
	baseUrl =  window.location.href.split('/?')[0]; 
	$('.rememberTab').on('click',function(){ 
		var stateObject = {};
		tabId = $(this).attr('href').substring(1,$(this).attr('href').length);
		window.history.pushState(stateObject,'',baseUrl+'/?tabRef='+tabId);
	});
	var hasParam = (document.URL).indexOf("tabRef") > -1; 
		if(hasParam == true){
			tab = getUrlParameters("tabRef", "", true);	
			$('.rememberTab').each(function(){
				if($(this).attr('href') == '#'+tab){
					$(this).parent().addClass('active').siblings().removeClass('active');
				}
			});
			
			$('.rememberTab').parent().parent().next().children('.tab-pane').each(function(){
				if($(this).attr('id') == tab){
					$(this).addClass('active');
				}else {
					$(this).removeClass('active');
				}
			});
		}	
//---------------------------------------- END: rememberTab ---------------------------------------------


//====================================== START: Resize grid on browser resized by Rey ======================================
jQuery(window).bind('resize', function() {
	targetContainer = $('.ui-widget-content').parent();
	targetGrid = $('.ui-jqgrid-view');
    // Get width of parent container
    var width = jQuery(targetContainer).attr('clientWidth');
    if (width == null || width < 1){
        // For IE, revert to offsetWidth if necessary
        width = jQuery(targetContainer).attr('offsetWidth');
    }
    width = width - 2; // Fudge factor to prevent horizontal scrollbars
    if (width > 0 &&
        // Only resize if new width exceeds a minimal threshold
        // Fixes IE issue with in-place resizing when mousing-over frame bars
        Math.abs(width - jQuery(targetGrid).width()) > 5)
    {
        jQuery(targetGrid).setGridWidth(width);
        jQuery(targetGrid).children('.ui-jqgrid-bdiv').setGridWidth(width);
    }

}).trigger('resize');
//---------------------------------------- END: Resize grid on browser resized ---------------------------------------------
/*
//============================== START of the idle timer plugin by Rey ====================================
$('body').append('<div class="hide" id="timerDialog"  title="Your session is about to expire!">\
					<p>\
						<span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 50px 0;"></span>\
						You will be logged off in <span id="dialog-countdown" style="font-weight:bold"></span> seconds.\
					</p>\
					<p>Do you want to continue your session?</p>\
				 </div>\
				<div id="dialog-confirm" class="hide">\
					<div class="alert alert-info bigger-110">\
						These items will be permanently deleted and cannot be recovered.\
					</div>\
					<div class="space-6"></div>\
					<p class="bigger-110 bolder center grey">\
						<i class="ace-icon fa fa-hand-o-right blue bigger-120"></i>\
						Are you sure?\
					</p>\
				</div>'
);
//override dialog's title function to allow for HTML titles
$.widget("ui.dialog", $.extend({}, $.ui.dialog.prototype, {
	_title: function(title) {
		var $title = this.options.title || '&nbsp;'
		if( ("title_html" in this.options) && this.options.title_html == true )
			title.html($title);
		else title.text($title);
	}
}));

// setup the dialog
var dialog = $( "#timerDialog" ).removeClass('hide').dialog({
		modal: true,
		autoOpen: false,
		closeOnEscape: true,
		draggable: false,
		resizable: false,
		title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='ace-icon fa fa-check'></i> Your session is about to expire!</h4></div>",
		title_html: true,
		open: function(event, ui){
			  $(".ui-dialog-titlebar-close").hide();
		    },
		buttons: [ 
			{
				text: "Continue",
				"class" : "btn btn-xs",
				click: function() {
					$( this ).dialog( "close" ); 
				} 
			},
			{
				text: "Logoff",
				"class" : "btn btn-primary btn-xs",
				click: function() {
					$.idleTimeout.options.onTimeout.call(this);
					$( this ).dialog( "close" ); 
				} 
			}
		]
	});
			
var $countdown =  $("#dialog-countdown");
$.idleTimeout('#timerDialog', 'div.ui-dialog-buttonpane button:first', {
	failedRequests: 1,
	idleAfter: 900,
	pollingInterval: 10,
	keepAliveURL: 'sessions/keep_alive',
	serverResponseEquals: 'OK',
	onTimeout: function(){
		window.location = "login/dologout";
	},
	onIdle: function(){
		$(this).dialog("open");
	},
	onCountdown: function(counter){
		$countdown.html(counter); // update the counter
	}
});
//----------------------------- END of the idle timer plugin ---------------------------------------------
*/
});



/*=========================================================================================================
|																										  |
|										  FUNCTIONS 													  |
|																										  |
==========================================================================================================*/
/*==================================================================
					Get URL Parameters by Rey
====================================================================*/
function getUrlParameters(parameter, staticURL, decode){ 
	var currLocation = (staticURL.length)? staticURL : window.location.search;
	
	parArr = currLocation.split("?")[1].split("&");
	returnBool = true;
		
	for(var i = 0; i < parArr.length; i++){
		parr = parArr[i].split("=");
		if(parr[0] == parameter){
			return (decode) ? decodeURIComponent(parr[1]) : parr[1];
			returnBool = true;
		}else{
			returnBool = false;            
		}
	}

	if(!returnBool) return false;  
}
/*==================================================================
			Load Campaign Names on Dropdown Button  by Rey
====================================================================*/
function loadCampaign(obj){	
	var _html = ''; var _btn = ''; var ul = ''; var li = ''; var defaultVal = new Object;
	$.each(obj.campaign,function(key, value){ 		
		li +="<li>"+
				"<a href='javascript:void(0)' id='"+value.campaign_id+"' class='dropdown_campaign_select'>"+value.campaign_name+"</a>"+
			 "</li>";
	});
	
	_btn += "<button id='"+obj.default_campaign_id+"' data-toggle='dropdown' class='button_campaign_select btn btn-white btn-sm dropdown-toggle' data-toggle='dropdown'>"+obj.default_campaign_name+	
	"<i class='ace-icon fa fa-angle-down icon-on-right'></i>"+
	"</button>";
	
	ul += "<ul id='' class='dropdown-menu dropdown-info dropdown-menu-right'>";
	
	_html += _btn; _html += ul; _html += li; _html += "</ul>";
	$('#campaign-name').html(_html);	
}
/*==================================================================
			Load Business Names on Dropdown Button  by Rey
====================================================================*/
function loadBusiness(obj){ 
	var _html = ''; var _btn = ''; var ul = ''; var li = ''; var defaultVal = new Object; 
	
	var arrowDown = (obj.business.length > 1) ? "<i class='ace-icon fa fa-angle-down icon-on-right'></i>" : "";
	var disabledBtn = (obj.business.length == 1) ? "disabled" : "";

	$.each(obj.business,function(key, value){ 
		li += "<li>"+
			  "<a href='javascript:void(0)' id='"+value.business_id+"' class='dropdown_business_select'>"+value.business_name+"</a>"+
			  "</li>";
	});

	_btn += "<button id='"+obj.default_business_id+"' data-toggle='dropdown' class='"+disabledBtn+ " button_business_select btn btn-white btn-sm dropdown-toggle' data-toggle='dropdown'>"+obj.default_business_name+	
	arrowDown+
	"</button>";
	
	ul += "<ul id='' class='dropdown-menu dropdown-info dropdown-menu-right'>";
	_html += _btn; _html += ul; _html += li; _html += "</ul>";
	$('#business-name').html(_html);	
}
/*==================================================================
			Get Base URL  by Klaus
====================================================================*/
function BaseURL() {
   return location.protocol + "//" + location.hostname + 
      (location.port && ":" + location.port) + "/reports/";
}
/*==================================================================
		Trigger Hide and Show for Advance Search  by Klaus
====================================================================*/
function advanceSearch(ops){ 
	if(ops=='close'){
		$('#ace-settings-btn, #ace-settings-box').removeClass('open');
	}else{
		$('#ace-settings-btn, #ace-settings-box').addClass('open');
	}
}
/*==================================================================
						Ajax Dynamic  by Klaus
====================================================================*/
function ajax_dynamic(url,location,data,type,ops){	
	var ops = ops || 'val';
	var type = type || 'json';
	$.ajax({
		url: url,
		dataType:type,
		type:'POST',
		data: data,
		success:function(res){
			($(location).html(res)=='val')?$(location).val(res):$(location).html(res);
		},
		error:function(res){
			return false;
		}
	});
}
/*==================================================================
				  Dynamic Select Options by Klaus
====================================================================*/
function selectOption(ops,location,selected,type,data){	
	$.ajax({
		url: BaseURL()+'main_ctrl/ajaxMake_select',
		data:{ops:ops,selected:selected,type:type,data:data},
		type:'POST',
		success:function(res){
			$(location).html(res);
		}
	});
}
/*==================================================================
				  jqGrid Control by Klaus
====================================================================*/
function jqGrid_control(model){	
	$.ajax({
        url: model.container_url,
        type: 'POST',
        data: {
        	data_case: model.data_case,
        	data: model.data,
        	ops: model.ops,
        	adv: model.adv,
        	adv2: model.adv2,
        	headers: model.headers,
        	names: model.names,
        	index: (model.index=='true')?model.names:model.index,
        	widths: model.widths,
        	sortname: model.sortname,
        	searchable: model.searchable,
        	pager: (model.pager) ? true : false,
        	content_name: model.content_name
        },
        success: function(data){
			var obj = $.parseJSON(data);
			jqgrid_models = {
				'table': model.table,
				'pager': model.pager,
				'content_name': model.content_name,
				'gridlocation':  model.gridlocation,
				'sortname': model.sortname,
				'postData': obj.postData,
				'colNames': obj.colnames,
				'colModel': obj.colmodel,
				'autoresize': (model.autoresize)?model.autoresize:true,
				'autoHeight': (model.autoHeight)?model.autoHeight:false				
			}
			jqgrid_models['ops'] = model.jqGridOptions;
          	make_grid(jqgrid_models);
        }
    });
}
/*==================================================================
				jqGrid Functions create Grid by Klaus    
								WARNING!!!                   
					Overide function at View only 
							
====================================================================*/
function make_grid(model) {
	
	if(model.autoresize){
		//resize to fit page size
		$(window).on('resize.jqGrid', function () {
			$(model.table).jqGrid( 'setGridWidth', $(".grid_wrapper").width() );
	    })
	    //resize on sidebar collapse/expand
		var parent_column = $(model.table).closest('[class*="col-"]');
		$(document).on('settings.ace.jqGrid' , function(ev, event_name, collapsed) {
			if( event_name === 'sidebar_collapsed' || event_name === 'main_container_fixed' ) {
				$(model.table).jqGrid( 'setGridWidth', parent_column.width() );
			}
	    })
	}

	if(model.autoHeight){
		//resize to fit page size
		$(window).on('resize.jqGrid', function () {
			$(model.table).jqGrid( 'setGridHeight', window.innerHeight - 325 );
	    })
	}
	$(model.table).jqGrid({
		url:model.gridlocation,
		postData: model.postData,
		colNames: model.colNames,
		colModel: model.colModel,
		sortname: model.sortname,
		pager: (model.pager)?model.pager:false,
		mtype : (model.ops.mtype)?model.ops.mtype:"post",
		datatype: (model.ops.datatype)?model.ops.datatype:"json",            	
		rowNum: (model.ops.rowNum)?model.ops.rowNum:20,
		rownumbers : model.ops.rownumbers,
		rowList: (model.ops.rowList)?model.ops.rowList:[10,30,50],
		pgtext: (model.ops.pgtext) ? (model.ops.pgtext=='null') ? null : model.ops.pgtext : 'Page {0} of {1}',  
		pgbuttons:((model.ops.pgbuttons)?((model.ops.pgbuttons=='true')?true:false):true),  
		width: (model.ops.width)?model.ops.width:'600',
		loadonce: (model.ops.loadonce)?model.ops.loadonce:1,
		shrinkToFit: ((model.ops.shrinkToFit)?((model.ops.shrinkToFit=='true')?true:false):true),
		scrollrows: ((model.ops.scrollrows)?((model.ops.scrollrows=='true')?true:false):true),
		autowidth: ((model.ops.autowidth)?((model.ops.autowidth=='true')?true:false):true),
		height: (model.ops.height)?model.ops.height:300,
		subGrid: model.ops.subGrid,
		subGridOptions: model.ops.subGridOptions, 
		subGridRowExpanded: model.ops.subGridRowExpanded,
		sortorder: (model.ops.sortorder)?model.ops.sortorder:'asc',
		viewrecords: ((model.ops.viewrecords)?((model.ops.viewrecords=='true')?true:false):true),
		gridview: ((model.ops.gridview)?((model.ops.gridview=='true')?true:false):true),
		multiselect: model.ops.multiselect,
		editurl: model.ops.editurl,
		cellEdit: ((model.ops.cellEdit)?((model.ops.cellEdit=='true')?true:false):false),
		cellsubmit: (model.ops.cellsubmit) ? model.ops.cellsubmit : '',
		altRows: ((model.ops.altRows)?((model.ops.altRows=='true')?true:false):false),
		multiboxonly: ((model.ops.multiboxonly)?((model.ops.multiboxonly=='true')?true:false):false),
		userDataOnFooter: ((model.ops.userDataOnFooter)?((model.ops.userDataOnFooter=='true')?true:false):false),
		footerrow: ((model.ops.footerrow)?((model.ops.footerrow=='true')?true:false):false),
		grouping: ((model.ops.grouping)?((model.ops.grouping=='true')?true:false):false),
		groupingView: model.ops.groupingView,
		//toolbar: [true,"top"],
		caption: model.ops.caption,
		scroll: (model.ops.scroll)?model.ops.scroll:0,
		//scrollOffset: (model.ops.scrollOffset)?model.ops.scrollOffset:1,
		toppager: ((model.ops.toppager)?((model.ops.toppager=='true')?true:false):false),
		serializeGridData:model.ops.serializeGridData,
		col : {
			caption: "Show/Hide Columns",
			bSubmit: "Submit",
			bCancel: "Cancel"
		},
		rowattr: function (rd) {return rowattr_global(rd);},
		loadComplete: function(data) {
					var table = this;
					setTimeout(function(){
						styleCheckbox(table);
						updateActionIcons(table);
						updatePagerIcons(table);
						enableTooltips(table);
						inlineEdit(table);
						//only this function can be use globaly.
						loadComplete_global(model,data,this);
						
						makeHideShowColumns(model.table,model.colNames,model.colModel);	
					}, 0);
				},
		onSelectRow: function(rowId,status,e){onSelectRow_global(model,rowId,status,e);},
		onSelectAll: function(aRowids,status){onSelectAll_global(model,aRowids,status);},
		ondblClickRow: function(rowId,iRow,iCol,e){ondblClickRow_global(model,rowId,iRow,iCol,e);},	  
		onSortCol: function(index,iCol,sortorder){onSortCol_global(model,index,iCol,sortorder);},
		afterEditCell: function(rowid,cellname,value,iRow,iCol){afterEditCell_global(model,rowid,cellname,value,iRow,iCol);},
		onCellSelect: function(rowid,iCol,cellcontent,e){onCellSelect_global(model,rowid,iCol,cellcontent,e);},
		gridComplete: function(){gridComplete_global(model);},
		beforeRequest: function(){beforeRequest_global(model);}
		//multikey: (model.multikey)?model.multikey:"ctrlKey"
		/**
		,
		grouping:true, 
		groupingView : { 
		 groupField : ['name'],
		 groupDataSorted : true,
		 plusicon : 'fa fa-chevron-down bigger-110',
		 minusicon : 'fa fa-chevron-up bigger-110'
		},
		*/
	});

	
	$(window).triggerHandler('resize.jqGrid');//trigger window resize to make the grid get the correct size
	//switch element when editing inline
	function aceSwitch( cellvalue, options, cell ) {
		setTimeout(function(){
			$(cell) .find('input[type=checkbox]')
				.addClass('ace ace-switch ace-switch-5')
				.after('<span class="lbl"></span>');
		}, 0);
	}
	//enable datepicker
	function pickDate( cellvalue, options, cell ) {
		setTimeout(function(){
			$(cell) .find('input[type=text]')
					.datepicker({format:'yyyy-mm-dd' , autoclose:true}); 
		}, 0);
	}

	//navButtons
	jQuery(model.table).jqGrid('navGrid',model.pager,
		{ 	//navbar options
			edit: ((model.ops.toolbarEdit)?((model.ops.toolbarEdit=='true')?true:false):false),
			editicon : 'ace-icon fa fa-pencil blue',
			add: ((model.ops.toolbarAdd)?((model.ops.toolbarAdd=='true')?true:false):false),
			addicon : 'ace-icon fa fa-plus-circle purple',
			del: ((model.ops.toolbarDel)?((model.ops.toolbarDel=='true')?true:false):false),
			delicon : 'ace-icon fa fa-trash-o red',
			search: ((model.ops.toolbarSearch)?((model.ops.toolbarSearch=='true')?true:false):false),
			searchicon : 'ace-icon fa fa-search orange',
			refresh: ((model.ops.toolbarRefresh)?((model.ops.toolbarRefresh=='true')?true:false):false),
			refreshicon : 'ace-icon fa fa-refresh green',
			view: ((model.ops.toolbarView)?((model.ops.toolbarView=='true')?true:false):false),
			viewicon : 'ace-icon fa fa-search-plus grey',
		},
		{
			//edit record form
			closeAfterEdit: ((model.ops.closeAfterEdit)?((model.ops.closeAfterEdit=='true')?true:false):true),
			closeOnEscape: ((model.ops.closeOnEscape)?((model.ops.closeOnEscape=='true')?true:false):true),
			//width: 700,
			recreateForm: true,
			beforeShowForm : function(e) {
				var form = $(e[0]);
				form.closest('.ui-jqdialog').find('.ui-jqdialog-titlebar').wrapInner('<div class="widget-header" />')
				style_edit_form(form);
			},
			afterSubmit: function(response,postdata){edit_afterSubmit_global(model.table,response,postdata);return [response,postdata];},
		},
		{
			//new record form
			//width: 700,
			closeAfterAdd: ((model.ops.closeAfterAdd)?((model.ops.closeAfterAdd=='true')?true:false):true),
			recreateForm: true,
			viewPagerButtons: false,
			beforeShowForm : function(e) {
				var form = $(e[0]);
				form.closest('.ui-jqdialog').find('.ui-jqdialog-titlebar')
				.wrapInner('<div class="widget-header" />')
				style_edit_form(form);
			},
			afterSubmit: function(response,postdata){add_afterSubmit_global(model.table,response,postdata);return [response,postdata];},
			addfunc: function(){
				alert('111');
			}
		},
		{
			//delete record form
			recreateForm: true,
			beforeShowForm : function(e) {
				var form = $(e[0]);
				if(form.data('styled')) return false;
				
				form.closest('.ui-jqdialog').find('.ui-jqdialog-titlebar').wrapInner('<div class="widget-header" />')
				style_delete_form(form);
				
				form.data('styled', true);
			}
		},
		{
			//search form
			recreateForm: true,
			afterShowSearch: function(e){
				var form = $(e[0]);
				form.closest('.ui-jqdialog').find('.ui-jqdialog-title').wrap('<div class="widget-header" />')
				style_search_form(form);
			},
			afterRedraw: function(){
				style_search_filters($(this));
			}
			,
			multipleSearch: ((model.ops.multipleSearch)?((model.ops.multipleSearch=='true')?true:false):true),
			/**
			multipleGroup:true,
			showQuery: true
			*/
		},
		{
			//view record form
			recreateForm: true,
			beforeShowForm: function(e){
				var form = $(e[0]);
				form.closest('.ui-jqdialog').find('.ui-jqdialog-title').wrap('<div class="widget-header" />')
			}
		}
	)

	jqGrid_extension(model.table,model);

	//enable search/filter toolbar
	//jQuery(grid_selector).jqGrid('filterToolbar',{defaultSearch:true,stringResult:true})
	//jQuery(grid_selector).filterToolbar({});

	/*if(model.filter == null || model.filter == false)
		$(model.table).jqGrid('filterToolbar',{stringResult: true, searchOnEnter: false});*/
	                                                                                                                                                                
}

function style_form_update_buttons(form){
	var buttons = form.next().find('.EditButton .fm-button');
	buttons.addClass('btn btn-sm').find('[class*="-icon"]').hide();//ui-icon, s-icon
	buttons.eq(0).addClass('btn-primary').prepend('<i class="ace-icon fa fa-check"></i>');
	buttons.eq(1).prepend('<i class="ace-icon fa fa-times"></i>')
	
	buttons = form.next().find('.navButton a');
	buttons.find('.ui-icon').hide();
	buttons.eq(0).append('<i class="ace-icon fa fa-chevron-left"></i>');
	buttons.eq(1).append('<i class="ace-icon fa fa-chevron-right"></i>');
}


function style_delete_form(form) {
	var buttons = form.next().find('.EditButton .fm-button');
	buttons.addClass('btn btn-sm btn-white btn-round').find('[class*="-icon"]').hide();//ui-icon, s-icon
	buttons.eq(0).addClass('btn-danger').prepend('<i class="ace-icon fa fa-trash-o"></i>');
	buttons.eq(1).addClass('btn-default').prepend('<i class="ace-icon fa fa-times"></i>')
}

function style_search_filters(form) {
	form.find('.delete-rule').val('X');
	form.find('.add-rule').addClass('btn btn-xs btn-primary');
	form.find('.add-group').addClass('btn btn-xs btn-success');
	form.find('.delete-group').addClass('btn btn-xs btn-danger');
}
function style_search_form(form) {
	var dialog = form.closest('.ui-jqdialog');
	var buttons = dialog.find('.EditTable')
	buttons.find('.EditButton a[id*="_reset"]').addClass('btn btn-sm btn-info').find('.ui-icon').attr('class', 'ace-icon fa fa-retweet');
	buttons.find('.EditButton a[id*="_query"]').addClass('btn btn-sm btn-inverse').find('.ui-icon').attr('class', 'ace-icon fa fa-comment-o');
	buttons.find('.EditButton a[id*="_search"]').addClass('btn btn-sm btn-purple').find('.ui-icon').attr('class', 'ace-icon fa fa-search');
}

function beforeDeleteCallback(e) {
	var form = $(e[0]);
	if(form.data('styled')) return false;
	
	form.closest('.ui-jqdialog').find('.ui-jqdialog-titlebar').wrapInner('<div class="widget-header" />')
	style_delete_form(form);
	
	form.data('styled', true);
}

function beforeEditCallback(e) {
	var form = $(e[0]);
	form.closest('.ui-jqdialog').find('.ui-jqdialog-titlebar').wrapInner('<div class="widget-header" />')
	style_edit_form(form);
}



//it causes some flicker when reloading or navigating grid
//it may be possible to have some custom formatter to do this as the grid is being created to prevent this
//or go back to default browser checkbox styles for the grid
function styleCheckbox(table) {
/**
	$(table).find('input:checkbox').addClass('ace')
	.wrap('<label />')
	.after('<span class="lbl align-top" />')


	$('.ui-jqgrid-labels th[id*="_cb"]:first-child')
	.find('input.cbox[type=checkbox]').addClass('ace')
	.wrap('<label />').after('<span class="lbl align-top" />');
*/
}


//unlike navButtons icons, action icons in rows seem to be hard-coded
//you can change them like this in here if you want
function updateActionIcons(table) {
	/**
	var replacement = 
	{
		'ui-ace-icon fa fa-pencil' : 'ace-icon fa fa-pencil blue',
		'ui-ace-icon fa fa-trash-o' : 'ace-icon fa fa-trash-o red',
		'ui-icon-disk' : 'ace-icon fa fa-check green',
		'ui-icon-cancel' : 'ace-icon fa fa-times red'
	};
	$(table).find('.ui-pg-div span.ui-icon').each(function(){
		var icon = $(this);
		var $class = $.trim(icon.attr('class').replace('ui-icon', ''));
		if($class in replacement) icon.attr('class', 'ui-icon '+replacement[$class]);
	})
	*/
}

function inlineSaveReload(table){			
	$('.editable').on('keypress', function(e){
		 var _key = e.which || e.keyCode;
		 if (_key == 13) { 
			//e.preventDefault(); 
		
			reloadJqGrid(table);
	}			
	})
	
	$('.ui-inline-save').click(function(e){
		e.preventDefault();
		reloadJqGrid(table);
	})
}

function inlineEdit(table){
	$('.ui-inline-edit').click(function(e){
		e.preventDefault();
		styleCheckbox(table);
	});
}

//replace icons with FontAwesome icons like above
function updatePagerIcons(table) {
	var replacement = 
	{
		'ui-icon-seek-first' : 'ace-icon fa fa-angle-double-left bigger-140',
		'ui-icon-seek-prev' : 'ace-icon fa fa-angle-left bigger-140',
		'ui-icon-seek-next' : 'ace-icon fa fa-angle-right bigger-140',
		'ui-icon-seek-end' : 'ace-icon fa fa-angle-double-right bigger-140'
	};
	$('.ui-pg-table:not(.navtable) > tbody > tr > .ui-pg-button > .ui-icon').each(function(){
		var icon = $(this);
		var $class = $.trim(icon.attr('class').replace('ui-icon', ''));
		
		if($class in replacement) icon.attr('class', 'ui-icon '+replacement[$class]);
	})
}

function enableTooltips(table) {
	$('.navtable .ui-pg-button').tooltip({container:'body'});
	$(table).find('.ui-pg-div').tooltip({container:'body'});
};

var idsOfSelectedRows = [];
function updateIdsOfSelectedRows(id,isSelected){
	if(id!='clear'){
		var index = $.inArray(id, idsOfSelectedRows);
	    if (!isSelected && index >= 0) {
	        idsOfSelectedRows.splice(index, 1); // remove id from the list
	    } else if (index < 0) {
	        idsOfSelectedRows.push(id);
	    }
	}else idsOfSelectedRows = [];
};

function footerData_sum(id,fields){
	var gridTable = $(id);
	$.each( fields, function(k,v){
		gridVal = gridTable.jqGrid('getCol', v, false, 'sum');
		gridData = {};
		gridData[v] = (gridVal != 0 && gridVal) 
			? ( gridVal % 1 != 0 ) ? gridVal.toFixed(2) : gridVal
			: ' ';
		gridTable.jqGrid('footerData','set', gridData);
	});	
};
function footerData_avg(id,fields){
	var gridTable = $(id);
	$.each( fields, function(k,v){
		gridVal = gridTable.jqGrid('getCol', v, false, 'avg');
		gridData = {};
		gridData[v] = (gridVal != 0 && gridVal) ? gridVal.toFixed(2)+' %' : ' ';
		gridTable.jqGrid('footerData','set', gridData);
	});	
};

function footerData_avg_tag(id,fields){
	var gridTable = $(id);
	$.each( fields,function(key,val){
		gridVal = gridTable.jqGrid('getCol', val);
		var totalVal = 0;
		var count = 0;
		$.each( gridVal,function(k,v){
			var val_data = parseInt($(v).text());
			if( val_data != 0 && val_data ){
				totalVal += val_data;
				count++;
			}
		});
		total_data = totalVal/count;
		gridData = {};
		gridData[val] = (total_data != 0 && total_data) ? total_data.toFixed(2)+' %' : ' ';
		gridTable.jqGrid('footerData','set', gridData);
	})
}

function footerData_time(id,fields){
	var gridTable = $(id);

	$.each( fields,function(key,val){
		gridVal = gridTable.jqGrid('getCol', val);
		var totalVal = 0;
		var count = 0;
		$.each( gridVal,function(k,v){
			time = v.split(':');
			if( time.length == 3 ){
				t1 = ( time[0] ) ? parseInt(time[0])*3600 : 0;
				t2 = ( time[1] ) ? parseInt(time[1])*60 : 0;
				t3 = ( time[2] ) ? parseInt(time[2]) : 0;
				totalVal += (t1+t2+t3);
				if(totalVal)
					count++;
			}
		});
		gridData = {};
		var sec_time = new secToTime(totalVal/count);
		time_total = sec_time.toHHMMSS();
		gridData[val] = (time_total != 0 && time_total && time_total != 'NaN:NaN:NaN' ) ? time_total: ' ';
		gridTable.jqGrid('footerData','set', gridData);
	})
}

function footerData_percent(id,field,first,second){
	var gridTable = $(id);
	var totalVal = 0;
	first_val = gridTable.jqGrid('getCol', first, false, 'sum');
	second_val = gridTable.jqGrid('getCol', second, false, 'sum');
	totalVal = (first_val/second_val)*100;
	gridData = {};	
	gridData[field] = (totalVal != 0 && totalVal) ? totalVal.toFixed(2)+' %' : ' ';
	gridTable.jqGrid('footerData','set', gridData);
}

function footerData_quatient(id,field,first,second){
	var gridTable = $(id);
	var totalVal = 0;
	first_val = gridTable.jqGrid('getCol', first, false, 'sum');
	second_val = gridTable.jqGrid('getCol', second, false, 'sum');
	totalVal = (first_val/second_val);
	gridData = {};	
	gridData[field] = (totalVal != 0 && totalVal) ? totalVal.toFixed(2) : ' ';
	gridTable.jqGrid('footerData','set', gridData);
}

function secToTime(time){
	this.sec_num = time;
}

secToTime.prototype.toHHMMSS = function () {
    var sec_num = this.sec_num; // don't forget the second param
    var hours   = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num % 3600) / 60);
    var seconds = Math.round(sec_num % 60);

    if (hours   < 10) {hours   = "0"+hours;}
    if (minutes < 10) {minutes = "0"+minutes;}
    if (seconds < 10) {seconds = "0"+seconds;}
    var time    = hours+':'+minutes+':'+seconds;
    return time;
}

function footerColor(type,field){
	value = $('.footrow td[aria-describedby="grid_table_'+field+'"]').html();
	$.ajax({
		url: 'reports/jqGrid_kpi_ctrl/get_footerColor',
		type: 'POST',
		data: {value:value,type:type,date:sDate},
		dataType: 'json',
		success: function( res ){
			switch( res ){
				case 'goal':
					$('.footrow td[aria-describedby="grid_table_'+field+'"]').addClass('bg-green');
					$('.footrow td[aria-describedby="grid_table_overall_'+field+'"]').addClass('bg-green');
					break;
				case 'pass':
					$('.footrow td[aria-describedby="grid_table_'+field+'"]').addClass('bg-orange');
					$('.footrow td[aria-describedby="grid_table_overall_'+field+'"]').addClass('bg-orange');
					break;
				case 'fail':
					$('.footrow td[aria-describedby="grid_table_'+field+'"]').addClass('bg-red');
					$('.footrow td[aria-describedby="grid_table_overall_'+field+'"]').addClass('bg-red');
					break;
			}
		}
	})
}

/*==================================================================
				jqGrid Functions initializer by Klaus        	
						       WARNING!!!                          	
				        Donot use this functions             	
				      Overide function at View only       	
											
				      If Function has (include this)      	
				         include function in view            	
=====================================================================*/
function ondblClickRow_global(){};
function loadComplete_global(model,data,thisData){
	//include this if use multiselect
    $.each(idsOfSelectedRows,function(i,v){
		jQuery(model.table).jqGrid('setSelection', idsOfSelectedRows[i], false);
	})
	//end include
	_grid_model_loaded = model;
};
function onSelectRow_global(model,aRowids,status){
	//include this if use multiselect
	updateIdsOfSelectedRows(aRowids,status);
	//end include
};
function onSelectAll_global(model,aRowids,status){
	//include this if use multiselect
    $.each(aRowids,function(i,v){
		updateIdsOfSelectedRows(aRowids[i],status);
	})
	//end include
};
function rowattr_global(){};
function jqGrid_extension(){};
function onSortCol_global(){};
function afterEditCell_global(){};
function onCellSelect_global(){};
function gridComplete_global(){};
function beforeRequest_global(){};
function edit_afterSubmit_global(model,response,postdata){
	//Reload Grid after submit.
	$(model.table).jqGrid('setGridParam',{datatype:'json'}).trigger('reloadGrid');
};
function add_afterSubmit_global(model,response,postdata){
	//Reload Grid after submit.
	$(model.table).jqGrid('setGridParam',{datatype:'json'}).trigger('reloadGrid');
};

function getSortID(table,sort){
	var arr = [];
	if($(table).getRowData().length != 0){
		$.each($(table).getRowData(), function(i,v){
			arr.push(v[sort]);
		});
	}else return 0;
	return Math.max.apply(null, arr);
}

function compareData(table,sort,ops,id,quantity){
	var newRow = $(table).getRowData();
	var qty = '';
	var rowID = $(table).getDataIDs();
	var row = 0;
	for(var x=0;x<$(table).getDataIDs().length;x++){
		if($(table).getDataIDs().length!=0){
			if(newRow[x][sort]==id){
				row = rowID[x];
				qty = parseFloat(quantity)+parseFloat(newRow[x]['quantity']);
				break;
			}
		}
	}
	if(ops=='row')
		return row;
		return qty;
}

function searchOnGrid(url,table,kind,gridData){
	clearSearchOnGrid(table);
	$('#'+table).jqGrid('setGridParam',{postData:gridData,datatype:'json',url:url+"/load_jq_data/"+kind}).trigger("reloadGrid");
}
function clearSearchOnGrid(table){
	var grid = $('#'+table);
	grid.jqGrid('setGridParam',{search:false});
	var postData = grid.jqGrid("getGridParam", "postData");   
	$.extend(postData,{filters:""});
		for (k in postData) {
			if (k == "_search")
				postData._search = false;
			else if(k == "data" || k == "adv"){
				try {
					delete postData[k];
				} catch (e) { }
			}
			
		}
    grid.trigger("reloadGrid",[{page:1}]);
}
function setLabelColor(table,data,forClass){
	var res = data.split(',');
	$.each(res,function(i,v){
		$("#"+table).jqGrid('setLabel',v, '', forClass);
	});	
}

function jqGrid_caption(data){
	$.ajax({
		url: 'main_ctrl/jqGrid_caption',
		type: 'POST',
		data: data,
		dataType: 'json',
		success: function(res){
			$("#"+data.grid).jqGrid('setCaption',res);
		}
	})
}

function jqGrid_caption_return(data){
	return $.ajax({
		url: 'main_ctrl/jqGrid_caption',
		type: 'POST',
		async: false,  
		data: data
	});
}

function reloadJqGrid(table){
	$(table).jqGrid('setGridParam',{datatype:'json'}).trigger('reloadGrid');
}

/*===============================================================================================================
									End	JqGrid Functions
================================================================================================================*/	

Array.prototype.max = function() {
  return Math.max.apply(null, this);
};

Array.prototype.min = function() {
  return Math.min.apply(null, this);
};

//override dialog's title function to allow for HTML titles
function dialogWidget(){
	$.widget("ui.dialog", $.extend({}, $.ui.dialog.prototype, {
		_title: function(title) {
			var $title = this.options.title || '&nbsp;'
			if( ("title_html" in this.options) && this.options.title_html == true )
				title.html($title);
			else title.text($title);
		}
	}));
}

function dialogBox(model,button){
	var buttons = button||'';
	dialogWidget();

	var box = (model.type=='url') ? $(document.createElement("div")).load(model.data) : $("#"+model.data).removeClass('hide');
	box.dialog({
		modal: model.modal||true,
		title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='ace-icon fa "+model.title_icon+"'></i> "+model.title+"</h4></div>",
		title_html: model.title_html||true,
		width: model.width||300,
		height: model.height||'auto',
		resizable: model.resizable||false,
		buttons: model.buttons,
		close: function(event,ui){
			if( model.type=='url' ){
				$(this).remove();
				$("ui.dialog").empty();
			}
		}
	})
}
function showDialog(model,button){
	var buttons = button||'';
	dialogWidget();
	$('body').append("<div class='modalDialog'></div>");
	
	$.ajax({
		url: model.location,
		type: 'POST',
		data: {data:model.data,data_case:model.data_case},
		dataType: 'json',
		success: function(res){
			$('.modalDialog').dialog({
				modal: model.modal||true,
				title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='ace-icon fa "+model.title_icon+"'></i> "+model.title+"</h4></div>",
				title_html: model.title_html||true,
				width: model.width||300,
				height: model.height||'auto',
				resizable: model.resizable||false,
				buttons: model.buttons,
				close: function(event,ui){
					dialogClose(event,ui,this,model.closex);
				}
			})
			$('.modalDialog').html(res);
		}
	});	
}

$.get = function(name){
   if(name=(new RegExp('[?&]'+encodeURIComponent(name)+'=([^&]*)')).exec(location.search))
      return decodeURIComponent(name[1]);
}

$.getFolder = function( pathname ){
	switch( pathname ){
		case 'base': id = 1; break;
		case 'sub': id = 3; break;
		default: id = 2; break;
	}

	// check if error
	var error = ( (id == 2 && pathname != 'main') && pathname == '' ) ? true:false;
	if( error )
		return false;
	// end check error

	var origin = window.location.origin+'/';
	var pathArray = window.location.pathname.split( '/' );
	
	return pathArray[id];
}

$.fn.modalBox = function( options ){
	var that = $(this),
		selector = that.selector,
		ajax = ( that.length > 0 ) ? false : true;


	if( ajax && selector == '.ajaxModal' ){
		var el = document.createElement('div');
		modal_box = $(el);			
		modal_box.addClass('ajaxModal');
		$('body').append(modal_box);
		that = modal_box;
	}
	//override dialog's title function to allow for HTML titles
	$.widget("ui.dialog", $.extend({}, $.ui.dialog.prototype, {
		_title: function(title) {
			var $title = this.options.title || '&nbsp;'
			if( ("title_html" in this.options) && this.options.title_html == true )
				title.html($title);
			else title.text($title);
		}
	}));	
	
	var	settings = $.extend({
        // These are the defaults.
        modal: true,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='ace-icon fa fa-check'></i> jQuery UI Dialog</h4></div>",
        title_html: true,
        resizable: false,
        draggable: false,
        width: 300,
        position: { my: 'top', at: 'top+150' },
        height: 'auto',
        close: function(event,ui){
        	if( ajax && selector == '.ajaxModal' )
    			$.removeDialog(event,ui,modal_box)
        	else
				$.closeDialog();
		},
		modal_view: '',
		modal_data: '',
		modal_controller: ''
    }, options );

	// Pre set buttons
	var btn_array = [],
		btn_cancel = {},
		btn_ok = {};

	// save buttons in btn_array
	if( settings.buttons ){
		btn_array = $.merge( btn_array,settings.buttons );
	}

	// Pres set buttons for faster call
	if( settings.button_cancel ){
		var btn_color = settings.button_cancel_color || '';
		btn_cancel = {
						text: settings.button_cancel,
						"class" : "btn_cancel_mdl btn btn-minier "+btn_color,
						click: function() {
							$( this ).dialog( "close" ); 
						} 
					}
		btn_array.push(btn_cancel);
	}
	
	if( settings.button_ok ){
		var btn_color = settings.button_ok_color || 'btn-primary';
		btn_ok = {
					text: settings.button_ok,
					"class" : "btn_ok_mdl btn btn-minier "+btn_color,
					click: function() {} 
				}
		btn_array.push(btn_ok);
	}

	// Push buttons to btn_array first to prevent replacement of default or old value in buttons array.
	$.extend(settings, {
		buttons: btn_array
	});
	// END Pre set buttons

	if( ajax && selector == '.ajaxModal' ){
		// if ajaxModal is use
		var controller = ( settings.modal_controller ) ?  settings.modal_controller+'/' : $.getFolder()+'/';
		data_ctrl = {
			modal_data: settings.modal_data,
			modal_view: controller+'modals',
			view: settings.modal_view
		}

 		$.post( 'modal_ctrl', data_ctrl, function(res){
 			$('.ajaxModal').html(res);
 		});
 	}
 	if( ! settings.title ){
 		that.dialog(settings).siblings('.ui-dialog-titlebar').remove();
 	}

    return that.removeClass('hide').dialog(settings);
}

$.removeDialog = function(event,ui,modal_box){
	$(modal_box).remove();
	$(".ui.dialog").empty();
	$('.ui-dialog-content').dialog('close');
}

$.closeDialog = function(){
	$('.ui-dialog-content').dialog('close');
}

function dialogClose(event,ui,thisData,close){
	$(thisData).remove();
	$("ui.dialog").empty();
}

function closeDialog(){
	$('.ui-dialog-content').dialog('close');
}

function reloadGrid(table){
	$(table).jqGrid('setGridParam',{datatype:'json'}).trigger('reloadGrid');
}

/*===============================================================================================================
									Start highChart Functions
================================================================================================================*/	

/*=====================================================
	default data setting
	chart:id of div for chart
	data_case: use for switch condition
	data: any data needed for chart
	ops: option/operation for data
	adv: advance option or other function
======================================================*/
function highChart(data){
	$.ajax({
		url: 'main_ctrl/setHighChart',
		type: 'POST',
		data: data,
		dataType: 'json',
		success: function(res){
			make_highChart(res);
		}
	})
}
/*==================================================================
					Make High Chart by Klaus
====================================================================*/
function make_highChart(model){
	$('#'+model.chartName).highcharts({
        chart: {
            type: (model.chartType)?model.chartType:'column',
            zoomType: (model.chartZoomType)?model.chartZoomType:'',
            height: (model.chartHeight)?model.chartHeight:'400',
        },
        title: {
            text: (model.titleText)?model.titleText:''
        },
        subtitle: {
            text: (model.subtitleText)?model.subtitleText:''
        },
        xAxis: {
        	labels: { enabled: ((model.xAxisEnabled)?((model.xAxisEnabled=='true')?true:false):true) },
            categories: (model.xAxisCategories)?model.xAxisCategories:''
        },
        yAxis: {
        	labels: { enabled: ((model.yAxisEnabled)?((model.yAxisEnabled=='true')?true:false):true) },
            min: (model.yAxisMin)?model.yAxisMin:'',
            title: {
                text: (model.yAxisTitleText)?model.yAxisTitleText:''
            }
        },
        credits: {
        	enabled : ((model.creditsEnabled)?((model.creditsEnabled=='true')?true:false):false),
        },
        legend: {
        	width: (model.legendWidth)? model.legendWidth : '150',
        	enabled : ((model.legendEnabled)?((model.legendEnabled=='true')?true:false):true),
        	layout: (model.legendLayout) ? model.legendLayout : 'vertical',
        	align: (model.legendAlign) ? model.legendAlign : 'right',
        	verticalAlign: (model.legendVerticalAlighn) ? model.legendVerticalAlighn :'middle',
        	borderWidth: (model.legenBorderWidth) ? model.legenBorderWidth : 0
        },
        navigator: {
        	enabled : ((model.nagvigatorEnabled)?((model.nagvigatorEnabled=='true')?true:false):false),
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
            footerFormat: '</table>',
            shared: ((model.tooltipShared)?((model.tooltipShared=='true')?true:false):true),
            useHTML: ((model.tootltipUseHTML)?((model.tootltipUseHTML=='true')?true:false):true)
        },
        exporting: {
		         enabled: ((model.exportingEnabled)?((model.exportingEnabled=='true')?true:false):false),
		},
        plotOptions: {
            column: {
                pointPadding: (model.columnPointPadding)?model.columnPointPadding:0.2,
                borderWidth: (model.columnBorderWidth)?model.columnBorderWidth:0
            },
            line: {
            	dataLabels: {
            		enabled: ((model.lineDataLabelsEnabled)?((model.lineDataLabelsEnabled=='true')?true:false):false),
            	},
            	enableMouseTracking: (model.lineEnableMouseTracking) ? model.lineEnableMouseTracking : true
            }
        },
        series: (model.series)?model.series:''
    });
}

function highStock(data){
	$.ajax({
		url: 'main_ctrl/setHighStock',
		type: 'POST',
		data: data,
		dataType: 'json',
		success: function(res){
			make_highStock(res);
		}
	})
}

function make_highStock(model){
	var container = $('#'+model.containerID).width();
	$( window ).resize(function() {
		container = $('#'+model.containerID).width();
	});

	$('#'+model.containerID).highcharts('StockChart',{
        chart: {
            type: (model.chartType)?model.chartType:'line'
        },
		rangeSelector : {
			inputEnabled: container > (model.containerTreshold) ? model.containerTreshold : 480,
			selected : (model.rangeSelected) ? model.rangeSelected : 5,
			enabled : ((model.rangeSelectorEnabled)?((model.rangeSelectorEnabled=='true')?true:false):false),
		},
		title : {
			text : (model.titleText) ? model.titleText : ''
		},
		credits: {
        	enabled : ((model.creditsEnabled)?((model.creditsEnabled=='true')?true:false):false),
        },
        navigator: {
        	enabled : ((model.nagvigatorEnabled)?((model.nagvigatorEnabled=='true')?true:false):false),
        },
        scrollbar : {
        	enabled : ((model.scrollbarEnabled)?((model.scrollbarEnabled=='true')?true:false):false),
        },
        legend: {
        	enabled : ((model.legendEnabled)?((model.legendEnabled=='true')?true:false):true),
        	layout: (model.legendLayout) ? model.legendLayout : 'vertical',
        	align: (model.legendAlign) ? model.legendAlign : 'right',
        	verticalAlign: (model.legendVerticalAlighn) ? model.legendVerticalAlighn :'middle',
        	borderWidth: (model.legenBorderWidth) ? model.legenBorderWidth : 0
        },
		series : (model.seriesOptions) ? model.seriesOptions : ''

    });
}
/*====================================== End	highChart Functions =============================================
									


/*===============================================================================================================
											Modified Functions
================================================================================================================*/

function activate_chosen(){
    $('.chosen-select').chosen({allow_single_deselect:true}); 
    //resize the chosen on window resize
    $(window).on('resize.chosen', function() {
        var w = $('.chosen-select').parent().width();
        $('.chosen-select').next().css({'width':w});
    }).trigger('resize.chosen');
}
/*==================================================================
					Dropdown Local by Klaus
====================================================================*/
function dropDown_local(id,data,value,selected,type,first_data){
    dropDown_data = {
        data: data,
        value: value,
        type: type || 'option',
        selected: selected || '',
        first_data: first_data || ''
    }
    $.ajax({
        url: 'main_ctrl/get_dropDown_local',
        type: 'POST',
        data: dropDown_data,
        dataType: 'json',
        success: function(res){
            $('#'+id).html(res);
            dropDown_local_function();
        }
    })
}
function dropDown_local_function(){};

/*==================================================================
			Custom Widget Toolbar of jqGrid by Rey
====================================================================*/
function setWidgetToolbarItems(setting){	
	/* set export to file */
	if(setting.exportToFile){	
		/* set widget-toolbar div */
		var exportfile = setting.exportToFile;
		var _tblName = exportfile.excel[0].table;
		tblNameNoHash = _tblName.substring(1,_tblName.length);	
		
		if($('#gview_'+tblNameNoHash+' .ui-jqgrid-titlebar .widget-toolbar').size() == 0){	// append once
			$('#gview_'+tblNameNoHash+' .ui-jqgrid-titlebar').prepend('<div class="widget-toolbar" style="margin-right:5px;">\
																	  <div class="widget-menu">\
																			<a data-toggle="dropdown" data-action="settings" href="#" title="Actions">\
																				<i class="ace-icon fa fa-bars orange2"></i>\
																			</a>\
																			<ul class="dropdown-menu dropdown-menu-left dropdown-light-blue dropdown-caret dropdown-closer">\
																			</ul>\
																		</div>\
																	</div>'); 
		}
		
		exportToExcel(exportfile.excel[0],exportfile.excel[1]);
	}
	
	/* set generic Button */
	if(setting.widgetButton){
		if($('.ui-jqgrid-titlebar .widget-toolbar').size() == 0){	// append once
			$('.ui-jqgrid-titlebar').append('<div class="widget-toolbar" style="margin-right:30px;">\
											<div class="widget-menu">\
												<a data-toggle="dropdown" data-action="settings" href="#" title="Actions">\
													<i class="ace-icon fa fa-bars orange2"></i>\
												</a>\
												<ul class="dropdown-menu dropdown-menu-right dropdown-light-blue dropdown-caret dropdown-closer">\
												</ul>\
											</div>\
										</div>'); 
		}
		showButtons(setting.widgetButton);
	}
	//widgetToolbarAction_global();
}

/*==================================================================
	Export to Excel Using jqGrid Object Data for PhpExcel by Rey
====================================================================*/
function exportToExcel(obj,data){ 	
	var _headers = obj.colNames;		
	var _tblName = obj.table;
	tblNameNoHash = _tblName.substring(1,_tblName.length);	
	$('.widget-menu ul').html('<li class="">\
											<a href="javascript:void(0)" target="_new" data-table_id="'+tblNameNoHash+'" class="export_to_file" data-toggle="tab">\
											<i class="pink ace-icon fa fa-book bigger-110"></i>\
											Export to Excel</a>\
										</li>');	
	$('.export_to_file').on('click',function(){ 
		var origData = data;
		var _tableName = $(this).attr('data-table_id');
		var _cols = [];
		var _rowData = [];	
		var checkedColsArray = [];
		
		$('.hide_show_checkbox').each(function(){
			if($(this).is(':checked')){
				checkedColsArray.push($(this).attr('id'));
			}
		});

		var i = 0; 
		$.each(obj.colModel, function(key,value){ //console.log(value);
			if($.inArray(value.index, checkedColsArray) > -1){
				_cols.push({ headertext: _headers[i], colName: value.name, isHidden:false});		
			}else{
				_cols.push({ headertext: _headers[i], colName: value.name, isHidden:true});	
			}
			i++;
		});
		var c = 0;
		var excelHeaders = [];
		$.each(origData.rows, function(key, val){
			_objData = val.cell;			
			
			if(typeof(_objData) == 'object'){
				_objData = val.cell;
			}else{
				_objData = val;
			}
			
			var rowArr = [];				
			$.each(_cols, function(k,v){ console.log(v.headertext +'- '+v.colName + ' - isHidden:'+ v.isHidden); 
				if(v.isHidden == false && v.headertext != 'Actions'){
					var div = document.createElement('div');
						div.innerHTML = _objData[v.colName];
					var cleanData = div.textContent || div.innerText || "";	
						cleanData = (cleanData == "undefined" ) ? '': cleanData;
						rowArr.push(cleanData);	
					if(c == 0){
						excelHeaders.push(v.headertext);
					}
				}				
			});			
			_rowData.push(rowArr);	
			c++;
		});
		//console.log(excelHeaders);
		var defaultExcelFilename = $(this).parents('.widget-toolbar').next().next().text();
			defaultExcelFilename = defaultExcelFilename.split(':')[0].replace(/ /g,"_");
		// increase post_max_size in .htaccess
		// set LimitRequestBody 0 in httpd.conf 
		$.ajax({
			type: "post",
			data: { '_excelHeaders':excelHeaders, '_excelData':_rowData, '_defaultFilename':defaultExcelFilename },
			url: 'export_to_excel/create_excel_file',
			dataType: "json",
			success: function(output){
				 window.open(output.url);
			}
	   });	   
	});
}
/*==================================================================
Invoked by function setWidgetToolbarItems to display Widget buttons 
							by Rey
====================================================================*/
function showButtons(buttonArray){	
	var list = '';
	
	$.each(buttonArray, function(key,value){
		list += ' 	<li class="">\
						<a href="javascript:void(0)" id="'+value.id+'"  class="'+value.classes+'" data-table-name="'+value.tableName+'" title="'+value.title+'" data-toggle="tab">\
						<i class="pink ace-icon fa '+value.icon+' bigger-110"></i>\
						'+value.text+'</a>\
					</li> ';
	});
	$('.widget-menu ul').append(list);
}
/*==================================================================
		Dynamic Show/Hide Columns on Setting Container by Rey
====================================================================*/
function makeHideShowColumns(tableName,colNames,colIndex){	
	columnIndex = [];
	columnNames = [];
	is_hidden = [];
	$.each(colIndex, function(key, value){  
		(value.hidden) ? is_hidden.push('') : is_hidden.push('checked');
		columnIndex.push(value.index);
	});
	$.each(colNames, function(key, value){
		columnNames.push(value);
	});

	var _html = '';
	var i = 0;
	$.each(columnNames, function(index, value){ 
		_html += '<div class="ace-settings-item">\
			<input type="checkbox" id="'+columnIndex[i]+'" class="ace ace-checkbox-2 hide_show_checkbox" data-table-name="'+tableName+'" '+is_hidden[i]+'>\
			<label for="ace-settings-navbar" class="lbl"> '+value+'</label>\
		</div>';
		i++;
	});
	$('.hide_show_columns_content').html(_html);
	
	$('.hide_show_checkbox').on('click',function(){
		is_checked = $(this).is(':checked');
		var columnName = $(this).attr('id');
		var columnText = $(this).next().text();

		if(is_checked){
			$(tableName).jqGrid('showCol',columnName );			
		}else{				
			$(tableName).jqGrid('hideCol',columnName );			
		}			
	});
}

function style_edit_form(form) {
	//enable datepicker on "sdate" field and switches for "stock" field
	form.find('input[class=has_date]').datepicker({format:'yyyy-mm-dd' , autoclose:true})
		.end().find('input[role=checkbox]')
			.addClass('ace ace-switch ace-switch-5').after('<span class="lbl"></span>');
			   //don't wrap inside a label element, the checkbox value won't be submitted (POST'ed)
			  //.addClass('ace ace-switch ace-switch-5').wrap('<label class="inline" />').after('<span class="lbl"></span>');

	//update buttons classes
	style_form_update_buttons(form);
}

function style_form_with_ui(form){
	//==================================//
	// edited by qualfon dev  START     //
	// remove element wrapper.          //
	// USE this function only if        //
	//  design exist in table           //
	//==================================//
	$.each(form.find('input[type=text]'),function(i,v){
		var div = document.createElement("div");
		div.innerHTML = $(this).val();
		$(this).val(div.textContent || div.innerText || "");
	});
}

/*=======================================================================
							Chart Functions
========================================================================*/

function actionChart(data){
	$('.action_graph').click(function(e){
		e.preventDefault();
		dialogBox(
			model = {
				type: 'url',
				data: 'generic/modal_ctrl/modal/?location=charts/reports_kpi_chart&data='+$(this).attr('href')+'&data_case='+data,
				title: $(this).attr('href'),
				title_icon: 'fa-bar-chart-o',
				width: 1000,
				height: 700
			}
		)
	})
}

/*function show_graph(data){
	$('#show_graph').click(function(e){
		e.preventDefault();
		dialogBox(
			model = {
				type: 'url',
				data: 'generic/modal_ctrl/modal/?location=charts/summary_kpi_chart&data=xx',
				title: 'Overall Summary',
				title_icon: 'fa-bar-chart-o',
				width: 1000,
				height: 700
			}
		)
	})
}*/

function show_graph(data){
	$('#show_graph').click(function(e){
		e.preventDefault();
		showDialog(
			model = {
				location: 'generic/modal_ctrl/modal/charts',
				data_case: 'charts/summary_kpi_chart',
				data: data,
				title: 'Overall Summary',
				title_icon: 'fa-bar-chart-o',
				width: 1000,
				height: 700
			}
		)
	})
}

/*function actionChart_local(data){
	$('.action_graph').click(function(e){
		data_id = $(this).attr('href');
		e.preventDefault();
		showDialog(
			model = {
				location: 'generic/modal_ctrl/modal',
				data_case: 'charts/overall_summary_chart',
				data: data.rows[data_id].cell,
				title: 'Summary',
				title_icon: 'fa-bar-chart-o',
				width: 1000,
				height: 700
			}
		)
	})
}*/

function top_up(data){
	$('#top_up').click(function(e){
		e.preventDefault();
		showDialog(
			model = {
				location: 'generic/modal_ctrl/modal',
				data_case: 'modals/top_up_modal',
				data: data,
				title: 'Top',
				title_icon: 'fa-users',
				width: 400,
				buttons: [
					{
						'html': "<i class='ace-icon fa fa-times'></i> Cancel",
						'class': 'btn_cancel btn btn-xs',
						click: function(){
							$(this).dialog('close');
						}
					},
					{
						'html': "<i class='ace-icon fa fa-check'></i> Search",
						'class': 'btn_submit btn btn-primary btn-xs',
						click: function(){
							//$(this).dialog('close');
						}
					}
				]
			}
		)
	})
}


function input_tags(){
	var tag_input = $('.input_tags');
	try{
		tag_input.tag(
		  {
			placeholder:tag_input.attr('placeholder'),
			//enable typeahead by specifying the source array
			source: ace.vars['US_STATES'],//defined in ace.js >> ace.enable_search_ahead
			/**
			//or fetch data from database, fetch those that match "query"
			source: function(query, process) {
			  $.ajax({url: 'remote_source.php?q='+encodeURIComponent(query)})
			  .done(function(result_items){
				process(result_items);
			  });
			}
			*/
		  }
		);

		//programmatically add a new
		//var $tag_obj = $('.input_tags').data('tag');
		//$tag_obj.add('Programmatically Added');
	}
	catch(e) {
		//display a textarea for old IE, because it doesn't support this plugin or another one I tried!
		tag_input.after('<textarea id="'+tag_input.attr('id')+'" name="'+tag_input.attr('name')+'" rows="3">'+tag_input.val()+'</textarea>').remove();
		//$('.input_tags').autosize({append: "\n"});
	}
}

function getcurrentdaterange(){
	var ctrmon = ((curdate.getMonth()+1)<=9)?'0'+(curdate.getMonth()+1):curdate.getMonth()+1;
	var noDays = new Date(curdate.getFullYear(), ctrmon , 0).getDate();				
	var firstdate = ctrmon+'/01/'+curdate.getFullYear();
	var lastdate = ctrmon+'/'+noDays+'/'+curdate.getFullYear();
	return firstdate+' - '+lastdate;
}

$.stripHtml = function(html){
	var div = document.createElement("div");
	div.innerHTML = html;
	var text = div.textContent || div.innerText || "";
	return text;
}