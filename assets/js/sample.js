// ===== GLOBAL VARIABLE ===== //

/*===============================================================================================================
										FUNCTIONS
================================================================================================================*/	
function make_gridx(model) {
	model.height = model.height || 300;	
	$(model.table).jqGrid({
	  url:model.gridlocation,      	// another controller function for generating data
	  mtype : "post",             	// Ajax request type. It also could be GET
	  datatype: "json",            	// supported formats XML, JSON or Arrray
	  colNames: model.colNames,     // Grid column headings
	  colModel: model.colModel,
	  rowNum: model.rowNum,
	  rownumbers : model.rownumbers,
	  rowList: model.rowlist,
	  width: '820',
	  loadonce: model.loadonce,
	  shrinkToFit: true,
	  scrollrows: true,
	  autowidth: model.autowidth,
	  height: model.height,	// please don't change the value - Rey
	  //height: $("#container").height(),
	  subGrid: model.subGrid,
	  subGridRowExpanded: model.subGridRowExpanded,
	  sortname: model.sortname,
	  sortorder: model.sortorder,
	  rowList: model.rowList,
	  pager: model.pager,
	  viewrecords: true,
	  gridview: true,
	  multiselect: model.multiselect,
	  //toolbar: [true,"top"],
	  caption: model.caption,
	  onSelectRow: model.onSelectRow,
	  onSelectAll:model.onSelectAll,
	  loadComplete: model.loadComplete,
	  ondblClickRow: model.onDoubleClick,	  
	  serializeGridData:model.serializeGridData,
	  onSortCol: model.onSortCol,
	  postData: model.postData,
	  afterEditCell:model.afterEditCell,
	  onCellSelect: model.onCellSelect,
	  subGrid: model.subGrid,
	  gridComplete: model.gridComplete,
	  scroll: model.scroll

	})/*.navGrid(model.pager,{edit:model.edit,add:false,del:false,search:false, refresh: model.refresh});
	if(model.filter == null || model.filter == false)
		$(model.table).jqGrid('filterToolbar',{stringResult: true, searchOnEnter: false});*/


	
}
